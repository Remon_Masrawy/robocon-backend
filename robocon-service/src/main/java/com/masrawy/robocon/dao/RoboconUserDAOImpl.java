package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconUserEB;
import com.masrawy.robocon.query.RoboconUserCriteriaFilter;

@Repository("roboconUserDAO")
@Transactional
public class RoboconUserDAOImpl extends AbstractHibernateDAO<RoboconUserEB, Long, RoboconUserCriteriaFilter> implements RoboconUserDAO{

	public RoboconUserDAOImpl(){
		super(RoboconUserEB.class);
	}
}
