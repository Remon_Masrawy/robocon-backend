package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconStorefrontEB;
import com.masrawy.robocon.query.RoboconStorefrontCriteriaFilter;

public interface RoboconStorefrontDAO extends DAO<RoboconStorefrontEB, Long, RoboconStorefrontCriteriaFilter>{

	public void refresh(RoboconStorefrontEB roboconStorefrontEB);
}
