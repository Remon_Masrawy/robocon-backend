package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconProviderEB;
import com.masrawy.robocon.query.RoboconProviderCriteriaFilter;

public interface RoboconProviderDAO extends DAO<RoboconProviderEB, Long, RoboconProviderCriteriaFilter>, RoboconFilterDAO{

	public void refresh(RoboconProviderEB roboconProviderEB);
}
