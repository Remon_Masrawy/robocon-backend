package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconSongTrendEB;
import com.masrawy.robocon.query.RoboconTrendCriteriaFilter;

public interface RoboconSongTrendDAO extends DAO<RoboconSongTrendEB, Long, RoboconTrendCriteriaFilter>{

}
