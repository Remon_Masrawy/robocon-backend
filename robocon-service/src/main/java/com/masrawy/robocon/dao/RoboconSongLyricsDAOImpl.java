package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconSongLyricsEB;
import com.masrawy.robocon.query.RoboconSongLyricsCriteriaFilter;

@Repository("roboconSongLyricsDAO")
@Transactional
public class RoboconSongLyricsDAOImpl extends AbstractHibernateDAO<RoboconSongLyricsEB, Long, RoboconSongLyricsCriteriaFilter> implements RoboconSongLyricsDAO{

	public RoboconSongLyricsDAOImpl(){
		super(RoboconSongLyricsEB.class);
	}
}
