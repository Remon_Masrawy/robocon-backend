package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconSongLyricsEB;
import com.masrawy.robocon.query.RoboconSongLyricsCriteriaFilter;

public interface RoboconSongLyricsDAO extends DAO<RoboconSongLyricsEB, Long, RoboconSongLyricsCriteriaFilter>{

}
