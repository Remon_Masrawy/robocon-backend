package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconOperatorEB;
import com.masrawy.robocon.query.RoboconOperatorCriteriaFilter;

public interface OperatorDAO extends DAO<RoboconOperatorEB, Long, RoboconOperatorCriteriaFilter>{

	public void refresh(RoboconOperatorEB roboconOperatorEB);
}
