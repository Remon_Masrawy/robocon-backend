package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconArtistPhotoEB;
import com.masrawy.robocon.query.RoboconArtistPhotoCriteriaFilter;

public interface RoboconArtistPhotoDAO extends DAO<RoboconArtistPhotoEB, Long, RoboconArtistPhotoCriteriaFilter>{

}
