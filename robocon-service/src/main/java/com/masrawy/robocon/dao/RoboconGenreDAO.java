package com.masrawy.robocon.dao;

import org.hibernate.Session;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconGenreEB;
import com.masrawy.robocon.query.RoboconGenreCriteriaFilter;

public interface RoboconGenreDAO extends DAO<RoboconGenreEB, Long, RoboconGenreCriteriaFilter>, RoboconFilterDAO{

	public void refresh(RoboconGenreEB roboconGenreEB);
	public Session getSession();
}
