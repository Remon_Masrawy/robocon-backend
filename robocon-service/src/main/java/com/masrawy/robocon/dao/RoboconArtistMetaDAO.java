package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconArtistMetaEB;
import com.masrawy.robocon.query.RoboconArtistMetaCriteriaFilter;

public interface RoboconArtistMetaDAO extends DAO<RoboconArtistMetaEB, Long, RoboconArtistMetaCriteriaFilter>{

}
