package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconUserSongFavoriteEB;
import com.masrawy.robocon.query.RoboconUserSongFavoritesCriteriaFilter;

public interface RoboconUserSongFavoritesDAO extends DAO<RoboconUserSongFavoriteEB, Long, RoboconUserSongFavoritesCriteriaFilter>{

	public void refreshSongFavorite(RoboconUserSongFavoriteEB roboconUserSongFavoriteEB);
}
