package com.masrawy.robocon.dao;

public interface RoboconFilterDAO {

	public void enableMetaLangFilter();
	public void enableMetaLangSearchFilter();
}
