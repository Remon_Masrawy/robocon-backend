package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconSongTrendEB;
import com.masrawy.robocon.query.RoboconTrendCriteriaFilter;

@Transactional
@Repository("roboconSongTrendDAO")
public class RoboconSongTrendDAOImpl extends AbstractHibernateDAO<RoboconSongTrendEB, Long, RoboconTrendCriteriaFilter> implements RoboconSongTrendDAO{

	public RoboconSongTrendDAOImpl(){
		super(RoboconSongTrendEB.class);
	}
}
