package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconUserSingEB;
import com.masrawy.robocon.query.RoboconUserSingCriteriaFilter;

public interface RoboconUserSingDAO extends DAO<RoboconUserSingEB, Long, RoboconUserSingCriteriaFilter>{

}
