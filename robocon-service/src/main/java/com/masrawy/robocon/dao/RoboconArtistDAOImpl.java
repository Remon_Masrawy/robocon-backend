package com.masrawy.robocon.dao;

import static com.masrawy.robocon.model.RoboconConstants.ARTIST_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.SONG_META_LANG_FILTER;

import org.hibernate.Filter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconArtistEB;
import com.masrawy.robocon.query.RoboconArtistCriteriaFilter;

@Repository("roboconArtistDAO")
@Transactional
public class RoboconArtistDAOImpl extends AbstractHibernateDAO<RoboconArtistEB, Long, RoboconArtistCriteriaFilter> implements RoboconArtistDAO{

	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	public RoboconArtistDAOImpl(){
		super(RoboconArtistEB.class);
	}

	@Override
	public void refreshArtist(RoboconArtistEB roboconArtistEB) {
		currentSession().refresh(roboconArtistEB);
	}

	@Override
	public void enableMetaLangFilter() {
		Filter filter = currentSession().enableFilter(ARTIST_META_LANG_FILTER);
		filter.setParameter("lang", ControlFieldsHelper.getUserLang());
		filter.setParameter("default_lang", defaultLangCode);
		
		Filter songFilter = currentSession().enableFilter(SONG_META_LANG_FILTER);
		songFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		songFilter.setParameter("default_lang", defaultLangCode);
	}

	@Override
	public void enableMetaLangSearchFilter() {
		Filter songFilter = currentSession().enableFilter(SONG_META_LANG_FILTER);
		songFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		songFilter.setParameter("default_lang", defaultLangCode);
	}
}
