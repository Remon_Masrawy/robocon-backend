package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconArtistPhotoEB;
import com.masrawy.robocon.query.RoboconArtistPhotoCriteriaFilter;

@Repository("roboconArtistPhotoDAO")
@Transactional
public class RoboconArtistPhotoDAOImpl extends AbstractHibernateDAO<RoboconArtistPhotoEB, Long, RoboconArtistPhotoCriteriaFilter> implements RoboconArtistPhotoDAO{

	public RoboconArtistPhotoDAOImpl(){
		super(RoboconArtistPhotoEB.class);
	}
}
