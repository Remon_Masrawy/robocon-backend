package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconArtistSocialEB;
import com.masrawy.robocon.query.RoboconArtistSocialCriteriaFilter;

@Repository("roboconArtistSocialDAO")
@Transactional
public class RoboconArtistSocialDAOImpl extends AbstractHibernateDAO<RoboconArtistSocialEB, Long, RoboconArtistSocialCriteriaFilter> implements RoboconArtistSocialDAO{

	public RoboconArtistSocialDAOImpl(){
		super(RoboconArtistSocialEB.class);
	}
}
