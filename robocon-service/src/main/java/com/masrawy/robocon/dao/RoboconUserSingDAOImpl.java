package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconUserSingEB;
import com.masrawy.robocon.query.RoboconUserSingCriteriaFilter;

@Repository("roboconUserSingDAO")
@Transactional
public class RoboconUserSingDAOImpl extends AbstractHibernateDAO<RoboconUserSingEB, Long, RoboconUserSingCriteriaFilter> implements RoboconUserSingDAO{

	public RoboconUserSingDAOImpl(){
		super(RoboconUserSingEB.class);
	}
}
