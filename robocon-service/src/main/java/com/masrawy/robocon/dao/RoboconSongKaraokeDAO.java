package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconSongKaraokeEB;
import com.masrawy.robocon.query.RoboconSongKaraokeCriteriaFilter;

public interface RoboconSongKaraokeDAO extends DAO<RoboconSongKaraokeEB, Long, RoboconSongKaraokeCriteriaFilter>{

}
