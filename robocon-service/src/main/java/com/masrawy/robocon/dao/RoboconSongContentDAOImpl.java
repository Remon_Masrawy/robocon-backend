package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconSongContentEB;
import com.masrawy.robocon.query.RoboconSongContentCriteriaFilter;

@Repository("roboconSongContentDAO")
@Transactional
public class RoboconSongContentDAOImpl extends AbstractHibernateDAO<RoboconSongContentEB, Long, RoboconSongContentCriteriaFilter> implements RoboconSongContentDAO{

	public RoboconSongContentDAOImpl(){
		super(RoboconSongContentEB.class);
	}
}
