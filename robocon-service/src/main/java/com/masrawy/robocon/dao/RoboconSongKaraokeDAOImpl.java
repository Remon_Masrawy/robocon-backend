package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconSongKaraokeEB;
import com.masrawy.robocon.query.RoboconSongKaraokeCriteriaFilter;

@Repository("roboconSongKaraokeDAO")
@Transactional
public class RoboconSongKaraokeDAOImpl extends AbstractHibernateDAO<RoboconSongKaraokeEB, Long, RoboconSongKaraokeCriteriaFilter> implements RoboconSongKaraokeDAO{

	public RoboconSongKaraokeDAOImpl(){
		super(RoboconSongKaraokeEB.class);
	}
}
