package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconConfigurationEB;
import com.masrawy.robocon.query.RoboconConfigurationCriteriaFilter;

public interface RoboconConfigurationDAO extends DAO<RoboconConfigurationEB, Long, RoboconConfigurationCriteriaFilter>{

}
