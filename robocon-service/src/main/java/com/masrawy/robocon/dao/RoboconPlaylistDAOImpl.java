package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconPlaylistEB;
import com.masrawy.robocon.query.RoboconPlaylistCriteriaFilter;

@Repository("roboconPlaylistDAO")
@Transactional
public class RoboconPlaylistDAOImpl extends AbstractHibernateDAO<RoboconPlaylistEB, Long, RoboconPlaylistCriteriaFilter> implements RoboconPlaylistDAO{

	public RoboconPlaylistDAOImpl(){
		super(RoboconPlaylistEB.class);
	}
}
