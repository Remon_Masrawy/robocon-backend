package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconSongContentEB;
import com.masrawy.robocon.query.RoboconSongContentCriteriaFilter;

public interface RoboconSongContentDAO extends DAO<RoboconSongContentEB, Long, RoboconSongContentCriteriaFilter>{

}
