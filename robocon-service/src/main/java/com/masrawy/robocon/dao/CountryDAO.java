package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.CountryEB;
import com.masrawy.robocon.query.CountryCriteriaFilter;

public interface CountryDAO extends DAO<CountryEB, Long, CountryCriteriaFilter>{

	public void refresh(CountryEB countryEB);
}
