package com.masrawy.robocon.dao;

import org.hibernate.Session;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconAlbumEB;
import com.masrawy.robocon.query.RoboconAlbumCriteriaFilter;

public interface RoboconAlbumDAO extends DAO<RoboconAlbumEB, Long, RoboconAlbumCriteriaFilter>, RoboconFilterDAO{

	public Session getSession();
	
	public void refresh(RoboconAlbumEB roboconAlbumEB);
	
}
