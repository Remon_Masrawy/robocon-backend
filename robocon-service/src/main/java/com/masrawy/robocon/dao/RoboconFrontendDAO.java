package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconFrontendEB;
import com.masrawy.robocon.query.RoboconFrontendCriteriaFilter;

public interface RoboconFrontendDAO extends DAO<RoboconFrontendEB, Long, RoboconFrontendCriteriaFilter>{

	public void refresh(RoboconFrontendEB roboconFrontendEB);
	
	public void enableMetaLangFilter();
	
	public void enableMetaLangSearchFilter();
}
