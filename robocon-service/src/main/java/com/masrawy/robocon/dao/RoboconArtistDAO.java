package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconArtistEB;
import com.masrawy.robocon.query.RoboconArtistCriteriaFilter;

public interface RoboconArtistDAO extends DAO<RoboconArtistEB, Long, RoboconArtistCriteriaFilter>, RoboconFilterDAO{

	public void refreshArtist(RoboconArtistEB roboconArtistEB);
}
