package com.masrawy.robocon.dao;

import org.hibernate.Filter;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconUserSongHistoryEB;
import com.masrawy.robocon.query.RoboconUserSongHistoryCriteriaFilter;

public interface RoboconUserSongHistoryDAO extends DAO<RoboconUserSongHistoryEB, Long, RoboconUserSongHistoryCriteriaFilter>{

	public void refresh(RoboconUserSongHistoryEB roboconUserSongHistoryEB);
	
	public Filter enableFilter(String filterName);
	
	public void enableMetaLangFilter();
}
