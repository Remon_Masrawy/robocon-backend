package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconUserSongFavoriteEB;
import com.masrawy.robocon.query.RoboconUserSongFavoritesCriteriaFilter;

@Repository("roboconUserSongFavoritesDAO")
@Transactional
public class RoboconUserSongFavoritesDAOImpl extends AbstractHibernateDAO<RoboconUserSongFavoriteEB, Long, RoboconUserSongFavoritesCriteriaFilter> implements RoboconUserSongFavoritesDAO{

	public RoboconUserSongFavoritesDAOImpl(){
		super(RoboconUserSongFavoriteEB.class);
	}
	
	public void refreshSongFavorite(RoboconUserSongFavoriteEB roboconUserSongFavoriteEB){
		currentSession().refresh(roboconUserSongFavoriteEB);
	}
}
