package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconArtistSocialEB;
import com.masrawy.robocon.query.RoboconArtistSocialCriteriaFilter;

public interface RoboconArtistSocialDAO extends DAO<RoboconArtistSocialEB, Long, RoboconArtistSocialCriteriaFilter>{

}
