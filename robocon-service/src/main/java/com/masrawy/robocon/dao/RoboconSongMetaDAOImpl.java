package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconSongMetaEB;
import com.masrawy.robocon.query.RoboconSongMetaCriteriaFilter;

@Repository("roboconSongMetaDAO")
@Transactional
public class RoboconSongMetaDAOImpl extends AbstractHibernateDAO<RoboconSongMetaEB, Long, RoboconSongMetaCriteriaFilter> implements RoboconSongMetaDAO{

	public RoboconSongMetaDAOImpl(){
		super(RoboconSongMetaEB.class);
	}
}
