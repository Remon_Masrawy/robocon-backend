package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.CountryEB;
import com.masrawy.robocon.query.CountryCriteriaFilter;

@Repository("countryDAO")
@Transactional
public class CountryDAOImpl extends AbstractHibernateDAO<CountryEB, Long, CountryCriteriaFilter> implements CountryDAO{

	public CountryDAOImpl(){
		super(CountryEB.class);
	}

	@Override
	public void refresh(CountryEB countryEB) {
		currentSession().refresh(countryEB);
	}
}
