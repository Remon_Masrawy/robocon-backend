package com.masrawy.robocon.dao;

import org.hibernate.Session;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconSongEB;
import com.masrawy.robocon.query.RoboconSongCriteriaFilter;

public interface RoboconSongDAO extends DAO<RoboconSongEB, Long, RoboconSongCriteriaFilter>{

	public Session getSession();
	public void refresh(RoboconSongEB roboconSongEB);
	public void enableMetaLangFilter();
	public void enableSearchMetaLangFilter();
	public void enableMetaNameFilter(String name);
}
