package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconOperatorEB;
import com.masrawy.robocon.query.RoboconOperatorCriteriaFilter;

@Repository("operatorDAO")
@Transactional
public class OperatorDAOImpl extends AbstractHibernateDAO<RoboconOperatorEB, Long, RoboconOperatorCriteriaFilter> implements OperatorDAO{

	public OperatorDAOImpl(){
		super(RoboconOperatorEB.class);
	}

	@Override
	public void refresh(RoboconOperatorEB roboconOperatorEB) {
		currentSession().refresh(roboconOperatorEB);
	}
}
