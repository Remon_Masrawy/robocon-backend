package com.masrawy.robocon.dao;

import static com.masrawy.robocon.model.RoboconConstants.GENRE_META_LANG_FILTER;

import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconGenreEB;
import com.masrawy.robocon.query.RoboconGenreCriteriaFilter;

@Repository("roboconGenreDAO")
@Transactional
public class RoboconGenreDAOImpl extends AbstractHibernateDAO<RoboconGenreEB, Long, RoboconGenreCriteriaFilter> implements RoboconGenreDAO{

	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	public RoboconGenreDAOImpl(){
		super(RoboconGenreEB.class);
	}
	
	@Override
	public void enableMetaLangFilter(){
		Filter filter = currentSession().enableFilter(GENRE_META_LANG_FILTER);
		filter.setParameter("lang", ControlFieldsHelper.getUserLang());
		filter.setParameter("default_lang", defaultLangCode);
	}
	
	@Override
	public void refresh(RoboconGenreEB roboconGenreEB) {
		currentSession().refresh(roboconGenreEB);
	}
	
	@Override
	public Session getSession(){
		return currentSession();
	}

	@Override
	public void enableMetaLangSearchFilter() {
		// TODO Auto-generated method stub
		
	}
}
