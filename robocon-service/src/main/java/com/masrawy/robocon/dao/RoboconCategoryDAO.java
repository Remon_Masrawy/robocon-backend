package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconCategoryEB;
import com.masrawy.robocon.query.RoboconCategoryCriteriaFilter;

public interface RoboconCategoryDAO extends DAO<RoboconCategoryEB, Long, RoboconCategoryCriteriaFilter>{

	public void refresh(RoboconCategoryEB roboconCategoryEB);
	public void enableMetaLangFilter();
}
