package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconConfigurationEB;
import com.masrawy.robocon.query.RoboconConfigurationCriteriaFilter;

@Repository("roboconConfigurationDAO")
@Transactional
public class RoboconConfigurationDAOImpl extends AbstractHibernateDAO<RoboconConfigurationEB, Long, RoboconConfigurationCriteriaFilter> implements RoboconConfigurationDAO{

	public RoboconConfigurationDAOImpl(){
		super(RoboconConfigurationEB.class);
	}
}
