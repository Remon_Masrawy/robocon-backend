package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconArtistEventEB;
import com.masrawy.robocon.query.RoboconArtistEventCriteriaFilter;

public interface RoboconArtistEventDAO extends DAO<RoboconArtistEventEB, Long, RoboconArtistEventCriteriaFilter>{

	public void refreshEntity(RoboconArtistEventEB roboconArtistEventEB);
	public void enableMetaLangFilter();
	public void enableMetaLangSearchFilter();
}
