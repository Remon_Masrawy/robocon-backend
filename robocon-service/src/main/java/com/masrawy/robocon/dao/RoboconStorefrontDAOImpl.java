package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconStorefrontEB;
import com.masrawy.robocon.query.RoboconStorefrontCriteriaFilter;

@Repository("roboconStorefrontDAO")
@Transactional
public class RoboconStorefrontDAOImpl extends AbstractHibernateDAO<RoboconStorefrontEB, Long, RoboconStorefrontCriteriaFilter> implements RoboconStorefrontDAO{

	public RoboconStorefrontDAOImpl(){
		super(RoboconStorefrontEB.class);
	}

	@Override
	public void refresh(RoboconStorefrontEB roboconStorefrontEB) {
		currentSession().refresh(roboconStorefrontEB);
	}
}
