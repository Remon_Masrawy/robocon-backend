package com.masrawy.robocon.dao;

import static com.masrawy.robocon.model.RoboconConstants.CATEGORY_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.FRONTEND_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.GENRE_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.PROVIDER_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.SONG_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.ALBUM_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.ARTIST_META_LANG_FILTER;

import org.hibernate.Filter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconFrontendEB;
import com.masrawy.robocon.query.RoboconFrontendCriteriaFilter;

@Repository("roboconFrontendDAO")
@Transactional
public class RoboconFrontendDAOImpl extends AbstractHibernateDAO<RoboconFrontendEB, Long, RoboconFrontendCriteriaFilter> implements RoboconFrontendDAO{

	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	public RoboconFrontendDAOImpl(){
		super(RoboconFrontendEB.class);
	}

	@Override
	public void refresh(RoboconFrontendEB roboconFrontendEB) {
		currentSession().refresh(roboconFrontendEB);
	}
	
	@Override
	public void enableMetaLangFilter() {
		Filter filter = currentSession().enableFilter(FRONTEND_META_LANG_FILTER);
		filter.setParameter("lang", ControlFieldsHelper.getUserLang());
		filter.setParameter("default_lang", defaultLangCode);
		
		Filter categoryFilter = currentSession().enableFilter(CATEGORY_META_LANG_FILTER);
		categoryFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		categoryFilter.setParameter("default_lang", defaultLangCode);
		
		Filter songFilter = currentSession().enableFilter(SONG_META_LANG_FILTER);
		songFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		songFilter.setParameter("default_lang", defaultLangCode);
		
		Filter albumFilter = currentSession().enableFilter(ALBUM_META_LANG_FILTER);
		albumFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		albumFilter.setParameter("default_lang", defaultLangCode);
		
		Filter artistFilter = currentSession().enableFilter(ARTIST_META_LANG_FILTER);
		artistFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		artistFilter.setParameter("default_lang", defaultLangCode);
		
		Filter genreFilter = currentSession().enableFilter(GENRE_META_LANG_FILTER);
		genreFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		genreFilter.setParameter("default_lang", defaultLangCode);
		
		Filter providerFilter = currentSession().enableFilter(PROVIDER_META_LANG_FILTER);
		providerFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		providerFilter.setParameter("default_lang", defaultLangCode);
	}
	
	@Override
	public void enableMetaLangSearchFilter(){
		Filter categoryFilter = currentSession().enableFilter(CATEGORY_META_LANG_FILTER);
		categoryFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		categoryFilter.setParameter("default_lang", defaultLangCode);
		
		Filter songFilter = currentSession().enableFilter(SONG_META_LANG_FILTER);
		songFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		songFilter.setParameter("default_lang", defaultLangCode);
		
		Filter albumFilter = currentSession().enableFilter(ALBUM_META_LANG_FILTER);
		albumFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		albumFilter.setParameter("default_lang", defaultLangCode);
		
		Filter artistFilter = currentSession().enableFilter(ARTIST_META_LANG_FILTER);
		artistFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		artistFilter.setParameter("default_lang", defaultLangCode);
		
		Filter genreFilter = currentSession().enableFilter(GENRE_META_LANG_FILTER);
		genreFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		genreFilter.setParameter("default_lang", defaultLangCode);
		
		Filter providerFilter = currentSession().enableFilter(PROVIDER_META_LANG_FILTER);
		providerFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		providerFilter.setParameter("default_lang", defaultLangCode);
	}
}
