package com.masrawy.robocon.dao;

import static com.masrawy.robocon.model.RoboconConstants.ALBUM_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.ARTIST_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.GENRE_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.PROVIDER_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.SONG_META_LANG_FILTER;

import org.hibernate.Filter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconUserSongHistoryEB;
import com.masrawy.robocon.query.RoboconUserSongHistoryCriteriaFilter;

@Repository("roboconUserSongHistoryDAO")
@Transactional
public class RoboconUserSongHistoryDAOImpl extends AbstractHibernateDAO<RoboconUserSongHistoryEB, Long, RoboconUserSongHistoryCriteriaFilter> implements RoboconUserSongHistoryDAO{

	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	public RoboconUserSongHistoryDAOImpl(){
		super(RoboconUserSongHistoryEB.class);
	}
	
	@Override
	public void refresh(RoboconUserSongHistoryEB roboconUserSongHistoryEB){
		currentSession().refresh(roboconUserSongHistoryEB);
	}
	
	public Filter enableFilter(String filterName){
		return currentSession().enableFilter(filterName);
	}
	
	@Override
	public void enableMetaLangFilter() {
		Filter filter = currentSession().enableFilter(SONG_META_LANG_FILTER);
		filter.setParameter("lang", ControlFieldsHelper.getUserLang());
		filter.setParameter("default_lang", defaultLangCode);
		
		Filter artistFilter = currentSession().enableFilter(ARTIST_META_LANG_FILTER);
		artistFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		artistFilter.setParameter("default_lang", defaultLangCode);
		
		Filter albumFilter = currentSession().enableFilter(ALBUM_META_LANG_FILTER);
		albumFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		albumFilter.setParameter("default_lang", defaultLangCode);
		
		Filter genreFilter = currentSession().enableFilter(GENRE_META_LANG_FILTER);
		genreFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		genreFilter.setParameter("default_lang", defaultLangCode);
		
		Filter providerFilter = currentSession().enableFilter(PROVIDER_META_LANG_FILTER);
		providerFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		providerFilter.setParameter("default_lang", defaultLangCode);
	}
}
