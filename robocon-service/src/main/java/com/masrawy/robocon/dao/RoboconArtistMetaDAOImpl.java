package com.masrawy.robocon.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconArtistMetaEB;
import com.masrawy.robocon.query.RoboconArtistMetaCriteriaFilter;

@Repository("roboconArtistMetaDAO")
@Transactional
public class RoboconArtistMetaDAOImpl extends AbstractHibernateDAO<RoboconArtistMetaEB, Long, RoboconArtistMetaCriteriaFilter> implements RoboconArtistMetaDAO{

	public RoboconArtistMetaDAOImpl(){
		super(RoboconArtistMetaEB.class);
	}
}
