package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconSongMetaEB;
import com.masrawy.robocon.query.RoboconSongMetaCriteriaFilter;

public interface RoboconSongMetaDAO extends DAO<RoboconSongMetaEB, Long, RoboconSongMetaCriteriaFilter>{

}
