package com.masrawy.robocon.dao;

import static com.masrawy.robocon.model.RoboconConstants.PROVIDER_META_LANG_FILTER;

import org.hibernate.Filter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconProviderEB;
import com.masrawy.robocon.query.RoboconProviderCriteriaFilter;

@Repository("roboconProviderDAO")
@Transactional
public class RoboconProviderDAOImpl extends AbstractHibernateDAO<RoboconProviderEB, Long, RoboconProviderCriteriaFilter> implements RoboconProviderDAO{

	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	public RoboconProviderDAOImpl(){
		super(RoboconProviderEB.class);
	}

	@Override
	public void refresh(RoboconProviderEB roboconProviderEB) {
		currentSession().refresh(roboconProviderEB);
	}

	@Override
	public void enableMetaLangFilter() {
		Filter filter = currentSession().enableFilter(PROVIDER_META_LANG_FILTER);
		filter.setParameter("lang", ControlFieldsHelper.getUserLang());
		filter.setParameter("default_lang", defaultLangCode);
	}

	@Override
	public void enableMetaLangSearchFilter() {
		// TODO Auto-generated method stub
		
	}
}
