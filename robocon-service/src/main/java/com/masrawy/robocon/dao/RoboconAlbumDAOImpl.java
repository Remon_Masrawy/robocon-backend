package com.masrawy.robocon.dao;

import static com.masrawy.robocon.model.RoboconConstants.ALBUM_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.SONG_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.ARTIST_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.PROVIDER_META_LANG_FILTER;

import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconAlbumEB;
import com.masrawy.robocon.query.RoboconAlbumCriteriaFilter;

@Repository("roboconAlbumDAO")
@Transactional
public class RoboconAlbumDAOImpl extends AbstractHibernateDAO<RoboconAlbumEB, Long, RoboconAlbumCriteriaFilter> implements RoboconAlbumDAO{

	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	public RoboconAlbumDAOImpl(){
		super(RoboconAlbumEB.class);
	}

	@Override
	public Session getSession() {
		return currentSession();
	}

	@Override
	public void refresh(RoboconAlbumEB roboconAlbumEB) {
		currentSession().refresh(roboconAlbumEB);
	}

	@Override
	public void enableMetaLangFilter() {
		Filter albumFilter = currentSession().enableFilter(ALBUM_META_LANG_FILTER);
		albumFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		albumFilter.setParameter("default_lang", defaultLangCode);
		
		Filter songFilter = currentSession().enableFilter(SONG_META_LANG_FILTER);
		songFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		songFilter.setParameter("default_lang", defaultLangCode);
		
		Filter artistFilter = currentSession().enableFilter(ARTIST_META_LANG_FILTER);
		artistFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		artistFilter.setParameter("default_lang", defaultLangCode);
		
		Filter providerFilter = currentSession().enableFilter(PROVIDER_META_LANG_FILTER);
		providerFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		providerFilter.setParameter("default_lang", defaultLangCode);
	}
	
	@Override
	public void enableMetaLangSearchFilter() {
		Filter songFilter = currentSession().enableFilter(SONG_META_LANG_FILTER);
		songFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		songFilter.setParameter("default_lang", defaultLangCode);
		
		Filter artistFilter = currentSession().enableFilter(ARTIST_META_LANG_FILTER);
		artistFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		artistFilter.setParameter("default_lang", defaultLangCode);
		
		Filter providerFilter = currentSession().enableFilter(PROVIDER_META_LANG_FILTER);
		providerFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		providerFilter.setParameter("default_lang", defaultLangCode);
	}
	
}
