package com.masrawy.robocon.dao;

import static com.masrawy.robocon.model.RoboconConstants.EVENT_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.ARTIST_META_LANG_FILTER;

import org.hibernate.Filter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconArtistEventEB;
import com.masrawy.robocon.query.RoboconArtistEventCriteriaFilter;

@Repository("roboconArtistEventDAO")
@Transactional
public class RoboconArtistEventDAOImpl extends AbstractHibernateDAO<RoboconArtistEventEB, Long, RoboconArtistEventCriteriaFilter> implements RoboconArtistEventDAO{

	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	public RoboconArtistEventDAOImpl(){
		super(RoboconArtistEventEB.class);
	}

	@Override
	public void refreshEntity(RoboconArtistEventEB roboconArtistEventEB) {
		currentSession().refresh(roboconArtistEventEB);
	}
	
	@Override
	public void enableMetaLangFilter(){
		Filter filter = currentSession().enableFilter(EVENT_META_LANG_FILTER);
		filter.setParameter("lang", ControlFieldsHelper.getUserLang());
		filter.setParameter("default_lang", defaultLangCode);
		
		Filter artistFilter = currentSession().enableFilter(ARTIST_META_LANG_FILTER);
		artistFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		artistFilter.setParameter("default_lang", defaultLangCode);
	}
	
	@Override
	public void enableMetaLangSearchFilter(){
		Filter artistFilter = currentSession().enableFilter(ARTIST_META_LANG_FILTER);
		artistFilter.setParameter("lang", ControlFieldsHelper.getUserLang());
		artistFilter.setParameter("default_lang", defaultLangCode);
	}
	
}
