package com.masrawy.robocon.dao;

import static com.masrawy.robocon.model.RoboconConstants.CATEGORY_META_LANG_FILTER;

import org.hibernate.Filter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.persistent.dao.AbstractHibernateDAO;
import com.masrawy.robocon.model.RoboconCategoryEB;
import com.masrawy.robocon.query.RoboconCategoryCriteriaFilter;

@Repository("roboconCategoryDAO")
@Transactional
public class RoboconCategoryDAOImpl extends AbstractHibernateDAO<RoboconCategoryEB, Long, RoboconCategoryCriteriaFilter> implements RoboconCategoryDAO{

	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	public RoboconCategoryDAOImpl(){
		super(RoboconCategoryEB.class);
	}

	@Override
	public void refresh(RoboconCategoryEB roboconCategoryEB) {
		currentSession().refresh(roboconCategoryEB);
	}
	
	@Override
	public void enableMetaLangFilter() {
		Filter filter = currentSession().enableFilter(CATEGORY_META_LANG_FILTER);
		filter.setParameter("lang", ControlFieldsHelper.getUserLang());
		filter.setParameter("default_lang", defaultLangCode);
	}
}
