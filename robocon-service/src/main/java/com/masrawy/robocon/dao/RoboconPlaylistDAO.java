package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconPlaylistEB;
import com.masrawy.robocon.query.RoboconPlaylistCriteriaFilter;

public interface RoboconPlaylistDAO extends DAO<RoboconPlaylistEB, Long, RoboconPlaylistCriteriaFilter>{

}
