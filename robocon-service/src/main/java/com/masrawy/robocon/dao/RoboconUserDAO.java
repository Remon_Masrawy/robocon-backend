package com.masrawy.robocon.dao;

import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.model.RoboconUserEB;
import com.masrawy.robocon.query.RoboconUserCriteriaFilter;

public interface RoboconUserDAO extends DAO<RoboconUserEB, Long, RoboconUserCriteriaFilter>{

}
