package com.masrawy.robocon.model;

import static com.cdm.core.persistent.usertype.HibernateUserTypes.STRING_ENUM;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

@Entity
@Cacheable
@Table(name = "ROBOCON_ARTIST_SOCIAL", uniqueConstraints = {@UniqueConstraint(name = "UNIQUE_ARTIST_SOCIAL", columnNames = {"ARTIST_ID", "SOCIAL"})})
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconArtistSocialEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "ROBOCON_ARTIST_SOCIAL_ID_SEQ", sequenceName = "ROBOCON_ARTIST_SOCIAL_ID_SEQ", initialValue = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ROBOCON_ARTIST_SOCIAL_ID_SEQ")
	private Long id;
	
	@JoinColumn(name = "ARTIST_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY, optional = false)
	private RoboconArtistEB roboconArtistEB;
	
	@Type(type = STRING_ENUM, parameters = { @Parameter(name = "enumClass", value = "com.masrawy.robocon.model.RoboconSocialEnum") })
	@Column(name = "SOCIAL", nullable = false)
	private RoboconSocialEnum roboconSocial;
	
	@Column(name = "LINK", nullable = false)
	private String link;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public RoboconArtistEB getRoboconArtistEB() {
		return roboconArtistEB;
	}
	public void setRoboconArtistEB(RoboconArtistEB roboconArtistEB) {
		this.roboconArtistEB = roboconArtistEB;
	}
	public RoboconSocialEnum getRoboconSocial() {
		return roboconSocial;
	}
	public void setRoboconSocial(RoboconSocialEnum roboconSocial) {
		this.roboconSocial = roboconSocial;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	@Override
	public Long pk() {
		return getId();
	}
}
