package com.masrawy.robocon.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Cacheable
@Table(name = "ROBOCON_PLAYLIST")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconPlaylistEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_PLAYLIST_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_PLAYLIST_ID_SEQ"),
            @Parameter(name = "initial_value", value = "1000000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ROBOCON_PLAYLIST_ID_SEQ")
	private Long id;
	
	@Column(name = "KEYWORD", nullable = false, unique = true)
	private String keyword;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@Filter(name = "META_LANG_FILTER", condition = "lang = :lang")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconPlaylistEB")
	private List<RoboconPlaylistMetaEB> roboconPlaylistMetaEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(	name 				= "ROBOCON_PLAYLIST_SONGS", 
				joinColumns 		= {@JoinColumn(name = "PLAYLIST_ID", 	referencedColumnName = "ID", nullable = false)},
				inverseJoinColumns 	= {@JoinColumn(name = "SONG_ID", 		referencedColumnName = "ID", nullable = false)},
				uniqueConstraints 	= {@UniqueConstraint(name = "UNIQUE_PLAYLIST_SONG", columnNames = {"PLAYLIST_ID", "SONG_ID"})})
	private List<RoboconSongEB> roboconSongsEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(	name 				= "ROBOCON_PLAYLIST_ALBUMS", 
				joinColumns 		= {@JoinColumn(name = "PLAYLIST_ID", 	referencedColumnName = "ID", nullable = false)},
				inverseJoinColumns 	= {@JoinColumn(name = "ALBUM_ID", 		referencedColumnName = "ID", nullable = false)},
				uniqueConstraints 	= {@UniqueConstraint(name = "UNIQUE_PLAYLIST_ALBUM", columnNames = {"PLAYLIST_ID", "ALBUM_ID"})})
	private List<RoboconAlbumEB> roboconAlbumsEB;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	//=============== Meta ===============
	public List<RoboconPlaylistMetaEB> getRoboconPlaylistMetaEB() {
		if(roboconPlaylistMetaEB == null){
			roboconPlaylistMetaEB = new ArrayList<RoboconPlaylistMetaEB>();
		}
		return roboconPlaylistMetaEB;
	}
	public void setRoboconPlaylistMetaEB(List<RoboconPlaylistMetaEB> roboconPlaylistMetaEB) {
		this.roboconPlaylistMetaEB = roboconPlaylistMetaEB;
	}
	public void addRoboconPlaylistMetaEB(RoboconPlaylistMetaEB roboconPlaylistMetaEB){
		roboconPlaylistMetaEB.setRoboconPlaylistEB(this);
		getRoboconPlaylistMetaEB().add(roboconPlaylistMetaEB);
	}
	//=============== Songs ===============
	public List<RoboconSongEB> getRoboconSongsEB() {
		if(roboconSongsEB == null){
			roboconSongsEB= new ArrayList<RoboconSongEB>();
		}
		return roboconSongsEB;
	}
	public void setRoboconSongsEB(List<RoboconSongEB> roboconSongsEB) {
		this.roboconSongsEB = roboconSongsEB;
	}
	public void addRoboconSongEB(RoboconSongEB roboconSongEB){
		roboconSongEB.addRoboconPlaylistEB(this);
		getRoboconSongsEB().add(roboconSongEB);
	}
	//=============== Albums ===============
	public List<RoboconAlbumEB> getRoboconAlbumsEB() {
		if(roboconAlbumsEB == null){
			roboconAlbumsEB = new ArrayList<RoboconAlbumEB>();
		}
		return roboconAlbumsEB;
	}
	public void setRoboconAlbumsEB(List<RoboconAlbumEB> roboconAlbumsEB) {
		this.roboconAlbumsEB = roboconAlbumsEB;
	}
	public void addRoboconAlbumEB(RoboconAlbumEB roboconAlbumEB){
		roboconAlbumEB.addRoboconPlaylistEB(this);
		getRoboconAlbumsEB().add(roboconAlbumEB);
	}
	@Override
	public Long pk() {
		return getId();
	}

}
