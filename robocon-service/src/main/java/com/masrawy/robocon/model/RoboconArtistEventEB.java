package com.masrawy.robocon.model;

import static com.cdm.core.persistent.usertype.HibernateUserTypes.DATE_ONLY;
import static com.masrawy.robocon.model.RoboconConstants.EVENT_META_LANG_FILTER;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import com.cdm.core.datatypes.DateOnly;

@Entity
@Cacheable
@Table(name = "ROBOCON_ARTIST_EVENT")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconArtistEventEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_ARTIST_EVENT_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_ARTIST_EVENT_ID_SEQ"),
            @Parameter(name = "initial_value", value = "1000000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(generator = "ROBOCON_ARTIST_EVENT_ID_SEQ", strategy = GenerationType.AUTO)
	private Long id;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@JoinTable(name = "ROBOCON_ARTISTS_EVENTS",
			joinColumns = {@JoinColumn(name = "EVENT_ID", referencedColumnName = "ID", nullable = false)},
			inverseJoinColumns = {@JoinColumn(name = "ARTIST_ID", referencedColumnName = "ID", nullable = false)})
	@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY)
	private List<RoboconArtistEB> roboconArtistsEB;
	
	@OrderBy("ID")
	@Filter(name = EVENT_META_LANG_FILTER)
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconArtistEventEB")
	private List<RoboconArtistEventMetaEB> roboconArtistEventMetaEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconArtistEventEB")
	private List<RoboconArtistEventPhotoEB> roboconArtistEventPhotosEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconArtistEventEB")
	private List<RoboconArtistEventTagEB> roboconArtistEventTagsEB;
	
	@Column(name = "BOOKING_TICKET_URL", nullable = false)
	private String bookingTicketUrl;
	
	@Column(name = "BEGIN_DATE", nullable = false)
	@Type(type = DATE_ONLY)
	private DateOnly beginDate;
	
	@Column(name = "END_DATE", nullable = false)
	@Type(type = DATE_ONLY)
	private DateOnly endDate;
	
	@Column(name = "BEGIN_TIME")
	private String beginTime;
	
	@Column(name = "END_TIME")
	private String endTime;
	
	@Column(name = "LATITUDE")
	private Double latitude;
	
	@Column(name = "LONGITUDE")
	private Double longitude;
	
	@Column(name = "TELEPHONE")
	private String telephone;
	
	@Column(name = "ACTIVE")
	private Boolean isActive = true;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	//=============== Artist ===============
	public List<RoboconArtistEB> getRoboconArtistsEB() {
		if(roboconArtistsEB == null){
			roboconArtistsEB = new ArrayList<RoboconArtistEB>();
		}
		return roboconArtistsEB;
	}
	public void setRoboconArtistsEB(List<RoboconArtistEB> roboconArtistsEB) {
		this.roboconArtistsEB = roboconArtistsEB;
	}
	public void addRoboconArtistEB(RoboconArtistEB roboconArtistEB){
		getRoboconArtistsEB().add(roboconArtistEB);
	}
	public void removeRoboconArtistEB(RoboconArtistEB roboconArtistEB){
		getRoboconArtistsEB().remove(roboconArtistEB);
	}
	//=============== Meta ===============
	public List<RoboconArtistEventMetaEB> getRoboconArtistEventMetaEB() {
		if(roboconArtistEventMetaEB == null){
			roboconArtistEventMetaEB = new ArrayList<RoboconArtistEventMetaEB>();
		}
		return roboconArtistEventMetaEB;
	}
	public void setRoboconArtistEventMetaEB(List<RoboconArtistEventMetaEB> roboconArtistEventMetaEB) {
		this.roboconArtistEventMetaEB = roboconArtistEventMetaEB;
	}
	public void addRoboconArtistEventMetaEB(RoboconArtistEventMetaEB roboconArtistEventMetaEB){
		roboconArtistEventMetaEB.setRoboconArtistEventEB(this);
		getRoboconArtistEventMetaEB().add(roboconArtistEventMetaEB);
	}
	public void addRoboconArtistEventMetaEB(List<RoboconArtistEventMetaEB> roboconArtistEventMetaEB){
		for(RoboconArtistEventMetaEB roboconArtistEventMeta : roboconArtistEventMetaEB){
			addRoboconArtistEventMetaEB(roboconArtistEventMeta);
		}
	}
	//=============== Photo ===============
	public List<RoboconArtistEventPhotoEB> getRoboconArtistEventPhotosEB() {
		if(roboconArtistEventPhotosEB == null){
			roboconArtistEventPhotosEB = new ArrayList<RoboconArtistEventPhotoEB>();
		}
		return roboconArtistEventPhotosEB;
	}
	public void setRoboconArtistEventPhotosEB(List<RoboconArtistEventPhotoEB> roboconArtistEventPhotosEB) {
		this.roboconArtistEventPhotosEB = roboconArtistEventPhotosEB;
	}
	public void addRoboconArtistPhotoEB(RoboconArtistEventPhotoEB roboconArtistEventPhotoEB){
		roboconArtistEventPhotoEB.setRoboconArtistEventEB(this);
		getRoboconArtistEventPhotosEB().add(roboconArtistEventPhotoEB);
	}
	//=============== Tags ===============
	public List<RoboconArtistEventTagEB> getRoboconArtistEventTagsEB() {
		return roboconArtistEventTagsEB;
	}
	public void setRoboconArtistEventTagsEB(List<RoboconArtistEventTagEB> roboconArtistEventTagsEB) {
		this.roboconArtistEventTagsEB = roboconArtistEventTagsEB;
	}
	public String getBookingTicketUrl() {
		return bookingTicketUrl;
	}
	public void setBookingTicketUrl(String bookingTicketUrl) {
		this.bookingTicketUrl = bookingTicketUrl;
	}
	public DateOnly getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(DateOnly beginDate) {
		this.beginDate = beginDate;
	}
	public DateOnly getEndDate() {
		return endDate;
	}
	public void setEndDate(DateOnly endDate) {
		this.endDate = endDate;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
