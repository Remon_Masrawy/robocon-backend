package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.PROVIDER_META_LANG_FILTER;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * 
 * <p>
 * 	<strong>Robocon Provider</strong>
 * </p>
 * 
 * @author Remon Gaber
 * @since 01-01-2017
 * @version 1.0
 *
 */

@Entity
@Cacheable
@Table(name = "ROBOCON_PROVIDER")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconProviderEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_PROVIDER_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_PROVIDER_ID_SEQ"),
            @Parameter(name = "initial_value", value = "100000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ROBOCON_PROVIDER_ID_SEQ")
	private Long id;
	
	@OrderBy("ID")
	@Filter(name = PROVIDER_META_LANG_FILTER)
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconProviderEB")
	private List<RoboconProviderMetaEB> roboconProviderMetaEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, orphanRemoval = false, mappedBy = "roboconProviderEB")
	private List<RoboconAlbumEB> roboconAlbumsEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, orphanRemoval = false, mappedBy = "roboconProviderEB")
	private List<RoboconSongEB> roboconSongsEB;
	
	/* ============================= Getter And Setter Methods ============================= */
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<RoboconProviderMetaEB> getRoboconProviderMetaEB() {
		if(roboconProviderMetaEB == null){
			roboconProviderMetaEB = new ArrayList<RoboconProviderMetaEB>();
		}
		return roboconProviderMetaEB;
	}
	public void setRoboconProviderMetaEB(List<RoboconProviderMetaEB> roboconProviderMetaEB) {
		this.roboconProviderMetaEB = roboconProviderMetaEB;
	}
	public void addRoboconProviderMetaEB(RoboconProviderMetaEB roboconProviderMetaEB){
		roboconProviderMetaEB.setRoboconProviderEB(this);
		getRoboconProviderMetaEB().add(roboconProviderMetaEB);
	}
	//=============== Albums ===============
	public List<RoboconAlbumEB> getRoboconAlbumsEB() {
		if(roboconAlbumsEB == null){
			roboconAlbumsEB = new ArrayList<RoboconAlbumEB>();
		}
		return roboconAlbumsEB;
	}
	public void setRoboconAlbumsEB(List<RoboconAlbumEB> roboconAlbumsEB) {
		this.roboconAlbumsEB = roboconAlbumsEB;
	}
	public void addRoboconAlbumEB(RoboconAlbumEB roboconAlbumEB){
		roboconAlbumEB.setRoboconProviderEB(this);
		getRoboconAlbumsEB().add(roboconAlbumEB);
	}
	//=============== Songs ===============
	public List<RoboconSongEB> getRoboconSongsEB() {
		if(roboconSongsEB == null){
			roboconSongsEB = new ArrayList<RoboconSongEB>();
		}
		return roboconSongsEB;
	}
	public void setRoboconSongsEB(List<RoboconSongEB> roboconSongsEB) {
		this.roboconSongsEB = roboconSongsEB;
	}
	public void addRoboconSongEB(RoboconSongEB roboconSongEB){
		roboconSongEB.setRoboconProviderEB(this);
		getRoboconSongsEB().add(roboconSongEB);
	}
	@Override
	public Long pk() {
		return getId();
	}

}
