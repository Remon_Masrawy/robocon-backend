package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Table(name = "ROBOCON_ARTIST_PHOTOS")
@PrimaryKeyJoinColumn(name = "PHOTO_ID", referencedColumnName = "ID")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconArtistPhotoEB extends AbstractRoboconPhotoEB{

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "ARTIST_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY, optional = false)
	private RoboconArtistEB roboconArtistEB;

	public RoboconArtistEB getRoboconArtistEB() {
		return roboconArtistEB;
	}

	public void setRoboconArtistEB(RoboconArtistEB roboconArtistEB) {
		this.roboconArtistEB = roboconArtistEB;
	}
}
