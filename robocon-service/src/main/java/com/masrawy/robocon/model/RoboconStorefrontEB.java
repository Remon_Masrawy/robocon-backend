package com.masrawy.robocon.model;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = "ROBOCON_STOREFRONT", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME" }), @UniqueConstraint(columnNames = { "KEYWORD" }) })
public class RoboconStorefrontEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_STOREFRONT_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_STOREFRONT_ID_SEQ"),
            @Parameter(name = "initial_value", value = "1000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(generator = "ROBOCON_STOREFRONT_ID_SEQ", strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "KEYWORD", nullable = false, updatable = false)
	private String keyword;
	
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@Column(name = "ACTIVE")
	private Boolean active;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, mappedBy = "roboconStorefrontsEB")
	private List<RoboconCategoryEB> roboconCategoriesEB;
	
	public Long getId() {
		return id;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<RoboconCategoryEB> getRoboconCategoriesEB() {
		return roboconCategoriesEB;
	}

	public void setRoboconCategoriesEB(List<RoboconCategoryEB> roboconCategoriesEB) {
		this.roboconCategoriesEB = roboconCategoriesEB;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Override
	public Long pk() {
		return getId();
	}

}
