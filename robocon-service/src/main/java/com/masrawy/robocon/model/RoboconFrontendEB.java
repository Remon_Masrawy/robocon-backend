package com.masrawy.robocon.model;

import static com.cdm.core.persistent.usertype.HibernateUserTypes.STRING_ENUM;
import static com.masrawy.robocon.model.RoboconConstants.FRONTEND_META_LANG_FILTER;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

@Entity
@Cacheable
@Table(name = "ROBOCON_FRONTEND")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
	@com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = RoboconSongFrontendEB.class, name = "SONG"),
	@com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = RoboconAlbumFrontendEB.class, name = "Album"),
})
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public abstract class RoboconFrontendEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_FRONTEND_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_FRONTEND_ID_SEQ"),
            @Parameter(name = "initial_value", value = "1000000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(generator = "ROBOCON_FRONTEND_ID_SEQ", strategy = GenerationType.AUTO)
	private Long id;
	
	@OrderBy("ID")
	@Filter(name = FRONTEND_META_LANG_FILTER)
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconFrontendEB")
	private List<RoboconFrontendMetaEB> roboconFrontendMetaEB;
	
	@Column(name = "TYPE", nullable = false, updatable = false)
	@Type(type = STRING_ENUM, parameters = {@Parameter(name = "enumClass", value = "com.masrawy.robocon.model.RoboconFrontendTypeEnum")})
	private RoboconFrontendTypeEnum type;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@JoinTable(name = "ROBOCON_FRONTEND_CATEGORIES", 
			joinColumns = {@JoinColumn(name = "FRONTEND_ID", referencedColumnName = "ID", table = "ROBOCON_FRONTEND")}, 
			inverseJoinColumns = {@JoinColumn(name = "CATEGORY_ID", referencedColumnName = "ID", table = "ROBOCON_CATEGORY")},
			uniqueConstraints = {@UniqueConstraint(name = "UNIQUE_FRONTEND_CATEGORY", columnNames = {"FRONTEND_ID", "CATEGORY_ID"})})
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY)
	private List<RoboconCategoryEB> roboconCategoriesEB;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<RoboconFrontendMetaEB> getRoboconFrontendMetaEB() {
		if(roboconFrontendMetaEB == null){
			roboconFrontendMetaEB = new ArrayList<RoboconFrontendMetaEB>();
		}
		return roboconFrontendMetaEB;
	}

	public void setRoboconFrontendMetaEB(List<RoboconFrontendMetaEB> roboconFrontendMetaEB) {
		this.roboconFrontendMetaEB = roboconFrontendMetaEB;
	}

	public RoboconFrontendTypeEnum getType() {
		return type;
	}

	public void setType(RoboconFrontendTypeEnum type) {
		this.type = type;
	}

	public List<RoboconCategoryEB> getRoboconCategoriesEB() {
		return roboconCategoriesEB;
	}

	public void setRoboconCategoriesEB(List<RoboconCategoryEB> roboconCategoriesEB) {
		this.roboconCategoriesEB = roboconCategoriesEB;
	}

	@Override
	public Long pk() {
		return getId();
	}
	
	
}
