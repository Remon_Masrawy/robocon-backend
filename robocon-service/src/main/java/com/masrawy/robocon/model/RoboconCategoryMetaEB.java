package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.CATEGORY_META_LANG_FILTER;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.ParamDef;

@Entity
@Cacheable
@Table(name = "ROBOCON_CATEGORY_META", uniqueConstraints = { 
		@UniqueConstraint(name = "UNIQUE_CATEGORY_NAME", columnNames = { "NAME" }),
		@UniqueConstraint(name = "UNIQUE_CATEGORY_META_LANG", columnNames = { "CATEGORY_ID", "LANG" })})
@PrimaryKeyJoinColumn(name = "META_ID", referencedColumnName = "ID")
@FilterDefs({
	@FilterDef(	name = CATEGORY_META_LANG_FILTER, 
				defaultCondition = "lang = (COALESCE((SELECT meta.lang FROM ROBOCON_CATEGORY_META meta WHERE meta.lang = :lang AND CATEGORY_ID = meta.CATEGORY_ID), :default_lang))", 
				parameters = {
							@ParamDef(type = "java.lang.String", name = "lang"),
							@ParamDef(type = "java.lang.String", name = "default_lang")
						})
})
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconCategoryMetaEB extends AbstractRoboconMetaEB{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "CATEGORY_ID", referencedColumnName = "ID", nullable = false, updatable = false)
	private RoboconCategoryEB roboconCategoryEB;

	public RoboconCategoryEB getRoboconCategoryEB() {
		return roboconCategoryEB;
	}

	public void setRoboconCategoryEB(RoboconCategoryEB roboconCategoryEB) {
		this.roboconCategoryEB = roboconCategoryEB;
	}
	
}
