package com.masrawy.robocon.model;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Cacheable
@Table(name = "COUNTRIES", uniqueConstraints = {
								@UniqueConstraint(columnNames = {"NAME"}),
								@UniqueConstraint(columnNames = {"ISO"}),
								@UniqueConstraint(columnNames = {"CODE"})
							})
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class CountryEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_COUNTRY_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_COUNTRY_ID_SEQ"),
            @Parameter(name = "initial_value", value = "1000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(generator = "ROBOCON_COUNTRY_ID_SEQ", strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "ISO", nullable = false)
	private String iso;
	
	@Column(name = "CODE", nullable = false)
	private String code;
	
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, orphanRemoval = false, mappedBy = "countryEB")
	private List<RoboconOperatorEB> roboconOperatorsEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, mappedBy = "countriesEB")
	private List<RoboconCategoryEB> roboconCategoriesEB;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIso() {
		return iso;
	}
	public void setIso(String iso) {
		this.iso = iso;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<RoboconOperatorEB> getRoboconOperatorsEB() {
		return roboconOperatorsEB;
	}
	public void setRoboconOperatorsEB(List<RoboconOperatorEB> roboconOperatorsEB) {
		this.roboconOperatorsEB = roboconOperatorsEB;
	}
	public List<RoboconCategoryEB> getRoboconCategoriesEB() {
		return roboconCategoriesEB;
	}
	public void setRoboconCategoriesEB(List<RoboconCategoryEB> roboconCategoriesEB) {
		this.roboconCategoriesEB = roboconCategoriesEB;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
