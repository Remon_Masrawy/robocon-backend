package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.EVENT_META_LANG_FILTER;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.ParamDef;

@Entity
@Cacheable
@Table(name = "ROBOCON_ARTIST_EVENT_META", uniqueConstraints = {@UniqueConstraint(name = "UNIQUE_ARTIST_EVENT_META_LOCALE", columnNames = {"ARTIST_EVENT_ID", "LANG"})})
@FilterDefs({
	@FilterDef(	name = EVENT_META_LANG_FILTER, 
				defaultCondition = "lang = (COALESCE((SELECT meta.lang FROM ROBOCON_ARTIST_EVENT_META meta WHERE meta.lang = :lang AND ARTIST_EVENT_ID = meta.ARTIST_EVENT_ID), :default_lang))", 
				parameters = {
							@ParamDef(type = "java.lang.String", name = "lang"),
							@ParamDef(type = "java.lang.String", name = "default_lang")
						})
})
@PrimaryKeyJoinColumn(name = "META_ID", referencedColumnName = "ID")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconArtistEventMetaEB extends AbstractRoboconMetaEB{

	private static final long serialVersionUID = 1L;
	
	@JoinColumn(name = "ARTIST_EVENT_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY, optional = false)
	private RoboconArtistEventEB roboconArtistEventEB;
	
	@Column(name = "EVENT_LOCATION", nullable = false)
	private String location;
	
	@Column(name = "EVENT_CITY", nullable = false)
	private String city;
	
	@Column(name = "EVENT_COUNTRY", nullable = false)
	private String country;

	public RoboconArtistEventEB getRoboconArtistEventEB() {
		return roboconArtistEventEB;
	}
	public void setRoboconArtistEventEB(RoboconArtistEventEB roboconArtistEventEB) {
		this.roboconArtistEventEB = roboconArtistEventEB;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
}
