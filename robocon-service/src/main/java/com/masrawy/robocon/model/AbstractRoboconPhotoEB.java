package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.cdm.core.file.model.FileEB;

@Entity
@Cacheable
@Inheritance(strategy = InheritanceType.JOINED)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = "ROBOCON_PHOTOS", uniqueConstraints = {@UniqueConstraint(name = "UNIQUE_PHOTO_CHECKSUM", columnNames = { "CHECKSUM" })})
public abstract class AbstractRoboconPhotoEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "ROBOCON_PHOTO_ID_SEQ", sequenceName = "ROBOCON_PHOTO_ID_SEQ", initialValue = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ROBOCON_PHOTO_ID_SEQ")
	private Long id;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
	@JoinColumn(name = "CHECKSUM", referencedColumnName = "checksum", nullable = false)
	private FileEB file;
	
	@Column(name = "PHOTO_SIZE")
	private Long size;
	
	@NotNull
	@Column(name = "MAIN_PHOTO", nullable = false)
	private Boolean isMainPhoto = false;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public FileEB getFile() {
		return file;
	}
	public void setFile(FileEB file) {
		this.file = file;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public Boolean getIsMainPhoto() {
		return isMainPhoto;
	}
	public void setIsMainPhoto(Boolean isMainPhoto) {
		this.isMainPhoto = isMainPhoto == null? false : isMainPhoto;
	}
	@Override
	public Long pk() {
		return getId();
	}
}
