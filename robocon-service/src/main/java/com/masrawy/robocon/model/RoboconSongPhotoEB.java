package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Table(name = "ROBOCON_SONG_PHOTOS")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@PrimaryKeyJoinColumn(name = "PHOTO_ID", referencedColumnName = "ID")
public class RoboconSongPhotoEB extends AbstractRoboconPhotoEB{

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "SONG_ID")
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	private RoboconSongEB roboconSongEB;
	
	public RoboconSongEB getRoboconSongEB() {
		return roboconSongEB;
	}
	public void setRoboconSongEB(RoboconSongEB roboconSongEB) {
		this.roboconSongEB = roboconSongEB;
	}
}
