package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.cdm.core.persistent.model.AbstractEntity;

/**
 * <p>Robocon Trends</p>
 * 
 * @author Remon Gaber
 * @since 01-01-2017
 * @version 1.0
 */

@Entity
@Cacheable
@Table(name = "ROBOCON_SONG_TRENDS")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconSongTrendEB extends AbstractEntity<Long>{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_TREND_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_TREND_ID_SEQ"),
            @Parameter(name = "initial_value", value = "100000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ROBOCON_TREND_ID_SEQ")
	private Long id;
	
	@JoinColumn(name = "SONG_ID", referencedColumnName = "ID")
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	private RoboconSongEB roboconSongEB;
	private Integer count;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public RoboconSongEB getRoboconSongEB() {
		return roboconSongEB;
	}
	public void setRoboconSongEB(RoboconSongEB roboconSongEB) {
		this.roboconSongEB = roboconSongEB;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	@Override
	public Long pk() {
		return getId();
	}
}
