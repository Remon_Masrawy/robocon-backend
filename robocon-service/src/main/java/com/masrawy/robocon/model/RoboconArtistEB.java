package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.ARTIST_META_LANG_FILTER;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Cacheable
@Table(name = "ROBOCON_ARTIST")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconArtistEB extends AbstractRoboconPersonEB{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_ARTIST_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_ARTIST_ID_SEQ"),
            @Parameter(name = "initial_value", value = "1000000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ROBOCON_ARTIST_ID_SEQ")
	private Long id;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, mappedBy = "roboconArtistsEB")
	private List<RoboconSongEB> roboconSongsEB;
	
	@OrderBy("ID")
	@Filter(name = ARTIST_META_LANG_FILTER)
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconArtistEB")
	private List<RoboconArtistMetaEB> roboconArtistMetaEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconArtistEB")
	private List<RoboconArtistPhotoEB> roboconArtistPhotosEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconArtistEB")
	private List<RoboconArtistTagEB> roboconArtistTagsEB;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconArtistEB")
	private List<RoboconArtistSocialEB> roboconArtistSocialsEB;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, mappedBy = "roboconArtistsEB")
	private List<RoboconArtistEventEB> roboconArtistEventEB;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	//=============== Meta ===============
	public List<RoboconArtistMetaEB> getRoboconArtistMetaEB() {
		if(roboconArtistMetaEB == null){
			roboconArtistMetaEB = new ArrayList<RoboconArtistMetaEB>();
		}
		return roboconArtistMetaEB;
	}
	public void setRoboconArtistMetaEB(List<RoboconArtistMetaEB> roboconArtistMetaEB) {
		this.roboconArtistMetaEB = roboconArtistMetaEB;
	}
	public void addRoboconArtistMetaEB(RoboconArtistMetaEB roboconArtistMetaEB){
		roboconArtistMetaEB.setRoboconArtistEB(this);
		getRoboconArtistMetaEB().add(roboconArtistMetaEB);
	}
	public void addRoboconArtistMetaEB(List<RoboconArtistMetaEB> roboconArtistMetaEB){
		for(RoboconArtistMetaEB roboconArtistMeta : roboconArtistMetaEB){
			addRoboconArtistMetaEB(roboconArtistMeta);
		}
	}
	public void removeRoboconArtistMetaEB(RoboconArtistMetaEB roboconArtistMetaEB){
		roboconArtistMetaEB.setRoboconArtistEB(null);
		getRoboconArtistMetaEB().remove(roboconArtistMetaEB);
	}
	//=============== Songs ===============
	public List<RoboconSongEB> getRoboconSongsEB() {
		if(roboconSongsEB == null){
			roboconSongsEB = new ArrayList<RoboconSongEB>();
		}
		return roboconSongsEB;
	}
	public void setRoboconSongsEB(List<RoboconSongEB> roboconSongsEB) {
		this.roboconSongsEB = roboconSongsEB;
	}
	public void addRoboconSongEB(RoboconSongEB roboconSongEB){
		roboconSongEB.addRoboconArtistEB(this);
		getRoboconSongsEB().add(roboconSongEB);
	}
	public void removeSongEB(RoboconSongEB roboconSongEB){
		getRoboconSongsEB().remove(roboconSongEB);
	}
	//=============== Photos ===============
	public List<RoboconArtistPhotoEB> getRoboconArtistPhotosEB() {
		if(roboconArtistPhotosEB == null){
			roboconArtistPhotosEB = new ArrayList<RoboconArtistPhotoEB>();
		}
		return roboconArtistPhotosEB;
	}
	public void setRoboconArtistPhotosEB(List<RoboconArtistPhotoEB> roboconArtistPhotosEB) {
		this.roboconArtistPhotosEB = roboconArtistPhotosEB;
	}
	public void addRoboconArtistPhotoEB(RoboconArtistPhotoEB roboconArtistPhotoEB){
		roboconArtistPhotoEB.setRoboconArtistEB(this);
		getRoboconArtistPhotosEB().add(roboconArtistPhotoEB);
	}
	public void addRoboconArtistPhotosEB(List<RoboconArtistPhotoEB> roboconArtistPhotosEB){
		for(RoboconArtistPhotoEB roboconArtistPhoto : roboconArtistPhotosEB){
			addRoboconArtistPhotoEB(roboconArtistPhoto);
		}
	}
	public void removeRoboconArtistPhotoEB(RoboconArtistPhotoEB roboconArtistPhotoEB){
		roboconArtistPhotoEB.setRoboconArtistEB(null);
		getRoboconArtistPhotosEB().remove(roboconArtistPhotoEB);
	}
	//=============== Tags ===============
	public List<RoboconArtistTagEB> getRoboconArtistTagsEB() {
		if(roboconArtistTagsEB == null){
			roboconArtistTagsEB = new ArrayList<RoboconArtistTagEB>();
		}
		return roboconArtistTagsEB;
	}
	public void setRoboconArtistTagsEB(List<RoboconArtistTagEB> roboconArtistTagsEB) {
		this.roboconArtistTagsEB = roboconArtistTagsEB;
	}
	public void addRoboconArtistTagEB(RoboconArtistTagEB roboconArtistTagEB){
		roboconArtistTagEB.setRoboconArtistEB(this);
		getRoboconArtistTagsEB().add(roboconArtistTagEB);
	}
	public void addRoboconArtistTagsEB(List<RoboconArtistTagEB> roboconArtistTagsEB){
		for(RoboconArtistTagEB roboconArtistTagEB : roboconArtistTagsEB){
			addRoboconArtistTagEB(roboconArtistTagEB);
		}
	}
	public void removeRoboconArtistTagEB(RoboconArtistTagEB roboconArtistTagEB){
		roboconArtistTagEB.setRoboconArtistEB(null);
		getRoboconArtistTagsEB().remove(roboconArtistTagEB);
	}
	//=============== Social Links ===============
	public List<RoboconArtistSocialEB> getRoboconArtistSocialsEB() {
		if(roboconArtistSocialsEB == null){
			roboconArtistSocialsEB = new ArrayList<RoboconArtistSocialEB>();
		}
		return roboconArtistSocialsEB;
	}
	public void setRoboconArtistSocialsEB(List<RoboconArtistSocialEB> roboconArtistSocialsEB) {
		this.roboconArtistSocialsEB = roboconArtistSocialsEB;
	}
	public void addRoboconArtistSocialEB(RoboconArtistSocialEB roboconArtistSocialEB){
		roboconArtistSocialEB.setRoboconArtistEB(this);
		getRoboconArtistSocialsEB().add(roboconArtistSocialEB);
	}
	public void addRoboconArtistSocialsEB(List<RoboconArtistSocialEB> roboconArtistSocialsEB){
		for(RoboconArtistSocialEB roboconArtistSocialEB : roboconArtistSocialsEB){
			addRoboconArtistSocialEB(roboconArtistSocialEB);
		}
	}
	public void removeRoboconArtistSocialEB(RoboconArtistSocialEB roboconArtistSocialEB){
		roboconArtistSocialEB.setRoboconArtistEB(null);
		getRoboconArtistSocialsEB().remove(roboconArtistSocialEB);
	}
	//=============== Events ===============
	public List<RoboconArtistEventEB> getRoboconArtistEventEB() {
		if(roboconArtistEventEB == null){
			roboconArtistEventEB = new ArrayList<RoboconArtistEventEB>();
		}
		return roboconArtistEventEB;
	}
	public void setRoboconArtistEventEB(List<RoboconArtistEventEB> roboconArtistEventEB) {
		this.roboconArtistEventEB = roboconArtistEventEB;
	}
	public void addRoboconArtistEventEB(RoboconArtistEventEB roboconArtistEventEB){
		roboconArtistEventEB.addRoboconArtistEB(this);
		getRoboconArtistEventEB().add(roboconArtistEventEB);
	}
	public void addRoboconArtistEventsEB(List<RoboconArtistEventEB> roboconArtistEventsEB){
		for(RoboconArtistEventEB roboconArtistEventEB : roboconArtistEventsEB){
			addRoboconArtistEventEB(roboconArtistEventEB);
		}
	}
	public void removeRoboconArtistEventEB(RoboconArtistEventEB roboconArtistEventEB){
		roboconArtistEventEB.removeRoboconArtistEB(this);
		getRoboconArtistEventEB().remove(roboconArtistEventEB);
	}
	@Override
	public Long pk() {
		return getId();
	}

}
