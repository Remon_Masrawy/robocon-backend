package com.masrawy.robocon.model;

import static com.cdm.core.persistent.usertype.HibernateUserTypes.DATE_ONLY;
import static com.cdm.core.persistent.usertype.HibernateUserTypes.STRING_ENUM;

import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import com.cdm.core.datatypes.DateOnly;
import com.cdm.core.security.model.GenderEnum;

@MappedSuperclass
public abstract class AbstractRoboconPersonEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Type(type=DATE_ONLY)
	private DateOnly birthDate;
	
	@Type(type=DATE_ONLY)
	private DateOnly deathDate;
	
	@Type(type = STRING_ENUM, parameters = { @Parameter(name = "enumClass", value = "com.cdm.core.security.model.GenderEnum") })
	private GenderEnum geneder = GenderEnum.UNSPECIFIED;
	
	private Double rate;
	
	public DateOnly getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(DateOnly birthDate) {
		this.birthDate = birthDate;
	}
	public DateOnly getDeathDate() {
		return deathDate;
	}
	public void setDeathDate(DateOnly deathDate) {
		this.deathDate = deathDate;
	}
	public GenderEnum getGeneder() {
		return geneder;
	}
	public void setGeneder(GenderEnum geneder) {
		this.geneder = geneder;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
}
