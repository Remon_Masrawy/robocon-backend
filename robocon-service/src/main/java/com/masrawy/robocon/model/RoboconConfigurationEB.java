package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Table(name = "ROBOCON_CONFIG")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconConfigurationEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ROBOCON_CONFIG_ID_SEQ", sequenceName = "ROBOCON_CONFIG_ID_SEQ", initialValue = 1)
	@GeneratedValue(generator = "ROBOCON_CONFIG_ID_SEQ", strategy = GenerationType.AUTO)
	private Long id;
	
	private String welcomeEmailSubject;

	private String resetPasswordEmailSubject;

	private String verifyEmailSubject;
	
	@Column(name = "allowNonverifiedEmails", nullable = false, columnDefinition = "bool default true")
	private Boolean allowNonverifiedEmails;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWelcomeEmailSubject() {
		return welcomeEmailSubject;
	}

	public void setWelcomeEmailSubject(String welcomeEmailSubject) {
		this.welcomeEmailSubject = welcomeEmailSubject;
	}

	public String getResetPasswordEmailSubject() {
		return resetPasswordEmailSubject;
	}

	public void setResetPasswordEmailSubject(String resetPasswordEmailSubject) {
		this.resetPasswordEmailSubject = resetPasswordEmailSubject;
	}

	public String getVerifyEmailSubject() {
		return verifyEmailSubject;
	}

	public void setVerifyEmailSubject(String verifyEmailSubject) {
		this.verifyEmailSubject = verifyEmailSubject;
	}

	public Boolean getAllowNonverifiedEmails() {
		return allowNonverifiedEmails;
	}

	public void setAllowNonverifiedEmails(Boolean allowNonverifiedEmails) {
		this.allowNonverifiedEmails = allowNonverifiedEmails;
	}

	@Override
	public Long pk() {
		return getId();
	}
}
