package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.SONG_HISTORY_DURATION_FORMULA;
import static com.masrawy.robocon.model.RoboconConstants.SONG_HISTORY_LATEST_HIT_FORMULA;
import static com.masrawy.robocon.model.RoboconConstants.SONG_HISTORY_HIT_COUNT_FORMULA;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Formula;

@Entity
@Cacheable
@Table(name = "ROBOCON_USER_SONG_HISTORY")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconUserSongHistoryEB extends AbstractRoboconHistoryEB{

	private static final long serialVersionUID = 1L;

	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "SONG_ID", referencedColumnName = "ID")
	private RoboconSongEB roboconSongEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconUserSongHistoryEB")
	private List<RoboconUserSongHitEB> roboconUserSongHitsEB;
	
	@Formula(SONG_HISTORY_DURATION_FORMULA)
	private Long duration;
	
	@Formula(SONG_HISTORY_LATEST_HIT_FORMULA)
	private Date latestHit;
	
	@Formula(SONG_HISTORY_HIT_COUNT_FORMULA)
	private Integer hitCount;
	
	public RoboconSongEB getRoboconSongEB() {
		return roboconSongEB;
	}
	public void setRoboconSongEB(RoboconSongEB roboconSongEB) {
		this.roboconSongEB = roboconSongEB;
	}
	public List<RoboconUserSongHitEB> getRoboconUserSongHitsEB() {
		if(roboconUserSongHitsEB == null){
			roboconUserSongHitsEB = new ArrayList<RoboconUserSongHitEB>();
		}
		return roboconUserSongHitsEB;
	}
	public void setRoboconUserSongHitsEB(List<RoboconUserSongHitEB> roboconUserSongHitsEB) {
		this.roboconUserSongHitsEB = roboconUserSongHitsEB;
	}
	public void addRoboconUserSongHitEB(RoboconUserSongHitEB roboconUserSongHitEB){
		roboconUserSongHitEB.setRoboconUserSongHistoryEB(this);
		getRoboconUserSongHitsEB().add(roboconUserSongHitEB);
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	public Date getLatestHit() {
		return latestHit;
	}
	public void setLatestHit(Date latestHit) {
		this.latestHit = latestHit;
	}
	public Integer getHitCount() {
		return hitCount;
	}
	public void setHitCount(Integer hitCount) {
		this.hitCount = hitCount;
	}
}
