package com.masrawy.robocon.model;

import java.io.Serializable;

import com.cdm.core.persistent.audit.model.AbstractShAuditEntity;

public abstract class AbstractRoboconEntity<ID extends Serializable> extends AbstractShAuditEntity<ID> {

	private static final long serialVersionUID = 1L;

}
