package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.META_LANG_FILTER;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.apache.solr.analysis.ArabicStemFilterFactory;
import org.apache.solr.analysis.ASCIIFoldingFilterFactory;
import org.apache.solr.analysis.ArabicNormalizationFilterFactory;
import org.apache.solr.analysis.FrenchLightStemFilterFactory;
import org.apache.solr.analysis.HTMLStripCharFilterFactory;
import org.apache.solr.analysis.LowerCaseFilterFactory;
import org.apache.solr.analysis.MappingCharFilterFactory;
import org.apache.solr.analysis.NGramFilterFactory;
import org.apache.solr.analysis.PorterStemFilterFactory;
import org.apache.solr.analysis.RemoveDuplicatesTokenFilterFactory;
import org.apache.solr.analysis.ReverseStringFilterFactory;
import org.apache.solr.analysis.SnowballPorterFilterFactory;
import org.apache.solr.analysis.StandardFilterFactory;
import org.apache.solr.analysis.StandardTokenizerFactory;
import org.apache.solr.analysis.StopFilterFactory;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.ParamDef;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.AnalyzerDefs;
import org.hibernate.search.annotations.CharFilterDef;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

import com.cdm.core.security.model.LanguageEB;

@Entity
@Cacheable
@Table(name = "ROBOCON_META")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@FilterDefs({
	@FilterDef(	name = META_LANG_FILTER, 
				defaultCondition = "lang in (coalesce((select distinct(lang) from robocon_genre_meta where genre_id=? lang = :lang), :default_lang))", 
				parameters = {
							@ParamDef(type = "java.lang.String", name = "lang"),
							@ParamDef(type = "java.lang.String", name = "default_lang")
						})
})
@AnalyzerDefs({
	@AnalyzerDef(name = "customanalyzer",
			tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
			charFilters = {
					@CharFilterDef(factory = MappingCharFilterFactory.class, 
							params = {
								@Parameter(name = "mapping", value = "analyzer/mapping-chars.properties")
							}),
					@CharFilterDef(factory = HTMLStripCharFilterFactory.class)
			},
			filters = {
					@TokenFilterDef(factory = StopFilterFactory.class, 
							params = {
								@Parameter(name="words", value= "analyzer/stoplist.properties" ),
						      	@Parameter(name="ignoreCase", value="true")
						    }),
					@TokenFilterDef(factory = StandardFilterFactory.class),
					@TokenFilterDef(factory = LowerCaseFilterFactory.class),
					@TokenFilterDef(factory = ReverseStringFilterFactory.class),
					@TokenFilterDef(factory = ASCIIFoldingFilterFactory.class),
					@TokenFilterDef(factory = SnowballPorterFilterFactory.class),
					@TokenFilterDef(factory = PorterStemFilterFactory.class),
					@TokenFilterDef(factory = RemoveDuplicatesTokenFilterFactory.class),
					@TokenFilterDef(factory = ArabicStemFilterFactory.class),
					@TokenFilterDef(factory = ArabicNormalizationFilterFactory.class),
					@TokenFilterDef(factory = FrenchLightStemFilterFactory.class),
					@TokenFilterDef(factory = NGramFilterFactory.class, 
							params = {
								@Parameter(name = "minGramSize", value = "2"),
								@Parameter(name = "maxGramSize", value = "4")
							})
		})
})
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public abstract class AbstractRoboconMetaEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "ROBOCON_META_ID_SEQ", sequenceName = "ROBOCON_META_ID_SEQ", initialValue = 1)
	@GeneratedValue(generator = "ROBOCON_META_ID_SEQ", strategy = GenerationType.AUTO)
	private Long id;
	
	@Field
	@Analyzer(definition = "customanalyzer")
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@Field
	@Analyzer(definition = "customanalyzer")
	@Column(name = "DESCRIPTION", nullable = false, length = 2000)
	private String description;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "LANG", referencedColumnName = "code", nullable = false, insertable = true, updatable = true)
	private LanguageEB lang;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public LanguageEB getLang() {
		return lang;
	}
	public void setLang(LanguageEB lang) {
		this.lang = lang;
	}
	@Override
	public Long pk() {
		return getId();
	}
}
