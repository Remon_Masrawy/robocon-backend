package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.ARTIST_META_LANG_FILTER;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.ParamDef;

@Entity
@Cacheable
@Table(name = "ROBOCON_ARTIST_META", 
	uniqueConstraints = {
			@UniqueConstraint(name = "UNIQUE_ARTIST_NAME", columnNames = {"NAME"}),
			@UniqueConstraint(name = "UNIQUE_ARTIST_META_LOCALE", columnNames = {"ARTIST_ID", "LANG"})
			})
@PrimaryKeyJoinColumn(name = "META_ID", referencedColumnName = "ID")
@FilterDefs({
	@FilterDef(	name = ARTIST_META_LANG_FILTER, 
				defaultCondition = "lang = (COALESCE((SELECT meta.lang FROM ROBOCON_ARTIST_META meta WHERE meta.lang = :lang AND ARTIST_ID = meta.ARTIST_ID), :default_lang))", 
				parameters = {
							@ParamDef(type = "java.lang.String", name = "lang"),
							@ParamDef(type = "java.lang.String", name = "default_lang")
						})
})
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconArtistMetaEB extends AbstractRoboconMetaEB{

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "ARTIST_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY, optional = false)
	private RoboconArtistEB roboconArtistEB;

	public RoboconArtistEB getRoboconArtistEB() {
		return roboconArtistEB;
	}

	public void setRoboconArtistEB(RoboconArtistEB roboconArtistEB) {
		this.roboconArtistEB = roboconArtistEB;
	}
	
}
