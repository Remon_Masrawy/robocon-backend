package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.ALBUM_META_LANG_FILTER;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Cacheable
@Table(name = "ROBOCON_ALBUM")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconAlbumEB extends RoboconObjectEB{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_ALBUM_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_ALBUM_ID_SEQ"),
            @Parameter(name = "initial_value", value = "1000000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ROBOCON_ALBUM_ID_SEQ")
	private Long id;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconAlbumEB")
	private List<RoboconSongEB> roboconSongsEB;
	
	@OrderBy("ID")
	@Filter(name = ALBUM_META_LANG_FILTER)
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconAlbumEB")
	private List<RoboconAlbumMetaEB> roboconAlbumMetaEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconAlbumEB")
	private List<RoboconAlbumPhotoEB> roboconAlbumPhotoEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconAlbumEB")
	private List<RoboconAlbumTagEB> roboconAlbumTagEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "PROVIDER_ID", referencedColumnName = "ID", nullable = false, updatable = true, insertable = true)
	private RoboconProviderEB roboconProviderEB;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, mappedBy="roboconAlbumsEB")
	private List<RoboconAlbumFrontendEB> roboconFrontendsEB;
	
	@OrderBy("ID")
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, mappedBy = "roboconAlbumsEB")
	private List<RoboconPlaylistEB> roboconPlaylistsEB;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	//=============== Songs ===============
	public List<RoboconSongEB> getRoboconSongsEB() {
		if(roboconSongsEB == null){
			roboconSongsEB = new ArrayList<RoboconSongEB>();
		}
		return roboconSongsEB;
	}
	public void setRoboconSongsEB(List<RoboconSongEB> roboconSongsEB) {
		this.roboconSongsEB = roboconSongsEB;
	}
	public void addRoboconSongEB(RoboconSongEB roboconSongEB){
		roboconSongEB.setRoboconAlbumEB(this);
		getRoboconSongsEB().add(roboconSongEB);
	}
	//=============== Meta ===============
	public List<RoboconAlbumMetaEB> getRoboconAlbumMetaEB() {
		if(roboconAlbumMetaEB == null){
			roboconAlbumMetaEB = new ArrayList<RoboconAlbumMetaEB>();
		}
		return roboconAlbumMetaEB;
	}
	public void setRoboconAlbumMetaEB(List<RoboconAlbumMetaEB> roboconAlbumMetaEB) {
		this.roboconAlbumMetaEB = roboconAlbumMetaEB;
	}
	public void addRoboconAlbumMetaEB(RoboconAlbumMetaEB roboconAlbumMetaEB){
		roboconAlbumMetaEB.setRoboconAlbumEB(this);
		getRoboconAlbumMetaEB().add(roboconAlbumMetaEB);
	}
	//=============== Photos ===============
	public List<RoboconAlbumPhotoEB> getRoboconAlbumPhotoEB() {
		if(roboconAlbumPhotoEB == null){
			roboconAlbumPhotoEB = new ArrayList<RoboconAlbumPhotoEB>();
		}
		return roboconAlbumPhotoEB;
	}
	public void setRoboconAlbumPhotoEB(List<RoboconAlbumPhotoEB> roboconAlbumPhotoEB) {
		this.roboconAlbumPhotoEB = roboconAlbumPhotoEB;
	}
	public void addRoboconAlbumPhotoEB(RoboconAlbumPhotoEB roboconAlbumPhotoEB){
		roboconAlbumPhotoEB.setRoboconAlbumEB(this);
		getRoboconAlbumPhotoEB().add(roboconAlbumPhotoEB);
	}
	//=============== Tags ===============
	public List<RoboconAlbumTagEB> getRoboconAlbumTagEB() {
		if(roboconAlbumTagEB == null){
			roboconAlbumTagEB = new ArrayList<RoboconAlbumTagEB>();
		}
		return roboconAlbumTagEB;
	}
	public void setRoboconAlbumTagEB(List<RoboconAlbumTagEB> roboconAlbumTagEB) {
		this.roboconAlbumTagEB = roboconAlbumTagEB;
	}
	public void addRoboconAlbumTagEB(RoboconAlbumTagEB roboconAlbumTagEB){
		roboconAlbumTagEB.setRoboconAlbumEB(this);
		getRoboconAlbumTagEB().add(roboconAlbumTagEB);
	}
	//=============== Provider ===============
	public RoboconProviderEB getRoboconProviderEB() {
		return roboconProviderEB;
	}
	public void setRoboconProviderEB(RoboconProviderEB roboconProviderEB) {
		this.roboconProviderEB = roboconProviderEB;
	}
	//=============== Frontends ===============
	public List<RoboconAlbumFrontendEB> getRoboconFrontendsEB() {
		if(roboconFrontendsEB == null){
			roboconFrontendsEB = new ArrayList<RoboconAlbumFrontendEB>();
		}
		return roboconFrontendsEB;
	}
	public void setRoboconFrontendsEB(List<RoboconAlbumFrontendEB> roboconFrontendsEB) {
		this.roboconFrontendsEB = roboconFrontendsEB;
	}
	public void addRoboconFrontendEB(RoboconAlbumFrontendEB roboconFrontendEB){
		getRoboconFrontendsEB().add(roboconFrontendEB);
	}
	//================== Playlists ==================
	public List<RoboconPlaylistEB> getRoboconPlaylistsEB() {
		if(roboconPlaylistsEB == null){
			roboconPlaylistsEB = new ArrayList<RoboconPlaylistEB>();
		}
		return roboconPlaylistsEB;
	}
	public void setRoboconPlaylistsEB(List<RoboconPlaylistEB> roboconPlaylistsEB) {
		this.roboconPlaylistsEB = roboconPlaylistsEB;
	}
	public void addRoboconPlaylistEB(RoboconPlaylistEB roboconPlaylistEB){
		getRoboconPlaylistsEB().add(roboconPlaylistEB);
	}
	@Override
	public Long pk() {
		return getId();
	}

}
