package com.masrawy.robocon.model;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = "ROBOCON_OPERATORS", uniqueConstraints = {
			@UniqueConstraint(name = "UNIQUE_ROBOCON_OPERATOR_MCC_MNC", columnNames = {"MCC", "MNC"})
		})
public class RoboconOperatorEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_OPERATOR_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_OPERATOR_ID_SEQ"),
            @Parameter(name = "initial_value", value = "100000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(generator = "ROBOCON_OPERATOR_ID_SEQ", strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "MCC", nullable = false)
	private String mcc;
	
	@Column(name = "MNC")
	private String mnc;
	
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "COUNTRY_ID", referencedColumnName = "ID", nullable = false)
	private CountryEB countryEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, mappedBy = "roboconOperatorsEB")
	private List<RoboconCategoryEB> roboconCategoriesEB;
	
	private Boolean allowed = true;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMcc() {
		return mcc;
	}

	public void setMcc(String mcc) {
		this.mcc = mcc;
	}

	public String getMnc() {
		return mnc;
	}

	public void setMnc(String mnc) {
		this.mnc = mnc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getAllowed() {
		return allowed;
	}

	public void setAllowed(Boolean allowed) {
		this.allowed = allowed;
	}
	
	public List<RoboconCategoryEB> getRoboconCategoriesEB() {
		return roboconCategoriesEB;
	}

	public void setRoboconCategoriesEB(List<RoboconCategoryEB> roboconCategoriesEB) {
		this.roboconCategoriesEB = roboconCategoriesEB;
	}

	public CountryEB getCountryEB() {
		return countryEB;
	}
	public void setCountryEB(CountryEB countryEB) {
		this.countryEB = countryEB;
	}

	@Override
	public Long pk() {
		return getId();
	}

}
