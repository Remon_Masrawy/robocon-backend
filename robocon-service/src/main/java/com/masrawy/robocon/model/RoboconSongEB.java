package com.masrawy.robocon.model;

import static com.cdm.core.persistent.usertype.HibernateUserTypes.DATE_ONLY;
import static com.masrawy.robocon.model.RoboconConstants.SONG_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.SONG_META_NAME_FILTER;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import com.cdm.core.datatypes.DateOnly;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = "ROBOCON_SONG", uniqueConstraints = {@UniqueConstraint(name = "UNIQUE_ROBOCON_SONG_ISRC", columnNames = {"ISRC"})})
public class RoboconSongEB extends RoboconObjectEB{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_SONG_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_SONG_ID_SEQ"),
            @Parameter(name = "initial_value", value = "1000000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ROBOCON_SONG_ID_SEQ")
	private Long id;
	
	@Column(name = "ISRC", nullable = true)
	private String isrc;
	
	@Type(type=DATE_ONLY)
	@Column(name = "CREATED_DATE", nullable = false)
	private DateOnly createdDate;
	
	@Column(name = "YEAR")
	private Long year;
	
	@Column(name = "RATING")
	private Double rating;
	
	@Column(name = "VIDEO_LINK")
	private String videoLink;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY)
	@JoinTable(name = "ROBOCON_SONG_ARTIST", 
				joinColumns			= {@JoinColumn(name = "SONG_ID", 	table = "ROBOCON_SONG", 	referencedColumnName = "ID", nullable = false)},
				inverseJoinColumns 	= {@JoinColumn(name = "ARTIST_ID", 	table = "ROBOCON_ARTIST", 	referencedColumnName = "ID", nullable = false)},
				uniqueConstraints 	= {@UniqueConstraint(name = "UNIQUE_SONG_ARTIST", columnNames = {"SONG_ID", "ARTIST_ID"})})
	private List<RoboconArtistEB> roboconArtistsEB;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY)
	@JoinTable(name = "ROBOCON_MAIN_SONG_ARTIST", 
				joinColumns			= {@JoinColumn(name = "SONG_ID", 	table = "ROBOCON_SONG", 	referencedColumnName = "ID", nullable = false)},
				inverseJoinColumns 	= {@JoinColumn(name = "ARTIST_ID", 	table = "ROBOCON_ARTIST", 	referencedColumnName = "ID", nullable = false)},
				uniqueConstraints 	= {@UniqueConstraint(name = "UNIQUE_MAIN_SONG_ARTIST", columnNames = {"SONG_ID", "ARTIST_ID"})})
	private List<RoboconArtistEB> mainRoboconArtistEB;
	
	@NotNull
	@OrderBy("ID")
	@Filter(name = SONG_META_LANG_FILTER)
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@Filters({@Filter(name = SONG_META_LANG_FILTER), @Filter(name = SONG_META_NAME_FILTER)})
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconSongEB")
	private List<RoboconSongMetaEB> roboconSongMetaEB;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconSongEB")
	private List<RoboconSongContentEB> roboconSongContentEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconSongEB")
	private List<RoboconSongKaraokeEB> roboconSongKaraokeEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconSongEB")
	private List<RoboconSongLyricsEB> roboconSongLyricsEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconSongEB")
	private List<RoboconUserSingEB> roboconUserSingsEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconSongEB")
	private List<RoboconSongPhotoEB> roboconSongPhotosEB;
	
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconSongEB")
	private List<RoboconSongTagEB> roboconSongTagsEB;
	
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "ALBUM_ID", referencedColumnName = "ID", nullable = false, insertable = true, updatable = true)
	private RoboconAlbumEB roboconAlbumEB;
	
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "PROVIDER_ID", referencedColumnName = "ID", nullable = false, insertable = true, updatable = true)
	private RoboconProviderEB roboconProviderEB;
	
	@OrderBy("ID")
	@JoinTable(	name 				= "ROBOCON_SONGS_GENRES",
				joinColumns 		= { @JoinColumn(name = "SONG_ID", referencedColumnName = "ID", nullable = false) },
				inverseJoinColumns 	= { @JoinColumn(name = "GENRE_ID", referencedColumnName = "ID", nullable = false) },
				uniqueConstraints 	= { @UniqueConstraint(name = "UNIQUE_SONG_GENRE", columnNames = { "SONG_ID", "GENRE_ID" }) })
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY)
	private List<RoboconGenreEB> roboconGenresEB;
	
	@OrderBy("ID")
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, mappedBy="roboconSongsEB")
	private List<RoboconSongFrontendEB> roboconFrontendsEB;
	
	@OrderBy("ID")
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "roboconSongsEB")
	private List<RoboconPlaylistEB> roboconPlaylistsEB;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconSongEB")
	private List<RoboconUserSongFavoriteEB> roboconUserSongFavoritesEB;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconSongEB")
	private List<RoboconUserSongHistoryEB> roboconUserSongHistoryEB;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIsrc() {
		return isrc;
	}
	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}
	public DateOnly getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(DateOnly createdDate) {
		this.createdDate = createdDate;
	}
	public Long getYear() {
		return year;
	}
	public void setYear(Long year) {
		this.year = year;
	}
	public Double getRating() {
		return rating;
	}
	public void setRating(Double rating) {
		this.rating = rating;
	}
	public String getVideoLink() {
		return videoLink;
	}
	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}
	//================== Main Artist ==================
	public List<RoboconArtistEB> getMainRoboconArtistEB() {
		return mainRoboconArtistEB;
	}
	public void setMainRoboconArtistEB(List<RoboconArtistEB> mainRoboconArtistEB) {
		this.mainRoboconArtistEB = mainRoboconArtistEB;
	}
	//================== Meta ==================
	public List<RoboconSongMetaEB> getRoboconSongMetaEB() {
		if(roboconSongMetaEB == null){
			roboconSongMetaEB = new ArrayList<RoboconSongMetaEB>();
		}
		return roboconSongMetaEB;
	}
	public void setRoboconSongMetaEB(List<RoboconSongMetaEB> roboconSongMetaEB) {
		this.roboconSongMetaEB = roboconSongMetaEB;
	}
	public void addRoboconSongMetaEB(RoboconSongMetaEB roboconSongMetaEB){
		roboconSongMetaEB.setRoboconSongEB(this);
		getRoboconSongMetaEB().add(roboconSongMetaEB);
	}
	public void removeRoboconSongMetaEB(RoboconSongMetaEB roboconSongMetaEB){
		roboconSongMetaEB.setRoboconSongEB(null);
		getRoboconSongMetaEB().remove(roboconSongMetaEB);
	}
	//================== Artists ==================
	public List<RoboconArtistEB> getRoboconArtistsEB() {
		if(roboconArtistsEB == null){
			roboconArtistsEB = new ArrayList<RoboconArtistEB>();
		}
		return roboconArtistsEB;
	}
	public void setRoboconArtistsEB(List<RoboconArtistEB> roboconArtistsEB) {
		this.roboconArtistsEB = roboconArtistsEB;
	}
	public void addRoboconArtistEB(RoboconArtistEB roboconArtistEB){
		getRoboconArtistsEB().add(roboconArtistEB);
	}
	//================== Album ==================
	public RoboconAlbumEB getRoboconAlbumEB() {
		return roboconAlbumEB;
	}
	public void setRoboconAlbumEB(RoboconAlbumEB roboconAlbumEB) {
		this.roboconAlbumEB = roboconAlbumEB;
	}
	//================== Content ==================
	public List<RoboconSongContentEB> getRoboconSongContentEB() {
		if(roboconSongContentEB == null){
			roboconSongContentEB = new ArrayList<RoboconSongContentEB>();
		}
		return roboconSongContentEB;
	}
	public void setRoboconSongContentEB(List<RoboconSongContentEB> roboconSongContentEB) {
		this.roboconSongContentEB = roboconSongContentEB;
	}
	public void addRoboconSongContentEB(RoboconSongContentEB roboconSongContentEB){
		roboconSongContentEB.setRoboconSongEB(this);
		getRoboconSongContentEB().add(roboconSongContentEB);
	}
	public void removeRoboconSongContentEB(RoboconSongContentEB roboconSongContentEB){
		roboconSongContentEB.setRoboconSongEB(null);
		getRoboconSongContentEB().remove(roboconSongContentEB);
	}
	//================== Karaoke ==================
	public List<RoboconSongKaraokeEB> getRoboconSongKaraokeEB() {
		if(roboconSongKaraokeEB == null){
			roboconSongKaraokeEB = new ArrayList<RoboconSongKaraokeEB>();
		}
		return roboconSongKaraokeEB;
	}
	public void setRoboconSongKaraokeEB(List<RoboconSongKaraokeEB> roboconSongKaraokeEB) {
		this.roboconSongKaraokeEB = roboconSongKaraokeEB;
	}
	public void addRoboconSongKaraokeEB(RoboconSongKaraokeEB roboconSongKaraokeEB){
		roboconSongKaraokeEB.setRoboconSongEB(this);
		getRoboconSongKaraokeEB().add(roboconSongKaraokeEB);
	}
	public void removeRoboconSongKaraokeEB(RoboconSongKaraokeEB roboconSongKaraokeEB){
		roboconSongKaraokeEB.setRoboconSongEB(null);
		getRoboconSongKaraokeEB().remove(roboconSongKaraokeEB);
	}
	//================== Lyrics ==================
	public List<RoboconSongLyricsEB> getRoboconSongLyricsEB() {
		if(roboconSongLyricsEB == null){
			roboconSongLyricsEB = new ArrayList<RoboconSongLyricsEB>();
		}
		return roboconSongLyricsEB;
	}
	public void setRoboconSongLyricsEB(List<RoboconSongLyricsEB> roboconSongLyricsEB) {
		this.roboconSongLyricsEB = roboconSongLyricsEB;
	}
	public void addRoboconSongLyricsEB(RoboconSongLyricsEB roboconSongLyricsEB){
		roboconSongLyricsEB.setRoboconSongEB(this);
		getRoboconSongLyricsEB().add(roboconSongLyricsEB);
	}
	public void removeRoboconSongLyricsEB(RoboconSongLyricsEB roboconSongLyricsEB){
		roboconSongLyricsEB.setRoboconSongEB(null);
		getRoboconSongLyricsEB().remove(roboconSongLyricsEB);
	}
	//================== Sings ==================
	public List<RoboconUserSingEB> getRoboconUserSingsEB() {
		if(roboconUserSingsEB == null){
			roboconUserSingsEB = new ArrayList<RoboconUserSingEB>();
		}
		return roboconUserSingsEB;
	}
	public void setRoboconUserSingsEB(List<RoboconUserSingEB> roboconUserSingsEB) {
		this.roboconUserSingsEB = roboconUserSingsEB;
	}
	public void addRoboconUserSingEB(RoboconUserSingEB roboconUserSingEB){
		roboconUserSingEB.setRoboconSongEB(this);
		getRoboconUserSingsEB().add(roboconUserSingEB);
	}
	public void removeRoboconUserSingEB(RoboconUserSingEB roboconUserSingEB){
		roboconUserSingEB.setRoboconSongEB(null);
		getRoboconUserSingsEB().remove(roboconUserSingEB);
	}
	//================== Photos ==================
	public List<RoboconSongPhotoEB> getRoboconSongPhotosEB() {
		if(roboconSongPhotosEB == null){
			roboconSongPhotosEB = new ArrayList<RoboconSongPhotoEB>();
		}
		return roboconSongPhotosEB;
	}
	public void setRoboconSongPhotosEB(List<RoboconSongPhotoEB> roboconSongPhotosEB) {
		this.roboconSongPhotosEB = roboconSongPhotosEB;
	}
	public void addRoboconSongPhotosEB(RoboconSongPhotoEB roboconSongPhotosEB){
		roboconSongPhotosEB.setRoboconSongEB(this);
		getRoboconSongPhotosEB().add(roboconSongPhotosEB);
	}
	//================== Tags ==================
	public List<RoboconSongTagEB> getRoboconSongTagsEB() {
		if(roboconSongTagsEB == null){
			roboconSongTagsEB = new ArrayList<RoboconSongTagEB>();
		}
		return roboconSongTagsEB;
	}
	public void setRoboconSongTagsEB(List<RoboconSongTagEB> roboconSongTagsEB) {
		this.roboconSongTagsEB = roboconSongTagsEB;
	}
	public void addRoboconSongTagsEB(RoboconSongTagEB roboconSongTagEB){
		roboconSongTagEB.setRoboconSongEB(this);
		getRoboconSongTagsEB().add(roboconSongTagEB);
	}
	//================== Genre ==================
	public List<RoboconGenreEB> getRoboconGenresEB() {
		return roboconGenresEB;
	}
	public void setRoboconGenresEB(List<RoboconGenreEB> roboconGenresEB) {
		this.roboconGenresEB = roboconGenresEB;
	}
	//================== Provider ==================
	public RoboconProviderEB getRoboconProviderEB() {
		return roboconProviderEB;
	}
	public void setRoboconProviderEB(RoboconProviderEB roboconProviderEB) {
		this.roboconProviderEB = roboconProviderEB;
	}
	//================== Frontends ==================
	public List<RoboconSongFrontendEB> getRoboconFrontendsEB() {
		if(roboconFrontendsEB == null){
			roboconFrontendsEB = new ArrayList<RoboconSongFrontendEB>();
		}
		return roboconFrontendsEB;
	}
	public void setRoboconFrontendsEB(List<RoboconSongFrontendEB> roboconFrontendsEB) {
		this.roboconFrontendsEB = roboconFrontendsEB;
	}
	public void addRoboconFrontendEB(RoboconSongFrontendEB roboconFrontendEB){
		getRoboconFrontendsEB().add(roboconFrontendEB);
	}
	//================== Playlists ==================
	public List<RoboconPlaylistEB> getRoboconPlaylistsEB() {
		if(roboconPlaylistsEB == null){
			roboconPlaylistsEB = new ArrayList<RoboconPlaylistEB>();
		}
		return roboconPlaylistsEB;
	}
	public void setRoboconPlaylistsEB(List<RoboconPlaylistEB> roboconPlaylistsEB) {
		this.roboconPlaylistsEB = roboconPlaylistsEB;
	}
	public void addRoboconPlaylistEB(RoboconPlaylistEB roboconPlaylistEB){
		getRoboconPlaylistsEB().add(roboconPlaylistEB);
	}
	//================== Favorites ==================
	public List<RoboconUserSongFavoriteEB> getRoboconUserSongFavoritesEB() {
		if(roboconUserSongFavoritesEB == null){
			roboconUserSongFavoritesEB = new ArrayList<RoboconUserSongFavoriteEB>();
		}
		return roboconUserSongFavoritesEB;
	}
	public void setRoboconUserSongFavoritesEB(List<RoboconUserSongFavoriteEB> roboconUserSongFavoritesEB) {
		this.roboconUserSongFavoritesEB = roboconUserSongFavoritesEB;
	}
	public void addRoboconUserSongFavoriteEB(RoboconUserSongFavoriteEB roboconUserSongFavoriteEB){
		getRoboconUserSongFavoritesEB().add(roboconUserSongFavoriteEB);
	}
	//================== History ==================
	public List<RoboconUserSongHistoryEB> getRoboconUserSongHistoryEB() {
		if(roboconUserSongHistoryEB == null){
			roboconUserSongHistoryEB = new ArrayList<RoboconUserSongHistoryEB>();
		}
		return roboconUserSongHistoryEB;
	}
	public void setRoboconUserSongHistoryEB(List<RoboconUserSongHistoryEB> roboconUserSongHistoryEB) {
		this.roboconUserSongHistoryEB = roboconUserSongHistoryEB;
	}
	@Override
	public Long pk() {
		return getId();
	}
}
