package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Table(name = "ROBOCON_ARTIST_EVENT_TAGS")
@PrimaryKeyJoinColumn(name = "TAG_ID", referencedColumnName = "ID")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconArtistEventTagEB extends AbstractRoboconTagEB{

	private static final long serialVersionUID = 1L;
	
	@JoinColumn(name = "EVENT_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY, optional = false)
	private RoboconArtistEventEB roboconArtistEventEB;

	public RoboconArtistEventEB getRoboconArtistEventEB() {
		return roboconArtistEventEB;
	}

	public void setRoboconArtistEventEB(RoboconArtistEventEB roboconArtistEventEB) {
		this.roboconArtistEventEB = roboconArtistEventEB;
	}

}
