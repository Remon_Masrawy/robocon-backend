package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.GENRE_META_LANG_FILTER;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.ParamDef;
import org.hibernate.search.annotations.Indexed;

@Entity
@Cacheable
@Indexed(index = "GENRES")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = "ROBOCON_GENRE_META", uniqueConstraints = { 
		@UniqueConstraint(name = "UNIQUE_GENRE_NAME", columnNames = { "NAME" }),
		@UniqueConstraint(name = "UNIQUE_GENRE_META_LANG", columnNames = { "GENRE_ID", "LANG" })})
@PrimaryKeyJoinColumn(name = "META_ID", referencedColumnName = "ID")
@FilterDefs({
	@FilterDef(	name = GENRE_META_LANG_FILTER, 
				defaultCondition = "lang = (COALESCE((SELECT meta.lang FROM ROBOCON_GENRE_META meta WHERE meta.lang = :lang AND GENRE_ID = meta.GENRE_ID), :default_lang))", 
				parameters = {
							@ParamDef(type = "java.lang.String", name = "lang"),
							@ParamDef(type = "java.lang.String", name = "default_lang")
						})
})
public class RoboconGenreMetaEB extends AbstractRoboconMetaEB{

	private static final long serialVersionUID = 1L;
	
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "GENRE_ID", referencedColumnName = "ID", nullable = false, updatable = false)
	private RoboconGenreEB roboconGenreEB;

	public RoboconGenreEB getRoboconGenreEB() {
		return roboconGenreEB;
	}

	public void setRoboconGenreEB(RoboconGenreEB roboconGenreEB) {
		this.roboconGenreEB = roboconGenreEB;
	}
	
}
