package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.cdm.core.security.model.UserEB;

@Entity
@Cacheable
@Table(name = "ROBOCON_USER")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconUserEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;
	
	@MapsId
	@OneToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.EAGER)
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	private UserEB user;
	
	@Column(name = "is_active", nullable = true, columnDefinition = "bool default false")
	private Boolean isActive  = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserEB getUser() {
		return user;
	}

	public void setUser(UserEB user) {
		this.user = user;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public Long pk() {
		return getId();
	}

}
