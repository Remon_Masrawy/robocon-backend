package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Table(name = "ROBOCON_USER_SONG_FAVORITE")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconUserSongFavoriteEB extends AbstractRoboconUserFavoriteEB{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "SONG_ID", referencedColumnName = "ID", nullable = false)
	private RoboconSongEB roboconSongEB;

	public RoboconSongEB getRoboconSongEB() {
		return roboconSongEB;
	}

	public void setRoboconSongEB(RoboconSongEB roboconSongEB) {
		roboconSongEB.addRoboconUserSongFavoriteEB(this);
		this.roboconSongEB = roboconSongEB;
	}
	
}
