package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstraint.UNIQUE_PROVIDER_NAME;
import static com.masrawy.robocon.model.RoboconConstants.PROVIDER_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstraint.UNIQUE_PROVIDER_META_LANG;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.ParamDef;

@Entity
@Cacheable
@Table(name = "ROBOCON_PROVIDER_META",
		uniqueConstraints = { 
				@UniqueConstraint(name = UNIQUE_PROVIDER_NAME, columnNames = { "NAME" }),
				@UniqueConstraint(name = UNIQUE_PROVIDER_META_LANG, columnNames = { "PROVIDER_ID", "LANG" })})
@PrimaryKeyJoinColumn(name = "META_ID", referencedColumnName = "ID")
@FilterDefs({
	@FilterDef(	name = PROVIDER_META_LANG_FILTER, 
				defaultCondition = "lang = (COALESCE((SELECT meta.lang FROM ROBOCON_PROVIDER_META meta WHERE meta.lang = :lang AND PROVIDER_ID = meta.PROVIDER_ID), :default_lang))", 
				parameters = {
							@ParamDef(type = "java.lang.String", name = "lang"),
							@ParamDef(type = "java.lang.String", name = "default_lang")
						})
})
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconProviderMetaEB extends AbstractRoboconMetaEB{

	private static final long serialVersionUID = 1L;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "PROVIDER_ID", referencedColumnName = "ID")
	private RoboconProviderEB roboconProviderEB;

	public RoboconProviderEB getRoboconProviderEB() {
		return roboconProviderEB;
	}

	public void setRoboconProviderEB(RoboconProviderEB roboconProviderEB) {
		this.roboconProviderEB = roboconProviderEB;
	}
}
