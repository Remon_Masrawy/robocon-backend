package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Table(name = "ROBOCON_USER_SONG_HIT")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconUserSongHitEB extends AbstractRoboconHitEB{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "DURATION")
	private Long duration = 0L;
	
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "HISTORY_ID", nullable = false)
	private RoboconUserSongHistoryEB roboconUserSongHistoryEB;

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public RoboconUserSongHistoryEB getRoboconUserSongHistoryEB() {
		return roboconUserSongHistoryEB;
	}

	public void setRoboconUserSongHistoryEB(RoboconUserSongHistoryEB roboconUserSongHistoryEB) {
		this.roboconUserSongHistoryEB = roboconUserSongHistoryEB;
	}

}
