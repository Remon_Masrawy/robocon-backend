package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.GENRE_META_LANG_FILTER;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import com.cdm.core.persistent.model.AbstractEntity;

/**
 * <p>Robocon Genre</p>
 * 
 * @author Remon Gaber
 * @since 01-01-2017
 * @version 1.0
 */

@Entity
@Cacheable
@Table(name = "ROBOCON_GENRE")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconGenreEB extends AbstractEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_GENRE_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_GENRE_ID_SEQ"),
            @Parameter(name = "initial_value", value = "100000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ROBOCON_GENRE_ID_SEQ")
	private Long id;
	
	@OrderBy("ID")
	@Filter(name = GENRE_META_LANG_FILTER)
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconGenreEB")
	private List<RoboconGenreMetaEB> roboconGenreMetaEB;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.LAZY, mappedBy = "roboconGenresEB")
	private List<RoboconSongEB> roboconSongsEB;
	
	/* ********************************************************************************
	 * Getter and Setter Methods
	 */
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<RoboconGenreMetaEB> getRoboconGenreMetaEB() {
		if(roboconGenreMetaEB == null){
			roboconGenreMetaEB = new ArrayList<RoboconGenreMetaEB>();
		}
		return roboconGenreMetaEB;
	}
	public void setRoboconGenreMetaEB(List<RoboconGenreMetaEB> roboconGenreMetaEB) {
		this.roboconGenreMetaEB = roboconGenreMetaEB;
	}
	public void addRoboconGenreMetaEB(RoboconGenreMetaEB roboconGenreMetaEB){
		roboconGenreMetaEB.setRoboconGenreEB(this);
		getRoboconGenreMetaEB().add(roboconGenreMetaEB);
	}
	public List<RoboconSongEB> getRoboconSongsEB() {
		if(roboconSongsEB == null){
			roboconSongsEB = new ArrayList<RoboconSongEB>();
		}
		return roboconSongsEB;
	}
	public void setRoboconSongsEB(List<RoboconSongEB> roboconSongsEB) {
		roboconSongsEB = this.roboconSongsEB;
	}
	public void addRoboconSongEB(RoboconSongEB roboconSongEB){
		roboconSongEB.getRoboconGenresEB().add(this);
		getRoboconSongsEB().add(roboconSongEB);
	}
	@Override
	public Long pk() {
		return getId();
	}

}
