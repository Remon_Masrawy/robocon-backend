package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.ALBUM_META_LANG_FILTER;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.ParamDef;

@Entity
@Cacheable
@Table(name = "ROBOCON_ALBUM_META", 
		uniqueConstraints = { 
				@UniqueConstraint(name = "UNIQUE_ALBUM_NAME", columnNames = {"NAME"}),
				@UniqueConstraint(name = "UNIQUE_ALBUM_META_LOCALE", columnNames = {"ALBUM_ID", "LANG"})
				})
@PrimaryKeyJoinColumn(name = "META_ID", referencedColumnName = "ID")
@FilterDefs({
	@FilterDef(	name = ALBUM_META_LANG_FILTER, 
				defaultCondition = "lang = (COALESCE((SELECT meta.lang FROM ROBOCON_ALBUM_META meta WHERE meta.lang = :lang AND ALBUM_ID = meta.ALBUM_ID), :default_lang))", 
				parameters = {
							@ParamDef(type = "java.lang.String", name = "lang"),
							@ParamDef(type = "java.lang.String", name = "default_lang")
						})
})
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconAlbumMetaEB extends AbstractRoboconMetaEB{

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "ALBUM_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	private RoboconAlbumEB roboconAlbumEB;

	public RoboconAlbumEB getRoboconAlbumEB() {
		return roboconAlbumEB;
	}

	public void setRoboconAlbumEB(RoboconAlbumEB roboconAlbumEB) {
		this.roboconAlbumEB = roboconAlbumEB;
	}
}
