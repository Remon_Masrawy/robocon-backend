package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.CATEGORY_META_LANG_FILTER;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Cacheable
@Table(name = "ROBOCON_CATEGORY")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconCategoryEB extends AbstractRoboconEntity<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GenericGenerator(name = "ROBOCON_CATEGORY_ID_SEQ", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
            @Parameter(name = "sequence_name", value = "ROBOCON_CATEGORY_ID_SEQ"),
            @Parameter(name = "initial_value", value = "100000"),
            @Parameter(name = "optimizer", value = "hilo")
    })
	@GeneratedValue(generator = "ROBOCON_CATEGORY_ID_SEQ", strategy = GenerationType.AUTO)
	private Long id;
	
	@OrderBy("ID")
	@Filter(name = CATEGORY_META_LANG_FILTER)
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "roboconCategoryEB")
	private List<RoboconCategoryMetaEB> roboconCategoryMetaEB;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@JoinTable(name 				= "ROBOCON_CATEGORY_STOREFRONTS", 
			joinColumns 			= {@JoinColumn(name = "CATEGORY_ID", referencedColumnName = "ID", table = "ROBOCON_CATEGORY")}, 
			inverseJoinColumns 		= {@JoinColumn(name = "STOREFRONT_ID", referencedColumnName = "ID", table = "ROBOCON_STOREFRONT")},
			uniqueConstraints 		= {@UniqueConstraint(name = "UNIQUE_CATEGORY_STOREFRONT", columnNames = {"CATEGORY_ID", "STOREFRONT_ID"})})
	@ManyToMany(cascade 			= { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY)
	private List<RoboconStorefrontEB> roboconStorefrontsEB;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@JoinTable(name 				= "ROBOCON_CATEGORY_OPERATORS", 
			joinColumns 			= {@JoinColumn(name = "CATEGORY_ID", referencedColumnName = "ID", table = "ROBOCON_CATEGORY")}, 
			inverseJoinColumns 		= {@JoinColumn(name = "OPERATOR_ID", referencedColumnName = "ID", table = "ROBOCON_OPERATORS")},
			uniqueConstraints 		= {@UniqueConstraint(name = "UNIQUE_CATEGORY_OPERATOR", columnNames = {"CATEGORY_ID", "OPERATOR_ID"})})
	@ManyToMany(cascade 			= { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY)
	private List<RoboconOperatorEB> roboconOperatorsEB;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@JoinTable(name 				= "ROBOCON_CATEGORY_COUNTRIES", 
			joinColumns 			= {@JoinColumn(name = "CATEGORY_ID", referencedColumnName = "ID", table = "ROBOCON_CATEGORY")}, 
			inverseJoinColumns 		= {@JoinColumn(name = "COUNTRY_ID", referencedColumnName = "ID", table = "COUNTRIES")},
			uniqueConstraints 		= {@UniqueConstraint(name = "UNIQUE_CATEGORY_COUNTRIES", columnNames = {"CATEGORY_ID", "COUNTRY_ID"})})
	@ManyToMany(cascade 			= { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY)
	private List<CountryEB> countriesEB;
	
	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@ManyToMany(cascade 			= { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, mappedBy="roboconCategoriesEB")
	private List<RoboconFrontendEB> roboconFrontendsEB;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<RoboconCategoryMetaEB> getRoboconCategoryMetaEB() {
		if(roboconCategoryMetaEB == null){
			roboconCategoryMetaEB = new ArrayList<RoboconCategoryMetaEB>();
		}
		return roboconCategoryMetaEB;
	}

	public void setRoboconCategoryMetaEB(List<RoboconCategoryMetaEB> roboconCategoryMetaEB) {
		this.roboconCategoryMetaEB = roboconCategoryMetaEB;
	}

	/**
	 * 
	 * @return List<RoboconOperatorEB>
	 */
	
	public List<RoboconOperatorEB> getRoboconOperatorsEB() {
		if(roboconOperatorsEB == null){
			roboconOperatorsEB = new ArrayList<RoboconOperatorEB>();
		}
		return roboconOperatorsEB;
	}

	public void setRoboconOperatorsEB(List<RoboconOperatorEB> roboconOperatorsEB) {
		this.roboconOperatorsEB = roboconOperatorsEB;
	}

	/**
	 * 
	 * @return List<RoboconStorefrontEB>
	 */
	
	public List<RoboconStorefrontEB> getRoboconStorefrontsEB() {
		if(roboconStorefrontsEB == null){
			roboconStorefrontsEB = new ArrayList<RoboconStorefrontEB>();
		}
		return roboconStorefrontsEB;
	}

	public void setRoboconStorefrontsEB(List<RoboconStorefrontEB> roboconStorefrontsEB) {
		this.roboconStorefrontsEB = roboconStorefrontsEB;
	}

	/**
	 * 
	 * @return List<CountryEB>
	 */
	
	public List<CountryEB> getCountriesEB() {
		return countriesEB;
	}

	public void setCountriesEB(List<CountryEB> countriesEB) {
		this.countriesEB = countriesEB;
	}

	/**
	 * 
	 * @return List<RoboconFrontendEB>
	 */
	
	public List<RoboconFrontendEB> getRoboconFrontendsEB() {
		return roboconFrontendsEB;
	}

	public void setRoboconFrontendsEB(List<RoboconFrontendEB> roboconFrontendsEB) {
		this.roboconFrontendsEB = roboconFrontendsEB;
	}

	@Override
	public Long pk() {
		return getId();
	}

}
