package com.masrawy.robocon.model;

import static com.masrawy.robocon.model.RoboconConstants.SONG_META_LANG_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.SONG_META_NAME_FILTER;
import static com.masrawy.robocon.model.RoboconConstants.SONG_META_LANG_QUERY;
import static com.masrawy.robocon.model.RoboconConstants.SONG_META_NAME_QUERY;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.ParamDef;

@Entity
@Cacheable
@Table(name = "ROBOCON_SONG_META", 
		uniqueConstraints = {
				@UniqueConstraint(name = "UNIQUE_SONG_NAME", columnNames = { "NAME" }),
				@UniqueConstraint(name = "UNIQUE_SONG_META_LANG", columnNames = { "SONG_ID", "LANG" })
		})
@PrimaryKeyJoinColumn(name = "META_ID", referencedColumnName = "ID")
@FilterDefs({
	@FilterDef(	name = SONG_META_LANG_FILTER, 
				defaultCondition = SONG_META_LANG_QUERY, 
				parameters = {
							@ParamDef(type = "java.lang.String", name = "lang"),
							@ParamDef(type = "java.lang.String", name = "default_lang")
						}),
	@FilterDef(	name = SONG_META_NAME_FILTER,
				defaultCondition = SONG_META_NAME_QUERY,
				parameters = {@ParamDef(type = "java.lang.String", name = "name")})
})
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconSongMetaEB extends AbstractRoboconMetaEB{

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "SONG_ID", referencedColumnName = "ID")
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	private RoboconSongEB roboconSongEB;
	
	public RoboconSongEB getRoboconSongEB() {
		return roboconSongEB;
	}
	public void setRoboconSongEB(RoboconSongEB roboconSongEB) {
		this.roboconSongEB = roboconSongEB;
	}

}
