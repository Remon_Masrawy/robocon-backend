package com.masrawy.robocon.model;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconSongFrontendEB extends RoboconFrontendEB{

	private static final long serialVersionUID = 1L;

	@OrderBy("ID")
	@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	@JoinTable(	name 				= "ROBOCON_FRONTEND_SONGS",
				joinColumns 		= { @JoinColumn(name = "FRONTEND_ID", referencedColumnName = "ID", nullable = false) },
				inverseJoinColumns 	= { @JoinColumn(name = "SONG_ID", referencedColumnName = "ID", nullable = false) },
				uniqueConstraints 	= { @UniqueConstraint(name = "UNIQUE_FRONTEND_SONG", columnNames = { "SONG_ID", "FRONTEND_ID" }) })
	@ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY)
	private List<RoboconSongEB> roboconSongsEB;
	
	public List<RoboconSongEB> getRoboconSongsEB() {
		return roboconSongsEB;
	}

	public void setRoboconSongsEB(List<RoboconSongEB> roboconSongsEB) {
		this.roboconSongsEB = roboconSongsEB;
	}
}