package com.masrawy.robocon.model;

import static com.cdm.core.persistent.usertype.HibernateUserTypes.STRING_ENUM;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

/*@Entity*/
@Cacheable
/*@Table(name = "ROBOCON_USER_HISTORY")*/
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public abstract class AbstractRoboconHistoryEB extends AbstractRoboconEntity<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "ROBOCON_USER_HISTORY_ID_SEQ", sequenceName = "ROBOCON_USER_HISTORY_ID_SEQ", initialValue = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ROBOCON_USER_HISTORY_ID_SEQ")
	private Long id;
	
	@Column(name = "USER_ID", nullable = false)
	private Long userId;
	
	@Column(name = "DEVICE_ID", nullable = true)
	private String deviceId;
	
	@Type(type = STRING_ENUM, parameters = {@Parameter(name = "enumClass", value = "com.masrawy.robocon.model.RoboconUserActionEnum")})
	@Column(name = "ACTION", nullable = false)
	private RoboconUserActionEnum roboconUserAction;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public RoboconUserActionEnum getRoboconUserAction() {
		return roboconUserAction;
	}
	public void setRoboconUserAction(RoboconUserActionEnum roboconUserAction) {
		this.roboconUserAction = roboconUserAction;
	}
	
	@Override
	public Long pk() {
		return getId();
	}

}
