package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = "ROBOCON_SONG_CONTENT", 
		uniqueConstraints = {
				@UniqueConstraint(name = "UNIQUE_ROBOCON_SONG_CONTENT", columnNames = {"SONG_ID", "BIT_RATE"}),
				@UniqueConstraint(name = "UNIQUE_ROBOCON_SONG_CONTENT_CHECKSUM", columnNames = {"CHECKSUM"})})
public class RoboconSongContentEB extends AbstractRoboconFileEB<Long>{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "CONTENT_ID_SEQ", sequenceName = "CONTENT_ID_SEQ", initialValue = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CONTENT_ID_SEQ")
	private Long id;
	
	@JoinColumn(name = "SONG_ID", referencedColumnName = "ID")
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	private RoboconSongEB roboconSongEB;
	
	@Column(name = "BIT_RATE")
	private String bitRate;
	@Column(name = "SAMPLE_RATE")
	private String sampleRate;
	private String channels;
	private Integer duration;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public RoboconSongEB getRoboconSongEB() {
		return roboconSongEB;
	}
	public void setRoboconSongEB(RoboconSongEB roboconSongEB) {
		this.roboconSongEB = roboconSongEB;
	}
	public String getBitRate() {
		return bitRate;
	}
	public void setBitRate(String bitRate) {
		this.bitRate = bitRate;
	}
	public String getSampleRate() {
		return sampleRate;
	}
	public void setSampleRate(String sampleRate) {
		this.sampleRate = sampleRate;
	}
	public String getChannels() {
		return channels;
	}
	public void setChannels(String channels) {
		this.channels = channels;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
