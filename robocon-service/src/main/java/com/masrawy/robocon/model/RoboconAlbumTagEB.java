package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Table(name = "ROBOCON_ALBUM_TAGS")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@PrimaryKeyJoinColumn(name = "TAG_ID", referencedColumnName = "ID")
public class RoboconAlbumTagEB extends AbstractRoboconTagEB{

private static final long serialVersionUID = 1L;
	
	@JoinColumn(name = "ALBUM_ID", referencedColumnName = "ID", nullable = false)
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	private RoboconAlbumEB roboconAlbumEB;

	public RoboconAlbumEB getRoboconAlbumEB() {
		return roboconAlbumEB;
	}

	public void setRoboconAlbumEB(RoboconAlbumEB roboconAlbumEB) {
		this.roboconAlbumEB = roboconAlbumEB;
	}
}
