package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = "ROBOCON_SONG_LYRICS", uniqueConstraints = {@UniqueConstraint(name = "UNIQUE_ROBOCON_SONG_LYRICS_CHECKSUM", columnNames = {"SONG_ID", "CHECKSUM"})})
public class RoboconSongLyricsEB extends AbstractRoboconFileEB<Long>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "ROBOCON_SONG_LYRICS_ID_SEQ", sequenceName = "ROBOCON_SONG_LYRICS_ID_SEQ")
	@GeneratedValue(generator = "ROBOCON_SONG_LYRICS_ID_SEQ", strategy = GenerationType.AUTO)
	private Long id;
	
	@JoinColumn(name = "SONG_ID", referencedColumnName = "ID", nullable = false, insertable = true, updatable = true)
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.DETACH }, fetch = FetchType.LAZY, optional = false)
	private RoboconSongEB roboconSongEB;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RoboconSongEB getRoboconSongEB() {
		return roboconSongEB;
	}

	public void setRoboconSongEB(RoboconSongEB roboconSongEB) {
		this.roboconSongEB = roboconSongEB;
	}

	@Override
	public Long pk() {
		return getId();
	}

}
