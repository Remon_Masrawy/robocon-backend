package com.masrawy.robocon.model;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Table(name = "ROBOCON_PLAYLIST_META")
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class RoboconPlaylistMetaEB extends AbstractRoboconMetaEB{

	private static final long serialVersionUID = 1L;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "PLAYLIST_ID", referencedColumnName = "ID")
	private RoboconPlaylistEB roboconPlaylistEB;

	public RoboconPlaylistEB getRoboconPlaylistEB() {
		return roboconPlaylistEB;
	}

	public void setRoboconPlaylistEB(RoboconPlaylistEB roboconPlaylistEB) {
		this.roboconPlaylistEB = roboconPlaylistEB;
	}
}
