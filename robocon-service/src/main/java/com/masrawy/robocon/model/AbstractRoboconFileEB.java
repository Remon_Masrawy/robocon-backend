package com.masrawy.robocon.model;

import static com.cdm.core.persistent.usertype.HibernateUserTypes.INTEGER_ENUM;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import com.masrawy.robocon.file.model.FileTypeEnum;

@MappedSuperclass
public abstract class AbstractRoboconFileEB<S extends Serializable> extends AbstractRoboconEntity<S>{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "FILE_NAME", nullable = false)
	private String fileName;
	@Column(name = "CHECKSUM", nullable = false)
	private String checksum;
	private String format;
	@Column(name = "FILE_TYPE")
	private FileTypeEnum fileType;
	@Column(name = "CONTENT_TYPE")
	private String contentType;
	private String extension;
	@Column(name = "FILE_SIZE")
	private Long fileSize;
	@Column(name = "FILE_MIME_TYPE")
	@Type(type = INTEGER_ENUM, parameters = { @Parameter(name = "enumClass", value = "com.masrawy.robocon.model.RoboconMimeTypeEnum") })
	private RoboconMimeTypeEnum fileMimeType;
	
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public FileTypeEnum getFileType() {
		return fileType;
	}
	public void setFileType(FileTypeEnum fileType) {
		this.fileType = fileType;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public Long getFileSize() {
		return fileSize;
	}
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	public RoboconMimeTypeEnum getFileMimeType() {
		return fileMimeType;
	}
	public void setFileMimeType(RoboconMimeTypeEnum fileMimeType) {
		this.fileMimeType = fileMimeType;
	}

}
