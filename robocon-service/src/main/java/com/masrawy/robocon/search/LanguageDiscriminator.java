package com.masrawy.robocon.search;

import org.hibernate.search.analyzer.Discriminator;

import com.masrawy.robocon.model.AbstractRoboconMetaEB;

public class LanguageDiscriminator implements Discriminator{

	@Override
	public String getAnalyzerDefinitionName(Object value, Object entity, String field) {
		if ( value == null || !( entity instanceof AbstractRoboconMetaEB ) ) {
            return null;
        }
		AbstractRoboconMetaEB meta = (AbstractRoboconMetaEB) value;
        return meta.getLang().getCode();
	}

}
