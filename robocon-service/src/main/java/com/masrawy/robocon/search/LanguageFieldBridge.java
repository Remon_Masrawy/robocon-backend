package com.masrawy.robocon.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.cdm.core.security.model.LanguageEB;

public class LanguageFieldBridge implements FieldBridge{

	@Override
	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		LanguageEB lang = (LanguageEB) value;
		String code = lang.getCode();
		Field field = new Field(name, code, luceneOptions.getStore(), luceneOptions.getIndex(), luceneOptions.getTermVector());
		field.setBoost( luceneOptions.getBoost() );
        document.add( field );
	}

}
