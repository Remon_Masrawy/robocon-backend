package com.masrawy.robocon.search;

import javax.annotation.PostConstruct;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class BuildIndexSearch {

	private static final Logger logger = LoggerFactory.getLogger(BuildIndexSearch.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@PostConstruct
	private void inialize(){
		try {
			FullTextSession fullTextSession = Search.getFullTextSession(sessionFactory.openSession());
			fullTextSession.flushToIndexes();
			fullTextSession.createIndexer().startAndWait();
		} catch (HibernateException e) {
			logger.error("Error:", e);
		} catch (InterruptedException e) {
			logger.error("Error:", e);
		}
	}
}
