package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconSongTrendDTO;
import com.masrawy.robocon.model.RoboconSongTrendEB;

@Component("roboconSongTrendTransformer")
public class RoboconSongTrendTransformerImpl extends AbstractDTOTransformer<RoboconSongTrendEB, RoboconSongTrendDTO> implements RoboconSongTrendTransformer{

	public RoboconSongTrendTransformerImpl(){
		super(RoboconSongTrendEB.class, RoboconSongTrendDTO.class);
	}
}
