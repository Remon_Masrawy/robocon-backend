package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconTagDTO;
import com.masrawy.robocon.model.RoboconAlbumTagEB;

@Component("roboconAlbumTagTransformer")
public class RoboconAlbumTagTransformerImpl extends AbstractDTOTransformer<RoboconAlbumTagEB, RoboconTagDTO> implements RoboconAlbumTagTransformer{

	public RoboconAlbumTagTransformerImpl(){
		super(RoboconAlbumTagEB.class, RoboconTagDTO.class);
	}
}
