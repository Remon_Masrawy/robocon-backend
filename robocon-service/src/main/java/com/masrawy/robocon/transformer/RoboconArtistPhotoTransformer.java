package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconPhotoDTO;
import com.masrawy.robocon.model.RoboconArtistPhotoEB;

public interface RoboconArtistPhotoTransformer extends DTOTransformer<RoboconArtistPhotoEB, RoboconPhotoDTO>{

}
