package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconSongLyricsDTO;
import com.masrawy.robocon.model.RoboconSongLyricsEB;

public interface RoboconSongLyricsTransformer extends DTOTransformer<RoboconSongLyricsEB, RoboconSongLyricsDTO>{

}
