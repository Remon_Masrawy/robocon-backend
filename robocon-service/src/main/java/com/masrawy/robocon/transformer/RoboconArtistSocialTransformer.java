package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconArtistSocialDTO;
import com.masrawy.robocon.model.RoboconArtistSocialEB;

public interface RoboconArtistSocialTransformer extends DTOTransformer<RoboconArtistSocialEB, RoboconArtistSocialDTO>{

}
