package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconTagDTO;
import com.masrawy.robocon.model.RoboconArtistTagEB;

@Component("roboconArtistTagTransformer")
public class RoboconArtistTagTransformerImpl extends AbstractDTOTransformer<RoboconArtistTagEB, RoboconTagDTO> implements RoboconArtistTagTransformer{

	public RoboconArtistTagTransformerImpl(){
		super(RoboconArtistTagEB.class, RoboconTagDTO.class);
	}
}
