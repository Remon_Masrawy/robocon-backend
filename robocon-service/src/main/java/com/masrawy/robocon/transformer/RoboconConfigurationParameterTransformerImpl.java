package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.configuration.dto.ConfigParameterDTO;
import com.cdm.core.configuration.model.ConfigParameterEB;
import com.cdm.core.dto.transformer.AbstractDTOTransformer;

@Component("roboconConfigurationParameterTransformer")
public class RoboconConfigurationParameterTransformerImpl extends AbstractDTOTransformer<ConfigParameterEB, ConfigParameterDTO> implements RoboconConfigurationParameterTransformer{

	public RoboconConfigurationParameterTransformerImpl(){
		super(ConfigParameterEB.class, ConfigParameterDTO.class);
	}
}
