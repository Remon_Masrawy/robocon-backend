package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconSongKaraokeDTO;
import com.masrawy.robocon.model.RoboconSongKaraokeEB;

@Component("roboconSongKaraokeTransformer")
public class RoboconSongKaraokeTransformerImpl extends AbstractDTOTransformer<RoboconSongKaraokeEB, RoboconSongKaraokeDTO> implements RoboconSongKaraokeTransformer{

	public RoboconSongKaraokeTransformerImpl(){
		super(RoboconSongKaraokeEB.class, RoboconSongKaraokeDTO.class);
	}
}
