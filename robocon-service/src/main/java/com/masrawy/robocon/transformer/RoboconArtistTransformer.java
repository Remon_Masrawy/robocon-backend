package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconArtistDTO;
import com.masrawy.robocon.model.RoboconArtistEB;

public interface RoboconArtistTransformer extends DTOTransformer<RoboconArtistEB, RoboconArtistDTO>{

}
