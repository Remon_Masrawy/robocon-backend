package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconFrontendDTO;
import com.masrawy.robocon.model.RoboconFrontendEB;

public interface RoboconFrontendTransformer extends DTOTransformer<RoboconFrontendEB, RoboconFrontendDTO>{

}
