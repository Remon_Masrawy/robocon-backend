package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.model.RoboconProviderMetaEB;

public interface RoboconProviderMetaTransformer extends DTOTransformer<RoboconProviderMetaEB, RoboconMetaDTO>{

}
