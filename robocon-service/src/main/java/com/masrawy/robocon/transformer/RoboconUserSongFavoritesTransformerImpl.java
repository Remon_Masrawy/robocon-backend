package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconUserSongFavoriteDTO;
import com.masrawy.robocon.model.RoboconUserSongFavoriteEB;

@Component("roboconUserSongFavoritesTransformer")
public class RoboconUserSongFavoritesTransformerImpl extends AbstractDTOTransformer<RoboconUserSongFavoriteEB, RoboconUserSongFavoriteDTO> implements RoboconUserSongFavoritesTransformer{

	public RoboconUserSongFavoritesTransformerImpl(){
		super(RoboconUserSongFavoriteEB.class, RoboconUserSongFavoriteDTO.class);
	}
}
