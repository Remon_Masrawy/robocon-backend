package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconArtistSocialDTO;
import com.masrawy.robocon.model.RoboconArtistSocialEB;

@Component("roboconArtistSocialTransformer")
public class RoboconArtistSocialTransformerImpl extends AbstractDTOTransformer<RoboconArtistSocialEB, RoboconArtistSocialDTO> implements RoboconArtistSocialTransformer{

	public RoboconArtistSocialTransformerImpl(){
		super(RoboconArtistSocialEB.class, RoboconArtistSocialDTO.class);
	}
}
