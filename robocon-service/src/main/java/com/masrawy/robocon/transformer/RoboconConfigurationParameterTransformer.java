package com.masrawy.robocon.transformer;

import com.cdm.core.configuration.dto.ConfigParameterDTO;
import com.cdm.core.configuration.model.ConfigParameterEB;
import com.cdm.core.dto.transformer.DTOTransformer;

public interface RoboconConfigurationParameterTransformer extends DTOTransformer<ConfigParameterEB, ConfigParameterDTO>{

}
