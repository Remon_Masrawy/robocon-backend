package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconArtistEventDTO;
import com.masrawy.robocon.model.RoboconArtistEventEB;

@Component("roboconArtistEventTransformer")
public class RoboconArtistEventTransformerImpl extends AbstractDTOTransformer<RoboconArtistEventEB, RoboconArtistEventDTO> implements RoboconArtistEventTransformer{

	public RoboconArtistEventTransformerImpl(){
		super(RoboconArtistEventEB.class, RoboconArtistEventDTO.class);
	}
}
