package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconUserDTO;
import com.masrawy.robocon.model.RoboconUserEB;

public interface RoboconUserTransformer extends DTOTransformer<RoboconUserEB, RoboconUserDTO>{

}
