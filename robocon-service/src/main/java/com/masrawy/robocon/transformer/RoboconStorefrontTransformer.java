package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconStorefrontDTO;
import com.masrawy.robocon.model.RoboconStorefrontEB;

public interface RoboconStorefrontTransformer extends DTOTransformer<RoboconStorefrontEB, RoboconStorefrontDTO>{

}
