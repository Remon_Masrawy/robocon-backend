package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconMicroAlbumDTO;
import com.masrawy.robocon.model.RoboconAlbumEB;

@Component("roboconMicroAlbumTransformer")
public class RoboconMicroAlbumTransformerImpl extends AbstractDTOTransformer<RoboconAlbumEB, RoboconMicroAlbumDTO> implements RoboconMicroAlbumTransformer{

	public RoboconMicroAlbumTransformerImpl(){
		super(RoboconAlbumEB.class, RoboconMicroAlbumDTO.class);
	}
}
