package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconProviderDTO;
import com.masrawy.robocon.model.RoboconProviderEB;

@Component("roboconProviderTransformer")
public class RoboconProviderTransformerImpl extends AbstractDTOTransformer<RoboconProviderEB, RoboconProviderDTO> implements RoboconProviderTransformer{

	public RoboconProviderTransformerImpl(){
		super(RoboconProviderEB.class, RoboconProviderDTO.class);
	}
}
