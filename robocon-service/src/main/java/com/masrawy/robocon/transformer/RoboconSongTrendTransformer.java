package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconSongTrendDTO;
import com.masrawy.robocon.model.RoboconSongTrendEB;

public interface RoboconSongTrendTransformer extends DTOTransformer<RoboconSongTrendEB, RoboconSongTrendDTO>{

}
