package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.model.RoboconGenreMetaEB;

@Component("roboconGenreMetaTransformer")
public class RoboconGenreMetaTransformerImpl extends AbstractDTOTransformer<RoboconGenreMetaEB, RoboconMetaDTO> implements RoboconGenreMetaTransformer{

	public RoboconGenreMetaTransformerImpl(){
		super(RoboconGenreMetaEB.class, RoboconMetaDTO.class);
	}
}
