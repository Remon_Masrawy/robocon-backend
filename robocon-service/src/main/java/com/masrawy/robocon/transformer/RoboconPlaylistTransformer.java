package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconPlaylistDTO;
import com.masrawy.robocon.model.RoboconPlaylistEB;

public interface RoboconPlaylistTransformer extends DTOTransformer<RoboconPlaylistEB, RoboconPlaylistDTO>{

}
