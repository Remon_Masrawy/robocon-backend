package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconMobileSongDTO;
import com.masrawy.robocon.model.RoboconSongEB;

@Component("roboconMobileSongTransformer")
public class RoboconMobileSongTransformerImpl extends AbstractDTOTransformer<RoboconSongEB, RoboconMobileSongDTO> implements RoboconMobileSongTransformer{

	public RoboconMobileSongTransformerImpl(){
		super(RoboconSongEB.class, RoboconMobileSongDTO.class);
	}
}
