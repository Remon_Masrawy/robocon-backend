package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconPhotoDTO;
import com.masrawy.robocon.model.AbstractRoboconPhotoEB;

@Component("RoboconPhotoTransformer")
public class RoboconPhotoTransformerImpl extends AbstractDTOTransformer<AbstractRoboconPhotoEB, RoboconPhotoDTO> implements RoboconPhotoTransformer{

	public RoboconPhotoTransformerImpl(){
		super(AbstractRoboconPhotoEB.class, RoboconPhotoDTO.class);
	}
}
