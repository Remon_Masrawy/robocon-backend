package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.CountryDTO;
import com.masrawy.robocon.model.CountryEB;

@Component("countryTransformer")
public class CountryTransformerImpl extends AbstractDTOTransformer<CountryEB, CountryDTO> implements CountryTransformer{

	public CountryTransformerImpl(){
		super(CountryEB.class, CountryDTO.class);
	}
}
