package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconSongLyricsDTO;
import com.masrawy.robocon.model.RoboconSongLyricsEB;

@Component("roboconSongLyricsTransformer")
public class RoboconSongLyricsTransformerImpl extends AbstractDTOTransformer<RoboconSongLyricsEB, RoboconSongLyricsDTO> implements RoboconSongLyricsTransformer{

	public RoboconSongLyricsTransformerImpl(){
		super(RoboconSongLyricsEB.class, RoboconSongLyricsDTO.class);
	}
}
