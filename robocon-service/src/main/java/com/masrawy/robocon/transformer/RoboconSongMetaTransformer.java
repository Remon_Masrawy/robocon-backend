package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.model.RoboconSongMetaEB;

public interface RoboconSongMetaTransformer extends DTOTransformer<RoboconSongMetaEB, RoboconMetaDTO>{

}
