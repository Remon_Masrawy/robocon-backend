package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconPhotoDTO;
import com.masrawy.robocon.model.RoboconArtistPhotoEB;

@Component("roboconArtistPhotoTransformer")
public class RoboconArtistPhotoTransformerImpl extends AbstractDTOTransformer<RoboconArtistPhotoEB, RoboconPhotoDTO> implements RoboconArtistPhotoTransformer{

	public RoboconArtistPhotoTransformerImpl(){
		super(RoboconArtistPhotoEB.class, RoboconPhotoDTO.class);
	}
}
