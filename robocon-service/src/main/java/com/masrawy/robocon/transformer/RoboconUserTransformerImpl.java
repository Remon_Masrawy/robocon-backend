package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconUserDTO;
import com.masrawy.robocon.model.RoboconUserEB;

@Component("roboconUserTransformer")
public class RoboconUserTransformerImpl extends AbstractDTOTransformer<RoboconUserEB, RoboconUserDTO> implements RoboconUserTransformer{

	public RoboconUserTransformerImpl(){
		super(RoboconUserEB.class, RoboconUserDTO.class);
	}
}