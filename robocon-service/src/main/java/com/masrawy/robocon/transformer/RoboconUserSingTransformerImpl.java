package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconUserSingDTO;
import com.masrawy.robocon.model.RoboconUserSingEB;

@Component("roboconUserSingTransformer")
public class RoboconUserSingTransformerImpl extends AbstractDTOTransformer<RoboconUserSingEB, RoboconUserSingDTO> implements RoboconUserSingTransformer{

	public RoboconUserSingTransformerImpl(){
		super(RoboconUserSingEB.class, RoboconUserSingDTO.class);
	}
}
