package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.CountryDTO;
import com.masrawy.robocon.model.CountryEB;

public interface CountryTransformer extends DTOTransformer<CountryEB, CountryDTO>{

}
