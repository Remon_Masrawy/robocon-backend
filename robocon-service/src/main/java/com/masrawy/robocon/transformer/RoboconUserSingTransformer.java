package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconUserSingDTO;
import com.masrawy.robocon.model.RoboconUserSingEB;

public interface RoboconUserSingTransformer extends DTOTransformer<RoboconUserSingEB, RoboconUserSingDTO>{

}
