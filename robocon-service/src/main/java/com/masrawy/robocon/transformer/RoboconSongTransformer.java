package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconSongDTO;
import com.masrawy.robocon.model.RoboconSongEB;

public interface RoboconSongTransformer extends DTOTransformer<RoboconSongEB, RoboconSongDTO>{

}
