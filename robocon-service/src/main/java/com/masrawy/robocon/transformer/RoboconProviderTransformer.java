package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconProviderDTO;
import com.masrawy.robocon.model.RoboconProviderEB;

public interface RoboconProviderTransformer extends DTOTransformer<RoboconProviderEB, RoboconProviderDTO>{

}
