package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconGenreDTO;
import com.masrawy.robocon.model.RoboconGenreEB;

@Component("roboconGenreTransformer")
public class RoboconGenreTransformerImpl extends AbstractDTOTransformer<RoboconGenreEB, RoboconGenreDTO> implements RoboconGenreTransformer{

	public RoboconGenreTransformerImpl(){
		super(RoboconGenreEB.class, RoboconGenreDTO.class);
	}
}
