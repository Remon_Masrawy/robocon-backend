package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconFrontendDTO;
import com.masrawy.robocon.model.RoboconFrontendEB;

@Component("roboconFrontendTransformer")
public class RoboconFrontendTransformerImpl extends AbstractDTOTransformer<RoboconFrontendEB, RoboconFrontendDTO> implements RoboconFrontendTransformer{

	public RoboconFrontendTransformerImpl(){
		super(RoboconFrontendEB.class, RoboconFrontendDTO.class);
	} 
	
}
