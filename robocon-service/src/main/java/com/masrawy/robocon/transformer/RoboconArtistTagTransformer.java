package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconTagDTO;
import com.masrawy.robocon.model.RoboconArtistTagEB;

public interface RoboconArtistTagTransformer extends DTOTransformer<RoboconArtistTagEB, RoboconTagDTO>{

}
