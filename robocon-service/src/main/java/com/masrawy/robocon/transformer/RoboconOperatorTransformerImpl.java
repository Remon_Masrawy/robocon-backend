package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconOperatorDTO;
import com.masrawy.robocon.model.RoboconOperatorEB;

@Component("operatorTransformer")
public class RoboconOperatorTransformerImpl extends AbstractDTOTransformer<RoboconOperatorEB, RoboconOperatorDTO> implements RoboconOperatorTransformer{

	public RoboconOperatorTransformerImpl(){
		super(RoboconOperatorEB.class, RoboconOperatorDTO.class);
	}
}
