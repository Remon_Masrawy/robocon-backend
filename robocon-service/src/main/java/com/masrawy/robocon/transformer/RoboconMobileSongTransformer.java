package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconMobileSongDTO;
import com.masrawy.robocon.model.RoboconSongEB;

public interface RoboconMobileSongTransformer extends DTOTransformer<RoboconSongEB, RoboconMobileSongDTO>{

}
