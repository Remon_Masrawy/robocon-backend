package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconArtistEventMetaDTO;
import com.masrawy.robocon.model.RoboconArtistEventMetaEB;

public interface RoboconArtistEventMetaTransformer extends DTOTransformer<RoboconArtistEventMetaEB, RoboconArtistEventMetaDTO>{

}
