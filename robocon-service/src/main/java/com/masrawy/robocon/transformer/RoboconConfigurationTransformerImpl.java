package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconConfigurationDTO;
import com.masrawy.robocon.model.RoboconConfigurationEB;

@Component("roboconConfigurationTransformer")
public class RoboconConfigurationTransformerImpl extends AbstractDTOTransformer<RoboconConfigurationEB, RoboconConfigurationDTO> implements RoboconConfigurationTransformer{

	public RoboconConfigurationTransformerImpl(){
		super(RoboconConfigurationEB.class, RoboconConfigurationDTO.class);
	}
}
