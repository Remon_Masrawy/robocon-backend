package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.model.RoboconArtistMetaEB;

@Component("roboconArtistMetaTransformer")
public class RoboconArtistMetaTransformerImpl extends AbstractDTOTransformer<RoboconArtistMetaEB, RoboconMetaDTO> implements RoboconArtistMetaTransformer{

	public RoboconArtistMetaTransformerImpl(){
		super(RoboconArtistMetaEB.class, RoboconMetaDTO.class);
	}
}
