package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconOperatorDTO;
import com.masrawy.robocon.model.RoboconOperatorEB;

public interface RoboconOperatorTransformer extends DTOTransformer<RoboconOperatorEB, RoboconOperatorDTO>{

}
