package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconPlaylistDTO;
import com.masrawy.robocon.model.RoboconPlaylistEB;

@Component("roboconPlaylistTransformer")
public class RoboconPlaylistTransformerImpl extends AbstractDTOTransformer<RoboconPlaylistEB, RoboconPlaylistDTO> implements RoboconPlaylistTransformer{

	public RoboconPlaylistTransformerImpl(){
		super(RoboconPlaylistEB.class, RoboconPlaylistDTO.class);
	}
}
