package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.model.RoboconGenreMetaEB;

public interface RoboconGenreMetaTransformer extends DTOTransformer<RoboconGenreMetaEB, RoboconMetaDTO>{

}
