package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconConfigurationDTO;
import com.masrawy.robocon.model.RoboconConfigurationEB;

public interface RoboconConfigurationTransformer extends DTOTransformer<RoboconConfigurationEB, RoboconConfigurationDTO>{

}
