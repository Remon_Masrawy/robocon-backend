package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconTagDTO;
import com.masrawy.robocon.model.RoboconAlbumTagEB;

public interface RoboconAlbumTagTransformer extends DTOTransformer<RoboconAlbumTagEB, RoboconTagDTO>{

}
