package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconUserSongHistoryDTO;
import com.masrawy.robocon.model.RoboconUserSongHistoryEB;

public interface RoboconUserSongHistoryTransformer extends DTOTransformer<RoboconUserSongHistoryEB, RoboconUserSongHistoryDTO>{

}
