package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconCategoryDTO;
import com.masrawy.robocon.model.RoboconCategoryEB;

public interface RoboconCategoryTransformer extends DTOTransformer<RoboconCategoryEB, RoboconCategoryDTO>{

}
