package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconArtistDTO;
import com.masrawy.robocon.model.RoboconArtistEB;

@Component("roboconArtistTransformer")
public class RoboconArtistTransformerImpl extends AbstractDTOTransformer<RoboconArtistEB, RoboconArtistDTO> implements RoboconArtistTransformer{

	public RoboconArtistTransformerImpl(){
		super(RoboconArtistEB.class, RoboconArtistDTO.class);
	}
}
