package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconAlbumDTO;
import com.masrawy.robocon.model.RoboconAlbumEB;

@Component("roboconAlbumTransformer")
public class RoboconAlbumTransformerImpl extends AbstractDTOTransformer<RoboconAlbumEB, RoboconAlbumDTO> implements RoboconAlbumTransformer{

	public RoboconAlbumTransformerImpl(){
		super(RoboconAlbumEB.class, RoboconAlbumDTO.class);
	}
}
