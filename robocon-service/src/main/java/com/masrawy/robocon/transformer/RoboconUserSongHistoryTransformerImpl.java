package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconUserSongHistoryDTO;
import com.masrawy.robocon.model.RoboconUserSongHistoryEB;

@Component("roboconUserSongHistoryTransformer")
public class RoboconUserSongHistoryTransformerImpl extends AbstractDTOTransformer<RoboconUserSongHistoryEB, RoboconUserSongHistoryDTO> implements RoboconUserSongHistoryTransformer{

	public RoboconUserSongHistoryTransformerImpl(){
		super(RoboconUserSongHistoryEB.class, RoboconUserSongHistoryDTO.class);
	}
}
