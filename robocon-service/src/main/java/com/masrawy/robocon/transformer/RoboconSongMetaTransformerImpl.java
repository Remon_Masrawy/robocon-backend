package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.model.RoboconSongMetaEB;

@Component("roboconSongMetaTransformer")
public class RoboconSongMetaTransformerImpl extends AbstractDTOTransformer<RoboconSongMetaEB, RoboconMetaDTO> implements RoboconSongMetaTransformer{

	public RoboconSongMetaTransformerImpl(){
		super(RoboconSongMetaEB.class, RoboconMetaDTO.class);
	}
}
