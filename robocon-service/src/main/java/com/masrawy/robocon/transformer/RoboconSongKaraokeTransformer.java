package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconSongKaraokeDTO;
import com.masrawy.robocon.model.RoboconSongKaraokeEB;

public interface RoboconSongKaraokeTransformer extends DTOTransformer<RoboconSongKaraokeEB, RoboconSongKaraokeDTO>{

}
