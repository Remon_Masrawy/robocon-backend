package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconMicroAlbumDTO;
import com.masrawy.robocon.model.RoboconAlbumEB;

public interface RoboconMicroAlbumTransformer extends DTOTransformer<RoboconAlbumEB, RoboconMicroAlbumDTO>{

}
