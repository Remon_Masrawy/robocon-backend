package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconArtistEventDTO;
import com.masrawy.robocon.model.RoboconArtistEventEB;

public interface RoboconArtistEventTransformer extends DTOTransformer<RoboconArtistEventEB, RoboconArtistEventDTO>{

}
