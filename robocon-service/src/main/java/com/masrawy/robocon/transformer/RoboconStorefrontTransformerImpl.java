package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconStorefrontDTO;
import com.masrawy.robocon.model.RoboconStorefrontEB;

@Component("roboconStorefrontTransformer")
public class RoboconStorefrontTransformerImpl extends AbstractDTOTransformer<RoboconStorefrontEB, RoboconStorefrontDTO> implements RoboconStorefrontTransformer{

	public RoboconStorefrontTransformerImpl(){
		super(RoboconStorefrontEB.class, RoboconStorefrontDTO.class);
	}
}
