package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconArtistEventMetaDTO;
import com.masrawy.robocon.model.RoboconArtistEventMetaEB;

@Component("roboconArtistEventMetaTransformer")
public class RoboconArtistEventMetaTransformerImpl extends AbstractDTOTransformer<RoboconArtistEventMetaEB, RoboconArtistEventMetaDTO> implements RoboconArtistEventMetaTransformer{

	public RoboconArtistEventMetaTransformerImpl(){
		super(RoboconArtistEventMetaEB.class, RoboconArtistEventMetaDTO.class);
	}
}
