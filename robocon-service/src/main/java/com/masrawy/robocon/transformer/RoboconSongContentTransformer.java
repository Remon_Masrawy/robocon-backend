package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconSongContentDTO;
import com.masrawy.robocon.model.RoboconSongContentEB;

public interface RoboconSongContentTransformer extends DTOTransformer<RoboconSongContentEB, RoboconSongContentDTO>{

}
