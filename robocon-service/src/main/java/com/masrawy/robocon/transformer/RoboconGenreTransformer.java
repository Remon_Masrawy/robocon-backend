package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconGenreDTO;
import com.masrawy.robocon.model.RoboconGenreEB;

public interface RoboconGenreTransformer extends DTOTransformer<RoboconGenreEB, RoboconGenreDTO>{

}
