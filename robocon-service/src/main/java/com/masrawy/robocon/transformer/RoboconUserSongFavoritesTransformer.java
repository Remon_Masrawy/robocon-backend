package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconUserSongFavoriteDTO;
import com.masrawy.robocon.model.RoboconUserSongFavoriteEB;

public interface RoboconUserSongFavoritesTransformer extends DTOTransformer<RoboconUserSongFavoriteEB, RoboconUserSongFavoriteDTO>{

}
