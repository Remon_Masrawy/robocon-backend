package com.masrawy.robocon.transformer;

import com.cdm.core.dto.transformer.DTOTransformer;
import com.masrawy.robocon.dto.RoboconAlbumDTO;
import com.masrawy.robocon.model.RoboconAlbumEB;

public interface RoboconAlbumTransformer extends DTOTransformer<RoboconAlbumEB, RoboconAlbumDTO>{

}
