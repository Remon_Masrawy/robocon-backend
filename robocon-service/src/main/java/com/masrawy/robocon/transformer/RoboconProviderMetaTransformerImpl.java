package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.model.RoboconProviderMetaEB;

@Component("roboconProviderMetaTransformer")
public class RoboconProviderMetaTransformerImpl extends AbstractDTOTransformer<RoboconProviderMetaEB, RoboconMetaDTO> implements RoboconProviderMetaTransformer{

	public RoboconProviderMetaTransformerImpl(){
		super(RoboconProviderMetaEB.class, RoboconMetaDTO.class);
	}
}
