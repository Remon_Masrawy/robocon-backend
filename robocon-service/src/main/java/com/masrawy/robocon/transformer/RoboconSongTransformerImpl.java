package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconSongDTO;
import com.masrawy.robocon.model.RoboconSongEB;

@Component("roboconSongTransformer")
public class RoboconSongTransformerImpl extends AbstractDTOTransformer<RoboconSongEB, RoboconSongDTO> implements RoboconSongTransformer{

	public RoboconSongTransformerImpl(){
		super(RoboconSongEB.class, RoboconSongDTO.class);
	}
	
}
