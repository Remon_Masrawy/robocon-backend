package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.model.RoboconAlbumMetaEB;

@Component("roboconAlbumMetaTransformer")
public class RoboconAlbumMetaTransformerImpl extends AbstractDTOTransformer<RoboconAlbumMetaEB, RoboconMetaDTO> implements RoboconAlbumMetaTransformer{

	public RoboconAlbumMetaTransformerImpl(){
		super(RoboconAlbumMetaEB.class, RoboconMetaDTO.class);
	}
}
