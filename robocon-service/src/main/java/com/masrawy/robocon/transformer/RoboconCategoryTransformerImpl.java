package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconCategoryDTO;
import com.masrawy.robocon.model.RoboconCategoryEB;

@Component("roboconCategoryTransformer")
public class RoboconCategoryTransformerImpl extends AbstractDTOTransformer<RoboconCategoryEB, RoboconCategoryDTO> implements RoboconCategoryTransformer{

	public RoboconCategoryTransformerImpl(){
		super(RoboconCategoryEB.class, RoboconCategoryDTO.class);
	}
}
