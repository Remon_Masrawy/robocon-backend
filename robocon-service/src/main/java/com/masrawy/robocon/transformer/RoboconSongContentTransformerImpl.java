package com.masrawy.robocon.transformer;

import org.springframework.stereotype.Component;

import com.cdm.core.dto.transformer.AbstractDTOTransformer;
import com.masrawy.robocon.dto.RoboconSongContentDTO;
import com.masrawy.robocon.model.RoboconSongContentEB;

@Component("roboconSongContentTransformer")
public class RoboconSongContentTransformerImpl extends AbstractDTOTransformer<RoboconSongContentEB, RoboconSongContentDTO> implements RoboconSongContentTransformer{

	public RoboconSongContentTransformerImpl(){
		super(RoboconSongContentEB.class, RoboconSongContentDTO.class);
	}
}
