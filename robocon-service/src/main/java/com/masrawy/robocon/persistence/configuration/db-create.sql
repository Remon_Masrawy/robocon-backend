--bbky
DROP DATABASE robocon;
CREATE DATABASE robocon WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_GB.UTF-8' LC_CTYPE = 'en_GB.UTF-8';
ALTER DATABASE robocon OWNER TO postgres;
-- \connect robocontest;
CREATE SCHEMA audit AUTHORIZATION postgres;