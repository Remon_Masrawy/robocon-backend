package com.masrawy.robocon.converter;

import java.util.ArrayList;
import java.util.List;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.model.RoboconAlbumEB;
import com.masrawy.robocon.model.RoboconSongEB;

@Component("roboconSongsAlbumsCounterConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconSongsAlbumsCounterConverter extends DozerConverter<List<RoboconSongEB>, Integer> implements CustomConverter{

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public RoboconSongsAlbumsCounterConverter(){
		super((Class<List<RoboconSongEB>>) (Class) List.class, Integer.class);
	}

	@Override
	public Integer convertTo(List<RoboconSongEB> songs, Integer counter) {
		List<RoboconAlbumEB> albums = new ArrayList<RoboconAlbumEB>();
		if(songs != null){
			for(RoboconSongEB song : songs){
				if(!albums.contains(song.getRoboconAlbumEB())){
					albums.add(song.getRoboconAlbumEB());
				}
			}
			return albums.size();
		}
		return 0;
	}

	@Override
	public List<RoboconSongEB> convertFrom(Integer source, List<RoboconSongEB> destination) {
		return null;
	}
}
