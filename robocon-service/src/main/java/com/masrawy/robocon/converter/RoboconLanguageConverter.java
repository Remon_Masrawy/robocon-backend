package com.masrawy.robocon.converter;

import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.cdm.core.security.dao.LanguageDAO;
import com.cdm.core.security.model.LanguageEB;
import com.masrawy.robocon.services.exception.RoboconInvalidLanguageException;

@Component("roboconLanguageConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconLanguageConverter extends DozerConverter<String, LanguageEB>{

	@Autowired
	private LanguageDAO languageDAO;
	
	public RoboconLanguageConverter(){
		super(String.class, LanguageEB.class);
	}

	@Override
	public LanguageEB convertTo(String source, LanguageEB destination) {
		if(source == null){
			return null;
		}
		LanguageEB languageEB = languageDAO.getLanguageByCodeIfExists(source);
		if(languageEB == null){
			String msg = "Language "+ source +" not supported exception";
			throw new RoboconInvalidLanguageException(msg);
		}
		return languageEB;
	}

	@Override
	public String convertFrom(LanguageEB source, String destination) {
		if(source == null){
			return null;
		}
		return source.getCode();
	}
}
