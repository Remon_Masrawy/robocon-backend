package com.masrawy.robocon.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dao.OperatorDAO;
import com.masrawy.robocon.model.RoboconOperatorEB;

@Component("roboconOperatorsToMapConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconOperatorToMapConverter extends DozerConverter<List<RoboconOperatorEB>, Map<Long, Map<String, String>>> implements CustomConverter{

	@Autowired
	private OperatorDAO operatorDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconOperatorToMapConverter(){
		super((Class<List<RoboconOperatorEB>>) (Class) List.class, (Class<Map<Long, Map<String, String>>>) (Class) Map.class);
	}

	@Override
	public Map<Long, Map<String, String>> convertTo(List<RoboconOperatorEB> operators, Map<Long, Map<String, String>> map) {
		map = new HashMap<Long, Map<String, String>>();
		if(operators != null){
			for(RoboconOperatorEB operator : operators){
				Map<String, String> info = new HashMap<String, String>();
				info.put("name", operator.getName());
				info.put("country", operator.getCountryEB().getName());
				map.put(operator.pk(), info);
			}
			
		}
		return map;
	}

	@Override
	public List<RoboconOperatorEB> convertFrom(Map<Long, Map<String, String>> map, List<RoboconOperatorEB> operators) {
		operators = new ArrayList<RoboconOperatorEB>();
		if(map != null){
			for(Long key : map.keySet()){
				operators.add(operatorDAO.getEntityByID(key));
			}
			return operators;
		}
		return null;
	}

}
