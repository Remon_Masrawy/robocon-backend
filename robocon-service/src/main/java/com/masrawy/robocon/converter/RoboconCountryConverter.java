package com.masrawy.robocon.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dto.CountryDTO;
import com.masrawy.robocon.model.CountryEB;
import com.masrawy.robocon.transformer.CountryTransformer;

@Component("roboconCountryConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconCountryConverter extends AbstractRoboconConverter<CountryDTO, CountryEB>{

	@Autowired
	private CountryTransformer countryTransformer;
	
	public RoboconCountryConverter(){
		super(CountryDTO.class, CountryEB.class);
	}

	@Override
	public CountryEB convertTo(CountryDTO source, CountryEB destination) {
		if(source == null){
			return null;
		}
		if(source.pk() == null){
			return null;
		}
		destination = new CountryEB();
		destination.setId(source.pk());
		return destination;
	}

	@Override
	public CountryDTO convertFrom(CountryEB source, CountryDTO destination) {
		return countryTransformer.transformEntityToDTO(source);
	}
}
