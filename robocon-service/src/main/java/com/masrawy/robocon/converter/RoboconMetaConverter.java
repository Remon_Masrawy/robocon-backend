package com.masrawy.robocon.converter;

import java.util.List;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.masrawy.robocon.model.AbstractRoboconMetaEB;

@SuppressWarnings("rawtypes")
@Component("roboconMetaConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconMetaConverter extends DozerConverter<List, String> implements CustomConverter{

	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	public RoboconMetaConverter(){
		super(List.class, String.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public String convertTo(List meta, String name) {
		String userLang = ControlFieldsHelper.getUserLang();
		if(meta != null && !meta.isEmpty()){
			if(userLang != null && !userLang.isEmpty()){
				for(AbstractRoboconMetaEB metaItem : (List<AbstractRoboconMetaEB>) meta){
					if(metaItem.getLang().getCode().equalsIgnoreCase(userLang)){
						return metaItem.getName();
					}
				}
			}
			if(defaultLangCode != null && !defaultLangCode.isEmpty()){
				for(AbstractRoboconMetaEB metaItem : (List<AbstractRoboconMetaEB>) meta){
					if(metaItem.getLang().getCode().equalsIgnoreCase(defaultLangCode)){
						return metaItem.getName();
					}
				}
			}
			return ((List<AbstractRoboconMetaEB>) meta).get(0).getName();
		}
		return null;
	}

	@Override
	public List convertFrom(String source, List destination) {
		return null;
	}
}
