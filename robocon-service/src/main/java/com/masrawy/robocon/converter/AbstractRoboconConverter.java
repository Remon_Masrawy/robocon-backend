package com.masrawy.robocon.converter;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;

import com.cdm.core.dto.model.DTO;
import com.cdm.core.persistent.model.Entity;

public abstract class AbstractRoboconConverter<D extends DTO<?>, E extends Entity<?>> extends DozerConverter<D, E> implements CustomConverter{

	public AbstractRoboconConverter(Class<D> prototypeA, Class<E> prototypeB) {
		super(prototypeA, prototypeB);
	}

}
