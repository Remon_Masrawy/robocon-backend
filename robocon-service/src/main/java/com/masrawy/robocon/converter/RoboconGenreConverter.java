package com.masrawy.robocon.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dto.RoboconGenreDTO;
import com.masrawy.robocon.model.RoboconGenreEB;
import com.masrawy.robocon.transformer.RoboconGenreTransformer;

@Component("roboconGenreConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconGenreConverter extends AbstractRoboconConverter<RoboconGenreDTO, RoboconGenreEB>{

	@Autowired
	private RoboconGenreTransformer roboconGenreTransformer;
	
	public RoboconGenreConverter(){
		super(RoboconGenreDTO.class, RoboconGenreEB.class);
	}

	@Override
	public RoboconGenreEB convertTo(RoboconGenreDTO source, RoboconGenreEB destination) {
		if(source == null){
			return null;
		}
		if(source.pk() == null){
			return null;
		}
		destination = new RoboconGenreEB();
		destination.setId(source.pk());
		return destination;
	}

	@Override
	public RoboconGenreDTO convertFrom(RoboconGenreEB source, RoboconGenreDTO destination) {
		return roboconGenreTransformer.transformEntityToDTO(source);
	}
}
