package com.masrawy.robocon.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dao.CountryDAO;
import com.masrawy.robocon.model.CountryEB;

@Component("roboconCountriesToMapConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconCountryToMapConverter extends DozerConverter<List<CountryEB>, Map<Long, String>> implements CustomConverter{

	@Autowired
	private CountryDAO countryDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconCountryToMapConverter(){
		super((Class<List<CountryEB>>) (Class) List.class, (Class<Map<Long, String>>) (Class) Map.class);
	}

	@Override
	public Map<Long, String> convertTo(List<CountryEB> countries, Map<Long, String> map) {
		map = new HashMap<Long, String>();
		if(countries != null){
			for(CountryEB country : countries){
				map.put(country.pk(), country.getName());
			}
			
		}
		return map;
	}

	@Override
	public List<CountryEB> convertFrom(Map<Long, String> map, List<CountryEB> countries) {
		countries = new ArrayList<CountryEB>();
		if(map != null){
			for(Long key : map.keySet()){
				countries.add(countryDAO.getEntityByID(key));
			}
			return countries;
		}
		return null;
	}

}
