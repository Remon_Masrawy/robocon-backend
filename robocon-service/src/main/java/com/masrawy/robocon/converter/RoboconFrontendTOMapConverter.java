package com.masrawy.robocon.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dao.RoboconFrontendDAO;
import com.masrawy.robocon.model.RoboconFrontendEB;
import com.masrawy.robocon.model.RoboconFrontendMetaEB;

@Component("roboconFrontendsToMapConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconFrontendTOMapConverter extends DozerConverter<List<RoboconFrontendEB>, Map<Long, String>> implements CustomConverter{

	@Autowired
	private RoboconFrontendDAO roboconFrontendDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconFrontendTOMapConverter(){
		super((Class<List<RoboconFrontendEB>>) (Class) List.class, (Class<Map<Long, String>>) (Class) Map.class);
	}

	@Override
	public Map<Long, String> convertTo(List<RoboconFrontendEB> frontends, Map<Long, String> map) {
		map = new HashMap<Long , String>();
		if(frontends != null){
			for(RoboconFrontendEB frontend : frontends){
				if(frontend.getRoboconFrontendMetaEB() != null){
					RoboconFrontendMetaEB meta = frontend.getRoboconFrontendMetaEB().get(0);
					map.put(frontend.pk(), meta.getName());
				}
			}
		}
		return map;
	}

	@Override
	public List<RoboconFrontendEB> convertFrom(Map<Long, String> map, List<RoboconFrontendEB> frontends) {
		frontends = new ArrayList<RoboconFrontendEB>();
		if(map != null){
			for(Long id : map.keySet()){
				frontends.add(roboconFrontendDAO.getEntityByID(id));
			}
			return frontends;
		}
		return null;
	}

}
