package com.masrawy.robocon.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dao.RoboconGenreDAO;
import com.masrawy.robocon.model.RoboconGenreEB;

@Component("roboconGenreToEntryConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconGenreToEntryConverter extends DozerConverter<List<RoboconGenreEB>, Map<Long, String>> implements CustomConverter{

	@Autowired
	private RoboconGenreDAO roboconGenreDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconGenreToEntryConverter(){
		super( (Class<List<RoboconGenreEB>>) (Class) RoboconGenreEB.class, (Class<Map<Long, String>>) (Class) Map.class);
	}

	@Override
	public Map<Long, String> convertTo(List<RoboconGenreEB> genres, Map<Long, String> map) {
		map = new HashMap<Long, String>();
		if(genres != null){
			String name = null;
			for(RoboconGenreEB genre : genres){
				if(genre.getRoboconGenreMetaEB() != null && !genre.getRoboconGenreMetaEB().isEmpty()){
					name = genre.getRoboconGenreMetaEB().get(0).getName();
				}
				map.put(genre.pk(), name);
			}
		}
		return map;
	}

	@Override
	public List<RoboconGenreEB> convertFrom(Map<Long, String> map, List<RoboconGenreEB> genres) {
		genres = new ArrayList<RoboconGenreEB>();
		if(map != null){
			for(Long key : map.keySet()){
				genres.add(roboconGenreDAO.getEntityByID(key));
			}
			return genres;
		}
		return null;
	}
}
