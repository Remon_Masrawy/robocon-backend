package com.masrawy.robocon.converter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.cdm.core.security.dao.RoleDAO;
import com.cdm.core.security.model.RoleEB;

@Component("roboconUserRolesConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconUserRolesConverter extends DozerConverter<Set<RoleEB>, List<String>>{

	@Autowired
	private RoleDAO RoleDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconUserRolesConverter(){
		super((Class<Set<RoleEB>>)(Class)Set.class, (Class<List<String>>)(Class)List.class);
	}

	@Override
	public List<String> convertTo(Set<RoleEB> source, List<String> destination) {
		if(source != null){
			destination = new ArrayList<String>();
			for(RoleEB role : source){
				destination.add(role.getCode());
			}
		}
		return destination;
	}

	@Override
	public Set<RoleEB> convertFrom(List<String> source, Set<RoleEB> destination) {
		if(source != null){
			destination = new HashSet<RoleEB>();
			for(String role : source){
				destination.add(RoleDAO.getEntityByID(role));
			}
		}
		return destination;
	}
}
