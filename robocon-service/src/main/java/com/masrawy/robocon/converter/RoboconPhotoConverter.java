package com.masrawy.robocon.converter;

import java.text.MessageFormat;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("roboconPhotoConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconPhotoConverter extends DozerConverter<String, String> implements CustomConverter{

	@Value("${robocon-server.image.url}")
	private String roboconServerImageURL;
	
	private final String WIDTH_PLACEHOLDER = "{width}";
	private final String HEIGHT_PLACEHOLDER = "{height}";
	
	public RoboconPhotoConverter(){
		super(String.class, String.class);
	}

	@Override
	public String convertTo(String source, String destination) {
		if(source != null){
			return MessageFormat.format(roboconServerImageURL, source);
		}
		return null;
	}

	@Override
	public String convertFrom(String source, String destination) {
		if(source != null){
			return MessageFormat.format(roboconServerImageURL, source, WIDTH_PLACEHOLDER, HEIGHT_PLACEHOLDER);
		}
		return null;
	}
}
