package com.masrawy.robocon.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dao.RoboconArtistDAO;
import com.masrawy.robocon.model.RoboconArtistEB;

@Component("roboconArtistsConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconArtistsConverter extends DozerConverter<List<RoboconArtistEB>, Map<Long, String>> implements CustomConverter{

	@Autowired
	private RoboconArtistDAO roboconArtistDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconArtistsConverter(){
		super((Class<List<RoboconArtistEB>>) (Class) List.class,(Class<Map<Long, String>>) (Class) Map.class);
	}

	@Override
	public Map<Long, String> convertTo(List<RoboconArtistEB> artists, Map<Long, String> map) {
		map = new HashMap<Long, String>();
		if(artists != null && !artists.isEmpty()){
			for(RoboconArtistEB artist : artists){
				String name = null;
				if(artist.getRoboconArtistMetaEB() != null && !artist.getRoboconArtistMetaEB().isEmpty()){
					name = artist.getRoboconArtistMetaEB().get(0).getName();
				}
				map.put(artist.pk(), name);
			}
		}
		return map;
	}

	@Override
	public List<RoboconArtistEB> convertFrom(Map<Long, String> map, List<RoboconArtistEB> artists) {
		artists = new ArrayList<RoboconArtistEB>();
		if(map != null && !map.isEmpty()){
			for(long artistId : map.keySet()){
				artists.add(roboconArtistDAO.getEntityByID(artistId));
			}
		}
		return artists;
	}
}
