package com.masrawy.robocon.converter;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dao.RoboconArtistEventDAO;
import com.masrawy.robocon.model.RoboconArtistEventEB;
import com.masrawy.robocon.model.RoboconArtistEventPhotoEB;

@Component("roboconEventsConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconEventsConverter extends DozerConverter<List<RoboconArtistEventEB>, Map<Long, Map<String, String>>> implements CustomConverter{

	@Value("${robocon-server.image.url}")
	private String roboconServerImageURL;
	
	private final String WIDTH_PLACEHOLDER = "{width}";
	private final String HEIGHT_PLACEHOLDER = "{height}";
	
	@Autowired
	private RoboconArtistEventDAO roboconArtistEventDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconEventsConverter(){
		super((Class<List<RoboconArtistEventEB>>) (Class) List.class,(Class<Map<Long, Map<String, String>>>) (Class) Map.class);
	}

	@Override
	public Map<Long, Map<String, String>> convertTo(List<RoboconArtistEventEB> events, Map<Long, Map<String, String>> map) {
		map = new HashMap<Long, Map<String, String>>();
		if(events != null && !events.isEmpty()){
			for(RoboconArtistEventEB event : events){
				Map<String, String> info = new HashMap<String, String>();
				String name = null;
				String checksum = null;
				String url = null;
				if(event.getRoboconArtistEventMetaEB() != null && !event.getRoboconArtistEventMetaEB().isEmpty()){
					name = event.getRoboconArtistEventMetaEB().get(0).getName();
				}
				if(event.getRoboconArtistEventPhotosEB() != null && !event.getRoboconArtistEventPhotosEB().isEmpty()){
					for(RoboconArtistEventPhotoEB photo : event.getRoboconArtistEventPhotosEB()){
						if(photo.getIsMainPhoto()){
							checksum = photo.getFile().getChecksum();
						}
					}
					if(checksum == null){
						checksum = event.getRoboconArtistEventPhotosEB().get(0).getFile().getChecksum();
					}
				}
				if(checksum != null){
					url = MessageFormat.format(roboconServerImageURL, checksum, WIDTH_PLACEHOLDER, HEIGHT_PLACEHOLDER);
				}
				info.put("name", name);
				info.put("url", url);
				map.put(event.pk(), info);
			}
		}
		return map;
	}

	@Override
	public List<RoboconArtistEventEB> convertFrom(Map<Long, Map<String, String>> map, List<RoboconArtistEventEB> events) {
		events = new ArrayList<RoboconArtistEventEB>();
		if(map != null && !map.isEmpty()){
			for(long artistId : map.keySet()){
				events.add(roboconArtistEventDAO.getEntityByID(artistId));
			}
		}
		return events;
	}
}
