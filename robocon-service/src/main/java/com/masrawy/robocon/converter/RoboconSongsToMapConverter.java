package com.masrawy.robocon.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dao.RoboconSongDAO;
import com.masrawy.robocon.model.RoboconSongEB;
import com.masrawy.robocon.model.RoboconSongMetaEB;

@Component("roboconSongsToMapConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconSongsToMapConverter extends DozerConverter<List<RoboconSongEB>, Map<Long, String>> implements CustomConverter{

	@Autowired
	private RoboconSongDAO roboconSongDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconSongsToMapConverter(){
		super((Class<List<RoboconSongEB>>) (Class) List.class, (Class<Map<Long, String>>) (Class) Map.class);
	}

	@Override
	public Map<Long, String> convertTo(List<RoboconSongEB> songs, Map<Long, String> map) {
		map = new HashMap<Long , String>();
		if(songs != null){
			for(RoboconSongEB song : songs){
				if(song.getRoboconSongMetaEB() != null){
					RoboconSongMetaEB meta = song.getRoboconSongMetaEB().get(0);
					map.put(song.pk(), meta.getName());
				}
			}
		}
		return map;
	}

	@Override
	public List<RoboconSongEB> convertFrom(Map<Long, String> map, List<RoboconSongEB> songs) {
		songs = new ArrayList<RoboconSongEB>();
		if(map != null){
			for(Long id : map.keySet()){
				songs.add(roboconSongDAO.getEntityByID(id));
			}
			return songs;
		}
		return null;
	}

}
