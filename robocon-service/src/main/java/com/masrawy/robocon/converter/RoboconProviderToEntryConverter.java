package com.masrawy.robocon.converter;

import java.util.HashMap;
import java.util.Map;
import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.masrawy.robocon.dao.RoboconProviderDAO;
import com.masrawy.robocon.model.RoboconProviderEB;

@Component("roboconProviderToEntryConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconProviderToEntryConverter extends DozerConverter<RoboconProviderEB, Map<Long, String>> implements CustomConverter{

	@Autowired
	private RoboconProviderDAO roboconProviderDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconProviderToEntryConverter(){
		super(RoboconProviderEB.class, (Class<Map<Long, String>>) (Class) Map.class);
	}

	@Override
	public Map<Long, String> convertTo(RoboconProviderEB provider, Map<Long, String> map) {
		map = new HashMap<Long, String>();
		if(provider != null){
			String name = null;
			if(provider.getRoboconProviderMetaEB() != null && !provider.getRoboconProviderMetaEB().isEmpty()){
				name = provider.getRoboconProviderMetaEB().get(0).getName();
			}
			map.put(provider.pk(), name);
		}
		return map;
	}

	@Override
	public RoboconProviderEB convertFrom(Map<Long, String> map, RoboconProviderEB album) {
		if(map != null && map.size() == 1){
			Long id = null;
			for(Long key : map.keySet()){
				id = key;
			}
			return roboconProviderDAO.getEntityByID(id);
		}
		return null;
	}
}
