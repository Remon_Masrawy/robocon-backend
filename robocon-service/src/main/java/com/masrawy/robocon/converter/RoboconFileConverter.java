package com.masrawy.robocon.converter;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cdm.core.file.dao.FileDAO;
import com.cdm.core.file.model.FileEB;

@Component("roboconFileConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconFileConverter extends DozerConverter<FileEB, String> {

	@Autowired
	private FileDAO fileDAO;
	
	@Value("${robocon-server.image.url}")
	private String roboconServerImageURL;
	
	private final String WIDTH_PLACEHOLDER = "{width}";
	private final String HEIGHT_PLACEHOLDER = "{height}";
	
	public RoboconFileConverter() {
		super(FileEB.class, String.class);
	}

	@Override
	public String convertTo(FileEB file, String destination) {
		if (file != null) {
			return MessageFormat.format(roboconServerImageURL, file.getChecksum(), WIDTH_PLACEHOLDER, HEIGHT_PLACEHOLDER);
		}
		return null;
	}

	@Override
	public FileEB convertFrom(String source, FileEB destination) {
		if (StringUtils.isNotEmpty(source)) {
			return fileDAO.getEntityByID(source);
		}
		return null;
	}

}
