package com.masrawy.robocon.converter;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dto.RoboconProviderDTO;
import com.masrawy.robocon.model.RoboconProviderEB;
import com.masrawy.robocon.transformer.RoboconProviderTransformer;

@Component("roboconProviderConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconProviderConverter extends AbstractRoboconConverter<RoboconProviderDTO, RoboconProviderEB>{

	public RoboconProviderConverter(){
		super(RoboconProviderDTO.class, RoboconProviderEB.class);
	}

	@Autowired
	private RoboconProviderTransformer roboconProviderTransformer;
	
	/**
	 * 
	 * Robocon Provider Converter {@link RoboconProviderConverter} for converting DTO Provider ID to EB Provider
	 * @param	RoboconProviderDTO
	 * @param	RoboconProviderEB
	 * 
	 */
	
	@Override
	public RoboconProviderEB convertTo(RoboconProviderDTO roboconProviderDTO, RoboconProviderEB roboconProviderEB) {
		if(roboconProviderDTO == null){
			return null;
		}
		if(roboconProviderDTO.pk() == null){
			return null;
		}
		roboconProviderEB = new RoboconProviderEB();
		roboconProviderEB.setId(roboconProviderDTO.pk());
		return roboconProviderEB;
	}

	@Override
	public RoboconProviderDTO convertFrom(RoboconProviderEB source, RoboconProviderDTO destination) {
		return roboconProviderTransformer.transformEntityToDTO(source);
	}
}
