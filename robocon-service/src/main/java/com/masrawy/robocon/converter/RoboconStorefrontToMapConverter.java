package com.masrawy.robocon.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dao.RoboconStorefrontDAO;
import com.masrawy.robocon.model.RoboconStorefrontEB;

@Component("roboconStorefrontsToMapConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconStorefrontToMapConverter extends DozerConverter<List<RoboconStorefrontEB>, Map<Long, String>> implements CustomConverter{

	@Autowired
	private RoboconStorefrontDAO roboconStorefrontDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconStorefrontToMapConverter(){
		super((Class<List<RoboconStorefrontEB>>) (Class) List.class, (Class<Map<Long, String>>) (Class) Map.class);
	}

	@Override
	public Map<Long, String> convertTo(List<RoboconStorefrontEB> storefronts, Map<Long, String> map) {
		map = new HashMap<Long, String>();
		if(storefronts != null){
			for(RoboconStorefrontEB storefront : storefronts){
				map.put(storefront.pk(), storefront.getName());
			}
		}
		return map;
	}

	@Override
	public List<RoboconStorefrontEB> convertFrom(Map<Long, String> map, List<RoboconStorefrontEB> storefronts) {
		storefronts = new ArrayList<RoboconStorefrontEB>();
		if(map != null){
			for(Long key : map.keySet()){
				storefronts.add(roboconStorefrontDAO.getEntityByID(key));
			}
			return storefronts;
		}
		return null;
	}

}
