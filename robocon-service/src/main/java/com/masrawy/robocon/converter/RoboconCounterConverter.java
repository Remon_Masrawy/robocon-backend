package com.masrawy.robocon.converter;

import java.util.List;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@SuppressWarnings("rawtypes")
@Component("roboconCounterConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconCounterConverter extends DozerConverter<List, Integer> implements CustomConverter{

	public RoboconCounterConverter(){
		super(List.class, Integer.class);
	}

	@Override
	public Integer convertTo(List source, Integer destination) {
		if(source != null){
			return source.size();
		}
		return 0;
	}

	@Override
	public List convertFrom(Integer source, List destination) {
		return null;
	}
}
