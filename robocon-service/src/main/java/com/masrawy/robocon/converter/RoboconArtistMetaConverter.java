package com.masrawy.robocon.converter;

import java.util.List;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.masrawy.robocon.model.RoboconArtistEB;
import com.masrawy.robocon.model.RoboconArtistMetaEB;

@SuppressWarnings("rawtypes")
@Component("roboconArtistMetaConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconArtistMetaConverter extends DozerConverter<List, String> implements CustomConverter{

	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	public RoboconArtistMetaConverter(){
		super(List.class, String.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public String convertTo(List artists, String name) {
		name = null;
		boolean isFound;
		String userLang = ControlFieldsHelper.getUserLang();
		if(artists != null && !artists.isEmpty()){
			for(RoboconArtistEB artist : (List<RoboconArtistEB>) artists){
				isFound = false;
				if(artist.getRoboconArtistMetaEB() != null && !artist.getRoboconArtistMetaEB().isEmpty()){
					if(userLang != null && !userLang.isEmpty()){
						for(RoboconArtistMetaEB meta : artist.getRoboconArtistMetaEB()){
							if(meta.getLang().getCode().equalsIgnoreCase(userLang)){
								if(name == null){
									name = meta.getName();
								}else{
									name += ", ";
									name += meta.getName();
								}
								isFound = true;
								break;
							}
						}
					}
					if(!isFound && defaultLangCode != null && !defaultLangCode.isEmpty()){
						for(RoboconArtistMetaEB meta : artist.getRoboconArtistMetaEB()){
							if(meta.getLang().getCode().equalsIgnoreCase(defaultLangCode)){
								if(name == null){
									name = meta.getName();
								}else{
									name += ", ";
									name += meta.getName();
								}
								isFound = true;
								break;
							}
						}
					}
					if(!isFound){
						if(name == null){
							name = ((List<RoboconArtistEB>) artists).get(0).getRoboconArtistMetaEB().get(0).getName();
						}else{
							name += ", ";
							name += ((List<RoboconArtistEB>) artists).get(0).getRoboconArtistMetaEB().get(0).getName();
						}
					}
				}
			}
		}
		return name;
	}

	@Override
	public List<RoboconArtistEB> convertFrom(String source, List destination) {
		return null;
	}
	
}
