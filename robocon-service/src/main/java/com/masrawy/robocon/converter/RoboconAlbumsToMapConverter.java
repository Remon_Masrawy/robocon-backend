package com.masrawy.robocon.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dao.RoboconAlbumDAO;
import com.masrawy.robocon.model.RoboconAlbumEB;
import com.masrawy.robocon.model.RoboconAlbumMetaEB;

@Component("roboconAlbumsToMapConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconAlbumsToMapConverter extends DozerConverter<List<RoboconAlbumEB>, Map<Long, String>> implements CustomConverter{

	@Autowired
	private RoboconAlbumDAO roboconAlbumDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconAlbumsToMapConverter(){
		super((Class<List<RoboconAlbumEB>>) (Class) List.class, (Class<Map<Long, String>>) (Class) Map.class);
	}

	@Override
	public Map<Long, String> convertTo(List<RoboconAlbumEB> albums, Map<Long, String> map) {
		map = new HashMap<Long , String>();
		if(albums != null){
			for(RoboconAlbumEB album : albums){
				if(album.getRoboconAlbumMetaEB() != null){
					RoboconAlbumMetaEB meta = album.getRoboconAlbumMetaEB().get(0);
					map.put(album.pk(), meta.getName());
				}
			}
		}
		return map;
	}

	@Override
	public List<RoboconAlbumEB> convertFrom(Map<Long, String> map, List<RoboconAlbumEB> albums) {
		albums = new ArrayList<RoboconAlbumEB>();
		if(map != null){
			for(Long id : map.keySet()){
				albums.add(roboconAlbumDAO.getEntityByID(id));
			}
			return albums;
		}
		return null;
	}

}
