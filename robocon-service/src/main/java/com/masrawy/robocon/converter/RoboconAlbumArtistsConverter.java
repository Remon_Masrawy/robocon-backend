package com.masrawy.robocon.converter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.model.RoboconArtistEB;
import com.masrawy.robocon.model.RoboconArtistMetaEB;
import com.masrawy.robocon.model.RoboconSongEB;

@Component("roboconAlbumArtistsConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconAlbumArtistsConverter extends DozerConverter<List<RoboconSongEB>, Map<Long, String>> implements CustomConverter{

	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconAlbumArtistsConverter(){
		super((Class<List<RoboconSongEB>>) (Class) List.class, (Class<Map<Long, String>>) ((Class) Map.class));
	}
	
	public RoboconAlbumArtistsConverter(Class<List<RoboconSongEB>> prototypeA, Class<Map<Long, String>> prototypeB) {
		super(prototypeA, prototypeB);
	}

	@Override
	public Map<Long, String> convertTo(List<RoboconSongEB> songs, Map<Long, String> destination) {
		destination = new HashMap<Long, String>();
		if(songs != null){
			for(RoboconSongEB song : songs){
				if(!song.getRoboconArtistsEB().isEmpty()){
					for(RoboconArtistEB artist : song.getRoboconArtistsEB()){
						if(artist.getRoboconArtistMetaEB().isEmpty()){
							if(!destination.containsKey(artist.pk())){
								destination.put(artist.pk(), "Artist");
							}
						}else{
							for(RoboconArtistMetaEB meta : artist.getRoboconArtistMetaEB()){
								if(!destination.containsKey(artist.pk())){
									destination.put(artist.pk(), meta.getName());
								}
							}
						}
					}
				}
			}
		}
		return destination;
	}

	@Override
	public List<RoboconSongEB> convertFrom(Map<Long, String> source, List<RoboconSongEB> destination) {
		return null;
	}

	
}
