package com.masrawy.robocon.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dto.RoboconAlbumDTO;
import com.masrawy.robocon.model.RoboconAlbumEB;
import com.masrawy.robocon.transformer.RoboconAlbumTransformer;

@Component("roboconAlbumConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconAlbumConverter extends AbstractRoboconConverter<RoboconAlbumDTO, RoboconAlbumEB>{

	@Autowired
	private RoboconAlbumTransformer roboconAlbumTransformer;
	
	public RoboconAlbumConverter(){
		super(RoboconAlbumDTO.class, RoboconAlbumEB.class);
	}

	@Override
	public RoboconAlbumEB convertTo(RoboconAlbumDTO source, RoboconAlbumEB destination) {
		if(source == null){
			return null;
		}
		if(source.pk() == null){
			return null;
		}
		destination = new RoboconAlbumEB();
		destination.setId(source.pk());
		return destination;
	}

	@Override
	public RoboconAlbumDTO convertFrom(RoboconAlbumEB source, RoboconAlbumDTO destination) {
		return roboconAlbumTransformer.transformEntityToDTO(source);
	}
}
