package com.masrawy.robocon.converter;

import java.util.HashMap;
import java.util.Map;
import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.masrawy.robocon.dao.RoboconArtistDAO;
import com.masrawy.robocon.model.RoboconArtistEB;

@Component("roboconArtistToEntryConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconArtistToEntryConverter extends DozerConverter<RoboconArtistEB, Map<Long, String>> implements CustomConverter{

	@Autowired
	private RoboconArtistDAO roboconArtistDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconArtistToEntryConverter(){
		super(RoboconArtistEB.class, (Class<Map<Long, String>>) (Class) Map.class);
	}

	@Override
	public Map<Long, String> convertTo(RoboconArtistEB artist, Map<Long, String> map) {
		map = new HashMap<Long, String>();
		if(artist != null){
			String name = null;
			if(artist.getRoboconArtistMetaEB() != null && !artist.getRoboconArtistMetaEB().isEmpty()){
				name = artist.getRoboconArtistMetaEB().get(0).getName();
			}
			map.put(artist.pk(), name);
		}
		return map;
	}

	@Override
	public RoboconArtistEB convertFrom(Map<Long, String> map, RoboconArtistEB album) {
		if(map != null && map.size() == 1){
			Long id = null;
			for(Long key : map.keySet()){
				id = key;
			}
			return roboconArtistDAO.getEntityByID(id);
		}
		return null;
	}
}
