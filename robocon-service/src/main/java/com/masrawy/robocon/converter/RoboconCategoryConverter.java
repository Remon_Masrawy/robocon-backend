package com.masrawy.robocon.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dto.RoboconCategoryDTO;
import com.masrawy.robocon.model.RoboconCategoryEB;
import com.masrawy.robocon.transformer.RoboconCategoryTransformer;

@Component("roboconCategoryConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconCategoryConverter extends AbstractRoboconConverter<RoboconCategoryDTO, RoboconCategoryEB>{

	@Autowired
	private RoboconCategoryTransformer roboconCategoryTransformer;
	
	public RoboconCategoryConverter(){
		super(RoboconCategoryDTO.class, RoboconCategoryEB.class);
	}

	@Override
	public RoboconCategoryEB convertTo(RoboconCategoryDTO source, RoboconCategoryEB destination) {
		if(source == null)
			return null;
		if(source.pk() == null)
			return null;
		destination = new RoboconCategoryEB();
		destination.setId(source.pk());
		return destination;
	}

	@Override
	public RoboconCategoryDTO convertFrom(RoboconCategoryEB source, RoboconCategoryDTO destination) {
		return roboconCategoryTransformer.transformEntityToDTO(source);
	}
}
