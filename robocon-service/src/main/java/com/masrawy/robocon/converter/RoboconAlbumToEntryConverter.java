package com.masrawy.robocon.converter;

import java.util.HashMap;
import java.util.Map;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dao.RoboconAlbumDAO;
import com.masrawy.robocon.model.RoboconAlbumEB;

@Component("roboconAlbumToEntryConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconAlbumToEntryConverter extends DozerConverter<RoboconAlbumEB, Map<Long, String>> implements CustomConverter{

	@Autowired
	private RoboconAlbumDAO roboconAlbumDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconAlbumToEntryConverter(){
		super(RoboconAlbumEB.class, (Class<Map<Long, String>>) (Class) Map.class);
	}

	@Override
	public Map<Long, String> convertTo(RoboconAlbumEB album, Map<Long, String> map) {
		map = new HashMap<Long, String>();
		if(album != null){
			String name = null;
			if(album.getRoboconAlbumMetaEB() != null && !album.getRoboconAlbumMetaEB().isEmpty()){
				name = album.getRoboconAlbumMetaEB().get(0).getName();
			}
			map.put(album.pk(), name);
		}
		return map;
	}

	@Override
	public RoboconAlbumEB convertFrom(Map<Long, String> map, RoboconAlbumEB album) {
		if(map != null && map.size() == 1){
			Long id = null;
			for(Long key : map.keySet()){
				id = key;
			}
			return roboconAlbumDAO.getEntityByID(id);
		}
		return null;
	}
}
