package com.masrawy.robocon.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dao.RoboconCategoryDAO;
import com.masrawy.robocon.model.RoboconCategoryEB;
import com.masrawy.robocon.model.RoboconCategoryMetaEB;

@Component("roboconCategoriesToMapConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconCategoriesToMapConverter extends DozerConverter<List<RoboconCategoryEB>, Map<Long, String>> implements CustomConverter{

	@Autowired
	private RoboconCategoryDAO roboconCategoryDAO;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public RoboconCategoriesToMapConverter(){
		super((Class<List<RoboconCategoryEB>>) (Class) List.class, (Class<Map<Long, String>>) (Class) Map.class);
	}

	@Override
	public Map<Long, String> convertTo(List<RoboconCategoryEB> categories, Map<Long, String> map) {
		map = new HashMap<Long , String>();
		if(categories != null){
			for(RoboconCategoryEB category : categories){
				if(category.getRoboconCategoryMetaEB() != null){
					RoboconCategoryMetaEB meta = category.getRoboconCategoryMetaEB().get(0);
					map.put(category.pk(), meta.getName());
				}
			}
		}
		return map;
	}

	@Override
	public List<RoboconCategoryEB> convertFrom(Map<Long, String> map, List<RoboconCategoryEB> categories) {
		categories = new ArrayList<RoboconCategoryEB>();
		if(map != null){
			for(Long id : map.keySet()){
				categories.add(roboconCategoryDAO.getEntityByID(id));
			}
			return categories;
		}
		return null;
	}

}
