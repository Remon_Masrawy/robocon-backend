package com.masrawy.robocon.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.masrawy.robocon.dto.RoboconArtistDTO;
import com.masrawy.robocon.model.RoboconArtistEB;
import com.masrawy.robocon.transformer.RoboconArtistTransformer;

@Component("roboconArtistConverter")
@Qualifier("dozerCustomConvertersWithId")
public class RoboconArtistConverter extends AbstractRoboconConverter<RoboconArtistDTO, RoboconArtistEB>{

	@Autowired
	private RoboconArtistTransformer roboconArtistTransformer;
	
	public RoboconArtistConverter(){
		super(RoboconArtistDTO.class, RoboconArtistEB.class);
	}

	@Override
	public RoboconArtistEB convertTo(RoboconArtistDTO source, RoboconArtistEB destination) {
		if(source == null){
			return null;
		}
		if(source.pk() == null){
			return null;
		}
		destination = new RoboconArtistEB();
		destination.setId(source.pk());
		return destination;
	}

	@Override
	public RoboconArtistDTO convertFrom(RoboconArtistEB source, RoboconArtistDTO destination) {
		return roboconArtistTransformer.transformEntityToDTO(source);
	}
}
