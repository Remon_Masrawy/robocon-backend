package com.masrawy.robocon.security.userdetails;

import org.springframework.security.core.AuthenticationException;

public class InvalidUsernamePasswordAuthenticationException extends AuthenticationException{

	private static final long serialVersionUID = 1L;

	public InvalidUsernamePasswordAuthenticationException(String msg, String token){
		super(msg);
	}
}
