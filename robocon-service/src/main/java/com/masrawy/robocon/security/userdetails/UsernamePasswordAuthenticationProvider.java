package com.masrawy.robocon.security.userdetails;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.Ordered;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.util.Assert;

public class UsernamePasswordAuthenticationProvider implements AuthenticationProvider, InitializingBean, Ordered{

	private static final Log logger = LogFactory.getLog(UsernamePasswordAuthenticationProvider.class);
	
	private AuthenticationUserDetailsService<UsernamePasswordAuthenticationToken> preAuthenticatedUserDetailsService = null;
    private UserDetailsChecker userDetailsChecker = new AccountStatusUserDetailsChecker();
    private boolean throwExceptionWhenTokenRejected = false;

    private int order = -1; // default: same as non-ordered
	
	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(preAuthenticatedUserDetailsService, "An AuthenticationUserDetailsService must be set");
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		if (!supports(authentication.getClass())) {
            return null;
        }

        /*if (logger.isDebugEnabled()) {
            logger.debug("PreAuthenticated authentication request: " + authentication);
        }*/

        if (authentication.getPrincipal() == null) {
            logger.debug("No pre-authenticated principal found in request.");

            if (throwExceptionWhenTokenRejected) {
                throw new BadCredentialsException("No pre-authenticated principal found in request.");
            }
            return null;
        }

        if (authentication.getCredentials() == null) {
            logger.debug("No pre-authenticated credentials found in request.");

            if (throwExceptionWhenTokenRejected) {
                throw new BadCredentialsException("No pre-authenticated credentials found in request.");
            }
            return null;
        }

        UserDetails ud = preAuthenticatedUserDetailsService.loadUserDetails((UsernamePasswordAuthenticationToken)authentication);

        userDetailsChecker.check(ud);

        UsernamePasswordAuthenticationToken result =
                new UsernamePasswordAuthenticationToken(ud, authentication.getCredentials(), ud.getAuthorities());
        result.setDetails(authentication.getDetails());

        return result;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}

    /**
     * Set the AuthenticatedUserDetailsService to be used to load the {@code UserDetails} for the authenticated user.
     *
     * @param uds
     */
    public void setPreAuthenticatedUserDetailsService(AuthenticationUserDetailsService<UsernamePasswordAuthenticationToken> uds) {
        this.preAuthenticatedUserDetailsService = uds;
    }

    /**
     * If true, causes the provider to throw a BadCredentialsException if the presented authentication
     * request is invalid (contains a null principal or credentials). Otherwise it will just return
     * null. Defaults to false.
     */
    public void setThrowExceptionWhenTokenRejected(boolean throwExceptionWhenTokenRejected) {
        this.throwExceptionWhenTokenRejected = throwExceptionWhenTokenRejected;
    }

    /**
     * Sets the strategy which will be used to validate the loaded <tt>UserDetails</tt> object
     * for the user. Defaults to an {@link AccountStatusUserDetailsChecker}.
     * @param userDetailsChecker
     */
    public void setUserDetailsChecker(UserDetailsChecker userDetailsChecker) {
        Assert.notNull(userDetailsChecker, "userDetailsChacker cannot be null");
        this.userDetailsChecker = userDetailsChecker;
    }
	
    public int getOrder() {
        return order;
    }

    public void setOrder(int i) {
        order = i;
    }
}
