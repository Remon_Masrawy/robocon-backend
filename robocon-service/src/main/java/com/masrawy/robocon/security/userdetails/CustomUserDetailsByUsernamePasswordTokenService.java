package com.masrawy.robocon.security.userdetails;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.persistent.exception.EntityNotFoundException;
import com.cdm.core.security.model.LoginResult;
import com.cdm.core.security.model.RoleEB;
import com.cdm.core.security.model.UserEB;
import com.cdm.core.security.userdetails.CustomUserDetails;
import com.masrawy.robocon.services.RoboconRegistrationService;

@Transactional
public class CustomUserDetailsByUsernamePasswordTokenService implements AuthenticationUserDetailsService<UsernamePasswordAuthenticationToken>{

	private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsByUsernamePasswordService.class);
	
	@Value(value = "${security.allowNonVerifiedUsers}")
	private boolean allowNonVerifiedUsers;
	
	@Autowired
	private RoboconRegistrationService manager;
	
	@Override
	public UserDetails loadUserDetails(UsernamePasswordAuthenticationToken token) throws UsernameNotFoundException, InvalidUsernamePasswordAuthenticationException {
		String username = token.getPrincipal().toString();
		String password = token.getCredentials().toString();
		
		try{
			LoginResult loginResult = manager.loginUser(username, password);
			UserEB userEB = loginResult.getUser();
			
			Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
			for(RoleEB role : userEB.getRoles()){
				grantedAuthorities.add(new SimpleGrantedAuthority(role.getCode()));
			}
			
			boolean isEnabled = allowNonVerifiedUsers ? userEB.isEnabled() : userEB.isEnabled() && userEB.isVerified();
			
			CustomUserDetails userDetails = new CustomUserDetails(userEB.getId(), username, "", isEnabled, userEB.isVerified(), userEB.getLang(), grantedAuthorities);
			
			return userDetails;
		}catch(EntityNotFoundException ex){
			String msg = "No Authentication found with username '" + username +"'";
			logger.error(msg);
			throw new InvalidUsernamePasswordAuthenticationException(msg, username);
		}
	}
}
