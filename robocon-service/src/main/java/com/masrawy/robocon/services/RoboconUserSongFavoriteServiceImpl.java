package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.OPERATOR_MCC;
import static com.masrawy.robocon.model.RoboconConstants.OPERATOR_MNC;
import static com.masrawy.robocon.model.RoboconConstants.STOREFRONT;
import static com.masrawy.robocon.model.RoboconConstants.USER_ID;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.dao.RoboconUserSongFavoritesDAO;
import com.masrawy.robocon.dto.RoboconUserSongFavoriteDTO;
import com.masrawy.robocon.model.RoboconSongEB;
import com.masrawy.robocon.model.RoboconUserSongFavoriteEB;
import com.masrawy.robocon.query.RoboconUserSongFavoritesCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconInvalidUserIdException;
import com.masrawy.robocon.transformer.RoboconUserSongFavoritesTransformer;

@Service("roboconUserSongFavoriteService")
@Transactional
public class RoboconUserSongFavoriteServiceImpl extends AbstractCrudService<RoboconUserSongFavoriteEB, RoboconUserSongFavoriteDTO, RoboconUserSongFavoriteDTO, Long, RoboconUserSongFavoritesCriteriaFilter> implements RoboconUserSongFavoriteService{

	private static final Logger logger = LoggerFactory.getLogger(RoboconUserSongFavoriteServiceImpl.class);
	
	@Autowired
	private RoboconUserSongFavoritesDAO roboconUserSongFavoritesDAO;
	
	@Autowired
	private RoboconSongService roboconSongService;
	
	@Autowired
	private RoboconUserSongFavoritesTransformer roboconUserSongFavoritesTransformer;
	
	@Override
	public RoboconUserSongFavoriteDTO createSongFavorite(Long songId) {
		logger.debug("Creating song favorite for song ({})", songId);
		
		RoboconSongEB roboconSongEB = roboconSongService.getRoboconSongById(songId);
		
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		
		RoboconUserSongFavoritesCriteriaFilter cf = new RoboconUserSongFavoritesCriteriaFilter();
		cf.setSongId(songId);
		cf.setUserId(NumberUtils.toLong(userIdStr));
		
		RoboconUserSongFavoriteEB roboconUserSongFavoriteEB = roboconUserSongFavoritesDAO.getEntityByCriteriaIfExists(cf);
		
		if(roboconUserSongFavoriteEB != null){
			logger.error("Error: A song favorite recored already exist for song ({}) with user id ({})", songId, userIdStr);
			return roboconUserSongFavoritesTransformer.transformEntityToDTO(roboconUserSongFavoriteEB);
		}
		
		roboconUserSongFavoriteEB = new RoboconUserSongFavoriteEB();
		roboconUserSongFavoriteEB.setRoboconSongEB(roboconSongEB);
		roboconUserSongFavoriteEB.setUserId(NumberUtils.toLong(userIdStr));
		
		roboconUserSongFavoritesDAO.createEntity(roboconUserSongFavoriteEB);
		roboconUserSongFavoritesDAO.flush();
		roboconUserSongFavoritesDAO.refreshSongFavorite(roboconUserSongFavoriteEB);
		
		logger.debug("Song favorite for song ({}) has been created successfully.", songId);
		
		return roboconUserSongFavoritesTransformer.transformEntityToDTO(roboconUserSongFavoriteEB);
	}

	@Override
	public List<RoboconUserSongFavoriteDTO> filterSongFavorites(Integer pageNum, Integer pageLimit) {
		logger.debug("Filtering user song favorites with page number {} and limit {}", pageNum, pageLimit);
		
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		
		Long userId = NumberUtils.toLong(userIdStr);
		String mcc = ControlFieldsHelper.getStrFieldValue(OPERATOR_MCC);
		String mnc = ControlFieldsHelper.getStrFieldValue(OPERATOR_MNC);
		String storefront = ControlFieldsHelper.getStrFieldValue(STOREFRONT);
		
		RoboconUserSongFavoritesCriteriaFilter cf = new RoboconUserSongFavoritesCriteriaFilter();
		cf.setUserId(userId);
		
		// Operator Filter
		if(mcc != null && !mcc.isEmpty() && mnc != null && !mnc.isEmpty()){
			cf.setOperatorMCC(mcc);
			cf.setOperatorMNC(mnc);
		}else{
			cf.isEmptyOperator(true);
		}
		
		// Storefront Filter
		if(storefront != null && !storefront.isEmpty()){
			cf.setStorefrontName(storefront);
		}else{
			cf.isEmptyStorefront(true);
		}
		
		// Pagination
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
			
		cf.orderByFavoriteCreatedDate(false);
		
		List<RoboconUserSongFavoriteEB> roboconUserSongFavoritesEB = roboconUserSongFavoritesDAO.getEntityListByCriteria(cf);
		return roboconUserSongFavoritesTransformer.transformEntityToDTO(roboconUserSongFavoritesEB);
	}

	@Override
	public void deleteSongFavorite(Long favoriteId) {
		logger.debug("Deleting song favorite with id {}.", favoriteId);
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		
		Long userId = NumberUtils.toLong(userIdStr);
		RoboconUserSongFavoritesCriteriaFilter cf = new RoboconUserSongFavoritesCriteriaFilter();
		cf.setUserId(userId);
		cf.setFavoriteId(favoriteId);
		RoboconUserSongFavoriteEB roboconUserSongFavoriteEB = roboconUserSongFavoritesDAO.getEntityByCriteriaIfExists(cf);
		if(roboconUserSongFavoriteEB != null){
			roboconUserSongFavoritesDAO.deleteEntity(roboconUserSongFavoriteEB);
		}
	}
	
	@Override
	protected DTOTransformer<RoboconUserSongFavoriteEB, RoboconUserSongFavoriteDTO> getTransformer() {
		return roboconUserSongFavoritesTransformer;
	}

	@Override
	protected DTOTransformer<RoboconUserSongFavoriteEB, RoboconUserSongFavoriteDTO> getSummaryTransformer() {
		return roboconUserSongFavoritesTransformer;
	}

	@Override
	protected DAO<RoboconUserSongFavoriteEB, Long, RoboconUserSongFavoritesCriteriaFilter> getDAO() {
		return roboconUserSongFavoritesDAO;
	}

}
