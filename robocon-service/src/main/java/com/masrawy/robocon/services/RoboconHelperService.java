package com.masrawy.robocon.services;

import java.util.List;

import com.cdm.core.dto.model.DTO;

public interface RoboconHelperService {

	public List<DTO<?>> filterMetaListLang(List<DTO<?>> dtos, String defaultLang);
}
