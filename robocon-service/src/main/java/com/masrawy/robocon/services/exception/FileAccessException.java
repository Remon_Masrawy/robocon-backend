package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class FileAccessException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	private static final String ERROR_MSG = "Cannot access file Exception.";
			
	public FileAccessException(){
		super(ERROR_MSG);
	}
	
	public FileAccessException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return RoboconErrorConstants.ROBOCON_INVALID_REQUEST;
	}

}
