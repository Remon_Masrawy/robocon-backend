package com.masrawy.robocon.services;

import java.util.List;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconUserSongFavoriteDTO;
import com.masrawy.robocon.query.RoboconUserSongFavoritesCriteriaFilter;

public interface RoboconUserSongFavoriteService extends CrudService<RoboconUserSongFavoriteDTO, RoboconUserSongFavoriteDTO, Long, RoboconUserSongFavoritesCriteriaFilter>{

	public RoboconUserSongFavoriteDTO createSongFavorite(Long songId);
	public List<RoboconUserSongFavoriteDTO> filterSongFavorites(Integer pageNum, Integer pageLimit);
	public void deleteSongFavorite(Long favoriteId);
}
