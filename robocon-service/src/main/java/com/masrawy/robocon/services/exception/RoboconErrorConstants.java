package com.masrawy.robocon.services.exception;

public class RoboconErrorConstants {

	public static final int ROBOCON_INVALID_REQUEST = 400;
	
	public static final int ROBOCON_FORBIDDEN_REQUEST = 403;
	
	public static final int ROBOCON_INTERNAL_ERROR = 500;
	
	public static final int ROBOCON_CREATION_ERROR = 191;
	
	public static final int ROBOCON_UPDATE_ERROR = 248;
	
	public static final int ROBOCON_USER_NOT_FOUND_ERROR = 249;
	
	public static final int ROBOCON_UPLOAD_ERROR = 250;
}
