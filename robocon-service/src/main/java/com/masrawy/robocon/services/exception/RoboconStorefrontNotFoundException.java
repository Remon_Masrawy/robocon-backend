package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class RoboconStorefrontNotFoundException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	private static final String ERROR_MSG = "Robocon Storefront Not Found Exception.";
	
	public RoboconStorefrontNotFoundException(){
		super(ERROR_MSG);
	}
	
	public RoboconStorefrontNotFoundException(String msg){
		super(msg);
	}
	
	@Override
	public int getErrorCode() {
		return RoboconErrorConstants.ROBOCON_INVALID_REQUEST;
	}

}
