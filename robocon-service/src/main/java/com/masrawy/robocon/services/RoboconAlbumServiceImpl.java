package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.SPECIAL_CHARACTERS_PATTERN;
import static com.masrawy.robocon.model.RoboconConstants.ARTIST_ID;
import static com.masrawy.robocon.model.RoboconConstants.PROVIDER_ID;
import static com.masrawy.robocon.model.RoboconConstants.KEYWORD;
import static com.masrawy.robocon.model.RoboconConstants.CODE_FROM;
import static com.masrawy.robocon.model.RoboconConstants.CODE_TO;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.file.model.FileEB;
import com.cdm.core.persistent.dao.DAO;
import com.cdm.core.util.DumpUtil;
import com.masrawy.robocon.dao.RoboconAlbumDAO;
import com.masrawy.robocon.dto.RoboconAlbumDTO;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.dto.RoboconTagDTO;
import com.masrawy.robocon.file.model.ImageUpload;
import com.masrawy.robocon.model.RoboconAlbumEB;
import com.masrawy.robocon.model.RoboconAlbumMetaEB;
import com.masrawy.robocon.model.RoboconAlbumPhotoEB;
import com.masrawy.robocon.model.RoboconAlbumTagEB;
import com.masrawy.robocon.query.RoboconAlbumCriteriaFilter;
import com.masrawy.robocon.query.RoboconAlbumSearchCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconAlbumCreationException;
import com.masrawy.robocon.services.exception.RoboconAlbumException;
import com.masrawy.robocon.services.exception.RoboconAlbumNotFoundException;
import com.masrawy.robocon.services.exception.RoboconAlbumPhotoUploadException;
import com.masrawy.robocon.services.exception.RoboconAlbumUpdateException;
import com.masrawy.robocon.transformer.RoboconAlbumMetaTransformer;
import com.masrawy.robocon.transformer.RoboconAlbumTagTransformer;
import com.masrawy.robocon.transformer.RoboconAlbumTransformer;
import com.masrawy.robocon.validation.RoboconBeanValidationManager;

@Service("roboconAlbumService")
@Transactional
public class RoboconAlbumServiceImpl extends AbstractCrudService<RoboconAlbumEB, RoboconAlbumDTO, RoboconAlbumDTO, Long, RoboconAlbumCriteriaFilter> implements RoboconAlbumService{

	private static final Logger logger = LoggerFactory.getLogger(RoboconAlbumServiceImpl.class);
	
	@Autowired
	private RoboconFileService roboconFileService;
	
	@Autowired
	private RoboconAlbumDAO roboconAlbumDAO;
	
	@Autowired
	private RoboconAlbumTransformer roboconAlbumTransformer;
	
	@Autowired
	private RoboconAlbumMetaTransformer roboconAlbumMetaTransformer;
	
	@Autowired
	private RoboconAlbumTagTransformer roboconAlbumTagTransformer;
	
	@Autowired
	private RoboconMessageService roboconMessageService;
	
	@Autowired
	private RoboconBeanValidationManager roboconBeanValidationManager;
	
	@Autowired
	ServletContext servletContext;
	
	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	@Override
	public RoboconAlbumDTO createAlbum(RoboconAlbumDTO roboconAlbumDTO){
		logger.debug("Creating Robocon Album.");
		try{
			RoboconAlbumEB roboconAlbumEB = createEntity(roboconAlbumDTO);
			roboconAlbumDTO = roboconAlbumTransformer.transformEntityToDTO(roboconAlbumEB);
			logger.debug("Album ({}) has been created successfully.", roboconAlbumDTO.pk());
			return roboconAlbumDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Album create exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot create album due to " + message;
			throw new RoboconAlbumCreationException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Album creation error due to {}.", ex.getMessage());
			String msg = "Cannot create album due to " + ex.getMessage();
			throw new RoboconAlbumCreationException(msg);
		}
	}
	
	@Override
	public RoboconAlbumDTO createAlbum(RoboconAlbumDTO roboconAlbumDTO, List<MultipartFile> files){
		roboconAlbumDTO = createAlbum(roboconAlbumDTO);
		return uploadPhotos(files, roboconAlbumDTO.pk());
	}
	
	@Override
	public RoboconAlbumDTO updateAlbum(RoboconAlbumDTO roboconAlbumDTO){
		logger.debug("Updating Robocon Album ({}).", roboconAlbumDTO.pk());
		try{
			RoboconAlbumEB roboconAlbumEB = updateEntity(roboconAlbumDTO);
			roboconAlbumDTO = roboconAlbumTransformer.transformEntityToDTO(roboconAlbumEB);
			return roboconAlbumDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Album update exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot update album due to " + message;
			throw new RoboconAlbumUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Album update error due to {}", ex.getCause());
			String msg = "Cannot update album due to " + ex.getMessage();
			throw new RoboconAlbumUpdateException(msg);
		}
	}
	
	@Override
	public RoboconAlbumDTO uploadPhoto(MultipartFile file, Long albumId){
		List<MultipartFile> files = new ArrayList<MultipartFile>();
		files.add(file);
		return uploadPhotos(files, albumId);
	}
	
	@Override
	public RoboconAlbumDTO uploadPhotos(List<MultipartFile> files, Long albumId){
		logger.debug("Uploading Photos to Robocon Album {}.", albumId);
		try{
			RoboconAlbumEB roboconAlbumEB = getRoboconAlbumById(albumId);
			for(MultipartFile file : files){
				FileEB fileEB = roboconFileService.uploadFileEB(file);
				RoboconAlbumPhotoEB roboconAlbumPhotoEB = new RoboconAlbumPhotoEB();
				roboconAlbumPhotoEB.setFile(fileEB);
				roboconAlbumPhotoEB.setSize(file.getSize());
				roboconAlbumEB.addRoboconAlbumPhotoEB(roboconAlbumPhotoEB);
			}
			roboconAlbumDAO.flush();
			roboconAlbumDAO.refresh(roboconAlbumEB);
			RoboconAlbumDTO roboconAlbumDTO = roboconAlbumTransformer.transformEntityToDTO(roboconAlbumEB);
			logger.debug("Robocon Photos has been uploaded successfully.");
			return roboconAlbumDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Album Photo upload exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Album Photo upload error occurred due to " + message;
			throw new RoboconAlbumPhotoUploadException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Album photo upload error due to {}", ex.getCause());
			throw new RoboconAlbumPhotoUploadException(ex.getMessage());
		}
	}
	
	@Override
	public RoboconAlbumDTO uploadPhotos(Long albumId, List<ImageUpload> files){
		logger.debug("Uploading Photos to Robocon Album {}.", albumId);
		try{
			RoboconAlbumEB roboconAlbumEB = getRoboconAlbumById(albumId);
			for(ImageUpload file : files){
				FileEB fileEB = roboconFileService.uploadFileEB(file.getFile());
				RoboconAlbumPhotoEB roboconAlbumPhotoEB = new RoboconAlbumPhotoEB();
				roboconAlbumPhotoEB.setFile(fileEB);
				roboconAlbumPhotoEB.setSize(file.getFile().getSize());
				roboconAlbumPhotoEB.setIsMainPhoto(file.getIsMainImage());
				roboconAlbumEB.addRoboconAlbumPhotoEB(roboconAlbumPhotoEB);
			}
			roboconAlbumDAO.flush();
			roboconAlbumDAO.refresh(roboconAlbumEB);
			RoboconAlbumDTO roboconAlbumDTO = roboconAlbumTransformer.transformEntityToDTO(roboconAlbumEB);
			logger.debug("Robocon Photos has been uploaded successfully.");
			return roboconAlbumDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Album Photo upload exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Album Photo upload error occurred due to " + message;
			throw new RoboconAlbumPhotoUploadException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Album photo upload error due to {}", ex.getCause());
			throw new RoboconAlbumPhotoUploadException(ex.getMessage());
		}
	}
	
	@Override
	public RoboconAlbumDTO addMeta(RoboconMetaDTO roboconMetaDTO, Long albumId) {
		logger.debug("Adding Meta {} to Album {}.", DumpUtil.dumpAsJson(roboconMetaDTO), albumId);
		try{
			roboconBeanValidationManager.validate(roboconMetaDTO);
			RoboconAlbumEB roboconAlbumEB = getRoboconAlbumById(albumId);
			RoboconAlbumMetaEB roboconAlbumMetaEB = roboconAlbumMetaTransformer.transformDTOToEntity(roboconMetaDTO);
			roboconAlbumEB.addRoboconAlbumMetaEB(roboconAlbumMetaEB);
			roboconAlbumDAO.flush();
			roboconAlbumDAO.refresh(roboconAlbumEB);
			RoboconAlbumDTO roboconAlbumDTO = roboconAlbumTransformer.transformEntityToDTO(roboconAlbumEB);
			return roboconAlbumDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Adding Album Meta exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Adding Album Meta error due to " + message;
			throw new RoboconAlbumUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Adding Album Meta error due to {}", ex.getCause());
			throw new RoboconAlbumUpdateException(ex.getMessage());
		}
	}

	@Override
	public RoboconAlbumDTO addTag(RoboconTagDTO roboconTagDTO, Long albumId) {
		logger.debug("Adding Tag {} to Album {}.", DumpUtil.dumpAsJson(roboconTagDTO), albumId);
		try{
			roboconBeanValidationManager.validate(roboconTagDTO);
			RoboconAlbumEB roboconAlbumEB = getRoboconAlbumById(albumId);
			RoboconAlbumTagEB roboconAlbumTagEB = roboconAlbumTagTransformer.transformDTOToEntity(roboconTagDTO);
			roboconAlbumEB.addRoboconAlbumTagEB(roboconAlbumTagEB);
			roboconAlbumDAO.flush();
			roboconAlbumDAO.refresh(roboconAlbumEB);
			RoboconAlbumDTO roboconAlbumDTO = roboconAlbumTransformer.transformEntityToDTO(roboconAlbumEB);
			return roboconAlbumDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Adding Album Tag exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Adding Album Tag error due to {}" + message;
			throw new RoboconAlbumUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Adding Album Tag error due to {}", ex.getCause());
			throw new RoboconAlbumUpdateException(ex.getMessage());
		}
	}
	
	@Override
	public RoboconAlbumDTO fetchAlbum(Long albumId){
		logger.debug("Fetching Album ({})", albumId);
		RoboconAlbumEB roboconAlbumEB = getRoboconAlbumByCriteria(albumId);
		RoboconAlbumDTO roboconAlbumDTO = roboconAlbumTransformer.transformEntityToDTO(roboconAlbumEB);
		return roboconAlbumDTO;
	}
	
	@Override
	public List<RoboconAlbumDTO> fetchAlbums(Integer pageNum, Integer pageLimit, HttpServletResponse response){
		logger.debug("Fetching albums for page number ({}) and limit ({}).", pageNum, pageLimit);
		RoboconAlbumCriteriaFilter cf = new RoboconAlbumCriteriaFilter();
		RoboconAlbumSearchCriteriaFilter scf = new RoboconAlbumSearchCriteriaFilter();
		scf.orderByAlbumId(true);
		Integer codeFrom = ControlFieldsHelper.getIntFieldValue(CODE_FROM);
		Integer codeTo = ControlFieldsHelper.getIntFieldValue(CODE_TO);
		Integer artistId = ControlFieldsHelper.getIntFieldValue(ARTIST_ID);
		Integer providerId = ControlFieldsHelper.getIntFieldValue(PROVIDER_ID);
		String keyword = ControlFieldsHelper.getStrFieldValue(KEYWORD);
		try {
			if(keyword != null && !keyword.isEmpty()){
				keyword = URLDecoder.decode(keyword, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot decode keyword:{}, error {}", keyword, e);
		}
		if(artistId != null){
			cf.setArtistId(Long.valueOf(artistId));
			scf.setArtistId(Long.valueOf(artistId));
		}
		if(providerId != null){
			cf.setProviderId(Long.valueOf(providerId));
			scf.setProviderId(Long.valueOf(providerId));
		}
		if(keyword != null && !keyword.isEmpty()){
			roboconAlbumDAO.enableMetaLangSearchFilter();
			cf.setAlbumName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
			scf.setAlbumName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
		}else{
			roboconAlbumDAO.enableMetaLangFilter();
		}
		
		if(codeFrom != null && codeTo != null){
			cf.setAlbumId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
			scf.setAlbumId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
		}else if(codeFrom != null){
			cf.setAlbumId(Long.valueOf(codeFrom));
			scf.setAlbumId(Long.valueOf(codeFrom));
		}else if(codeTo != null){
			cf.setAlbumId(Long.valueOf(codeTo));
			scf.setAlbumId(Long.valueOf(codeTo));
		}
		
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			scf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			scf.setMaxResult(pageLimit);
		}
		List<RoboconAlbumEB> roboconAlbumsEB = roboconAlbumDAO.getEntityListByCriteria(scf);
		int count = roboconAlbumDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		return roboconAlbumTransformer.transformEntityToDTO(roboconAlbumsEB);
	}
	
	@Override
	public void removeAlbum(Long albumId){
		logger.debug("Removing album with id ({}) and it's songs.", albumId);
		RoboconAlbumEB roboconAlbumEB = getRoboconAlbumByCriteria(albumId);
		try{
			if(!roboconAlbumEB.getRoboconAlbumMetaEB().isEmpty()){
				RoboconAlbumMetaEB roboconAlbumMetaEB = roboconAlbumEB.getRoboconAlbumMetaEB().get(0);
				logger.info("Removing album ({}). and ({}) song(s) found in it.", roboconAlbumMetaEB.getName(), roboconAlbumEB.getRoboconSongsEB().size());
			}
			roboconAlbumDAO.deleteEntity(roboconAlbumEB);
		}catch(Exception e){
			logger.error("Error: Cannot remove album ({}) due to ({})", albumId, e);
			String msg = "Cannot remove album ("+albumId+") due to "+e.getMessage();
			throw new RoboconAlbumException(msg);
		}
	}
	
	@Override
	protected void doBeforeValidate(RoboconAlbumDTO dto, boolean isNew) {
		roboconBeanValidationManager.validate(dto);
		super.doBeforeValidate(dto, isNew);
	}
	
	@Override
	protected void doBeforePersist(RoboconAlbumEB roboconAlbumEB, RoboconAlbumDTO roboconAlbumDTO, boolean isNew) {
		if(roboconAlbumEB.getRoboconAlbumMetaEB() != null && !roboconAlbumEB.getRoboconAlbumMetaEB().isEmpty()){
			for(RoboconAlbumMetaEB roboconAlbumMetaEB : roboconAlbumEB.getRoboconAlbumMetaEB()){
				if(roboconAlbumMetaEB.getRoboconAlbumEB() == null){
					roboconAlbumMetaEB.setRoboconAlbumEB(roboconAlbumEB);
				}
			}
		}
		if(roboconAlbumEB.getRoboconAlbumTagEB() != null && !roboconAlbumEB.getRoboconAlbumTagEB().isEmpty()){
			for(RoboconAlbumTagEB roboconAlbumTagEB : roboconAlbumEB.getRoboconAlbumTagEB()){
				if(roboconAlbumTagEB.getRoboconAlbumEB() == null){
					roboconAlbumTagEB.setRoboconAlbumEB(roboconAlbumEB);
				}
			}
		}
		super.doBeforePersist(roboconAlbumEB, roboconAlbumDTO, isNew);
	}
	
	@Override
	protected void doAfterPersist(RoboconAlbumEB entity, RoboconAlbumDTO dto, boolean isNew) {
		roboconAlbumDAO.flush();
		roboconAlbumDAO.refresh(entity);
		super.doAfterPersist(entity, dto, isNew);
	}
	
	private RoboconAlbumEB getRoboconAlbumById(Long albumId){
		RoboconAlbumEB roboconAlbumEB = roboconAlbumDAO.getEntityByIDIfExists(albumId);
		if(roboconAlbumEB == null){
			logger.error("Error: No Album found with id ({})", albumId);
			String msg = "Album with id (" + albumId + ") not found exception.";
			throw new RoboconAlbumNotFoundException(msg);
		}
		return roboconAlbumEB;
	}
	
	private RoboconAlbumEB getRoboconAlbumByCriteria(Long albumId){
		RoboconAlbumCriteriaFilter cf = new RoboconAlbumCriteriaFilter();
		cf.setAlbumId(albumId);
		RoboconAlbumEB roboconAlbumEB = roboconAlbumDAO.getEntityByCriteriaIfExists(cf);
		if(roboconAlbumEB == null){
			logger.error("Error: No Album found with id ({})", albumId);
			String msg = "Album with id (" + albumId + ") not found exception.";
			throw new RoboconAlbumNotFoundException(msg);
		}
		return roboconAlbumEB;
	}
	
	@Override
	protected DTOTransformer<RoboconAlbumEB, RoboconAlbumDTO> getTransformer() {
		return roboconAlbumTransformer;
	}

	@Override
	protected DTOTransformer<RoboconAlbumEB, RoboconAlbumDTO> getSummaryTransformer() {
		return roboconAlbumTransformer;
	}

	@Override
	protected DAO<RoboconAlbumEB, Long, RoboconAlbumCriteriaFilter> getDAO() {
		return roboconAlbumDAO;
	}
}
