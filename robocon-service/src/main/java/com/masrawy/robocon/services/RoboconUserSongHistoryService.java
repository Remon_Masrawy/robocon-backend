package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconUserSongHistoryDTO;
import com.masrawy.robocon.model.RoboconSongEB;
import com.masrawy.robocon.model.RoboconUserActionEnum;
import com.masrawy.robocon.model.RoboconUserSongHistoryEB;
import com.masrawy.robocon.query.RoboconUserSongHistoryCriteriaFilter;

public interface RoboconUserSongHistoryService extends CrudService<RoboconUserSongHistoryDTO, RoboconUserSongHistoryDTO, Long, RoboconUserSongHistoryCriteriaFilter>{

	public RoboconUserSongHistoryDTO createSongHistory(RoboconUserSongHistoryEB roboconUserSongHistoryEB);
	public RoboconUserSongHistoryDTO createOrUpdateSongHistory(RoboconSongEB song, RoboconUserActionEnum action);
	public List<RoboconUserSongHistoryDTO> listSongHistory(Integer pageNum, Integer pageLimit);
	public List<RoboconUserSongHistoryDTO> filterSongHistory(HttpServletRequest request, Integer pageNum, Integer pageLimit);
	public List<RoboconUserSongHistoryDTO> playAgain(HttpServletRequest request, Integer pageNum, Integer pageLimit);
	public void deleteSongHistory(Long historyId);
}
