package com.masrawy.robocon.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.cdm.core.security.dto.RoleDTO;
import com.cdm.core.security.dto.UserDTO;
import com.cdm.core.security.exception.UserNotFoundException;
import com.cdm.core.security.model.AccountTypeEnum;
import com.cdm.core.security.model.LoginResult;
import com.cdm.core.security.model.UserEB;
import com.cdm.core.security.service.RegistrationManager;
import com.cdm.core.security.service.UserManager;
import com.cdm.core.validation.BeanValidationManager;
import com.masrawy.robocon.dao.RoboconUserDAO;
import com.masrawy.robocon.dto.RoboconUserDTO;
import com.masrawy.robocon.model.RoboconUserEB;
import com.masrawy.robocon.services.exception.RoboconUserException;
import com.masrawy.robocon.transformer.RoboconUserTransformer;


@Service("roboconRegistrationService")
@Transactional
public class RoboconRegistrationServiceImpl implements RoboconRegistrationService{

	private static final Logger logger = LoggerFactory.getLogger(RoboconRegistrationServiceImpl.class);
	
	@Autowired
	private BeanValidationManager validationManager;
	
	@Autowired
	private RegistrationManager manager;
	
	@Autowired
	private UserManager userManager;
	
	@Autowired
	private RoboconUserDAO roboconUserDAO;
	
	@Autowired
	private RoboconUserTransformer roboconUserTransformer;
	
	@Override
	public RoboconUserDTO signup(RoboconUserDTO roboconUserDTO) {
		validationManager.validate(roboconUserDTO);
		if (StringUtils.isEmpty(roboconUserDTO.getFirstName())) {
			String email = roboconUserDTO.getEmail();
			int index = email.indexOf("@");
			String name;
			if (index > 0) {
				name = email.substring(0, index);
			} else {
				name = email;
			}
			name = name.replaceAll("[._]", " ");
			roboconUserDTO.setFirstName(name);
		}
		UserDTO userDTO = new UserDTO();
		userDTO.setUsername(roboconUserDTO.getUsername());
		userDTO.setEmail(roboconUserDTO.getEmail());
		userDTO.setFirstName(roboconUserDTO.getFirstName());
		userDTO.setLastName(roboconUserDTO.getLastName());
		userDTO.setPassword(roboconUserDTO.getPassword());
		userDTO.setType(AccountTypeEnum.LOCAL); 
		userDTO.setMobileNo(roboconUserDTO.getMobileNo());
		userDTO.setBirthDate(roboconUserDTO.getBirthDate());
		if(!CollectionUtils.isEmpty(roboconUserDTO.getRoleCodes())){
			List<RoleDTO> roles = new ArrayList<RoleDTO>();
			for(String role : roboconUserDTO.getRoleCodes()){
				roles.add(new RoleDTO(role));
			}
			userDTO.setRoles(roles);
		}
		// Verified user ??
		userDTO.setVerified(true);
		UserEB userEB = manager.signupUser(userDTO);
		RoboconUserEB roboconUserEB = createUser(userEB);
		return roboconUserTransformer.transformEntityToDTO(roboconUserEB);
	}
	
	protected RoboconUserEB createUser(UserEB userEB){
		RoboconUserEB roboconUserEB = new RoboconUserEB();
		roboconUserEB.setUser(userEB);
		roboconUserDAO.createEntity(roboconUserEB);
		return roboconUserEB;
	}
	
	@Override
	public LoginResult loginUser(String username, String password) {
		try{
			UserEB userEB = userManager.loadByUsername(username);
			RoboconUserEB roboconUserEB = roboconUserDAO.getEntityByIDIfExists(userEB.getId());
			if(roboconUserEB != null){
				if(!roboconUserEB.getIsActive()){
					String msg = "User " + username + " is not active";
					RoboconUserException ex = new RoboconUserException(msg);
					logger.error(msg);
					throw ex;
				}
			}
			if (!userManager.passwordMatchs(password, userEB.getPassword())) {
				String msg = "Bad Credentials for User " + username;
				BadCredentialsException ex = new BadCredentialsException(msg);
				logger.error(msg);
				throw ex;

			}
			return new LoginResult(userEB, username);
		}catch(RoboconUserException e){
			throw new DisabledException(e.getMessage());
		}catch(UserNotFoundException e){
			throw new BadCredentialsException(e.getMessage());
		}
	}
	
	@Override
	public RoboconUserDTO login(String username, String password){
		LoginResult loginResult = manager.loginUser(username, password, false);
		UserEB userEB = loginResult.getUser();
		RoboconUserEB roboconUserEB = roboconUserDAO.getEntityByIDIfExists(userEB.getId());
		if(roboconUserEB == null){
			roboconUserEB = new RoboconUserEB();
			roboconUserEB.setUser(userEB);
			roboconUserDAO.createEntity(roboconUserEB);
		}
		RoboconUserDTO roboconUserDTO = roboconUserTransformer.transformEntityToDTO(roboconUserEB);
		roboconUserDTO.setAuthToken(loginResult.getAuthToken());
		return roboconUserDTO;
	}
	
}
