package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.ARTIST_ID;
import static com.masrawy.robocon.model.RoboconConstants.CODE_FROM;
import static com.masrawy.robocon.model.RoboconConstants.CODE_TO;
import static com.masrawy.robocon.model.RoboconConstants.KEYWORD;
import static com.masrawy.robocon.model.RoboconConstants.SPECIAL_CHARACTERS_PATTERN;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.file.model.FileEB;
import com.cdm.core.persistent.dao.DAO;
import com.cdm.core.util.DumpUtil;
import com.masrawy.robocon.dao.RoboconArtistEventDAO;
import com.masrawy.robocon.dto.RoboconArtistEventDTO;
import com.masrawy.robocon.dto.RoboconArtistEventMetaDTO;
import com.masrawy.robocon.model.RoboconArtistEB;
import com.masrawy.robocon.model.RoboconArtistEventEB;
import com.masrawy.robocon.model.RoboconArtistEventMetaEB;
import com.masrawy.robocon.model.RoboconArtistEventPhotoEB;
import com.masrawy.robocon.model.RoboconArtistEventTagEB;
import com.masrawy.robocon.query.RoboconArtistEventCriteriaFilter;
import com.masrawy.robocon.query.RoboconArtistEventSearchCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconArtistEventNotFoundException;
import com.masrawy.robocon.services.exception.RoboconEventCreationException;
import com.masrawy.robocon.services.exception.RoboconEventPhotoUploadException;
import com.masrawy.robocon.services.exception.RoboconEventUpdateException;
import com.masrawy.robocon.transformer.RoboconArtistEventMetaTransformer;
import com.masrawy.robocon.transformer.RoboconArtistEventTransformer;
import com.masrawy.robocon.validation.RoboconBeanValidationManager;

@Service("roboconEventService")
@Transactional
public class RoboconEventServiceImpl extends AbstractCrudService<RoboconArtistEventEB, RoboconArtistEventDTO, RoboconArtistEventDTO, Long, RoboconArtistEventCriteriaFilter> implements RoboconEventService{

	@Autowired
	private RoboconArtistEventDAO roboconArtistEventDAO;
	
	@Autowired
	private RoboconArtistEventTransformer roboconArtistEventTransformer;
	
	@Autowired
	private RoboconArtistEventMetaTransformer roboconArtistEventMetaTransformer;
	
	@Autowired
	private RoboconBeanValidationManager roboconBeanValidationManager;
	
	@Autowired
	private RoboconFileService roboconFileService;
	
	@Autowired
	private RoboconMessageService roboconMessageService;
	
	@Override
	public RoboconArtistEventDTO createRoboconArtistEvent(RoboconArtistEventDTO roboconArtistEventDTO){
		logger.debug("Creating Event {}.", DumpUtil.dumpAsJson(roboconArtistEventDTO));
		try{
			RoboconArtistEventEB roboconArtistEventEB = createEntity(roboconArtistEventDTO);
			roboconArtistEventDTO = roboconArtistEventTransformer.transformEntityToDTO(roboconArtistEventEB);
			logger.debug("Event {} has been created successfully.", roboconArtistEventDTO.pk());
			return roboconArtistEventDTO;
		} catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Event creation exception due to constraint {} violation.\n Message: {}, \n Error: {}", ex.getConstraintName(), ex.getCause().getMessage(), ex);
			String msg = "Cannot create event due to " + message;
			throw new RoboconEventCreationException(msg);
		} catch(RuntimeException ex){
			logger.error("Error: Event creation exception due to {}", ex);
			String msg = "Cannot create event due to " + ex.getMessage();
			throw new RoboconEventCreationException(msg);
		}
	}
	
	@Override
	public RoboconArtistEventDTO updateRoboconArtistEvent(RoboconArtistEventDTO roboconArtistEventDTO){
		logger.debug("Updating Event {}.", DumpUtil.dumpAsJson(roboconArtistEventDTO));
		try{
			RoboconArtistEventEB roboconArtistEventEB = updateEntity(roboconArtistEventDTO);
			roboconArtistEventDTO = roboconArtistEventTransformer.transformEntityToDTO(roboconArtistEventEB);
			logger.debug("Event {} has been updated successfully.", roboconArtistEventDTO.pk());
			return roboconArtistEventDTO;
		} catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Event update exception due to constraint {} violation.\n Message: {}, \n Error: {}", ex.getConstraintName(), ex.getCause().getMessage(), ex);
			String msg = "Cannot update event due to " + message;
			throw new RoboconEventUpdateException(msg);
		} catch(RuntimeException ex){
			logger.error("Error: Event update exception due to {}", ex);
			String msg = "Cannot update event due to " + ex.getMessage();
			throw new RoboconEventUpdateException(msg);
		}
	}
	
	@Override
	public RoboconArtistEventDTO uploadPhoto(MultipartFile file, Long artistId){
		List<MultipartFile> files = new ArrayList<MultipartFile>();
		files.add(file);
		return uploadPhotos(files, artistId);
	}
	
	@Override
	public RoboconArtistEventDTO uploadPhotos(List<MultipartFile> files, Long eventId){
		logger.debug("Uploading Photos to Robocon Event {}.", eventId);
		try{
			RoboconArtistEventEB roboconArtistEventEB = getRoboconArtistEventIfExist(eventId);
			for(MultipartFile file : files){
				FileEB fileEB = roboconFileService.uploadFileEB(file);
				RoboconArtistEventPhotoEB roboconArtistEventPhotoEB = new RoboconArtistEventPhotoEB();
				roboconArtistEventPhotoEB.setFile(fileEB);
				roboconArtistEventPhotoEB.setSize(file.getSize());
				roboconArtistEventEB.addRoboconArtistPhotoEB(roboconArtistEventPhotoEB);
			}
			roboconArtistEventDAO.flush();
			roboconArtistEventDAO.refreshEntity(roboconArtistEventEB);
			logger.debug("Robocon Photos has been uploaded successfully.");
			return getTransformer().transformEntityToDTO(roboconArtistEventEB);
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Event Photo upload exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Event Photo upload error occurred due to " + message;
			throw new RoboconEventPhotoUploadException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Event photo upload error due to {}", ex.getCause());
			throw new RoboconEventPhotoUploadException(ex.getMessage());
		}
	}
	
	@Override
	public RoboconArtistEventDTO getRoboconArtistEvent(Long eventId){
		RoboconArtistEventEB roboconArtistEventEB = getRoboconArtistEventIfExist(eventId);
		RoboconArtistEventDTO roboconArtistEventDTO = roboconArtistEventTransformer.transformEntityToDTO(roboconArtistEventEB);
		return roboconArtistEventDTO;
	}
	
	@Override
	public List<RoboconArtistEventDTO> fetchRoboconArtistEvents(Integer pageNum, Integer pageLimit, HttpServletResponse response){
		RoboconArtistEventCriteriaFilter cf = new RoboconArtistEventCriteriaFilter();
		RoboconArtistEventSearchCriteriaFilter cfs = new RoboconArtistEventSearchCriteriaFilter();
		Integer codeFrom = ControlFieldsHelper.getIntFieldValue(CODE_FROM);
		Integer codeTo = ControlFieldsHelper.getIntFieldValue(CODE_TO);
		Integer artistId = ControlFieldsHelper.getIntFieldValue(ARTIST_ID);
		String keyword = ControlFieldsHelper.getStrFieldValue(KEYWORD);
		try {
			if(keyword != null && !keyword.isEmpty()){
				keyword = URLDecoder.decode(keyword, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot decode keyword:{}, error {}", keyword, e);
		}
		cfs.orderByEventId(true);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cfs.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cfs.setMaxResult(pageLimit);
		}
		if(artistId != null){
			cf.setArtistId(Long.valueOf(artistId));
			cfs.setArtistId(Long.valueOf(artistId));
		}
		if(keyword != null && !keyword.isEmpty()){
			roboconArtistEventDAO.enableMetaLangSearchFilter();
			cf.setEventName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
			cfs.setEventName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
		}else{
			roboconArtistEventDAO.enableMetaLangFilter();
		}
		if(codeFrom != null && codeTo != null){
			cf.setEventId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
			cfs.setEventId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
		}else if(codeFrom != null){
			cf.setEventId(Long.valueOf(codeFrom));
			cfs.setEventId(Long.valueOf(codeFrom));
		}else if(codeTo != null){
			cf.setEventId(Long.valueOf(codeTo));
			cfs.setEventId(Long.valueOf(codeTo));
		}
		List<RoboconArtistEventEB> roboconArtistEventsEB = roboconArtistEventDAO.getEntityListByCriteria(cfs);
		int count = roboconArtistEventDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		List<RoboconArtistEventDTO> roboconArtistEventsDTO = roboconArtistEventTransformer.transformEntityToDTO(roboconArtistEventsEB);
		return roboconArtistEventsDTO;
	}
	
	@Override
	public void removeRoboconArtistEvent(Long eventId){
		RoboconArtistEventEB roboconArtistEventEB = getRoboconArtistEventIfExist(eventId);
		logger.error("Removing Event {}.", DumpUtil.dumpAsJson(roboconArtistEventTransformer.transformEntityToDTO(roboconArtistEventEB)));
		roboconArtistEventDAO.deleteEntity(roboconArtistEventEB);
	}
	
	@Override
	public RoboconArtistEventDTO addRoboconArtistEventMeta(List<RoboconArtistEventMetaDTO> roboconArtistEventMetaDTO, Long roboconArtistEventId){
		logger.debug("Adding Meta {} to Event {}.", DumpUtil.dumpAsJson(roboconArtistEventMetaDTO), roboconArtistEventId);
		try{
			if(roboconArtistEventMetaDTO != null && !roboconArtistEventMetaDTO.isEmpty()){
				for(RoboconArtistEventMetaDTO roboconArtistEventMeta : roboconArtistEventMetaDTO){
					roboconBeanValidationManager.validate(roboconArtistEventMeta);
				}
			}
			RoboconArtistEventEB roboconArtistEventEB = getRoboconArtistEventIfExist(roboconArtistEventId);
			List<RoboconArtistEventMetaEB> roboconArtistEventMetaEB = roboconArtistEventMetaTransformer.transformDTOToEntity(roboconArtistEventMetaDTO);
			roboconArtistEventEB.addRoboconArtistEventMetaEB(roboconArtistEventMetaEB);
			roboconArtistEventDAO.flush();
			roboconArtistEventDAO.refreshEntity(roboconArtistEventEB);
			RoboconArtistEventDTO roboconArtistEventDTO = roboconArtistEventTransformer.transformEntityToDTO(roboconArtistEventEB);
			return roboconArtistEventDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Artist Event Meta Add exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot add event meta due to " + message;
			throw new RoboconEventUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Artist Event Meta Add error due to {}", ex.getCause());
			throw new RoboconEventUpdateException(ex.getMessage());
		}
	}
	
	@Override
	protected void doBeforeValidate(RoboconArtistEventDTO dto, boolean isNew) {
		roboconBeanValidationManager.validate(dto);
		super.doBeforeValidate(dto, isNew);
	}
	
	@Override
	protected void doBeforePersist(RoboconArtistEventEB roboconArtistEventEB, RoboconArtistEventDTO roboconArtistEventDTO, boolean isNew) {
		for(RoboconArtistEB roboconArtistEB : roboconArtistEventEB.getRoboconArtistsEB()){
			if(!roboconArtistEB.getRoboconArtistEventEB().contains(roboconArtistEventEB)){
				roboconArtistEB.getRoboconArtistEventEB().add(roboconArtistEventEB);
			}
		}
		for(RoboconArtistEventMetaEB roboconArtistEventMetaEB : roboconArtistEventEB.getRoboconArtistEventMetaEB()){
			if(roboconArtistEventMetaEB.getRoboconArtistEventEB() == null){
				roboconArtistEventMetaEB.setRoboconArtistEventEB(roboconArtistEventEB);
			}
		}
		for(RoboconArtistEventTagEB roboconArtistEventTagEB : roboconArtistEventEB.getRoboconArtistEventTagsEB()){
			if(roboconArtistEventTagEB.getRoboconArtistEventEB() == null){
				roboconArtistEventTagEB.setRoboconArtistEventEB(roboconArtistEventEB);
			}
		}
		for(RoboconArtistEventPhotoEB roboconArtistEventPhotoEB : roboconArtistEventEB.getRoboconArtistEventPhotosEB()){
			if(roboconArtistEventPhotoEB.getRoboconArtistEventEB() == null){
				roboconArtistEventPhotoEB.setRoboconArtistEventEB(roboconArtistEventEB);
			}
		}
		super.doBeforePersist(roboconArtistEventEB, roboconArtistEventDTO, isNew);
	}
	
	@Override
	protected void doAfterPersist(RoboconArtistEventEB entity, RoboconArtistEventDTO dto, boolean isNew) {
		roboconArtistEventDAO.flush();
		roboconArtistEventDAO.refreshEntity(entity);
		super.doAfterPersist(entity, dto, isNew);
	}
	
	protected RoboconArtistEventEB getRoboconArtistEventIfExist(Long roboconArtistEventId){
		RoboconArtistEventEB roboconArtistEventEB = roboconArtistEventDAO.getEntityByIDIfExists(roboconArtistEventId);
		if(roboconArtistEventEB == null){
			logger.error("Error: No Artist Event Found With id ({})", roboconArtistEventId);
			String msg = "No Artist Event found with id ({" + roboconArtistEventId + "})";
			throw new RoboconArtistEventNotFoundException(msg);
		}
		return roboconArtistEventEB;
	}
	
	@Override
	protected DTOTransformer<RoboconArtistEventEB, RoboconArtistEventDTO> getTransformer() {
		return roboconArtistEventTransformer;
	}

	@Override
	protected DTOTransformer<RoboconArtistEventEB, RoboconArtistEventDTO> getSummaryTransformer() {
		return roboconArtistEventTransformer;
	}

	@Override
	protected DAO<RoboconArtistEventEB, Long, RoboconArtistEventCriteriaFilter> getDAO() {
		return roboconArtistEventDAO;
	}

}
