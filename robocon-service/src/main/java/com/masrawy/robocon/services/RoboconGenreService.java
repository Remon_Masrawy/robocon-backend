package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconGenreDTO;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.query.RoboconGenreCriteriaFilter;

public interface RoboconGenreService extends CrudService<RoboconGenreDTO, RoboconGenreDTO, Long, RoboconGenreCriteriaFilter>{

	public RoboconGenreDTO createGenre(RoboconGenreDTO roboconGenreDTO);
	public RoboconGenreDTO updateGenre(RoboconGenreDTO roboconGenreDTO);
	public RoboconGenreDTO addGenreMeta(RoboconMetaDTO roboconMetaDTO, Long genreId);
	
	public RoboconGenreDTO fetchGenre(Long genreId);
	public List<RoboconGenreDTO> fetchGenresByPaging(Integer pageNum, Integer pageLimit, HttpServletResponse response);
	
	public void removeGenre(Long genreId);
}
