package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.ALBUM_ID;
import static com.masrawy.robocon.model.RoboconConstants.CODE_FROM;
import static com.masrawy.robocon.model.RoboconConstants.CODE_TO;
import static com.masrawy.robocon.model.RoboconConstants.KEYWORD;
import static com.masrawy.robocon.model.RoboconConstants.SONG_ID;
import static com.masrawy.robocon.model.RoboconConstants.SPECIAL_CHARACTERS_PATTERN;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.hibernate.sql.JoinFragment;
import org.hibernate.sql.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.persistent.dao.DAO;
import com.cdm.core.util.DumpUtil;
import com.masrawy.robocon.dao.RoboconGenreDAO;
import com.masrawy.robocon.dto.RoboconGenreDTO;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.model.RoboconGenreEB;
import com.masrawy.robocon.model.RoboconGenreMetaEB;
import com.masrawy.robocon.query.RoboconGenreCriteriaFilter;
import com.masrawy.robocon.query.RoboconGenreSearchCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconGenreCreationException;
import com.masrawy.robocon.services.exception.RoboconGenreException;
import com.masrawy.robocon.services.exception.RoboconGenreNotFoundException;
import com.masrawy.robocon.services.exception.RoboconGenreUpdateException;
import com.masrawy.robocon.transformer.RoboconGenreMetaTransformer;
import com.masrawy.robocon.transformer.RoboconGenreTransformer;
import com.masrawy.robocon.validation.RoboconBeanValidationManager;

@Service("roboconGenreService")
@Transactional
public class RoboconGenreServiceImpl extends AbstractCrudService<RoboconGenreEB, RoboconGenreDTO, RoboconGenreDTO, Long, RoboconGenreCriteriaFilter> implements RoboconGenreService{

	private final static Logger logger = LoggerFactory.getLogger(RoboconGenreServiceImpl.class);
	
	@Autowired
	private RoboconGenreDAO roboconGenreDAO;
	
	@Autowired
	private RoboconGenreTransformer roboconGenreTransformer;
	
	@Autowired
	private RoboconGenreMetaTransformer roboconGenreMetaTransformer;
	
	@Autowired
	private RoboconMessageService roboconMessageService;
	
	@Autowired
	private RoboconBeanValidationManager roboconBeanValidationManager;
	
	@Override
	public RoboconGenreDTO createGenre(RoboconGenreDTO roboconGenreDTO) {
		logger.debug("Creating Robocon Genre");
		RoboconGenreEB roboconGenreEB = null;
		try{
			roboconGenreEB = createEntity(roboconGenreDTO);
			roboconGenreDTO = roboconGenreTransformer.transformEntityToDTO(roboconGenreEB);
			logger.debug("Robocon Genre {} has been created successfully", roboconGenreDTO.pk());
			return roboconGenreDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Genre creation exception due to constraint {} violation.\n Message: {}, \n Message: {}", ex.getConstraintName(), message, ex.getCause().getMessage());
			String msg = "Cannot create robocon genre due to " + message;
			throw new RoboconGenreCreationException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Genre creation error due to {}.", ex.getMessage());
			throw new RoboconGenreCreationException(ex.getMessage());
		}
	}

	@Override
	public RoboconGenreDTO updateGenre(RoboconGenreDTO roboconGenreDTO) {
		logger.debug("Updating Robocon Genre ({})", roboconGenreDTO.pk());
		RoboconGenreEB roboconGenreEB = null;
		try{
			roboconGenreEB = updateEntity(roboconGenreDTO);
			roboconGenreDTO = roboconGenreTransformer.transformEntityToDTO(roboconGenreEB);
			logger.debug("Robocon Genre {} has been updated successfully", roboconGenreDTO.pk());
			return roboconGenreDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Genre update exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot update robocon genre due to " + message;
			throw new RoboconGenreUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Genre update error due to {}.", ex.getMessage());
			throw new RoboconGenreUpdateException(ex.getMessage());
		}
	}

	@Override
	public RoboconGenreDTO addGenreMeta(RoboconMetaDTO roboconMetaDTO, Long genreId) {
		logger.debug("Adding Meta {} to Genre {}.", DumpUtil.dumpAsJson(roboconMetaDTO), genreId);
		RoboconGenreEB roboconGenreEB = null;
		try{
			roboconBeanValidationManager.validate(roboconMetaDTO);
			roboconGenreEB = getRoboconGenreIfExists(genreId);
			RoboconGenreMetaEB roboconGenreMetaEB = roboconGenreMetaTransformer.transformDTOToEntity(roboconMetaDTO);
			roboconGenreEB.addRoboconGenreMetaEB(roboconGenreMetaEB);
			roboconGenreDAO.flush();
			roboconGenreDAO.refresh(roboconGenreEB);
			return roboconGenreTransformer.transformEntityToDTO(roboconGenreEB);
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Genre Meta update exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot update robocon genre meta due to " + message;
			throw new RoboconGenreUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Genre Meta update error due to {}.", ex.getMessage());
			throw new RoboconGenreUpdateException(ex.getMessage());
		}
	}
	
	@Override
	public RoboconGenreDTO fetchGenre(Long genreId) {
		logger.debug("Fetching Robocon Genre ({})", genreId);
		RoboconGenreEB roboconGenreEB = getRoboconGenreIfExists(genreId);
		return roboconGenreTransformer.transformEntityToDTO(roboconGenreEB);
	}

	@Override
	public List<RoboconGenreDTO> fetchGenresByPaging(Integer pageNum, Integer pageLimit, HttpServletResponse response) {
		logger.debug("Fetching Robocon Genres by paging for page number ({}) and limit ({})", pageNum, pageLimit);
		RoboconGenreCriteriaFilter cf = new RoboconGenreCriteriaFilter();
		RoboconGenreSearchCriteriaFilter cfs = new RoboconGenreSearchCriteriaFilter();
		Integer codeFrom = ControlFieldsHelper.getIntFieldValue(CODE_FROM);
		Integer codeTo = ControlFieldsHelper.getIntFieldValue(CODE_TO);
		String keyword = ControlFieldsHelper.getStrFieldValue(KEYWORD);
		Integer songId = ControlFieldsHelper.getIntFieldValue(SONG_ID);
		Integer albumId = ControlFieldsHelper.getIntFieldValue(ALBUM_ID);
		
		try {
			if(keyword != null && !keyword.isEmpty()){
				keyword = URLDecoder.decode(keyword, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot decode keyword:{}, error {}", keyword, e);
		}
		
		/*if(keyword != null && !keyword.isEmpty()){
			FullTextSession fullTextSession = Search.getFullTextSession(roboconGenreDAO.getSession());
			fullTextSession.setCacheMode(CacheMode.IGNORE);
			QueryBuilder qb = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(RoboconGenreMetaEB.class).get();
			org.apache.lucene.search.Query luceneQuery = qb.keyword().onField("name").matching(keyword).createQuery();
			FullTextQuery hibQuery = fullTextSession.createFullTextQuery(luceneQuery, RoboconGenreMetaEB.class);
			@SuppressWarnings("unchecked")
			List<RoboconGenreMetaEB> meta = hibQuery.list();
			logger.debug("******* Items *******:{}", meta);
			for(RoboconGenreMetaEB genreMeta : meta){
				RoboconGenreEB genre = genreMeta.getRoboconGenreEB();
				logger.debug("******* Genre *******:{}", genre);
			}
		}*/
		
		/*
		FullTextSession fullTextSession = Search.getFullTextSession(roboconGenreDAO.getSession());
		fullTextSession.setCacheMode(CacheMode.IGNORE);
		QueryBuilder qb = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(RoboconGenreEB.class).get();
		List<BooleanQuery> booleanQueries = new ArrayList<BooleanQuery>();
		if(keyword != null && !keyword.isEmpty()){
			BooleanQuery BooleanQuery = new BooleanQuery();
			Query kQuery = qb
					  .keyword().boostedTo(5f)
					  .fuzzy()
					  .withThreshold(0.8f)
					  .withPrefixLength(1)
					  .onField("roboconGenreMetaEB.name")
					  .matching(keyword)
					  .createQuery();
			BooleanQuery.add(kQuery, BooleanClause.Occur.MUST);
			booleanQueries.add(BooleanQuery);
		}
		Query query = null;
		if(!booleanQueries.isEmpty()){
			BooleanQuery[] queries = new BooleanQuery[booleanQueries.size()];
			for(int i=0; i<booleanQueries.size(); i++){
				queries[i] = booleanQueries.get(i);
			}
			query = Query.mergeBooleanQueries(queries);
		}else{
			query = qb.all().createQuery();
		}
		FullTextQuery hibQuery = fullTextSession.createFullTextQuery(query, RoboconGenreEB.class);
		hibQuery.setFirstResult(0);
		hibQuery.setMaxResults(10);
		logger.debug("******* Query Text *******:{}", query.toString());
		@SuppressWarnings("unchecked")
		List<Object[]> result = hibQuery.setProjection( ProjectionConstants.THIS, ProjectionConstants.SCORE ).list();
		result.isEmpty();*/
		
		cfs.orderByGenreId(true);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cfs.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cfs.setMaxResult(pageLimit);
		}
		if(keyword != null && !keyword.isEmpty()){
			cf.setGenreName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
			cfs.setGenreName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
		}else{
			roboconGenreDAO.enableMetaLangFilter();
		}
		if(codeFrom != null && codeTo != null){
			cf.setGenreId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
			cfs.setGenreId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
		}else if(codeFrom != null){
			cf.setGenreId(Long.valueOf(codeFrom));
			cfs.setGenreId(Long.valueOf(codeFrom));
		}else if(codeTo != null){
			cf.setGenreId(Long.valueOf(codeTo));
			cfs.setGenreId(Long.valueOf(codeTo));
		}
		if(songId != null){
			cf.setSongId(Long.valueOf(songId));
			cfs.setSongId(Long.valueOf(songId));
		}
		if(albumId != null){
			cf.setAlbumId(Long.valueOf(albumId));
			cfs.setAlbumId(Long.valueOf(albumId));
		}
		List<RoboconGenreEB> roboconGenresEB = roboconGenreDAO.getEntityListByCriteria(cfs);
		int count = roboconGenreDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		List<RoboconGenreDTO> genres = roboconGenreTransformer.transformEntityToDTO(roboconGenresEB);
		return genres;
	}

	public List<RoboconMetaDTO> searchGenres(Integer pageNum, Integer pageLimit, HttpServletResponse response){
		
		return null;
	}
	
	@Override
	public void removeGenre(Long genreId){
		logger.debug("Try removing robocon genre with id ({})", genreId);
		RoboconGenreCriteriaFilter cf = new RoboconGenreCriteriaFilter();
		cf.setGenreId(genreId);
		RoboconGenreEB roboconGenreEB = getRoboconGenreIfExists(genreId);
		if(roboconGenreEB.getRoboconSongsEB() != null && !roboconGenreEB.getRoboconSongsEB().isEmpty()){
			logger.error("Error, Cannot remove robocon genre ({}) as many songs depends on it.", genreId);
			String msg = "Cannot remove robocon genre ("+genreId+") as many songs depends on it exception.";
			throw new RoboconGenreException(msg);
		}
		roboconGenreDAO.deleteEntity(roboconGenreEB);
	}
	
	private RoboconGenreEB getRoboconGenreIfExists(Long genreId){
		RoboconGenreEB roboconGenreEB = roboconGenreDAO.getEntityByIDIfExists(genreId);
		if(roboconGenreEB == null){
			logger.error("Error: No Genre found with id ({})", genreId);
			String msg = "No Genre found with id (" + genreId + ")";
			throw new RoboconGenreNotFoundException(msg);
		}
		return roboconGenreEB;
	}
	
	@Override
	protected void doBeforeValidate(RoboconGenreDTO dto, boolean isNew) {
		roboconBeanValidationManager.validate(dto);
		super.doBeforeValidate(dto, isNew);
	}
	
	@Override
	protected void doBeforePersist(RoboconGenreEB roboconGenreEB, RoboconGenreDTO roboconGenreDTO, boolean isNew) {
		if(roboconGenreEB.getRoboconGenreMetaEB() != null && !roboconGenreEB.getRoboconGenreMetaEB().isEmpty()){
			for(RoboconGenreMetaEB roboconGenreMetaEB : roboconGenreEB.getRoboconGenreMetaEB()){
				if(roboconGenreMetaEB.getRoboconGenreEB() == null){
					roboconGenreMetaEB.setRoboconGenreEB(roboconGenreEB);
				}
			}
		}
		super.doBeforePersist(roboconGenreEB, roboconGenreDTO, isNew);
	}
	
	@Override
	protected void doAfterPersist(RoboconGenreEB entity, RoboconGenreDTO dto, boolean isNew) {
		roboconGenreDAO.flush();
		roboconGenreDAO.refresh(entity);
		super.doAfterPersist(entity, dto, isNew);
	}
	
	@Override
	protected DTOTransformer<RoboconGenreEB, RoboconGenreDTO> getTransformer() {
		return roboconGenreTransformer;
	}

	@Override
	protected DTOTransformer<RoboconGenreEB, RoboconGenreDTO> getSummaryTransformer() {
		return roboconGenreTransformer;
	}

	@Override
	protected DAO<RoboconGenreEB, Long, RoboconGenreCriteriaFilter> getDAO() {
		return roboconGenreDAO;
	}

}
