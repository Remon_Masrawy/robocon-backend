package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class CountryCreationException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;

	public static final String ERROR_MSG = "Cannot create country error.";
	
	public CountryCreationException(){
		super(ERROR_MSG);
	}
	
	public CountryCreationException(String msg){
		super(msg);
	}
	
	@Override
	public int getErrorCode() {
		return RoboconErrorConstants.ROBOCON_CREATION_ERROR;
	}
}
