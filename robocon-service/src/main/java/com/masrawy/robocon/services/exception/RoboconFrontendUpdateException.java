package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class RoboconFrontendUpdateException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	public static final String ERROR_MSG = "Cannot update robocon frontend exception.";
	
	public RoboconFrontendUpdateException(){
		super(ERROR_MSG);
	}
	
	public RoboconFrontendUpdateException(String msg){
		super(msg);
	}
	
	@Override
	public int getErrorCode() {
		return RoboconErrorConstants.ROBOCON_UPDATE_ERROR;
	}

}
