package com.masrawy.robocon.services.exception;

import static com.masrawy.robocon.services.exception.RoboconErrorConstants.ROBOCON_INVALID_REQUEST;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class RoboconAlbumNotFoundException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	public static final String ERROR_MSG = "Robocon Album not found Exception occurred.";
	
	public RoboconAlbumNotFoundException(){
		super(ERROR_MSG);
	}
	
	public RoboconAlbumNotFoundException(String msg){
		super(msg);
	}
	
	@Override
	public int getErrorCode() {
		return ROBOCON_INVALID_REQUEST;
	}

}
