package com.masrawy.robocon.services;

import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.dto.model.DTO;
import com.masrawy.robocon.file.model.FileUpload;

public interface RoboconUploadService {

	public DTO<?> uploadFile(FileUpload fileUpload, Long id);
	public DTO<?> uploadFile(MultipartFile file, Long id, Integer type);
}
