package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconSongDTO;
import com.masrawy.robocon.dto.RoboconSongKaraokeDTO;
import com.masrawy.robocon.dto.RoboconUserSingDTO;
import com.masrawy.robocon.model.RoboconSongEB;
import com.masrawy.robocon.query.RoboconSongCriteriaFilter;

public interface RoboconSongService extends CrudService<RoboconSongDTO, RoboconSongDTO, Long, RoboconSongCriteriaFilter>{

	public RoboconSongDTO createRoboconSong(RoboconSongDTO roboconSongDTO);
	public RoboconSongDTO updateRoboconSong(RoboconSongDTO roboconSongDTO);
	
	public RoboconSongEB getRoboconSongById(Long songId);
	public Object getRoboconSong(Long songId);
	public Object fetchRoboconSongs(Integer pageNum, Integer pageLimit, HttpServletResponse response);
	
	public RoboconSongDTO uploadRoboconSongContent(MultipartFile multipartFile, Long songId);
	public RoboconSongDTO uploadRoboconSongKaraoke(MultipartFile multipartFile, Long songId);
	public RoboconSongDTO uploadRoboconSongLyrics(MultipartFile multipartFile, Long songId);
	public RoboconSongDTO uploadRoboconSongPhoto(MultipartFile file, Long songId);
	public RoboconSongDTO uploadRoboconSongPhotos(List<MultipartFile> files, Long songId);
	public RoboconUserSingDTO uploadRoboconUserSing(MultipartFile multipartFile, Long songId);
	
	public List<RoboconSongKaraokeDTO> fetchRoboconSongKaraoke(Integer pageNum, Integer pageLimit);
	public List<RoboconSongKaraokeDTO> filterRoboconSongKaraoke(Long categoryId, Integer pageNum, Integer pageLimit);
	
	public List<RoboconUserSingDTO> fetchRoboconUserSing(Integer pageNum, Integer pageLimit, boolean verified);
	
	public void playSong(Long contentId, HttpServletRequest request, HttpServletResponse response);
	public void downloadSong(Long contentId, HttpServletRequest request, HttpServletResponse response);
	
	public void downloadKaraoke(Long karokeId, HttpServletRequest request, HttpServletResponse response);
	public void downloadLyrics(Long lyricsId, HttpServletRequest request, HttpServletResponse response);
	
	public void playUserSing(Long singId, HttpServletRequest request, HttpServletResponse response);
	public void downloadUserSing(Long singId, HttpServletRequest request, HttpServletResponse response);
	
	public void printLyrics(Long lyricsId, HttpServletRequest request, HttpServletResponse response);
	
	public void removeRoboconSong(Long songId);
	public RoboconSongDTO removeContent(Long contentId);
	public RoboconSongDTO removeKaraoke(Long karaokeId);
	public RoboconSongDTO removeLyrics(Long lyricsId);
	public void removeUserSing(Long singId);
	
	public void verifyUserSing(Long singId);
}
