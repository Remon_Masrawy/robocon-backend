package com.masrawy.robocon.services.exception;

import static com.masrawy.robocon.services.exception.RoboconErrorConstants.ROBOCON_INVALID_REQUEST;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class RoboconFrontendNotFoundException extends BaseRuntimeException implements ClientError{

private static final long serialVersionUID = 1L;
	
	public static final String ERROR_MSG = "Robocon Frontend not found Exception occurred.";
	
	public RoboconFrontendNotFoundException(){
		super(ERROR_MSG);
	}
	
	public RoboconFrontendNotFoundException(String msg){
		super(msg);
	}
	
	@Override
	public int getErrorCode() {
		return ROBOCON_INVALID_REQUEST;
	}

}
