package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

import static com.masrawy.robocon.services.exception.RoboconErrorConstants.ROBOCON_USER_NOT_FOUND_ERROR;

public class RoboconUserNotFoundException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	private static final String ERROR_MSG = "User not found exception occured.";
	
	public RoboconUserNotFoundException(){
		super(ERROR_MSG);
	}
	
	public RoboconUserNotFoundException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return ROBOCON_USER_NOT_FOUND_ERROR;
	}
	
	
}
