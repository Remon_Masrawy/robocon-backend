package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconCategoryDTO;
import com.masrawy.robocon.query.RoboconCategoryCriteriaFilter;

public interface RoboconCategoryService extends CrudService<RoboconCategoryDTO, RoboconCategoryDTO, Long, RoboconCategoryCriteriaFilter>{

	public RoboconCategoryDTO createCategory(RoboconCategoryDTO roboconCategoryDTO);
	
	public RoboconCategoryDTO updateCategory(RoboconCategoryDTO roboconCategoryDTO);
	
	public RoboconCategoryDTO getCategory(Long categoryId);
	
	public List<RoboconCategoryDTO> listCategories(Integer pageNum, Integer pageLimit, HttpServletResponse response);
	
	public void removeCategory(Long categoryId);
}
