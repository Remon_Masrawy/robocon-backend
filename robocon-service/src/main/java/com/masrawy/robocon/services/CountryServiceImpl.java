package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.KEYWORD;
import static com.masrawy.robocon.model.RoboconConstants.SPECIAL_CHARACTERS_PATTERN;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.dao.CountryDAO;
import com.masrawy.robocon.dto.CountryDTO;
import com.masrawy.robocon.model.CountryEB;
import com.masrawy.robocon.query.CountryCriteriaFilter;
import com.masrawy.robocon.services.exception.CountryCreationException;
import com.masrawy.robocon.services.exception.CountryUpdateException;
import com.masrawy.robocon.transformer.CountryTransformer;
import com.masrawy.robocon.validation.RoboconBeanValidationManager;

@Service("countryService")
@Transactional
public class CountryServiceImpl extends AbstractCrudService<CountryEB, CountryDTO, CountryDTO, Long, CountryCriteriaFilter> implements CountryService{

	private static final Logger logger = LoggerFactory.getLogger(CountryServiceImpl.class);
	
	@Autowired
	private CountryDAO countryDAO;
	
	@Autowired
	private CountryTransformer countryTransformer;
	
	@Autowired
	private RoboconMessageService roboconMessageService;
	
	@Autowired
	private RoboconBeanValidationManager roboconBeanValidationManager;

	@Override
	public CountryDTO createCountry(CountryDTO countryDTO) {
		logger.debug("Creating Country ({})", countryDTO.getName());
		try{
			CountryEB  countryEB = createEntity(countryDTO);
			logger.debug("Country {} created successfully.", countryDTO.getName());
			return countryTransformer.transformEntityToDTO(countryEB);
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Robocon Country creation exception due to constraint {} violation.\n Message: {}.", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Robocon Operator Country exception due to " + message;
			throw new CountryCreationException(msg);
		}catch(Exception e){
			String msg = "Error creating country due to: " + e.getMessage();
			logger.error("Error creating country due to: {}", e);
			throw new CountryCreationException(msg);
		}
	}

	@Override
	public CountryDTO updateCountry(CountryDTO countryDTO) {
		logger.debug("Updating Country ({})", countryDTO.getName());
		try{
			CountryEB  countryEB = updateEntity(countryDTO);
			logger.debug("Country {} updated successfully.", countryDTO.getName());
			return countryTransformer.transformEntityToDTO(countryEB);
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Robocon Country update exception due to constraint {} violation.\n Message: {}.", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Robocon Country update exception due to " + message;
			throw new CountryUpdateException(msg);
		}catch(Exception e){
			String msg = "Error updating country due to: " + e.getMessage();
			logger.error("Error updating country due to: {}", e);
			throw new CountryUpdateException(msg);
		}
	}

	@Override
	public List<CountryDTO> getCountries(Integer pageNum, Integer pageLimit, HttpServletResponse response) {
		logger.debug("Fetching Countries.");
		CountryCriteriaFilter cf = new CountryCriteriaFilter();
		String keyword = ControlFieldsHelper.getStrFieldValue(KEYWORD);
		try {
			if(keyword != null && !keyword.isEmpty()){
				keyword = URLDecoder.decode(keyword, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot decode keyword:{}, error {}", keyword, e);
		}
		if(keyword != null && !keyword.isEmpty()){
			cf.setCountryName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
		}
		cf.orderByCountryId(true);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
		List<CountryEB> countriesList = countryDAO.getEntityListByCriteria(cf);
		int count = countryDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		List<CountryDTO> countriesDTOList = countryTransformer.transformEntityToDTO(countriesList);
		return countriesDTOList;
	}

	@Override
	protected void doBeforeValidate(CountryDTO dto, boolean isNew) {
		roboconBeanValidationManager.validate(dto);
		super.doBeforeValidate(dto, isNew);
	}
	
	@Override
	protected void doAfterPersist(CountryEB entity, CountryDTO dto, boolean isNew) {
		countryDAO.flush();
		countryDAO.refresh(entity);
		super.doAfterPersist(entity, dto, isNew);
	}
	
	@Override
	protected DTOTransformer<CountryEB, CountryDTO> getTransformer() {
		return countryTransformer;
	}

	@Override
	protected DTOTransformer<CountryEB, CountryDTO> getSummaryTransformer() {
		return countryTransformer;
	}

	@Override
	protected DAO<CountryEB, Long, CountryCriteriaFilter> getDAO() {
		return countryDAO;
	}
}
