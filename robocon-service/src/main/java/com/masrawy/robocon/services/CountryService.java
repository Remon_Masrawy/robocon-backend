package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.CountryDTO;
import com.masrawy.robocon.query.CountryCriteriaFilter;

public interface CountryService extends CrudService<CountryDTO, CountryDTO, Long, CountryCriteriaFilter>{

	public CountryDTO createCountry(CountryDTO countryDTO);
	public CountryDTO updateCountry(CountryDTO countryDTO);
	public List<CountryDTO> getCountries(Integer pageNum, Integer pageLimit, HttpServletResponse response);
}
