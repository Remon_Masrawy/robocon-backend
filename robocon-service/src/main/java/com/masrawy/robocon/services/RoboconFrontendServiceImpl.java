package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.KEYWORD;
import static com.masrawy.robocon.model.RoboconConstants.OPERATOR_ID;
import static com.masrawy.robocon.model.RoboconConstants.OPERATOR_MCC;
import static com.masrawy.robocon.model.RoboconConstants.OPERATOR_MNC;
import static com.masrawy.robocon.model.RoboconConstants.SPECIAL_CHARACTERS_PATTERN;
import static com.masrawy.robocon.model.RoboconConstants.STOREFRONT;
import static com.masrawy.robocon.model.RoboconConstants.CODE_FROM;
import static com.masrawy.robocon.model.RoboconConstants.CODE_TO;
import static com.masrawy.robocon.model.RoboconConstants.COUNTRY;
import static com.masrawy.robocon.model.RoboconConstants.TYPE;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.dao.RoboconFrontendDAO;
import com.masrawy.robocon.dto.RoboconFrontendDTO;
import com.masrawy.robocon.model.RoboconFrontendEB;
import com.masrawy.robocon.model.RoboconFrontendMetaEB;
import com.masrawy.robocon.model.RoboconFrontendTypeEnum;
import com.masrawy.robocon.query.RoboconFrontendCriteriaFilter;
import com.masrawy.robocon.query.RoboconFrontendSearchCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconFrontendCreationException;
import com.masrawy.robocon.services.exception.RoboconFrontendNotFoundException;
import com.masrawy.robocon.services.exception.RoboconFrontendUpdateException;
import com.masrawy.robocon.transformer.RoboconFrontendTransformer;
import com.masrawy.robocon.validation.RoboconBeanValidationManager;
import com.maxmind.geoip.Location;

@Service("roboconFrontendService")
@Transactional
public class RoboconFrontendServiceImpl extends AbstractCrudService<RoboconFrontendEB, RoboconFrontendDTO, RoboconFrontendDTO, Long, RoboconFrontendCriteriaFilter> implements RoboconFrontendService{

	private static final Logger logger = LoggerFactory.getLogger(RoboconFrontendServiceImpl.class);
	
	@Autowired
	private RoboconFrontendTransformer roboconFrontendTransformer;
	
	@Autowired
	private LocationService locationService;
	
	@Autowired
	private RoboconFrontendDAO roboconFrontendDAO;
	
	@Autowired
	private RoboconMessageService roboconMessageService;
	
	@Autowired
	private RoboconBeanValidationManager roboconBeanValidationManager;
	
	@Override
	public RoboconFrontendDTO createFrontend(RoboconFrontendDTO roboconFrontendDTO) {
		logger.debug("Creating Robocon Frontend");
		RoboconFrontendEB roboconFrontendEB = null;
		try{
			roboconFrontendEB = createEntity(roboconFrontendDTO);
			roboconFrontendDTO = roboconFrontendTransformer.transformEntityToDTO(roboconFrontendEB);
			logger.debug("Robocon frontend {} has been created successfully", roboconFrontendDTO.pk());
			return roboconFrontendDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Frontend creation exception due to constraint {} violation.\n Message: {}, \n Message: {}", ex.getConstraintName(), message, ex.getCause().getMessage());
			String msg = "Cannot create robocon frontend due to " + message;
			throw new RoboconFrontendCreationException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Fronend creation error due to {}.", ex.getMessage());
			throw new RoboconFrontendCreationException(ex.getMessage());
		}
	}

	@Override
	public RoboconFrontendDTO updateFrontend(RoboconFrontendDTO roboconFrontendDTO) {
		logger.debug("Updating Robocon Frontend ({})", roboconFrontendDTO.pk());
		RoboconFrontendEB roboconFrontendEB = null;
		try{
			roboconFrontendEB = updateEntity(roboconFrontendDTO);
			roboconFrontendDTO = roboconFrontendTransformer.transformEntityToDTO(roboconFrontendEB);
			logger.debug("Robocon Frontend {} has been updated successfully", roboconFrontendDTO.pk());
			return roboconFrontendDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Frontend update exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot update robocon frontend due to " + message;
			throw new RoboconFrontendUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Frontend update error due to {}.", ex.getMessage());
			throw new RoboconFrontendUpdateException(ex.getMessage());
		}
	}

	@Override
	public RoboconFrontendDTO getFrontend(Long frontendId) {
		logger.debug("Fetching Robocon Frontend ({})", frontendId);
		RoboconFrontendEB roboconFrontendEB = getRoboconFrontendIfExists(frontendId);
		return roboconFrontendTransformer.transformEntityToDTO(roboconFrontendEB);
	}
	
	@Override
	public List<RoboconFrontendDTO> getFrontends(HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Fetching robocon frontend");
		RoboconFrontendCriteriaFilter cf = new RoboconFrontendCriteriaFilter();
		String mcc = ControlFieldsHelper.getStrFieldValue(OPERATOR_MCC);
		String mnc = ControlFieldsHelper.getStrFieldValue(OPERATOR_MNC);
		String storefront = ControlFieldsHelper.getStrFieldValue(STOREFRONT);
		Location location = locationService.getLocation(request);
		
		if(mcc != null && !mcc.isEmpty() && mnc != null && !mnc.isEmpty()){
			cf.setOperator(mcc, mnc);
		}else{
			cf.isEmptyOperator(true);
		}
		
		if(storefront != null && !storefront.isEmpty()){
			cf.setStorefrontName(storefront);
		}else{
			cf.isEmptyStorefront(true);
		}
		
		if(location != null && location.countryCode != null && !location.countryCode.isEmpty()){
			cf.setCountryCode(location.countryCode);
		}else{
			cf.setEmptyCountry(true);
		}
		
		cf.orderByFrontendId(true);
		roboconFrontendDAO.enableMetaLangFilter();
		int count = roboconFrontendDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		return getRecordsByCriteria(cf);
	}
	
	@Override
	public List<RoboconFrontendDTO> getFrontends(Integer pageNum, Integer pageLimit, HttpServletResponse response) {
		logger.debug("Fetching robocon frontend");
		RoboconFrontendCriteriaFilter cf = new RoboconFrontendCriteriaFilter();
		RoboconFrontendSearchCriteriaFilter cfs = new RoboconFrontendSearchCriteriaFilter();
		Integer codeFrom = ControlFieldsHelper.getIntFieldValue(CODE_FROM);
		Integer codeTo = ControlFieldsHelper.getIntFieldValue(CODE_TO);
		String keyword = ControlFieldsHelper.getStrFieldValue(KEYWORD);
		String mcc = ControlFieldsHelper.getStrFieldValue(OPERATOR_MCC);
		String mnc = ControlFieldsHelper.getStrFieldValue(OPERATOR_MNC);
		Integer operatorId = ControlFieldsHelper.getIntFieldValue(OPERATOR_ID);
		String storefront = ControlFieldsHelper.getStrFieldValue(STOREFRONT);
		String country = ControlFieldsHelper.getStrFieldValue(COUNTRY);
		String type = ControlFieldsHelper.getStrFieldValue(TYPE);
		
		try {
			if(keyword != null && !keyword.isEmpty()){
				keyword = URLDecoder.decode(keyword, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot decode keyword:{}, error {}", keyword, e);
		}
		
		if(operatorId != null){
			cf.setOperatorId(operatorId.longValue());
			cfs.setOperatorId(operatorId.longValue());
		}
		
		if(mcc != null && !mcc.isEmpty() && mnc != null && !mnc.isEmpty()){
			cf.setOperatorOnly(mcc, mnc);
			cfs.setOperatorOnly(mcc, mnc);
		}
		
		if(storefront != null && !storefront.isEmpty()){
			cf.setStorefrontNameOnly(storefront);
			cfs.setStorefrontNameOnly(storefront);
		}
		
		if(country != null && !country.isEmpty()){
			cf.setCountryCodeOnly(country);
			cfs.setCountryCodeOnly(country);
		}
		
		if(type != null && !type.isEmpty()){
			cf.setFrontendType(type);
			cfs.setFrontendType(type);
		}
		
		if(codeFrom != null && codeTo != null){
			cf.setFrontendId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
			cfs.setFrontendId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
		}else if(codeFrom != null){
			cf.setFrontendId(Long.valueOf(codeFrom));
			cfs.setFrontendId(Long.valueOf(codeFrom));
		}else if(codeTo != null){
			cf.setFrontendId(Long.valueOf(codeTo));
			cfs.setFrontendId(Long.valueOf(codeTo));
		}
		
		cfs.orderByFrontendId(true);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cfs.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cfs.setMaxResult(pageLimit);
		}
		if(keyword != null && !keyword.isEmpty()){
			roboconFrontendDAO.enableMetaLangSearchFilter();
			cf.setFrontendName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
			cfs.setFrontendName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
		}else{
			roboconFrontendDAO.enableMetaLangFilter();
		}
		int count = roboconFrontendDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		List<RoboconFrontendEB> roboconFrontendsEB = roboconFrontendDAO.getEntityListByCriteria(cfs);
		List<RoboconFrontendDTO> roboconFrontendsDTO = roboconFrontendTransformer.transformEntityToDTO(roboconFrontendsEB);
		return roboconFrontendsDTO;
	}
	
	@Override
	public Map<String, String> getFrontendTypes(){
		Map<String, String> types = new HashMap<String, String>();
		for(RoboconFrontendTypeEnum roboconFrontendTypeEnum : RoboconFrontendTypeEnum.values()){
			types.put(roboconFrontendTypeEnum.name(), roboconFrontendTypeEnum.getStrValue());
		}
		return types;
	}
	
	private RoboconFrontendEB getRoboconFrontendIfExists(Long frontendId){
		RoboconFrontendEB roboconFrontendEB = roboconFrontendDAO.getEntityByIDIfExists(frontendId);
		if(roboconFrontendEB == null){
			logger.error("Error: No Frontend found with id ({})", frontendId);
			String msg = "No Frontend found with id (" + frontendId + ")";
			throw new RoboconFrontendNotFoundException(msg);
		}
		return roboconFrontendEB;
	}
	
	@Override
	public void removeFrontend(Long frontendId) {
		logger.debug("Removing robocon frontend ({})", frontendId);
		roboconFrontendDAO.deleteEntity(frontendId);
		logger.debug("Robocon frontend {} has been removed successfully.", frontendId);
	}
	
	@Override
	protected void doBeforeValidate(RoboconFrontendDTO dto, boolean isNew) {
		roboconBeanValidationManager.validate(dto);
		super.doBeforeValidate(dto, isNew);
	}
	
	@Override
	protected void doBeforePersist(RoboconFrontendEB entity, RoboconFrontendDTO dto, boolean isNew) {
		for(RoboconFrontendMetaEB roboconFrontendMetaEB : entity.getRoboconFrontendMetaEB()){
			if(roboconFrontendMetaEB.getRoboconFrontendEB() == null){
				roboconFrontendMetaEB.setRoboconFrontendEB(entity);
			}
		}
//		for(RoboconSongEB roboconSongEB : entity.getRoboconSongsEB()){
//			if(!roboconSongEB.getRoboconFrontendsEB().contains(entity)){
//				roboconSongEB.addRoboconFrontendEB(entity);
//			}
//		}
//		for(RoboconAlbumEB roboconAlbumEB : entity.getRoboconAlbumsEB()){
//			if(!roboconAlbumEB.getRoboconFrontendsEB().contains(entity)){
//				roboconAlbumEB.addRoboconFrontendEB(entity);
//			}
//		}
		super.doBeforePersist(entity, dto, isNew);
	}
	
	@Override
	protected void doAfterPersist(RoboconFrontendEB entity, RoboconFrontendDTO dto, boolean isNew) {
		roboconFrontendDAO.flush();
		roboconFrontendDAO.refresh(entity);
		super.doAfterPersist(entity, dto, isNew);
	}
	
	@Override
	protected DTOTransformer<RoboconFrontendEB, RoboconFrontendDTO> getTransformer() {
		return roboconFrontendTransformer;
	}

	@Override
	protected DTOTransformer<RoboconFrontendEB, RoboconFrontendDTO> getSummaryTransformer() {
		return roboconFrontendTransformer;
	}

	@Override
	protected DAO<RoboconFrontendEB, Long, RoboconFrontendCriteriaFilter> getDAO() {
		return roboconFrontendDAO;
	}
}
