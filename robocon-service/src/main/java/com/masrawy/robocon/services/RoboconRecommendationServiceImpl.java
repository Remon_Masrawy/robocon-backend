package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.USER_ID;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
/*import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.masrawy.robocon.dao.RoboconSongDAO;
import com.masrawy.robocon.dto.RoboconSongDTO;
import com.masrawy.robocon.model.RoboconSongEB;
import com.masrawy.robocon.query.RoboconSongCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconInvalidUserIdException;
import com.masrawy.robocon.services.exception.RoboconSongRecommendationException;
import com.masrawy.robocon.transformer.RoboconSongTransformer;

@Transactional
@Service("roboconRecommendationService")
public class RoboconRecommendationServiceImpl implements RoboconRecommendationService{

	private static final Logger logger = LoggerFactory.getLogger(RoboconRecommendationServiceImpl.class);
	
	/*@Autowired
	private Recommender recommender;*/
	
	@Autowired
	private RoboconSongDAO roboconSongDAO;
	
	@Autowired
	private RoboconSongTransformer roboconSongTransformer;
	
	private static final Integer NUM_OF_RECOMMENDATIONS = 5;
	
	@Override
	public List<RoboconSongDTO> recommend(){
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		/*try{
			List<RoboconSongDTO> songs = new ArrayList<RoboconSongDTO>();
			List<RecommendedItem> recommendations = recommender.recommend(NumberUtils.toLong(userIdStr), NUM_OF_RECOMMENDATIONS);
			if(recommendations != null && !recommendations.isEmpty()){
				RoboconSongCriteriaFilter cf = new RoboconSongCriteriaFilter();
				List<Long> ids = new ArrayList<Long>();
				for (RecommendedItem recommendation : recommendations) {
					ids.add(recommendation.getItemID());
				}
				cf.setSongId(ids);
				List<RoboconSongEB> songsEB = roboconSongDAO.getEntityListByCriteria(cf);
				songs = roboconSongTransformer.transformEntityToDTO(songsEB);
			}
			return songs;
		}catch(TasteException e){
			logger.error("Error Occured.", e);
			throw new RoboconSongRecommendationException();
		}finally{
			logger.debug("Recommendation engine has been finished.");
		}*/
		return null;
	}
}
