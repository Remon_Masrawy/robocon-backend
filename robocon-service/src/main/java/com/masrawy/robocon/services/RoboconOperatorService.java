package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconOperatorDTO;
import com.masrawy.robocon.query.RoboconOperatorCriteriaFilter;

public interface RoboconOperatorService extends CrudService<RoboconOperatorDTO, RoboconOperatorDTO, Long, RoboconOperatorCriteriaFilter>{

	public RoboconOperatorDTO createOperator(RoboconOperatorDTO operatorDTO);
	public RoboconOperatorDTO updateOperator(RoboconOperatorDTO operatorDTO);
	public List<RoboconOperatorDTO> getOperators(Integer pageNum, Integer pageLimit, HttpServletResponse response);
}
