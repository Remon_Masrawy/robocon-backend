package com.masrawy.robocon.services;

import org.springframework.validation.FieldError;

public interface RoboconMessageService {

	public String getMessage(String code, String message);
	public String getMessage(String code, Object...args);
	public String getMessage(FieldError fieldError);
}
