package com.masrawy.robocon.services.exception;

import static com.masrawy.robocon.services.exception.RoboconErrorConstants.ROBOCON_INVALID_REQUEST;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class RoboconArtistNotFoundException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	public static final String ERROR_MSG = "Robocon Artist not found Exception occurred.";
	
	public RoboconArtistNotFoundException(){
		super(ERROR_MSG);
	}
	
	public RoboconArtistNotFoundException(String msg){
		super(msg);
	}
	
	@Override
	public int getErrorCode() {
		return ROBOCON_INVALID_REQUEST;
	}
}
