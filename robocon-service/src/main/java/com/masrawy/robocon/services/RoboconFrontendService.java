package com.masrawy.robocon.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconFrontendDTO;
import com.masrawy.robocon.query.RoboconFrontendCriteriaFilter;

public interface RoboconFrontendService extends CrudService<RoboconFrontendDTO, RoboconFrontendDTO, Long, RoboconFrontendCriteriaFilter>{

	public RoboconFrontendDTO createFrontend(RoboconFrontendDTO roboconFrontendDTO);
	public RoboconFrontendDTO updateFrontend(RoboconFrontendDTO roboconFrontendDTO);
	
	public RoboconFrontendDTO getFrontend(Long frontendId);
	
	public List<RoboconFrontendDTO> getFrontends(HttpServletRequest request, HttpServletResponse response);
	
	public List<RoboconFrontendDTO> getFrontends(Integer pageNum, Integer pageLimit, HttpServletResponse response);
	
	public Map<String, String> getFrontendTypes();
	
	public void removeFrontend(Long frontendId);
}
