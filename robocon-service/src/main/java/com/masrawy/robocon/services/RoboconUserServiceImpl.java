package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.file.model.FileEB;
import com.cdm.core.persistent.dao.DAO;
import com.cdm.core.security.SecurityUtil;
import com.cdm.core.security.model.AuthProviderEnum;
import com.cdm.core.security.model.LoginResult;
import com.cdm.core.security.model.UserEB;
import com.cdm.core.security.service.AuthTokenManager;
import com.cdm.core.security.service.RegistrationManager;
import com.cdm.core.security.service.UserManager;
import com.cdm.core.security.service.VerificationTokenManager;
import com.masrawy.robocon.dao.RoboconUserDAO;
import com.masrawy.robocon.dto.RoboconUserDTO;
import com.masrawy.robocon.model.RoboconUserEB;
import com.masrawy.robocon.query.RoboconUserCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconInvalidUserActionException;
import com.masrawy.robocon.services.exception.RoboconInvalidUserIdException;
import com.masrawy.robocon.services.exception.RoboconUserNotFoundException;
import com.masrawy.robocon.transformer.RoboconUserTransformer;

@Service("roboconUserService")
@Transactional
public class RoboconUserServiceImpl extends AbstractCrudService<RoboconUserEB, RoboconUserDTO, RoboconUserDTO, Long, RoboconUserCriteriaFilter> implements RoboconUserService{

	@Autowired
	private UserManager userManager;
	
	@Autowired
	private RegistrationManager manager;
	
	@Autowired
	private RoboconFileService roboconFileService;
	
	@Autowired
	private RoboconUserDAO roboconUserDAO;
	
	@Autowired
	private AuthTokenManager authTokenManager;
	
	@Autowired
	private VerificationTokenManager verificationTokenManager;

	@Autowired
	private RoboconUserTransformer roboconUserTransformer;
	
	@Override
	public RoboconUserDTO updateUser(RoboconUserDTO roboconUserDTO){
		UserEB userEB = userManager.getEntityIfExistById(roboconUserDTO.pk());
		if(userEB == null){
			throw notFoundUser(roboconUserDTO.pk());
		}
		RoboconUserEB roboconUserEB = roboconUserDAO.getEntityByIDIfExists(roboconUserDTO.pk());
		if(roboconUserEB == null){
			roboconUserEB = createUser(userEB);
		}
		if(!StringUtils.isEmpty(roboconUserDTO.getPassword())){
			userManager.changePassword(userEB, roboconUserDTO.getPassword());
			authTokenManager.expirAllUserTokens(userEB, AuthProviderEnum.LOCAL);
			verificationTokenManager.expirAllUserTokens(userEB);
		}
		getTransformer().transformDTOToEntity(roboconUserDTO, roboconUserEB);
		roboconUserDAO.updateEntity(roboconUserEB);
		return roboconUserTransformer.transformEntityToDTO(roboconUserEB);
	}
	
	@Override
	public RoboconUserDTO updateUserInfo(RoboconUserDTO roboconUserDTO){
		Long userId = SecurityUtil.getUserId();
		if(roboconUserDTO.pk() != userId){
			logger.error("Error: User {} Try to update user {} info", userId, roboconUserDTO.pk());
			throw new RoboconInvalidUserIdException();
		}
		roboconUserDTO.setIsActive(null);
		roboconUserDTO.setRoleCodes(null);
		return updateUser(roboconUserDTO);
	}
	
	@Override
	public RoboconUserDTO updateUserPhoto(MultipartFile multipartFile){
		Long userId = SecurityUtil.getUserId();
		FileEB fileEB = roboconFileService.uploadFileEB(multipartFile);
		UserEB userEB = userManager.updateUserPhoto(userId, fileEB);
		RoboconUserEB roboconUserEB = roboconUserDAO.getEntityByIDIfExists(userEB.getId());
		return roboconUserTransformer.transformEntityToDTO(roboconUserEB);
	}
	
	@Override
	public RoboconUserDTO changePassword(String oldPassword, String newPassword){
		Long userId = SecurityUtil.getUserId();
		logger.debug("Changing password for user {}", userId);
		LoginResult loginResult = manager.changeUserPassword(userId, oldPassword, newPassword);
		UserEB user = loginResult.getUser();
		RoboconUserEB roboconUserEB = roboconUserDAO.getEntityByIDIfExists(user.getId());
		if(roboconUserEB == null){
			roboconUserEB = createUser(user);
		}
		logger.debug("Password has been changed for User {}", roboconUserEB.getUser().getUsername());
		return roboconUserTransformer.transformEntityToDTO(roboconUserEB);
	}
	
	@Override
	public RoboconUserDTO inactiveUser(Long userId) {
		logger.debug("Inactivate User {}", userId);
		RoboconUserEB roboconUserEB = roboconUserDAO.getEntityByIDIfExists(userId);
		if(roboconUserEB == null){
			throw notFoundUser(userId);
		}
		roboconUserEB.setIsActive(false);
		logger.debug("User {} has been inactivated successfully.", roboconUserEB.getUser().getUsername());
		return getTransformer().transformEntityToDTO(roboconUserEB);
	}

	@Override
	public RoboconUserDTO activeUser(Long userId) {
		logger.debug("Activate User {}", userId);
		RoboconUserEB roboconUserEB = roboconUserDAO.getEntityByIDIfExists(userId);
		if(roboconUserEB == null){
			throw notFoundUser(userId);
		}
		roboconUserEB.setIsActive(true);
		logger.debug("User {} has been activated successfully.", roboconUserEB.getUser().getUsername());
		return getTransformer().transformEntityToDTO(roboconUserEB);
	}
	
	@Override
	public RoboconUserDTO getUser(){
		Long userId = SecurityUtil.getUserId();
		logger.debug("Retrieve Info for user {}", userId);
		UserEB userEB = userManager.getEntityById(userId);
		RoboconUserEB roboconUserEB = roboconUserDAO.getEntityByIDIfExists(userId);
		if(roboconUserEB == null){
			roboconUserEB = createUser(userEB);
		}
		return roboconUserTransformer.transformEntityToDTO(roboconUserEB);
	}
	
	@Override
	public RoboconUserDTO getUser(Long userId){
		logger.debug("Retrieve User {}", userId);
		UserEB userEB = userManager.getEntityIfExistById(userId);
		if(userEB == null){
			throw notFoundUser(userId);
		}
		RoboconUserEB roboconUserEB = roboconUserDAO.getEntityByIDIfExists(userId);
		if(roboconUserEB == null){
			roboconUserEB = createUser(userEB);
		}
		return roboconUserTransformer.transformEntityToDTO(roboconUserEB);
	}
	
	@Override
	public List<RoboconUserDTO> getUsers(Integer pageNum, Integer pageLimit, HttpServletResponse response){
		Long userId = SecurityUtil.getUserId();
		logger.debug("User {} list Users", userId);
		RoboconUserCriteriaFilter cf = new RoboconUserCriteriaFilter();
		cf.exclude(1L);
		cf.exclude(userId);
		cf.sortById(true);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
		List<RoboconUserEB> usersEB = roboconUserDAO.getEntityListByCriteria(cf);
		int count = roboconUserDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		return roboconUserTransformer.transformEntityToDTO(usersEB);
	}
	
	@Override
	public void deleteUser(Long userId){
		logger.debug("Deleting User {}", userId);
		Long uId = SecurityUtil.getUserId();
		if(userId.equals(1L) || userId.equals(2L)){
			logger.error("Error: User {} trying to delete user {} account, while this user is not allowable to be deleted.", uId, userId);
			throw new RoboconInvalidUserActionException("Trying to delete a non deleteable user");
		}
		UserEB userEB = userManager.getEntityIfExistById(userId);
		if(userEB == null){
			throw notFoundUser(userId);
		}
		RoboconUserEB roboconUserEB = roboconUserDAO.getEntityByIDIfExists(userId);
		if(roboconUserEB != null){
			roboconUserDAO.deleteEntity(roboconUserEB);
		}
		userEB.setVerified(false);
		verificationTokenManager.deleteAllUserTokens(userEB);
		authTokenManager.deleteAllUserTokens(userEB);
		userManager.deleteUser(userEB);
		logger.info("{} has been deleted successfully ", userEB);
	}
	
	private RoboconUserNotFoundException notFoundUser(Long userId){
		logger.error("Error: No User exists with id = {}", userId);
		String msg = "No User exists with id " + userId;
		return new RoboconUserNotFoundException(msg);
	}
	
	protected RoboconUserEB createUser(UserEB userEB){
		RoboconUserEB roboconUserEB = new RoboconUserEB();
		roboconUserEB.setUser(userEB);
		roboconUserDAO.createEntity(roboconUserEB);
		return roboconUserEB;
	}
	
	@Override
	protected DTOTransformer<RoboconUserEB, RoboconUserDTO> getTransformer() {
		return roboconUserTransformer;
	}

	@Override
	protected DTOTransformer<RoboconUserEB, RoboconUserDTO> getSummaryTransformer() {
		return roboconUserTransformer;
	}

	@Override
	protected DAO<RoboconUserEB, Long, RoboconUserCriteriaFilter> getDAO() {
		return roboconUserDAO;
	}

}
