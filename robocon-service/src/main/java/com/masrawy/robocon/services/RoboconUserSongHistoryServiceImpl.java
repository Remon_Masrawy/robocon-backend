package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.OPERATOR_MCC;
import static com.masrawy.robocon.model.RoboconConstants.OPERATOR_MNC;
import static com.masrawy.robocon.model.RoboconConstants.STOREFRONT;
import static com.masrawy.robocon.model.RoboconConstants.USER_ACTION;
import static com.masrawy.robocon.model.RoboconConstants.USER_ID;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.dao.RoboconUserSongHistoryDAO;
import com.masrawy.robocon.dto.RoboconUserSongHistoryDTO;
import com.masrawy.robocon.model.RoboconSongEB;
import com.masrawy.robocon.model.RoboconUserActionEnum;
import com.masrawy.robocon.model.RoboconUserSongHistoryEB;
import com.masrawy.robocon.model.RoboconUserSongHitEB;
import com.masrawy.robocon.query.RoboconUserSongHistoryCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconInvalidUserActionException;
import com.masrawy.robocon.services.exception.RoboconInvalidUserIdException;
import com.masrawy.robocon.services.exception.RoboconUserSongHistoryCreationException;
import com.masrawy.robocon.transformer.RoboconUserSongHistoryTransformer;
import com.maxmind.geoip.Location;

@Service("roboconUserSongHistoryService")
@Transactional
public class RoboconUserSongHistoryServiceImpl extends AbstractCrudService<RoboconUserSongHistoryEB, RoboconUserSongHistoryDTO, RoboconUserSongHistoryDTO, Long, RoboconUserSongHistoryCriteriaFilter> implements RoboconUserSongHistoryService{

	private final static Logger logger = LoggerFactory.getLogger(RoboconUserSongHistoryServiceImpl.class);
	
	@Autowired
	private RoboconUserSongHistoryDAO roboconUserSongHistoryDAO;
	
	@Autowired
	private LocationService locationService;
	
	@Autowired
	private RoboconMessageService roboconMessageService;
	
	@Autowired
	private RoboconUserSongHistoryTransformer roboconUserSongHistoryTransformer;
	
	@Override
	public RoboconUserSongHistoryDTO createSongHistory(RoboconUserSongHistoryEB roboconUserSongHistoryEB) {
		logger.debug("Creating history record for user {} with song {} and {} action", roboconUserSongHistoryEB.getUserId(), roboconUserSongHistoryEB.getRoboconSongEB().pk(), roboconUserSongHistoryEB.getRoboconUserAction().getStrValue());
		try{
			roboconUserSongHistoryDAO.createEntity(roboconUserSongHistoryEB);
			roboconUserSongHistoryDAO.flush();
			roboconUserSongHistoryDAO.refresh(roboconUserSongHistoryEB);
			return roboconUserSongHistoryTransformer.transformEntityToDTO(roboconUserSongHistoryEB);
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: User Song history creation exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot create user song history due to " + message;
			throw new RoboconUserSongHistoryCreationException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: User Song history creation error due to {}.", ex.getMessage());
			throw new RoboconUserSongHistoryCreationException(ex.getMessage());
		}
	}

	@Override
	public RoboconUserSongHistoryDTO createOrUpdateSongHistory(RoboconSongEB song, RoboconUserActionEnum action){
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		
		logger.info("Creating Song histoty for song ({}) with user id ({}) and action '{}'", song.pk(), userIdStr, action.getStrValue());
		
		RoboconUserSongHistoryCriteriaFilter cf = new RoboconUserSongHistoryCriteriaFilter();
		cf.setSongId(song.pk());
		cf.setUserId(NumberUtils.toLong(userIdStr));
		cf.setUserAction(action);
		
		RoboconUserSongHistoryEB roboconUserSongHistoryEB = roboconUserSongHistoryDAO.getEntityByCriteriaIfExists(cf);
		
		if(roboconUserSongHistoryEB != null){
			roboconUserSongHistoryEB.addRoboconUserSongHitEB(new RoboconUserSongHitEB());
			roboconUserSongHistoryDAO.updateEntity(roboconUserSongHistoryEB);
		}else{
			roboconUserSongHistoryEB = new RoboconUserSongHistoryEB();
			roboconUserSongHistoryEB.setUserId(NumberUtils.toLong(userIdStr));
			roboconUserSongHistoryEB.setRoboconSongEB(song);
			roboconUserSongHistoryEB.setRoboconUserAction(action);
			roboconUserSongHistoryEB.addRoboconUserSongHitEB(new RoboconUserSongHitEB());
			roboconUserSongHistoryDAO.createEntity(roboconUserSongHistoryEB);
		}
		roboconUserSongHistoryDAO.flush();
		roboconUserSongHistoryDAO.refresh(roboconUserSongHistoryEB);
		return roboconUserSongHistoryTransformer.transformEntityToDTO(roboconUserSongHistoryEB);
	}
	
	@Override
	public List<RoboconUserSongHistoryDTO> listSongHistory(Integer pageNum, Integer pageLimit) {
		logger.debug("Fetching robocon song history for page number {} and limit {}", pageNum, pageLimit);
		RoboconUserSongHistoryCriteriaFilter cf = new RoboconUserSongHistoryCriteriaFilter();
		cf.orderByHistoryId(true);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
		List<RoboconUserSongHistoryEB> roboconSongHistory = roboconUserSongHistoryDAO.getEntityListByCriteria(cf);
		return roboconUserSongHistoryTransformer.transformEntityToDTO(roboconSongHistory);
	}

	@Override
	public List<RoboconUserSongHistoryDTO> filterSongHistory(HttpServletRequest request, Integer pageNum, Integer pageLimit) {
		logger.debug("Filtering robocon song history with page number {} and limit {}", pageNum, pageLimit);
		RoboconUserSongHistoryCriteriaFilter cf = new RoboconUserSongHistoryCriteriaFilter();
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		Long userId = NumberUtils.toLong(userIdStr);
		String mcc = ControlFieldsHelper.getStrFieldValue(OPERATOR_MCC);
		String mnc = ControlFieldsHelper.getStrFieldValue(OPERATOR_MNC);
		String storefront = ControlFieldsHelper.getStrFieldValue(STOREFRONT);
		String userAction = ControlFieldsHelper.getStrFieldValue(USER_ACTION);
		Location location = locationService.getLocation(request);
		
		cf.setUserId(userId);
		
		// Action Filter
		if(!StringUtils.isEmpty(userAction)){
			try{
				RoboconUserActionEnum action = RoboconUserActionEnum.valueOf(userAction);
				cf.setUserAction(action);
			}catch(IllegalArgumentException ex){
				logger.error("Error: User action '{}' not supported exception.", userAction);
				String msg = "User action '"+userAction+"' not supported exception";
				throw new RoboconInvalidUserActionException(msg);
			}
		}
		
		// Operator Filter
		if(mcc != null && !mcc.isEmpty() && mnc != null && !mnc.isEmpty()){
			cf.setOperator(mcc, mnc);
		}else{
			cf.isEmptyOperator(true);
		}
		
		// Storefront Filter
		if(storefront != null && !storefront.isEmpty()){
			cf.setStorefrontName(storefront);
		}else{
			cf.isEmptyStorefront(true);
		}
		
		// Country Filter
		if(location != null && location.countryCode != null && !location.countryCode.isEmpty()){
			cf.setCountryCode(location.countryCode);
		}else{
			cf.setEmptyCountry(true);
		}
		
		// Pagination
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
		
		cf.orderByHitDate(false);
		
		roboconUserSongHistoryDAO.enableMetaLangFilter();
		
		List<RoboconUserSongHistoryEB> roboconSongHistory = roboconUserSongHistoryDAO.getEntityListByCriteria(cf);
		return roboconUserSongHistoryTransformer.transformEntityToDTO(roboconSongHistory);
	}

	@Override
	public List<RoboconUserSongHistoryDTO> playAgain(HttpServletRequest request, Integer pageNum, Integer pageLimit){
		logger.debug("Get robocon song play again list with page number {} and limit {}", pageNum, pageLimit);
		RoboconUserSongHistoryCriteriaFilter cf = new RoboconUserSongHistoryCriteriaFilter();
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		Long userId = NumberUtils.toLong(userIdStr);
		String mcc = ControlFieldsHelper.getStrFieldValue(OPERATOR_MCC);
		String mnc = ControlFieldsHelper.getStrFieldValue(OPERATOR_MNC);
		String storefront = ControlFieldsHelper.getStrFieldValue(STOREFRONT);
		Location location = locationService.getLocation(request);
		
		cf.setUserId(userId);
		cf.setUserAction(RoboconUserActionEnum.PLAY);
		
		// Operator Filter
		if(mcc != null && !mcc.isEmpty() && mnc != null && !mnc.isEmpty()){
			cf.setOperator(mcc, mnc);
		}else{
			cf.isEmptyOperator(true);
		}
		
		// Storefront Filter
		if(storefront != null && !storefront.isEmpty()){
			cf.setStorefrontName(storefront);
		}else{
			cf.isEmptyStorefront(true);
		}
		
		// Country Filter
		if(location != null && location.countryCode != null && !location.countryCode.isEmpty()){
			cf.setCountryCode(location.countryCode);
		}else{
			cf.setEmptyCountry(true);
		}
		
		// Pagination
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
		
		cf.orderByHitCount(false);
		
		roboconUserSongHistoryDAO.enableMetaLangFilter();
		
		List<RoboconUserSongHistoryEB> roboconSongHistory = roboconUserSongHistoryDAO.getEntityListByCriteria(cf);
		return roboconUserSongHistoryTransformer.transformEntityToDTO(roboconSongHistory);
	}
	
	@Override
	public void deleteSongHistory(Long historyId) {
		logger.debug("Deleting history {}", historyId);
		roboconUserSongHistoryDAO.deleteEntity(historyId);
	}
	
	@Override
	protected DTOTransformer<RoboconUserSongHistoryEB, RoboconUserSongHistoryDTO> getTransformer() {
		return roboconUserSongHistoryTransformer;
	}

	@Override
	protected DTOTransformer<RoboconUserSongHistoryEB, RoboconUserSongHistoryDTO> getSummaryTransformer() {
		return roboconUserSongHistoryTransformer;
	}

	@Override
	protected DAO<RoboconUserSongHistoryEB, Long, RoboconUserSongHistoryCriteriaFilter> getDAO() {
		return roboconUserSongHistoryDAO;
	}

}
