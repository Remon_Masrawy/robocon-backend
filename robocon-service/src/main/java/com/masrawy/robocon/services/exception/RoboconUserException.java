package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

import static com.masrawy.robocon.services.exception.RoboconErrorConstants.ROBOCON_INVALID_REQUEST;

public class RoboconUserException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	private static final String ERROR_MSG = "Robocon User Exception.";
	
	public RoboconUserException(){
		super(ERROR_MSG);
	}
	
	public RoboconUserException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return ROBOCON_INVALID_REQUEST;
	}
}
