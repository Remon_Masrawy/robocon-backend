package com.masrawy.robocon.services;


import com.cdm.core.security.model.LoginResult;
import com.masrawy.robocon.dto.RoboconUserDTO;

public interface RoboconRegistrationService {

	public RoboconUserDTO signup(RoboconUserDTO roboconUserDTO);
	
	public LoginResult loginUser(String username, String password);
	
	public RoboconUserDTO login(String username, String password);
	
}
