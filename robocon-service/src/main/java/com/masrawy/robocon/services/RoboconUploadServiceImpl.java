package com.masrawy.robocon.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import com.cdm.core.dto.model.DTO;
import com.masrawy.robocon.file.model.FileUpload;
import com.masrawy.robocon.model.RoboconMimeTypeEnum;
import com.masrawy.robocon.services.exception.InvalidFileMimeTypeException;

@Service("roboconUploadService")
@Transactional
public class RoboconUploadServiceImpl implements RoboconUploadService{

	@Autowired
	private RoboconSongService roboconSongService;
	
	@Autowired
	private RoboconAlbumService roboconAlbumService;
	
	@Autowired
	private RoboconArtistService roboconArtistService;
	
	@Autowired
	private RoboconEventService roboconEventService;
	
	@Override
	public DTO<?> uploadFile(FileUpload fileUpload, Long id) {
		switch(fileUpload.getMimeTypeEnum()){
		case VIDEO:
			break;
		case FULL_TRACK:
			return roboconSongService.uploadRoboconSongContent(fileUpload.getFile(), id);
		case STREAM:
			break;
		case IMAGE:
			break;
		case TRUETONE:
			break;
		case GAME:
			break;
		case THEME:
			break;
		case APPLICATION:
			break;
		case POLYPHONIC:
			break;
		case MONOPHONIC_16:
			break;
		case MONOPHONIC_128:
			break;
		case SONG_KARAOKE:
			return roboconSongService.uploadRoboconSongKaraoke(fileUpload.getFile(), id);
		case VIDEO_KARAOKE:
			break;
		case SONG_LYRICS:
			return roboconSongService.uploadRoboconSongLyrics(fileUpload.getFile(), id);
		case VIDEO_LYRICS:
			break;
		case USER_SING:
			return roboconSongService.uploadRoboconUserSing(fileUpload.getFile(), id);
		case ALBUM_IMAGE:
			return roboconAlbumService.uploadPhoto(fileUpload.getFile(), id);
		case ARTIST_IMAGE:
			return roboconArtistService.uploadPhoto(fileUpload.getFile(), id);
		case SONG_IMAGE:
			return roboconSongService.uploadRoboconSongPhoto(fileUpload.getFile(), id);
		default:
			break;
		}
		return null;
	}
	
	public DTO<?> uploadFile(MultipartFile file, Long id, Integer type){
		RoboconMimeTypeEnum mimeType = null;
		try{
			for(RoboconMimeTypeEnum roboconMimeTypeEnum : RoboconMimeTypeEnum.values()){
				if(roboconMimeTypeEnum.getIntValue() == type){
					mimeType = roboconMimeTypeEnum;
				}
			}
			if(mimeType == null)
				throw new InvalidFileMimeTypeException();
		}catch(Exception e){
			throw new InvalidFileMimeTypeException();
		}
		
		
		switch(mimeType){
			case VIDEO:
				break;
			case FULL_TRACK:
				return roboconSongService.uploadRoboconSongContent(file, id);
			case STREAM:
				break;
			case IMAGE:
				break;
			case TRUETONE:
				break;
			case GAME:
				break;
			case THEME:
				break;
			case APPLICATION:
				break;
			case POLYPHONIC:
				break;
			case MONOPHONIC_16:
				break;
			case MONOPHONIC_128:
				break;
			case SONG_KARAOKE:
				return roboconSongService.uploadRoboconSongKaraoke(file, id);
			case VIDEO_KARAOKE:
				break;
			case SONG_LYRICS:
				return roboconSongService.uploadRoboconSongLyrics(file, id);
			case VIDEO_LYRICS:
				break;
			case USER_SING:
				return roboconSongService.uploadRoboconUserSing(file, id);
			case ALBUM_IMAGE:
				return roboconAlbumService.uploadPhoto(file, id);
			case ARTIST_IMAGE:
				return roboconArtistService.uploadPhoto(file, id);
			case SONG_IMAGE:
				return roboconSongService.uploadRoboconSongPhoto(file, id);
			case EVENT_IMAGE:
				return roboconEventService.uploadPhoto(file, id);
			default:
				break;
		}
		return null;
	}
	
	
}
