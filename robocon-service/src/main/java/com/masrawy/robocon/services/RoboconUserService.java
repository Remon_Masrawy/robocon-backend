package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconUserDTO;
import com.masrawy.robocon.query.RoboconUserCriteriaFilter;

public interface RoboconUserService extends CrudService<RoboconUserDTO, RoboconUserDTO, Long, RoboconUserCriteriaFilter>{

	public RoboconUserDTO updateUser(RoboconUserDTO roboconUserDTO);
	
	public RoboconUserDTO updateUserInfo(RoboconUserDTO roboconUserDTO);
	
	public RoboconUserDTO updateUserPhoto(MultipartFile multipartFile);
	
	public RoboconUserDTO changePassword(String oldPassword, String newPassword);
	
	public RoboconUserDTO inactiveUser(Long userId);
	
	public RoboconUserDTO activeUser(Long userId);
	
	public RoboconUserDTO getUser(Long userId);
	
	public RoboconUserDTO getUser();
	
	public List<RoboconUserDTO> getUsers(Integer pageNum, Integer pageLimit, HttpServletResponse response);
	
	public void deleteUser(Long userId);
}
