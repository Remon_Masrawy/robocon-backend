package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconStorefrontDTO;
import com.masrawy.robocon.query.RoboconStorefrontCriteriaFilter;

public interface RoboconStorefrontService extends CrudService<RoboconStorefrontDTO, RoboconStorefrontDTO, Long, RoboconStorefrontCriteriaFilter>{

	public RoboconStorefrontDTO createStorefront(RoboconStorefrontDTO roboconStorefrontDTO);
	public RoboconStorefrontDTO updateStorefront(RoboconStorefrontDTO roboconStorefrontDTO);
	public RoboconStorefrontDTO getStorefront(Long storefrontId);
	public List<RoboconStorefrontDTO> getStorefronts(Integer pageNum, Integer pageLimit, HttpServletResponse response);
	public void removeStorefront(Long storefrontId);
}
