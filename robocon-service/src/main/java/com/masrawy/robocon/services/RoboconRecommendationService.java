package com.masrawy.robocon.services;

import java.util.List;

import com.masrawy.robocon.dto.RoboconSongDTO;

public interface RoboconRecommendationService {

	public List<RoboconSongDTO> recommend();
}
