package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;
import static com.masrawy.robocon.services.exception.RoboconErrorConstants.ROBOCON_INVALID_REQUEST;

public class RoboconFrontendException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	public static final String ERROR_MSG = "Robocon Frontend Exception occurred.";
	
	public RoboconFrontendException(){
		super(ERROR_MSG);
	}
	
	public RoboconFrontendException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return ROBOCON_INVALID_REQUEST;
	}
}
