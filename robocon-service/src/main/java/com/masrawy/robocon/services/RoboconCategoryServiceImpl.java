package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.KEYWORD;
import static com.masrawy.robocon.model.RoboconConstants.SPECIAL_CHARACTERS_PATTERN;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.dao.RoboconCategoryDAO;
import com.masrawy.robocon.dto.RoboconCategoryDTO;
import com.masrawy.robocon.model.RoboconCategoryEB;
import com.masrawy.robocon.model.RoboconCategoryMetaEB;
import com.masrawy.robocon.query.RoboconCategoryCriteriaFilter;
import com.masrawy.robocon.query.RoboconCategorySearchCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconArtistEventNotFoundException;
import com.masrawy.robocon.services.exception.RoboconCategoryCreationException;
import com.masrawy.robocon.services.exception.RoboconCategoryUpdateException;
import com.masrawy.robocon.transformer.RoboconCategoryTransformer;
import com.masrawy.robocon.validation.RoboconBeanValidationManager;

@Service("roboconCategoryService")
@Transactional
public class RoboconCategoryServiceImpl extends AbstractCrudService<RoboconCategoryEB, RoboconCategoryDTO, RoboconCategoryDTO, Long, RoboconCategoryCriteriaFilter> implements RoboconCategoryService{

	private static final Logger logger = LoggerFactory.getLogger(RoboconCategoryServiceImpl.class);
	
	@Autowired
	private RoboconCategoryDAO roboconCategoryDAO;
	
	@Autowired
	private RoboconCategoryTransformer roboconCategoryTransformer;
	
	@Autowired
	private RoboconMessageService roboconMessageService;
	
	@Autowired
	private RoboconBeanValidationManager roboconBeanValidationManager;
	
	@Override
	public RoboconCategoryDTO createCategory(RoboconCategoryDTO roboconCategoryDTO) {
		logger.debug("Creating Robocon Category.");
		try{
			RoboconCategoryEB roboconCategoryEB = createEntity(roboconCategoryDTO);
			roboconCategoryDTO = roboconCategoryTransformer.transformEntityToDTO(roboconCategoryEB);
			logger.debug("Category ({}) has been created successfully.", roboconCategoryDTO.pk());
			return roboconCategoryDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Category create exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot create category due to " + message;
			throw new RoboconCategoryCreationException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Category creation error due to {}.", ex.getMessage());
			String msg = "Cannot create category due to " + ex.getMessage();
			throw new RoboconCategoryCreationException(msg);
		}
	}

	@Override
	public RoboconCategoryDTO updateCategory(RoboconCategoryDTO roboconCategoryDTO) {
		logger.debug("Updating Robocon Category.");
		try{
			RoboconCategoryEB roboconCategoryEB = updateEntity(roboconCategoryDTO);
			roboconCategoryDTO = roboconCategoryTransformer.transformEntityToDTO(roboconCategoryEB);
			logger.debug("Category ({}) has been updated successfully.", roboconCategoryDTO.pk());
			return roboconCategoryDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Category update exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot update category due to " + message;
			throw new RoboconCategoryUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Category update error due to {}.", ex.getMessage());
			String msg = "Cannot update category due to " + ex.getMessage();
			throw new RoboconCategoryUpdateException(msg);
		}
	}

	@Override
	public RoboconCategoryDTO getCategory(Long categoryId){
		logger.debug("Fetching Robocon Category {}.", categoryId);
		RoboconCategoryEB roboconCategoryEB = roboconCategoryDAO.getEntityByIDIfExists(categoryId);
		if(roboconCategoryEB == null){
			logger.error("Error: No Artist Event Found With id ({})", categoryId);
			String msg = "No Artist Event found with id ({" + categoryId + "})";
			throw new RoboconArtistEventNotFoundException(msg);
		}
		return roboconCategoryTransformer.transformEntityToDTO(roboconCategoryEB);
	}
	
	@Override
	public List<RoboconCategoryDTO> listCategories(Integer pageNum, Integer pageLimit, HttpServletResponse response) {
		logger.debug("Fetching categories for page number ({}) and limit ({}).", pageNum, pageLimit);
		RoboconCategoryCriteriaFilter cf = new RoboconCategoryCriteriaFilter();
		RoboconCategorySearchCriteriaFilter cfs = new RoboconCategorySearchCriteriaFilter();
		String keyword = ControlFieldsHelper.getStrFieldValue(KEYWORD);
		try {
			if(keyword != null && !keyword.isEmpty()){
				keyword = URLDecoder.decode(keyword, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot decode keyword:{}, error {}", keyword, e);
		}
		if(keyword != null && !keyword.isEmpty()){
			cf.setCategoryName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
			cfs.setCategoryName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
		}else{
			roboconCategoryDAO.enableMetaLangFilter();
		}
		cfs.orderByCategoryId(true);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cfs.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cfs.setMaxResult(pageLimit);
		}
		List<RoboconCategoryEB> roboconCategoriesEB = roboconCategoryDAO.getEntityListByCriteria(cfs);
		int count = roboconCategoryDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		return roboconCategoryTransformer.transformEntityToDTO(roboconCategoriesEB);
	}
	
	@Override
	public void removeCategory(Long categoryId) {
		logger.debug("Removing Robocon Category ({}).", categoryId);
		roboconCategoryDAO.deleteEntity(categoryId);
	}
	
	@Override
	protected void doBeforeValidate(RoboconCategoryDTO dto, boolean isNew) {
		roboconBeanValidationManager.validate(dto);
		super.doBeforeValidate(dto, isNew);
	}
	
	@Override
	protected void doBeforePersist(RoboconCategoryEB entity, RoboconCategoryDTO dto, boolean isNew) {
		if(entity != null && !entity.getRoboconCategoryMetaEB().isEmpty()){
			for(RoboconCategoryMetaEB roboconCategoryMetaEB : entity.getRoboconCategoryMetaEB()){
				if(roboconCategoryMetaEB.getRoboconCategoryEB() == null){
					roboconCategoryMetaEB.setRoboconCategoryEB(entity);
				}
			}
		}
		super.doBeforePersist(entity, dto, isNew);
	}
	
	@Override
	protected void doAfterPersist(RoboconCategoryEB entity, RoboconCategoryDTO dto, boolean isNew) {
		roboconCategoryDAO.flush();
		roboconCategoryDAO.refresh(entity);
		super.doAfterPersist(entity, dto, isNew);
	}
	
	@Override
	protected DTOTransformer<RoboconCategoryEB, RoboconCategoryDTO> getTransformer() {
		return roboconCategoryTransformer;
	}

	@Override
	protected DTOTransformer<RoboconCategoryEB, RoboconCategoryDTO> getSummaryTransformer() {
		return roboconCategoryTransformer;
	}

	@Override
	protected DAO<RoboconCategoryEB, Long, RoboconCategoryCriteriaFilter> getDAO() {
		return roboconCategoryDAO;
	}

}
