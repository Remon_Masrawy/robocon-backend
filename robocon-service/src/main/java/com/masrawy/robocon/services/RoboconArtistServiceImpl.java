package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.CODE_FROM;
import static com.masrawy.robocon.model.RoboconConstants.CODE_TO;
import static com.masrawy.robocon.model.RoboconConstants.KEYWORD;
import static com.masrawy.robocon.model.RoboconConstants.SPECIAL_CHARACTERS_PATTERN;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.file.model.FileEB;
import com.cdm.core.persistent.dao.DAO;
import com.cdm.core.util.DumpUtil;
import com.masrawy.robocon.dao.RoboconArtistDAO;
import com.masrawy.robocon.dto.RoboconArtistDTO;
import com.masrawy.robocon.dto.RoboconArtistSocialDTO;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.dto.RoboconTagDTO;
import com.masrawy.robocon.model.RoboconArtistEB;
import com.masrawy.robocon.model.RoboconArtistEventEB;
import com.masrawy.robocon.model.RoboconArtistEventMetaEB;
import com.masrawy.robocon.model.RoboconArtistMetaEB;
import com.masrawy.robocon.model.RoboconArtistPhotoEB;
import com.masrawy.robocon.model.RoboconArtistSocialEB;
import com.masrawy.robocon.model.RoboconArtistTagEB;
import com.masrawy.robocon.model.RoboconSocialEnum;
import com.masrawy.robocon.query.RoboconArtistCriteriaFilter;
import com.masrawy.robocon.query.RoboconArtistSearchCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconArtistCreationException;
import com.masrawy.robocon.services.exception.RoboconArtistException;
import com.masrawy.robocon.services.exception.RoboconArtistNotFoundException;
import com.masrawy.robocon.services.exception.RoboconArtistPhotoUploadException;
import com.masrawy.robocon.services.exception.RoboconArtistUpdateException;
import com.masrawy.robocon.transformer.RoboconArtistMetaTransformer;
import com.masrawy.robocon.transformer.RoboconArtistSocialTransformer;
import com.masrawy.robocon.transformer.RoboconArtistTagTransformer;
import com.masrawy.robocon.transformer.RoboconArtistTransformer;
import com.masrawy.robocon.validation.RoboconBeanValidationManager;

@Service("roboconArtistService")
@Transactional
public class RoboconArtistServiceImpl extends AbstractCrudService<RoboconArtistEB, RoboconArtistDTO, RoboconArtistDTO, Long, RoboconArtistCriteriaFilter> implements RoboconArtistService{

	private static final Logger logger = LoggerFactory.getLogger(RoboconArtistServiceImpl.class);
	
	@Autowired
	private RoboconArtistDAO roboconArtistDAO;
	
	@Autowired
	private RoboconFileService roboconFileService;
	
	@Autowired
	private RoboconMessageService roboconMessageService;
	
	@Autowired
	private RoboconArtistTransformer roboconArtistTransformer;
	
	@Autowired
	private RoboconArtistMetaTransformer roboconArtistMetaTransformer;
	
	@Autowired
	private RoboconArtistTagTransformer roboconArtistTagTransformer;
	
	@Autowired
	private RoboconArtistSocialTransformer roboconArtistSocialTransformer;
	
	@Autowired
	private RoboconBeanValidationManager roboconBeanValidationManager;
	
	@Override
	public RoboconArtistDTO createRoboconArtist(RoboconArtistDTO roboconArtistDTO) {
		logger.debug("Creating Robocon Artist.");
		try{
			RoboconArtistEB roboconArtistEB = createEntity(roboconArtistDTO);
			roboconArtistDTO = getTransformer().transformEntityToDTO(roboconArtistEB);
			logger.debug("Artist {} has been created successfully.", roboconArtistDTO.getId());
			return roboconArtistDTO;
		} catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Artist creation exception due to constraint {} violation.\n Message: {}, \n Error: {}", ex.getConstraintName(), ex.getCause().getMessage(), ex);
			String msg = "Cannot create artist due to " + message;
			throw new RoboconArtistCreationException(msg);
		} catch(RuntimeException ex){
			logger.error("Error: Artist creation exception due to {}", ex);
			String msg = "Cannot create artist due to " + ex.getMessage();
			throw new RoboconArtistCreationException(msg);
		}
	}

	@Override
	public RoboconArtistDTO updateRoboconArtist(RoboconArtistDTO roboconArtistDTO) {
		logger.debug("Updating Robocon Artist {}", roboconArtistDTO.pk());
		try{
			RoboconArtistEB roboconArtistEB = updateEntity(roboconArtistDTO);
			roboconArtistDTO = getTransformer().transformEntityToDTO(roboconArtistEB);
			logger.debug("Artist {} has been updated successfully.", roboconArtistDTO.getId());
			return roboconArtistDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Artist update exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot update artist due to " + message;
			throw new RoboconArtistUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Artist update error due to {}", ex.getMessage());
			throw new RoboconArtistException(ex.getMessage());
		}
	}

	/*
	 * 
	 * Artist Meta Operations 
	 * 
	 */
	@Override
	public RoboconArtistDTO addRoboconArtistMeta(List<RoboconMetaDTO> roboconMetaDTO, Long artistId) {
		logger.debug("Adding Meta {} to Artist {}", DumpUtil.dumpAsJson(roboconMetaDTO), artistId);
		try{
			if(roboconMetaDTO != null && !roboconMetaDTO.isEmpty()){
				for(RoboconMetaDTO RoboconMetaDTO : roboconMetaDTO){
					roboconBeanValidationManager.validate(RoboconMetaDTO);
				}
			}
			RoboconArtistEB roboconArtistEB = getRoboconArtistIfExist(artistId);
			List<RoboconArtistMetaEB> roboconArtistMetaEB = roboconArtistMetaTransformer.transformDTOToEntity(roboconMetaDTO);
			roboconArtistEB.addRoboconArtistMetaEB(roboconArtistMetaEB);
			roboconArtistDAO.flush();
			roboconArtistDAO.refreshArtist(roboconArtistEB);
			RoboconArtistDTO roboconArtistDTO = getTransformer().transformEntityToDTO(roboconArtistEB);
			return roboconArtistDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Artist Meta Add exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot add meta to artist due to " + message;
			throw new RoboconArtistUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Artist Meta Add error due to {}", ex.getMessage());
			throw new RoboconArtistException(ex.getMessage());
		}
	}

	/*
	 * 
	 * Artist Tag Operations 
	 * 
	 */
	@Override
	public RoboconArtistDTO addRoboconArtistTags(List<RoboconTagDTO> roboconTagsDTO, Long artistId) {
		logger.debug("Adding Tag {} to Artist {}", DumpUtil.dumpAsJson(roboconTagsDTO), artistId);
		try{
			if(roboconTagsDTO != null && !roboconTagsDTO.isEmpty()){
				for(RoboconTagDTO roboconTag : roboconTagsDTO){
					roboconBeanValidationManager.validate(roboconTag);
				}
			}
			RoboconArtistEB roboconArtistEB = getRoboconArtistIfExist(artistId);
			List<RoboconArtistTagEB> roboconArtistTagsEB = roboconArtistTagTransformer.transformDTOToEntity(roboconTagsDTO);
			roboconArtistEB.addRoboconArtistTagsEB(roboconArtistTagsEB);
			roboconArtistDAO.flush();
			roboconArtistDAO.refreshArtist(roboconArtistEB);
			RoboconArtistDTO roboconArtistDTO = getTransformer().transformEntityToDTO(roboconArtistEB);
			return roboconArtistDTO;
		} catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Artist tag Add exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot add tag to artist due to " + message;
			throw new RoboconArtistUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Artist tag Add error due to {}", ex.getMessage());
			throw new RoboconArtistException(ex.getMessage());
		}
	}

	/*
	 * 
	 * Artist Social Operations 
	 * 
	 */

	@Override
	public RoboconArtistDTO addRoboconArtistSocials(List<RoboconArtistSocialDTO> roboconArtistSocialsDTO, Long artistId) {
		logger.debug("Adding Socials {} to Artist {}", DumpUtil.dumpAsJson(roboconArtistSocialsDTO), artistId);
		try{
			if(roboconArtistSocialsDTO != null && !roboconArtistSocialsDTO.isEmpty()){
				for(RoboconArtistSocialDTO roboconArtistSocialDTO : roboconArtistSocialsDTO){
					roboconBeanValidationManager.validate(roboconArtistSocialDTO);
				}
			}
			RoboconArtistEB roboconArtistEB = getRoboconArtistIfExist(artistId);
			List<RoboconArtistSocialEB> roboconArtistSocialsEB = roboconArtistSocialTransformer.transformDTOToEntity(roboconArtistSocialsDTO);
			roboconArtistEB.addRoboconArtistSocialsEB(roboconArtistSocialsEB);
			roboconArtistDAO.flush();
			roboconArtistDAO.refreshArtist(roboconArtistEB);
			RoboconArtistDTO roboconArtistDTO = getTransformer().transformEntityToDTO(roboconArtistEB);
			return roboconArtistDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Artist Social Add exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot add social to artist due to " + message;
			throw new RoboconArtistUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Artist Social Add error due to {}", ex.getCause());
			throw new RoboconArtistException(ex.getMessage());
		}
	}
	
	/*
	 * 
	 * Artist Photo Operations 
	 * 
	 */
	
	@Override
	public RoboconArtistDTO uploadPhoto(MultipartFile file, Long artistId){
		List<MultipartFile> files = new ArrayList<MultipartFile>();
		files.add(file);
		return uploadPhotos(files, artistId);
	}
	
	@Override
	public RoboconArtistDTO uploadPhotos(List<MultipartFile> files, Long artistId){
		logger.debug("Uploading Photos to Robocon Artist {}.", artistId);
		try{
			RoboconArtistEB roboconArtistEB = getRoboconArtistIfExist(artistId);
			for(MultipartFile file : files){
				FileEB fileEB = roboconFileService.uploadFileEB(file);
				RoboconArtistPhotoEB roboconArtistPhotoEB = new RoboconArtistPhotoEB();
				roboconArtistPhotoEB.setFile(fileEB);
				roboconArtistPhotoEB.setSize(file.getSize());
				roboconArtistEB.addRoboconArtistPhotoEB(roboconArtistPhotoEB);
			}
			roboconArtistDAO.flush();
			roboconArtistDAO.refreshArtist(roboconArtistEB);
			logger.debug("Robocon Photos has been uploaded successfully.");
			return getTransformer().transformEntityToDTO(roboconArtistEB);
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Artist Photo upload exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Artist Photo upload error occurred due to " + message;
			throw new RoboconArtistPhotoUploadException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Artist photo upload error due to {}", ex.getCause());
			throw new RoboconArtistPhotoUploadException(ex.getMessage());
		}
	}
	
	@Override
	public List<RoboconArtistDTO> getRoboconArtists(Integer pageNum, Integer pageLimit, HttpServletResponse response){
		logger.debug("Fetching Robocon Artists with page number : {} and limit {}.", pageNum, pageLimit);
		RoboconArtistCriteriaFilter cf = new RoboconArtistCriteriaFilter();
		RoboconArtistSearchCriteriaFilter cfs = new RoboconArtistSearchCriteriaFilter();
		cfs.orderByArtistId(true);
		Integer codeFrom = ControlFieldsHelper.getIntFieldValue(CODE_FROM);
		Integer codeTo = ControlFieldsHelper.getIntFieldValue(CODE_TO);
		String keyword = ControlFieldsHelper.getStrFieldValue(KEYWORD);
		try {
			if(keyword != null && !keyword.isEmpty()){
				keyword = URLDecoder.decode(keyword, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot decode keyword:{}, error {}", keyword, e);
		}
		if(keyword != null && !keyword.isEmpty()){
			roboconArtistDAO.enableMetaLangSearchFilter();
			cf.setArtistName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
			cfs.setArtistName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
		}else{
			roboconArtistDAO.enableMetaLangFilter();
		}
		
		if(codeFrom != null && codeTo != null){
			cf.setArtistId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
			cfs.setArtistId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
		}else if(codeFrom != null){
			cf.setArtistId(Long.valueOf(codeFrom));
			cfs.setArtistId(Long.valueOf(codeFrom));
		}else if(codeTo != null){
			cf.setArtistId(Long.valueOf(codeTo));
			cfs.setArtistId(Long.valueOf(codeTo));
		}
		
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cfs.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cfs.setMaxResult(pageLimit);
		}
		List<RoboconArtistEB> roboconArtistsEB = roboconArtistDAO.getEntityListByCriteria(cfs);
		int count = roboconArtistDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		return roboconArtistTransformer.transformEntityToDTO(roboconArtistsEB);
	}
	
	@Override
	public RoboconArtistDTO fetchRoboconArtist(Long artistId){
		logger.debug("Fetching Robocon Artist {}.", artistId);
		RoboconArtistEB roboconArtistEB = getRoboconArtistIfExist(artistId);
		return roboconArtistTransformer.transformEntityToDTO(roboconArtistEB);
	}
	
	@Override
	public Map<String, String> getSocials(){
		Map<String, String> socials = new HashMap<String, String>();
		for(RoboconSocialEnum roboconSocialEnum : RoboconSocialEnum.values()){
			socials.put(roboconSocialEnum.name(), roboconSocialEnum.getStrValue());
		}
		return socials;
	}
	
	@Override
	public void removeArtist(Long artistId){
		logger.debug("Removing robocon artist with id ({})", artistId);
		RoboconArtistEB roboconArtistEB = getRoboconArtistIfExist(artistId);
		if(!roboconArtistEB.getRoboconSongsEB().isEmpty()){
			logger.error("Error: Cannot remove artist due to there are songs and albums assinged to him/her.");
			String msg = "Cannot remove artist due to songs and albums assigned to him/her";
			throw new RoboconArtistException(msg);
		}
		try{
			if(!roboconArtistEB.getRoboconArtistMetaEB().isEmpty()){
				RoboconArtistMetaEB roboconArtistMetaEB = roboconArtistEB.getRoboconArtistMetaEB().get(0);
				logger.info("Removing robocon artist '{}'", roboconArtistMetaEB.getName());
			}
			roboconArtistDAO.deleteEntity(roboconArtistEB);
		}catch(Exception e){
			logger.error("Error: Cannot remove robocon artist due to {}", e);
			String msg = "Cannot remove robocon artist due to " + e.getMessage();
			throw new RoboconArtistException(msg);
		}
	}
	
	@Override
	protected void doBeforeValidate(RoboconArtistDTO dto, boolean isNew) {
		roboconBeanValidationManager.validate(dto);
		super.doBeforeValidate(dto, isNew);
	}
	
	@Override
	protected void doBeforePersist(RoboconArtistEB roboconArtistEB, RoboconArtistDTO roboconArtistDTO, boolean isNew) {
		if(!roboconArtistEB.getRoboconArtistMetaEB().isEmpty()){
			for(RoboconArtistMetaEB roboconArtistMetaEB : roboconArtistEB.getRoboconArtistMetaEB()){
				if(roboconArtistMetaEB.getRoboconArtistEB() != null){
					continue;
				}
				roboconArtistMetaEB.setRoboconArtistEB(roboconArtistEB);
			}
		}
		if(!roboconArtistEB.getRoboconArtistTagsEB().isEmpty()){
			for(RoboconArtistTagEB roboconArtistTagEB : roboconArtistEB.getRoboconArtistTagsEB()){
				if(roboconArtistTagEB.getRoboconArtistEB() != null){
					continue;
				}
				roboconArtistTagEB.setRoboconArtistEB(roboconArtistEB);
			}
		}
		if(!roboconArtistEB.getRoboconArtistSocialsEB().isEmpty()){
			for(RoboconArtistSocialEB roboconArtistSocialEB : roboconArtistEB.getRoboconArtistSocialsEB()){
				if(roboconArtistSocialEB.getRoboconArtistEB() != null){
					continue;
				}
				roboconArtistSocialEB.setRoboconArtistEB(roboconArtistEB);
			}
		}
		if(!roboconArtistEB.getRoboconArtistEventEB().isEmpty()){
			for(RoboconArtistEventEB roboconArtistEventEB : roboconArtistEB.getRoboconArtistEventEB()){
				if(!roboconArtistEventEB.getRoboconArtistsEB().contains(roboconArtistEB)){
					roboconArtistEventEB.addRoboconArtistEB(roboconArtistEB);
				}
				for(RoboconArtistEventMetaEB roboconArtistEventMetaEB : roboconArtistEventEB.getRoboconArtistEventMetaEB()){
					if(roboconArtistEventMetaEB.getRoboconArtistEventEB() == null){
						roboconArtistEventMetaEB.setRoboconArtistEventEB(roboconArtistEventEB);
					}
				}
			}
			
		}
		super.doBeforePersist(roboconArtistEB, roboconArtistDTO, isNew);
	}
	
	@Override
	protected void doAfterPersist(RoboconArtistEB entity, RoboconArtistDTO dto, boolean isNew) {
		roboconArtistDAO.flush();
		roboconArtistDAO.refreshArtist(entity);
		super.doAfterPersist(entity, dto, isNew);
	}
	
	protected RoboconArtistEB getRoboconArtistIfExist(Long artistId){
		RoboconArtistEB roboconArtistEB = roboconArtistDAO.getEntityByIDIfExists(artistId);
		if(roboconArtistEB == null){
			logger.error("Error: No Robocon Artist Found With ID {}.", artistId);
			String msg = "Robocon Artist With ID ("+ artistId +") Not Found Exception.";
			throw new RoboconArtistNotFoundException(msg);
		}
		return roboconArtistEB;
	}
	
	@Override
	protected DTOTransformer<RoboconArtistEB, RoboconArtistDTO> getTransformer() {
		return roboconArtistTransformer;
	}

	@Override
	protected DTOTransformer<RoboconArtistEB, RoboconArtistDTO> getSummaryTransformer() {
		return roboconArtistTransformer;
	}

	@Override
	protected DAO<RoboconArtistEB, Long, RoboconArtistCriteriaFilter> getDAO() {
		return roboconArtistDAO;
	}

}
