package com.masrawy.robocon.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.configuration.dao.ConfigParameterDAO;
import com.cdm.core.configuration.dto.ConfigParameterDTO;
import com.cdm.core.configuration.model.ConfigParameterEB;
import com.masrawy.robocon.dao.RoboconConfigurationDAO;
import com.masrawy.robocon.dto.RoboconConfigurationDTO;
import com.masrawy.robocon.model.RoboconConfigurationEB;
import com.masrawy.robocon.query.RoboconConfigurationParameterCriteriaFilter;
import com.masrawy.robocon.services.exception.InvalidConfigurationParameterException;
import com.masrawy.robocon.transformer.RoboconConfigurationParameterTransformer;
import com.masrawy.robocon.transformer.RoboconConfigurationTransformer;

@Service("roboconConfigurationService")
@Transactional
public class RoboconConfigurationServiceImpl implements RoboconConfigurationService{
	
	private static final Logger logger = LoggerFactory.getLogger(RoboconConfigurationServiceImpl.class);

	@Value("${configuration.parameters}")
	private List<String> configurationParameters;
	
	@Autowired
	private ConfigParameterDAO configParameterDAO;
	
	@Autowired
	private RoboconConfigurationDAO RoboconConfigurationDAO;
	
	@Autowired
	private RoboconConfigurationParameterTransformer roboconConfigurationParameterTransformer;
	
	@Autowired
	private RoboconConfigurationTransformer RoboconConfigurationTransformer;
	
	@Override
	public void createConfigParam(ConfigParameterDTO configParameterDTO) {
		if(!configParameterDTO.getName().isEmpty() && configurationParameters.contains(configParameterDTO.getName())){
			logger.debug("Creating configuration parameter with body: {}", configParameterDTO);
			ConfigParameterEB configParameterEB = roboconConfigurationParameterTransformer.transformDTOToEntity(configParameterDTO);
			configParameterDAO.createEntity(configParameterEB);
		}else{
			String msg = "Error: Invalid configuration parameter name: " + configParameterDTO.getName();
			logger.error(msg);
			throw new InvalidConfigurationParameterException(msg);
		}
	}

	@Override
	public List<ConfigParameterDTO> getConfigParamsList() {
		logger.info("Retrieving configuration parameters list.");
		RoboconConfigurationParameterCriteriaFilter cf = new RoboconConfigurationParameterCriteriaFilter();
		List<ConfigParameterEB> configParameterEBList = configParameterDAO.getEntityListByCriteria(cf);
		List<ConfigParameterDTO> configParameterDTOList = roboconConfigurationParameterTransformer.transformEntityToDTO(configParameterEBList);
		return configParameterDTOList;
	}

	@Override
	public void deleteConfigParam(String paramName) {
		logger.debug("Removing configuration parameter with name '{}'", paramName);
		if(!paramName.isEmpty() && configurationParameters.contains(paramName)){
			RoboconConfigurationParameterCriteriaFilter cf = new RoboconConfigurationParameterCriteriaFilter();
			cf.setName(paramName);
			ConfigParameterEB configParameterEB = configParameterDAO.getEntityByCriteriaIfExists(cf);
			if(configParameterEB != null){
				configParameterDAO.deleteEntity(configParameterEB);
			}
		}
	}

	@Override
	public void deleteAllConfigParams() {
		logger.debug("Removing all configuration parameters");
		RoboconConfigurationParameterCriteriaFilter cf = new RoboconConfigurationParameterCriteriaFilter();
		List<ConfigParameterEB> configParameterEBList = configParameterDAO.getEntityListByCriteria(cf);
		if(configParameterEBList != null && configParameterEBList.size() > 0){
			for(ConfigParameterEB configParameterEB : configParameterEBList){
				configParameterDAO.deleteEntity(configParameterEB);
			}
		}
	}

	@Override
	public void updateConfigParam(ConfigParameterDTO configParameterDTO) {
		if(!configParameterDTO.getName().isEmpty() && configurationParameters.contains(configParameterDTO.getName())){
			logger.debug("Updating configuration parameter with name '{}'", configParameterDTO.getName());
			RoboconConfigurationParameterCriteriaFilter cf = new RoboconConfigurationParameterCriteriaFilter();
			cf.setName(configParameterDTO.getName());
			ConfigParameterEB configParameterEB = configParameterDAO.getEntityByCriteriaIfExists(cf);
			if(configParameterEB != null){
				if(!configParameterEB.isEditabled()){
					String msg = "Error: Configuration parameter '" + configParameterDTO.getName() + "' is not editable";
					logger.error(msg);
					throw new InvalidConfigurationParameterException(msg);
				}
				ConfigParameterEB configParameter = roboconConfigurationParameterTransformer.transformDTOToEntity(configParameterDTO);
				configParameterEB.setDescription(configParameter.getDescription());
				configParameterEB.setValue(configParameter.getValue());
			}
		}else{
			String msg = "Error: Invalid configuration parameter name: " + configParameterDTO.getName();
			logger.error(msg);
			throw new InvalidConfigurationParameterException(msg);
		}
	}

	@Override
	public void createRoboconConfiguration(RoboconConfigurationDTO roboconConfigurationDTO) {
		if(roboconConfigurationDTO.getId() != null){
			roboconConfigurationDTO.setId(null);
		}
		RoboconConfigurationEB roboconConfigurationEB = RoboconConfigurationTransformer.transformDTOToEntity(roboconConfigurationDTO);
		RoboconConfigurationDAO.createEntity(roboconConfigurationEB);
	}

	@Override
	public void deleteRoboconConfiguration(RoboconConfigurationDTO roboconConfigurationDTO) {
		
	}

	@Override
	public void deleteAllRoboconConfigurations() {
		
	}

	@Override
	public void updateRoboconConfiguration(RoboconConfigurationDTO roboconConfigurationDTO) {
		
	}

	@Override
	public List<RoboconConfigurationDTO> getRoboconConfigurationsList() {
		return null;
	}
}
