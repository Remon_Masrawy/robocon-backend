package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconSongTrendDTO;
import com.masrawy.robocon.query.RoboconTrendCriteriaFilter;

public interface RoboconTrendService extends CrudService<RoboconSongTrendDTO, RoboconSongTrendDTO, Long, RoboconTrendCriteriaFilter>{

	public List<RoboconSongTrendDTO> getSongTrends(HttpServletRequest request, Integer pageNum, Integer pageLimit);
}
