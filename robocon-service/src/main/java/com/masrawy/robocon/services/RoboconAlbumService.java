package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconAlbumDTO;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.dto.RoboconTagDTO;
import com.masrawy.robocon.file.model.ImageUpload;
import com.masrawy.robocon.query.RoboconAlbumCriteriaFilter;

public interface RoboconAlbumService extends CrudService<RoboconAlbumDTO, RoboconAlbumDTO, Long, RoboconAlbumCriteriaFilter>{

	public RoboconAlbumDTO createAlbum(RoboconAlbumDTO roboconAlbumDTO);
	public RoboconAlbumDTO createAlbum(RoboconAlbumDTO roboconAlbumDTO, List<MultipartFile> files);
	public RoboconAlbumDTO updateAlbum(RoboconAlbumDTO roboconAlbumDTO);
	
	public RoboconAlbumDTO fetchAlbum(Long albumId);
	public List<RoboconAlbumDTO> fetchAlbums(Integer pageNum, Integer pageLimit, HttpServletResponse response);
	
	public void removeAlbum(Long albumId);
	
	public RoboconAlbumDTO uploadPhoto(MultipartFile file, Long albumId);
	public RoboconAlbumDTO uploadPhotos(List<MultipartFile> files, Long albumId);
	public RoboconAlbumDTO uploadPhotos(Long albumId, List<ImageUpload> files);
	public RoboconAlbumDTO addMeta(RoboconMetaDTO roboconMetaDTO, Long albumId);
	public RoboconAlbumDTO addTag(RoboconTagDTO roboconTagDTO, Long albumId);
}
