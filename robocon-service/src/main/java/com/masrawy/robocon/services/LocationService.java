package com.masrawy.robocon.services;

import javax.servlet.http.HttpServletRequest;

import com.maxmind.geoip.Location;

public interface LocationService {

	public Location getLocation(final HttpServletRequest request);
	
	public Location getLocation(final String ip);
	
	public boolean valid(final String ip);
}
