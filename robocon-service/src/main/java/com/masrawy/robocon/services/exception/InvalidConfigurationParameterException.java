package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class InvalidConfigurationParameterException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;

	public InvalidConfigurationParameterException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return RoboconErrorConstants.ROBOCON_INVALID_REQUEST;
	}
}
