package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class RoboconSongUploadException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	public static final String ERROR_MSG = "Robocon Song upload exception.";
	
	public RoboconSongUploadException(){
		super(ERROR_MSG);
	}
	
	public RoboconSongUploadException(String msg){
		super(msg);
	}
	
	@Override
	public int getErrorCode() {
		return RoboconErrorConstants.ROBOCON_UPLOAD_ERROR;
	}

}
