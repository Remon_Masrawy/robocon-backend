package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.ALBUM_ID;
import static com.masrawy.robocon.model.RoboconConstants.ARTIST_ID;
import static com.masrawy.robocon.model.RoboconConstants.GENRE_ID;
import static com.masrawy.robocon.model.RoboconConstants.KEYWORD;
import static com.masrawy.robocon.model.RoboconConstants.PROVIDER_ID;
import static com.masrawy.robocon.model.RoboconConstants.SONG_ID;
import static com.masrawy.robocon.model.RoboconConstants.SPECIAL_CHARACTERS_PATTERN;
import static com.masrawy.robocon.model.RoboconConstants.USER_ID;
import static com.masrawy.robocon.model.RoboconConstants.CODE_FROM;
import static com.masrawy.robocon.model.RoboconConstants.CODE_TO;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.file.model.FileEB;
import com.cdm.core.persistent.dao.DAO;
import com.cdm.core.security.SecurityUtil;
import com.cdm.core.security.dto.RoleDTO;
import com.cdm.core.security.service.UserService;
import com.masrawy.robocon.dao.RoboconSongContentDAO;
import com.masrawy.robocon.dao.RoboconSongDAO;
import com.masrawy.robocon.dao.RoboconSongKaraokeDAO;
import com.masrawy.robocon.dao.RoboconSongLyricsDAO;
import com.masrawy.robocon.dao.RoboconUserSingDAO;
import com.masrawy.robocon.dto.RoboconSongDTO;
import com.masrawy.robocon.dto.RoboconSongKaraokeDTO;
import com.masrawy.robocon.dto.RoboconUserSingDTO;
import com.masrawy.robocon.model.RoboconMimeTypeEnum;
import com.masrawy.robocon.model.RoboconSongContentEB;
import com.masrawy.robocon.model.RoboconSongEB;
import com.masrawy.robocon.model.RoboconSongKaraokeEB;
import com.masrawy.robocon.model.RoboconSongLyricsEB;
import com.masrawy.robocon.model.RoboconSongMetaEB;
import com.masrawy.robocon.model.RoboconSongPhotoEB;
import com.masrawy.robocon.model.RoboconSongTagEB;
import com.masrawy.robocon.model.RoboconUserActionEnum;
import com.masrawy.robocon.model.RoboconUserSingEB;
import com.masrawy.robocon.query.RoboconSongContentCriteriaFilter;
import com.masrawy.robocon.query.RoboconSongCriteriaFilter;
import com.masrawy.robocon.query.RoboconSongKaraokeCriteriaFilter;
import com.masrawy.robocon.query.RoboconSongLyricsCriteriaFilter;
import com.masrawy.robocon.query.RoboconSongSearchCriteriaFilter;
import com.masrawy.robocon.query.RoboconUserSingCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconInvalidUserIdException;
import com.masrawy.robocon.services.exception.RoboconSongContentNotFoundException;
import com.masrawy.robocon.services.exception.RoboconSongCreationException;
import com.masrawy.robocon.services.exception.RoboconSongKaraokeNotFoundException;
import com.masrawy.robocon.services.exception.RoboconSongLyricsNotFoundException;
import com.masrawy.robocon.services.exception.RoboconSongLyricsUploadException;
import com.masrawy.robocon.services.exception.RoboconSongNotFoundException;
import com.masrawy.robocon.services.exception.RoboconSongPhotoUploadException;
import com.masrawy.robocon.services.exception.RoboconSongUpdateException;
import com.masrawy.robocon.services.exception.RoboconSongUploadException;
import com.masrawy.robocon.services.exception.RoboconUserSingNotFoundException;
import com.masrawy.robocon.transformer.RoboconMobileSongTransformer;
import com.masrawy.robocon.transformer.RoboconSongKaraokeTransformer;
import com.masrawy.robocon.transformer.RoboconSongTransformer;
import com.masrawy.robocon.transformer.RoboconUserSingTransformer;
import com.masrawy.robocon.validation.RoboconBeanValidationManager;

@Service("roboconSongService")
@Transactional
public class RoboconSongServiceImpl extends AbstractCrudService<RoboconSongEB, RoboconSongDTO, RoboconSongDTO, Long, RoboconSongCriteriaFilter> implements RoboconSongService{

	private static final Logger logger = LoggerFactory.getLogger(RoboconSongServiceImpl.class);
	
	@Autowired
	private RoboconSongDAO roboconSongDAO;
	
	@Autowired
	private RoboconSongContentDAO roboconSongContentDAO;
	
	@Autowired
	private RoboconSongKaraokeDAO roboconSongKaraokeDAO;
	
	@Autowired
	private RoboconSongLyricsDAO roboconSongLyricsDAO;
	
	@Autowired
	private RoboconUserSingDAO roboconUserSingDAO;
	
	@Autowired
	private RoboconSongTransformer roboconSongTransformer;
	
	@Autowired
	private RoboconMobileSongTransformer roboconMobileSongTransformer;
	
	@Autowired
	private RoboconUserSingTransformer roboconUserSingTransformer;
	
	@Autowired
	private RoboconSongKaraokeTransformer roboconSongKaraokeTransformer;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoboconFileService roboconFileService;
	
	@Autowired
	private RoboconMessageService roboconMessageService;
	
	@Autowired
	private RoboconUserSongHistoryService roboconUserSongHistoryService;
	
	@Autowired
	private RoboconBeanValidationManager roboconBeanValidationManager;
	
	@Value("${lang.default.code}")
	private String defaultLangCode;
	
	@Override
	public RoboconSongDTO createRoboconSong(RoboconSongDTO roboconSongDTO) {
		logger.debug("Creating Robocon Song.");
		try{
			RoboconSongEB roboconSongEB = createEntity(roboconSongDTO);
			roboconSongDTO = roboconSongTransformer.transformEntityToDTO(roboconSongEB);
			logger.debug("Robocon Song {} has been created successfully.", roboconSongEB.pk());
			return roboconSongDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Robocon Song creation exception due to constraint {} violation.\n Message: {}.", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Robocon Song creation exception due to " + message;
			throw new RoboconSongCreationException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Robocon Song creation error due to {}", ex.getMessage());
			String msg = "Robocon Song creation exception due to " + ex.getMessage();
			throw new RoboconSongCreationException(msg);
		}
	}
	
	public RoboconSongDTO updateRoboconSong(RoboconSongDTO roboconSongDTO){
		logger.debug("Updating Robocon Song {}", roboconSongDTO.pk());
		try{
			RoboconSongEB roboconSongEB = updateEntity(roboconSongDTO);
			roboconSongDTO = roboconSongTransformer.transformEntityToDTO(roboconSongEB);
			logger.debug("Robocon Song {} has been updated successfully.", roboconSongEB.pk());
			return roboconSongDTO;
		} catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Robocon Song creation exception due to constraint {} violation.\n Message: {}.", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Robocon Song creation exception due to " + message;
			throw new RoboconSongUpdateException(msg);
		} catch(RuntimeException ex){
			logger.error("Error: Robocon Song update error due to {}", ex.getMessage());
			String msg = "Robocon Song update exception due to " + ex.getMessage();
			throw new RoboconSongUpdateException(msg);
		}
	}

	@Override
	public RoboconSongDTO uploadRoboconSongContent(MultipartFile multipartFile, Long songId){
		logger.debug("Adding Robocon Content to Song ({})", songId);
		RoboconSongEB roboconSongEB = getRoboconSongById(songId);
		try{
			RoboconSongContentEB roboconSongContentEB = (RoboconSongContentEB) roboconFileService.uploadFile(multipartFile, roboconSongEB.pk(), RoboconMimeTypeEnum.FULL_TRACK);
			roboconSongContentEB.setFileMimeType(RoboconMimeTypeEnum.FULL_TRACK);
			roboconSongEB.addRoboconSongContentEB(roboconSongContentEB);
			roboconSongDAO.flush();
			RoboconSongDTO roboconSongDTO = roboconSongTransformer.transformEntityToDTO(roboconSongEB);
			logger.debug("Robocon Content has been added successfully to Song ({})", roboconSongDTO.pk());
			return roboconSongDTO;
		}catch(ConstraintViolationException ex){
			logger.error("Error: Robocon Song update exception due to constraint {} violation.\n Message: {}.", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Robocon Song update exception due to " + ex.getCause().getMessage();
			throw new RoboconSongUploadException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Robocon Song update error due to {}", ex.getMessage());
			String msg = "Robocon Song update exception due to " + ex.getMessage();
			throw new RoboconSongUploadException(msg);
		}
	}
	
	@Override
	public RoboconSongDTO uploadRoboconSongKaraoke(MultipartFile multipartFile, Long songId){
		logger.debug("Adding Robocon Karaoke to Song ({})", songId);
		RoboconSongEB roboconSongEB = getRoboconSongById(songId);
		try{
			RoboconSongKaraokeEB roboconSongKaraokeEB = (RoboconSongKaraokeEB) roboconFileService.uploadFile(multipartFile, roboconSongEB.pk(), RoboconMimeTypeEnum.SONG_KARAOKE);
			roboconSongKaraokeEB.setFileMimeType(RoboconMimeTypeEnum.SONG_KARAOKE);
			roboconSongEB.addRoboconSongKaraokeEB(roboconSongKaraokeEB);
			roboconSongDAO.flush();
			RoboconSongDTO roboconSongDTO = roboconSongTransformer.transformEntityToDTO(roboconSongEB);
			logger.debug("Robocon Karaoke has been added successfully to Song ({})", roboconSongDTO.pk());
			return roboconSongDTO;
		}catch(ConstraintViolationException ex){
			logger.error("Error: Robocon Song update exception due to constraint {} violation.\n Message: {}.", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Robocon Song update exception due to " + ex.getCause().getMessage();
			throw new RoboconSongUploadException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Robocon Song update error due to {}", ex.getMessage());
			String msg = "Robocon Song update exception due to " + ex.getMessage();
			throw new RoboconSongUploadException(msg);
		}
	}
	
	@Override
	public RoboconUserSingDTO uploadRoboconUserSing(MultipartFile multipartFile, Long songId){
		logger.debug("Uploading robocon user sing for song {}.", songId);
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		
		RoboconSongEB roboconSongEB = getRoboconSongById(songId);
		try{
			RoboconUserSingEB roboconUserSingEB = (RoboconUserSingEB) roboconFileService.uploadFile(multipartFile, roboconSongEB.pk(), RoboconMimeTypeEnum.USER_SING);
			roboconUserSingEB.setFileMimeType(RoboconMimeTypeEnum.USER_SING);
			roboconUserSingEB.setUserId(Long.valueOf(userIdStr));
			roboconSongEB.addRoboconUserSingEB(roboconUserSingEB);
			roboconSongDAO.flush();
			RoboconUserSingDTO roboconUserSingDTO = roboconUserSingTransformer.transformEntityToDTO(roboconUserSingEB);
			logger.debug("Robocon user sing has been added successfully to Song ({})", roboconSongEB.pk());
			return roboconUserSingDTO;
		}catch(ConstraintViolationException ex){
			logger.error("Error: Robocon Song update exception due to constraint {} violation.\n Message: {}.", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Robocon Song update exception due to " + ex.getCause().getMessage();
			throw new RoboconSongUploadException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Robocon Song update error due to {}", ex.getMessage());
			String msg = "Robocon Song update exception due to " + ex.getMessage();
			throw new RoboconSongUploadException(msg);
		}
	}
	
	@Override
	public RoboconSongDTO uploadRoboconSongLyrics(MultipartFile multipartFile, Long songId){
		logger.debug("Adding Robocon Lyrics to Song ({})", songId);
		RoboconSongEB roboconSongEB = getRoboconSongById(songId);
		try{
			RoboconSongLyricsEB roboconSongLyricsEB = (RoboconSongLyricsEB) roboconFileService.uploadFile(multipartFile, roboconSongEB.pk(), RoboconMimeTypeEnum.SONG_LYRICS);
			roboconSongLyricsEB.setFileMimeType(RoboconMimeTypeEnum.SONG_LYRICS);
			roboconSongEB.addRoboconSongLyricsEB(roboconSongLyricsEB);
			roboconSongDAO.flush();
			RoboconSongDTO roboconSongDTO = roboconSongTransformer.transformEntityToDTO(roboconSongEB);
			logger.debug("Robocon Lyrics has been added successfully to Song ({})", roboconSongDTO.pk());
			return roboconSongDTO;
		}catch(ConstraintViolationException ex){
			logger.error("Error: Robocon Song update exception due to constraint {} violation.\n Message: {}.", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Robocon Song update exception due to " + ex.getCause().getMessage();
			throw new RoboconSongLyricsUploadException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Robocon Song update error due to {}", ex.getMessage());
			String msg = "Robocon Song update exception due to " + ex.getMessage();
			throw new RoboconSongLyricsUploadException(msg);
		}
	}
	
	@Override
	public RoboconSongDTO uploadRoboconSongPhoto(MultipartFile file, Long songId){
		List<MultipartFile> files = new ArrayList<MultipartFile>();
		files.add(file);
		return uploadRoboconSongPhotos(files, songId);
	}
	
	@Override
	public RoboconSongDTO uploadRoboconSongPhotos(List<MultipartFile> files, Long songId){
		logger.debug("Uploading Photos to Robocon Song {}.", songId);
		try{
			RoboconSongEB roboconSongEB = getRoboconSongById(songId);
			for(MultipartFile file : files){
				FileEB fileEB = roboconFileService.uploadFileEB(file);
				RoboconSongPhotoEB roboconSongPhotoEB = new RoboconSongPhotoEB();
				roboconSongPhotoEB.setFile(fileEB);
				roboconSongPhotoEB.setSize(file.getSize());
				roboconSongEB.addRoboconSongPhotosEB(roboconSongPhotoEB);
			}
			roboconSongDAO.flush();
			roboconSongDAO.refresh(roboconSongEB);
			logger.debug("Robocon Song Photos has been uploaded successfully.");
			return getTransformer().transformEntityToDTO(roboconSongEB);
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Song Photo upload exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Song Photo upload error occurred due to " + message;
			throw new RoboconSongPhotoUploadException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Song photo upload error due to {}", ex.getCause());
			throw new RoboconSongPhotoUploadException(ex.getMessage());
		}
	}
	
	@Override
	public Object getRoboconSong(Long songId){
		RoboconSongEB roboconSongEB = getRoboconSongById(songId);
		switch(ControlFieldsHelper.getUserAgent().toUpperCase()){
		case "ANDROID":
		case "IOS":
			return roboconMobileSongTransformer.transformEntityToDTO(roboconSongEB);
		case "WEB":
		default:
			return roboconSongTransformer.transformEntityToDTO(roboconSongEB);
	}
	}
	
	@Override
	public Object fetchRoboconSongs(Integer pageNum, Integer pageLimit, HttpServletResponse response){
		logger.debug("Fetching Songs for page ({}) with limit ({})", pageNum, pageLimit);
		RoboconSongCriteriaFilter cf = new RoboconSongCriteriaFilter();
		RoboconSongSearchCriteriaFilter cfs = new RoboconSongSearchCriteriaFilter();
		Integer codeFrom = ControlFieldsHelper.getIntFieldValue(CODE_FROM);
		Integer codeTo = ControlFieldsHelper.getIntFieldValue(CODE_TO);
		Integer artistId = ControlFieldsHelper.getIntFieldValue(ARTIST_ID);
		Integer albumId = ControlFieldsHelper.getIntFieldValue(ALBUM_ID);
		Integer providerId = ControlFieldsHelper.getIntFieldValue(PROVIDER_ID);
		Integer genreId = ControlFieldsHelper.getIntFieldValue(GENRE_ID);
		String keyword = ControlFieldsHelper.getStrFieldValue(KEYWORD);
		try {
			if(keyword != null && !keyword.isEmpty()){
				keyword = URLDecoder.decode(keyword, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot decode keyword:{}, error {}", keyword, e);
		}
		cfs.orderBySongId(true);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cfs.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cfs.setMaxResult(pageLimit);
		}
		if(keyword != null && !keyword.isEmpty()){
			roboconSongDAO.enableSearchMetaLangFilter();
			cf.setSongName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
			cfs.setSongName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
		}else{
			roboconSongDAO.enableMetaLangFilter();
		}
		if(artistId != null){
			cf.setArtistId(Long.valueOf(artistId));
			cfs.setArtistId(Long.valueOf(artistId));
		}
		if(albumId != null){
			cf.setAlbumId(Long.valueOf(albumId));
			cfs.setAlbumId(Long.valueOf(albumId));
		}
		if(providerId != null){
			cf.setProviderId(Long.valueOf(providerId));
			cfs.setProviderId(Long.valueOf(providerId));
		}
		if(genreId != null){
			cf.setGenreId(Long.valueOf(genreId));
			cfs.setGenreId(Long.valueOf(genreId));
		}
		
		if(codeFrom != null && codeTo != null){
			cf.setSongId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
			cfs.setSongId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
		}else if(codeFrom != null){
			cf.setSongId(Long.valueOf(codeFrom));
			cfs.setSongId(Long.valueOf(codeFrom));
		}else if(codeTo != null){
			cf.setSongId(Long.valueOf(codeTo));
			cfs.setSongId(Long.valueOf(codeTo));
		}
		
		List<RoboconSongEB> roboconSongsEB = roboconSongDAO.getEntityListByCriteria(cfs);
		int count = roboconSongDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		switch(ControlFieldsHelper.getUserAgent().toUpperCase()){
			case "ANDROID":
			case "IOS":
				return roboconMobileSongTransformer.transformEntityToDTO(roboconSongsEB);
			case "WEB":
			default:
				return roboconSongTransformer.transformEntityToDTO(roboconSongsEB);
		}
	}
	
	@Override
	public List<RoboconSongKaraokeDTO> fetchRoboconSongKaraoke(Integer pageNum, Integer pageLimit){
		logger.debug("Fetching Song karaoke for page ({}) with limit ({})", pageNum, pageLimit);
		RoboconSongKaraokeCriteriaFilter cf = new RoboconSongKaraokeCriteriaFilter();
		cf.orderByKaraokeId(true);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
		List<RoboconSongKaraokeEB> roboconSongKaraokeEB = roboconSongKaraokeDAO.getEntityListByCriteria(cf);
		return roboconSongKaraokeTransformer.transformEntityToDTO(roboconSongKaraokeEB);
	}
	
	@Override
	public List<RoboconSongKaraokeDTO> filterRoboconSongKaraoke(Long categoryId, Integer pageNum, Integer pageLimit){
		logger.debug("Filtering Song karaoke with category ({}), for page ({}) with limit ({})", categoryId, pageNum, pageLimit);
		RoboconSongKaraokeCriteriaFilter cf = new RoboconSongKaraokeCriteriaFilter();
		cf.orderByKaraokeId(true);
		cf.setCategoryId(categoryId);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
		List<RoboconSongKaraokeEB> roboconSongKaraokeEB = roboconSongKaraokeDAO.getEntityListByCriteria(cf);
		return roboconSongKaraokeTransformer.transformEntityToDTO(roboconSongKaraokeEB);
	}
	
	@Override
	public List<RoboconUserSingDTO> fetchRoboconUserSing(Integer pageNum, Integer pageLimit, boolean verified){
		RoboconUserSingCriteriaFilter cf = new RoboconUserSingCriteriaFilter();
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(NumberUtils.isNumber(userIdStr)){
			cf.setUserId(Long.valueOf(userIdStr));
		}
		String songIdStr = ControlFieldsHelper.getStrFieldValue(SONG_ID);
		if(NumberUtils.isNumber(songIdStr)){
			cf.setSongId(Long.valueOf(songIdStr));
		}
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
		cf.orderByUserSingId(true);
		cf.isVerified(verified);
		List<RoboconUserSingEB> roboconUserSingEB = roboconUserSingDAO.getEntityListByCriteria(cf);
		return roboconUserSingTransformer.transformEntityToDTO(roboconUserSingEB);
	}
	
	@Override
	public void playSong(Long contentId, HttpServletRequest request, HttpServletResponse response){
		logger.debug("Playing Robocon Song Content with content id ({})", contentId);
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		/*if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}*/
		RoboconSongContentEB roboconSongContentEB = roboconSongContentDAO.getEntityByIDIfExists(contentId);
		if(roboconSongContentEB == null){
			logger.error("Error: No Content found with id ({}).", contentId);
			String msg = "No Song Content found with id " +  contentId;
			throw new RoboconSongContentNotFoundException(msg);
		}
		roboconFileService.download(roboconSongContentEB.getRoboconSongEB().pk(), roboconSongContentEB, RoboconUserActionEnum.PLAY, request, response);
		if(NumberUtils.isNumber(userIdStr)){
			roboconUserSongHistoryService.createOrUpdateSongHistory(roboconSongContentEB.getRoboconSongEB(), RoboconUserActionEnum.PLAY);
		}
	}
	
	@Override
	public void downloadSong(Long contentId, HttpServletRequest request, HttpServletResponse response){
		logger.debug("Downloading Robocon Song Content with content id ({})", contentId);
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		RoboconSongContentEB roboconSongContentEB = roboconSongContentDAO.getEntityByIDIfExists(contentId);
		if(roboconSongContentEB == null){
			logger.error("Error: No Content found with id ({}).", contentId);
			String msg = "No Song Content found with id " +  contentId;
			throw new RoboconSongContentNotFoundException(msg);
		}
		roboconFileService.download(roboconSongContentEB.getRoboconSongEB().pk(), roboconSongContentEB, RoboconUserActionEnum.DOWNLOAD, request, response);
		
		roboconUserSongHistoryService.createOrUpdateSongHistory(roboconSongContentEB.getRoboconSongEB(), RoboconUserActionEnum.DOWNLOAD);
	}
	
	@Override
	public void downloadKaraoke(Long karokeId, HttpServletRequest request, HttpServletResponse response){
		logger.debug("Downloading Robocon Song Karaoke with karaoke id ({})", karokeId);
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		RoboconSongKaraokeEB roboconSongKaraokeEB = getRoboconSongKaraokeById(karokeId);
		roboconFileService.download(roboconSongKaraokeEB.getRoboconSongEB().pk(), roboconSongKaraokeEB, RoboconUserActionEnum.DOWNLOAD, request, response);
	}
	
	@Override
	public void downloadLyrics(Long lyricsId, HttpServletRequest request, HttpServletResponse response){
		logger.debug("Downloading Robocon Song lyrics with lyrics id ({})", lyricsId);
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		RoboconSongLyricsEB roboconSongLyricsEB = getRoboconSongLyricsById(lyricsId);
		roboconFileService.download(roboconSongLyricsEB.getRoboconSongEB().pk(), roboconSongLyricsEB, RoboconUserActionEnum.DOWNLOAD, request, response);
	}
	
	@Override
	public void downloadUserSing(Long singId, HttpServletRequest request, HttpServletResponse response){
		logger.debug("Downloading Robocon User Sing with sing id ({})", singId);
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		boolean verifiedOnly = true;
		for(RoleDTO roleDTO : userService.getRecordById(SecurityUtil.getUserId()).getRoles()){
			if(roleDTO.getCode().equalsIgnoreCase("ROLE_ADMIN")){
				verifiedOnly = false;
			}
		}
		RoboconUserSingEB roboconUserSingEB = getRoboconUserSingById(singId, verifiedOnly);
		roboconFileService.download(roboconUserSingEB.getRoboconSongEB().pk(), roboconUserSingEB, RoboconUserActionEnum.DOWNLOAD, request, response);
	}
	
	@Override
	public void playUserSing(Long singId, HttpServletRequest request, HttpServletResponse response){
		logger.debug("Playing Robocon User Sing with sing id ({})", singId);
		String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}
		boolean verifiedOnly = true;
		for(RoleDTO roleDTO : userService.getRecordById(SecurityUtil.getUserId()).getRoles()){
			if(roleDTO.getCode().equalsIgnoreCase("ROLE_ADMIN")){
				verifiedOnly = false;
			}
		}
		RoboconUserSingEB roboconUserSingEB = getRoboconUserSingById(singId, verifiedOnly);
		roboconFileService.download(roboconUserSingEB.getRoboconSongEB().pk(), roboconUserSingEB, RoboconUserActionEnum.PLAY, request, response);
	}
	
	@Override
	public void printLyrics(Long lyricsId, HttpServletRequest request, HttpServletResponse response){
		logger.debug("Printing Robocon Song lyrics with lyrics id ({})", lyricsId);
		/*String userIdStr = ControlFieldsHelper.getStrFieldValue(USER_ID);
		if(!NumberUtils.isNumber(userIdStr)){
			logger.debug("Error: invalid user id or not found error.");
			throw new RoboconInvalidUserIdException();
		}*/
		RoboconSongLyricsEB roboconSongLyricsEB = getRoboconSongLyricsById(lyricsId);
		roboconFileService.download(roboconSongLyricsEB.getRoboconSongEB().pk(), roboconSongLyricsEB, RoboconUserActionEnum.PRINT, request, response);
	}
	
	@Override
	public void removeRoboconSong(Long songId){
		logger.debug("Deleteing song with id {}.", songId);
		roboconSongDAO.deleteEntity(songId);
		logger.debug("Song {} has been deleted successfully.", songId);
	}
	
	@Override
	public RoboconSongDTO removeContent(Long contentId){
		RoboconSongContentEB roboconSongContentEB = getRoboconSongContentById(contentId);
		RoboconSongEB roboconSongEB = roboconSongContentEB.getRoboconSongEB();
		roboconSongEB.removeRoboconSongContentEB(roboconSongContentEB);
		roboconSongContentDAO.deleteEntity(roboconSongContentEB);
		roboconSongContentDAO.flush();
		roboconSongDAO.flush();
		roboconSongDAO.refresh(roboconSongEB);
		return roboconSongTransformer.transformEntityToDTO(roboconSongEB);
	}
	
	@Override
	public RoboconSongDTO removeKaraoke(Long karaokeId){
		RoboconSongKaraokeEB roboconSongKaraokeEB = getRoboconSongKaraokeById(karaokeId);
		RoboconSongEB roboconSongEB = roboconSongKaraokeEB.getRoboconSongEB();
		roboconSongEB.removeRoboconSongKaraokeEB(roboconSongKaraokeEB);
		roboconSongKaraokeDAO.deleteEntity(roboconSongKaraokeEB);
		roboconSongKaraokeDAO.flush();
		roboconSongDAO.flush();
		roboconSongDAO.refresh(roboconSongEB);
		return roboconSongTransformer.transformEntityToDTO(roboconSongEB);
	}
	
	@Override
	public RoboconSongDTO removeLyrics(Long lyricsId){
		RoboconSongLyricsEB roboconSongLyricsEB = getRoboconSongLyricsById(lyricsId);
		RoboconSongEB roboconSongEB = roboconSongLyricsEB.getRoboconSongEB();
		roboconSongEB.removeRoboconSongLyricsEB(roboconSongLyricsEB);
		roboconSongLyricsDAO.deleteEntity(roboconSongLyricsEB);
		roboconSongLyricsDAO.flush();
		roboconSongDAO.flush();
		roboconSongDAO.refresh(roboconSongEB);
		return roboconSongTransformer.transformEntityToDTO(roboconSongEB);
	}
	
	@Override
	public void removeUserSing(Long singId){
		RoboconUserSingEB roboconUserSingEB = getRoboconUserSingById(singId, false);
		RoboconSongEB roboconSongEB = roboconUserSingEB.getRoboconSongEB();
		roboconSongEB.removeRoboconUserSingEB(roboconUserSingEB);
		roboconUserSingDAO.deleteEntity(roboconUserSingEB);
		roboconUserSingDAO.flush();
		roboconSongDAO.flush();
	}
	
	@Override
	public void verifyUserSing(Long singId){
		logger.debug("Verifing robocon user sing ({})", singId);
		RoboconUserSingEB roboconUserSingEB = getRoboconUserSingById(singId, false);
		roboconUserSingEB.setVerified(true);
	}
	
	@Override
	public RoboconSongEB getRoboconSongById(Long songId){
		RoboconSongCriteriaFilter cf = new RoboconSongCriteriaFilter();
		cf.setSongId(songId);
		RoboconSongEB roboconSongEB = roboconSongDAO.getEntityByCriteriaIfExists(cf);
		if(roboconSongEB == null){
			logger.error("Error: No Robocon Song found with id ({})", songId);
			String msg = "No Robocon Song found with id ("+songId+")"+" Exception.";
			throw new RoboconSongNotFoundException(msg);
		}
		return roboconSongEB;
	}
	
	private RoboconSongContentEB getRoboconSongContentById(Long contentId){
		RoboconSongContentCriteriaFilter cf = new RoboconSongContentCriteriaFilter();
		cf.setContentId(contentId);
		RoboconSongContentEB roboconSongContentEB = roboconSongContentDAO.getEntityByCriteriaIfExists(cf);
		if(roboconSongContentEB == null){
			logger.error("Error: No Robocon content found with id ({})", contentId);
			String msg = "No Robocon content found with id ("+contentId+") Exception";
			throw new RoboconSongContentNotFoundException(msg);
		}
		return roboconSongContentEB;
	}
	
	private RoboconSongKaraokeEB getRoboconSongKaraokeById(Long karaokeId){
		RoboconSongKaraokeCriteriaFilter cf = new RoboconSongKaraokeCriteriaFilter();
		cf.setKaraokeId(karaokeId);
		RoboconSongKaraokeEB roboconSongKaraokeEB = roboconSongKaraokeDAO.getEntityByCriteriaIfExists(cf);
		if(roboconSongKaraokeEB == null){
			logger.error("Error: No Robocon karaoke found with id ({})", karaokeId);
			String msg = "No Robocon karaoke found with id ("+karaokeId+") Exception";
			throw new RoboconSongKaraokeNotFoundException(msg);
		}
		return roboconSongKaraokeEB;
	}
	
	private RoboconSongLyricsEB getRoboconSongLyricsById(Long lyricsId){
		RoboconSongLyricsCriteriaFilter cf = new RoboconSongLyricsCriteriaFilter();
		cf.setSongLyricsId(lyricsId);
		RoboconSongLyricsEB roboconSongLyricsEB = roboconSongLyricsDAO.getEntityByCriteriaIfExists(cf);
		if(roboconSongLyricsEB == null){
			logger.error("Error: No Robocon lyrics found with id ({})", lyricsId);
			String msg = "No Robocon lyrics found with id ("+lyricsId+") Exception";
			throw new RoboconSongLyricsNotFoundException(msg);
		}
		return roboconSongLyricsEB;
	}
	
	private RoboconUserSingEB getRoboconUserSingById(Long singId, boolean verifiedOnly){
		RoboconUserSingCriteriaFilter cf = new RoboconUserSingCriteriaFilter();
		cf.setUserSingId(singId);
		if(verifiedOnly){
			cf.isVerified(verifiedOnly);
		}
		RoboconUserSingEB roboconUserSingEB = roboconUserSingDAO.getEntityByCriteriaIfExists(cf);
		if(roboconUserSingEB == null){
			logger.error("Error: No Robocon user sing found with id ({})", singId);
			String msg = "No Robocon user sing found with id ("+singId+") Exception";
			throw new RoboconUserSingNotFoundException(msg);
		}
		return roboconUserSingEB;
	}
	
	@Override
	protected void doBeforeValidate(RoboconSongDTO dto, boolean isNew) {
		roboconBeanValidationManager.validate(dto);
		super.doBeforeValidate(dto, isNew);
	}
	
	@Override
	protected void doBeforePersist(RoboconSongEB roboconSongEB, RoboconSongDTO roboconSongDTO, boolean isNew) {
		if(roboconSongEB.getRoboconSongMetaEB() != null && !roboconSongEB.getRoboconSongMetaEB().isEmpty()){
			for(RoboconSongMetaEB roboconSongMetaEB : roboconSongEB.getRoboconSongMetaEB()){
				if(roboconSongMetaEB.getRoboconSongEB() == null){
					roboconSongMetaEB.setRoboconSongEB(roboconSongEB);
				}
			}
		}
		if(roboconSongEB.getRoboconSongTagsEB() != null && !roboconSongEB.getRoboconSongTagsEB().isEmpty()){
			for(RoboconSongTagEB roboconSongTagEB : roboconSongEB.getRoboconSongTagsEB()){
				if(roboconSongTagEB.getRoboconSongEB() == null){
					roboconSongTagEB.setRoboconSongEB(roboconSongEB);
				}
			}
		}
		super.doBeforePersist(roboconSongEB, roboconSongDTO, isNew);
	}
	
	@Override
	protected void doAfterPersist(RoboconSongEB entity, RoboconSongDTO dto, boolean isNew) {
		getDAO().flush();
		roboconSongDAO.refresh(entity);
		super.doAfterPersist(entity, dto, isNew);
	}
	
	@Override
	protected DTOTransformer<RoboconSongEB, RoboconSongDTO> getTransformer() {
		return roboconSongTransformer;
	}

	@Override
	protected DTOTransformer<RoboconSongEB, RoboconSongDTO> getSummaryTransformer() {
		return roboconSongTransformer;
	}

	@Override
	protected DAO<RoboconSongEB, Long, RoboconSongCriteriaFilter> getDAO() {
		return roboconSongDAO;
	}

}
