package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.CODE_FROM;
import static com.masrawy.robocon.model.RoboconConstants.CODE_TO;
import static com.masrawy.robocon.model.RoboconConstants.KEYWORD;
import static com.masrawy.robocon.model.RoboconConstants.SPECIAL_CHARACTERS_PATTERN;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.persistent.dao.DAO;
import com.cdm.core.util.DumpUtil;
import com.masrawy.robocon.dao.RoboconProviderDAO;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.dto.RoboconProviderDTO;
import com.masrawy.robocon.model.RoboconProviderEB;
import com.masrawy.robocon.model.RoboconProviderMetaEB;
import com.masrawy.robocon.query.RoboconProviderCriteriaFilter;
import com.masrawy.robocon.query.RoboconProviderSearchCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconProviderCreationException;
import com.masrawy.robocon.services.exception.RoboconProviderException;
import com.masrawy.robocon.services.exception.RoboconProviderNotFoundException;
import com.masrawy.robocon.services.exception.RoboconProviderUpdateException;
import com.masrawy.robocon.transformer.RoboconProviderMetaTransformer;
import com.masrawy.robocon.transformer.RoboconProviderTransformer;
import com.masrawy.robocon.validation.RoboconBeanValidationManager;

@Service("roboconProviderService")
@Transactional
public class RoboconProviderServiceImpl extends AbstractCrudService<RoboconProviderEB, RoboconProviderDTO, RoboconProviderDTO, Long, RoboconProviderCriteriaFilter> implements RoboconProviderService{

	private final static Logger logger = LoggerFactory.getLogger(RoboconProviderServiceImpl.class);
	
	@Autowired
	private RoboconProviderDAO roboconProviderDAO;
	
	@Autowired
	private RoboconProviderTransformer roboconProviderTransformer;
	
	@Autowired
	private RoboconProviderMetaTransformer roboconProviderMetaTransformer;
	
	@Autowired
	private RoboconMessageService roboconMessageService;
	
	@Autowired
	private RoboconBeanValidationManager roboconBeanValidationManager;
	
	@Override
	public RoboconProviderDTO createProvider(RoboconProviderDTO roboconProviderDTO) {
		logger.debug("Creating Provider");
		RoboconProviderEB roboconProviderEB = null;
		try{
			roboconProviderEB = createEntity(roboconProviderDTO);
			roboconProviderDTO = roboconProviderTransformer.transformEntityToDTO(roboconProviderEB);
			logger.debug("Provider ({}) has been created successfully.", roboconProviderDTO.pk());
			return roboconProviderDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Provider create exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot create provider due to " + message;
			throw new RoboconProviderCreationException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Provider creation error due to {}.", ex.getMessage());
			throw new RoboconProviderCreationException(ex.getMessage());
		}
	}

	@Override
	public RoboconProviderDTO updateProvider(RoboconProviderDTO roboconProviderDTO) {
		logger.debug("Updating Provider ({})", roboconProviderDTO.pk());
		RoboconProviderEB roboconProviderEB = null;
		try{
			roboconProviderEB = updateEntity(roboconProviderDTO);
			roboconProviderDTO = roboconProviderTransformer.transformEntityToDTO(roboconProviderEB);
			logger.debug("Provider ({}) has been updated successfully.", roboconProviderDTO.pk());
			return roboconProviderDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Provider update exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot update provider due to " + message;
			throw new RoboconProviderUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Provider update error due to {}.", ex.getMessage());
			throw new RoboconProviderUpdateException(ex.getMessage());
		}
	}


	@Override
	public RoboconProviderDTO addMeta(RoboconMetaDTO roboconMetaDTO, Long providerId) {
		logger.debug("Adding Meta {} to Provider ({})", DumpUtil.dumpAsJson(roboconMetaDTO), providerId);
		try{
			roboconBeanValidationManager.validate(roboconMetaDTO);
			RoboconProviderEB roboconProviderEB = getProviderIfExists(providerId);
			RoboconProviderMetaEB roboconProviderMetaEB = roboconProviderMetaTransformer.transformDTOToEntity(roboconMetaDTO);
			roboconProviderEB.addRoboconProviderMetaEB(roboconProviderMetaEB);
			roboconProviderDAO.flush();
			roboconProviderDAO.refresh(roboconProviderEB);
			return roboconProviderTransformer.transformEntityToDTO(roboconProviderEB);
		}catch(ConstraintViolationException ex){
			logger.error("Error: Provider Meta update  exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			throw new RoboconProviderUpdateException(ex.getCause().getMessage());
		}catch(RuntimeException ex){
			logger.error("Error: Provider Meta update error due to {}.", ex.getMessage());
			throw new RoboconProviderUpdateException(ex.getMessage());
		}
	}
	
	@Override
	public RoboconProviderDTO fetchProvider(Long providerId) {
		logger.debug("Fetching Provider with id ({}).", providerId);
		RoboconProviderEB roboconProviderEB = getProviderIfExists(providerId);
		RoboconProviderDTO roboconProviderDTO = roboconProviderTransformer.transformEntityToDTO(roboconProviderEB);
		return roboconProviderDTO;
	}

	@Override
	public List<RoboconProviderDTO> fetchProvidersByPaging(Integer pageNum, Integer pageLimit, HttpServletResponse response) {
		logger.debug("Fetching Providers for page number ({}) and limit ({})", pageNum, pageLimit);
		RoboconProviderCriteriaFilter cf = new RoboconProviderCriteriaFilter();
		RoboconProviderSearchCriteriaFilter cfs = new RoboconProviderSearchCriteriaFilter();
		Integer codeFrom = ControlFieldsHelper.getIntFieldValue(CODE_FROM);
		Integer codeTo = ControlFieldsHelper.getIntFieldValue(CODE_TO);
		String keyword = ControlFieldsHelper.getStrFieldValue(KEYWORD);
		try {
			if(keyword != null && !keyword.isEmpty()){
				keyword = URLDecoder.decode(keyword, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot decode keyword:{}, error {}", keyword, e);
		}
		cfs.orderByProviderId(true);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cfs.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cfs.setMaxResult(pageLimit);
		}
		if(keyword != null && !keyword.isEmpty()){
			cf.setProviderName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
			cfs.setProviderName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
		}else{
			roboconProviderDAO.enableMetaLangFilter();
		}
		if(codeFrom != null && codeTo != null){
			cf.setProviderId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
			cfs.setProviderId(Long.valueOf(codeFrom), Long.valueOf(codeTo));
		}else if(codeFrom != null){
			cf.setProviderId(Long.valueOf(codeFrom));
			cfs.setProviderId(Long.valueOf(codeFrom));
		}else if(codeTo != null){
			cf.setProviderId(Long.valueOf(codeTo));
			cfs.setProviderId(Long.valueOf(codeTo));
		}
		List<RoboconProviderEB> roboconProvidersEB = roboconProviderDAO.getEntityListByCriteria(cfs);
		int count = roboconProviderDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		return roboconProviderTransformer.transformEntityToDTO(roboconProvidersEB);
	}

	@Override
	public void removeProvider(Long providerId){
		logger.debug("Try removing robocon provider with id ({}).", providerId);
		RoboconProviderEB roboconProviderEB = getProviderIfExists(providerId);
		if(!roboconProviderEB.getRoboconSongsEB().isEmpty()){
			logger.error("Error: Cannot remove robocon provider ({}), as songs exist depends on it.", providerId);
			String msg = "Cannot remove robocon provider ("+providerId+"), as songs exist depends on it exception.";
			throw new RoboconProviderException(msg);
		}
		if(!roboconProviderEB.getRoboconAlbumsEB().isEmpty()){
			logger.error("Error: Cannot remove robocon provider ({}), as albums exist depends on it.", providerId);
			String msg = "Cannot remove robocon provider ("+providerId+"), as albums exist depends on it exception.";
			throw new RoboconProviderException(msg);
		}
		roboconProviderDAO.deleteEntity(roboconProviderEB);
	}
	
	private RoboconProviderEB getProviderIfExists(Long providerId){
		RoboconProviderEB roboconProviderEB = roboconProviderDAO.getEntityByIDIfExists(providerId);
		if(roboconProviderEB == null){
			logger.error("Error: No Provider found with id ({}) exception", providerId);
			String msg = "No Provider found with id (" + providerId + ").";
			throw new RoboconProviderNotFoundException(msg);
		}
		return roboconProviderEB;
	}
	
	@Override
	protected void doBeforeValidate(RoboconProviderDTO dto, boolean isNew) {
		roboconBeanValidationManager.validate(dto);
		super.doBeforeValidate(dto, isNew);
	}
	
	@Override
	protected void doBeforePersist(RoboconProviderEB roboconProviderEB, RoboconProviderDTO roboconProviderDTO, boolean isNew) {
		if(roboconProviderEB.getRoboconProviderMetaEB() != null && !roboconProviderEB.getRoboconProviderMetaEB().isEmpty()){
			for(RoboconProviderMetaEB roboconProviderMetaEB : roboconProviderEB.getRoboconProviderMetaEB()){
				if(roboconProviderMetaEB.getRoboconProviderEB() == null){
					roboconProviderMetaEB.setRoboconProviderEB(roboconProviderEB);
				}
			}
		}
		super.doBeforePersist(roboconProviderEB, roboconProviderDTO, isNew);
	}
	
	@Override
	protected void doAfterPersist(RoboconProviderEB entity, RoboconProviderDTO dto, boolean isNew) {
		roboconProviderDAO.flush();
		roboconProviderDAO.refresh(entity);
		super.doAfterPersist(entity, dto, isNew);
	}
	
	@Override
	protected DTOTransformer<RoboconProviderEB, RoboconProviderDTO> getTransformer() {
		return roboconProviderTransformer;
	}

	@Override
	protected DTOTransformer<RoboconProviderEB, RoboconProviderDTO> getSummaryTransformer() {
		return roboconProviderTransformer;
	}

	@Override
	protected DAO<RoboconProviderEB, Long, RoboconProviderCriteriaFilter> getDAO() {
		return roboconProviderDAO;
	}
}
