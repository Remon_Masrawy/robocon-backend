package com.masrawy.robocon.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.configuration.service.ConfigurationService;
import com.cdm.core.exception.common.ConfigurationException;
import com.cdm.core.exception.common.InvalidArgumentException;
import com.cdm.core.file.exception.FileUploadException;
import com.cdm.core.file.exception.FileUploadException.UploadError;
import com.cdm.core.file.service.FileSystemServiceImpl;
import com.cdm.core.persistent.model.Entity;
import com.cdm.core.util.UUIDGenerator;
import com.masrawy.robocon.file.model.FileTypeEnum;
import com.masrawy.robocon.file.model.RoboconFileInfo;
import com.masrawy.robocon.model.AbstractRoboconFileEB;
import com.masrawy.robocon.model.RoboconMimeTypeEnum;
import com.masrawy.robocon.model.RoboconSongContentEB;
import com.masrawy.robocon.model.RoboconSongKaraokeEB;
import com.masrawy.robocon.model.RoboconSongLyricsEB;
import com.masrawy.robocon.model.RoboconUserActionEnum;
import com.masrawy.robocon.model.RoboconUserSingEB;
import com.masrawy.robocon.services.exception.DirectoryNotExistException;
import com.masrawy.robocon.services.exception.FileAccessException;
import com.masrawy.robocon.services.exception.RoboconSongContentNotFoundException;

@Transactional
public class RoboconFileServiceImpl extends FileSystemServiceImpl implements RoboconFileService{

	private static final Logger logger = LoggerFactory.getLogger(RoboconFileServiceImpl.class);
	
	@Autowired
	private ConfigurationService configurationService;
	
	private static final String TEMP_FOLDER = "temp";
	
	private List<String> supportedRoboconFileContentTypes;
	
	private List<String> supportedRoboconFileLyricsTypes;
	
	AudioFile audioFile;
	
	@Override
	public Entity<?> uploadFile(MultipartFile multipartFile, Long dirId, RoboconMimeTypeEnum roboconMimeTypeEnum){
		
		logger.info("Uploading MultipartFile File " + toString(multipartFile));
		
		RoboconFileInfo fileInfo = upload(dirId, multipartFile, roboconMimeTypeEnum);
		
		AudioHeader audioHeader = null;
		
		switch(roboconMimeTypeEnum){
		case FULL_TRACK:
			audioHeader = getAudioHeader(fileInfo.getFile());
			RoboconSongContentEB roboconSongContentEB = new RoboconSongContentEB();
			roboconSongContentEB.setFileName(fileInfo.getOriginalName());
			roboconSongContentEB.setChecksum(fileInfo.getChecksum());
			roboconSongContentEB.setContentType(fileInfo.getContentType());
			roboconSongContentEB.setExtension(FilenameUtils.getExtension(fileInfo.getOriginalName()));
			roboconSongContentEB.setFileType(FileTypeEnum.AUDIO);
			roboconSongContentEB.setFileSize(fileInfo.getFile().length());
			if(audioHeader != null){
				roboconSongContentEB.setBitRate(audioHeader.getBitRate());
				roboconSongContentEB.setFormat(audioHeader.getFormat());
				roboconSongContentEB.setSampleRate(audioHeader.getSampleRate());
				roboconSongContentEB.setChannels(audioHeader.getChannels());
				roboconSongContentEB.setDuration(audioHeader.getTrackLength());
			}
			return roboconSongContentEB;
		case SONG_KARAOKE:
			audioHeader = getAudioHeader(fileInfo.getFile());
			RoboconSongKaraokeEB roboconSongKaraokeEB = new RoboconSongKaraokeEB();
			roboconSongKaraokeEB.setFileName(fileInfo.getOriginalName());
			roboconSongKaraokeEB.setChecksum(fileInfo.getChecksum());
			roboconSongKaraokeEB.setContentType(fileInfo.getContentType());
			roboconSongKaraokeEB.setExtension(FilenameUtils.getExtension(fileInfo.getOriginalName()));
			roboconSongKaraokeEB.setFileType(FileTypeEnum.AUDIO);
			roboconSongKaraokeEB.setFileSize(fileInfo.getFile().length());
			if(audioHeader != null){
				roboconSongKaraokeEB.setBitRate(audioHeader.getBitRate());
				roboconSongKaraokeEB.setFormat(audioHeader.getFormat());
				roboconSongKaraokeEB.setSampleRate(audioHeader.getSampleRate());
				roboconSongKaraokeEB.setChannels(audioHeader.getChannels());
				roboconSongKaraokeEB.setDuration(audioHeader.getTrackLength());
			}
			return roboconSongKaraokeEB;
		case SONG_LYRICS:
			RoboconSongLyricsEB roboconSongLyricsEB = new RoboconSongLyricsEB();
			roboconSongLyricsEB.setFileName(fileInfo.getOriginalName());
			roboconSongLyricsEB.setChecksum(fileInfo.getChecksum());
			roboconSongLyricsEB.setContentType(fileInfo.getContentType());
			roboconSongLyricsEB.setExtension(FilenameUtils.getExtension(fileInfo.getOriginalName()));
			roboconSongLyricsEB.setFileType(FileTypeEnum.TEXT);
			roboconSongLyricsEB.setFileSize(fileInfo.getFile().length());
			return roboconSongLyricsEB;
		case USER_SING:
			audioHeader = getAudioHeader(fileInfo.getFile());
			RoboconUserSingEB roboconUserSingEB = new RoboconUserSingEB();
			roboconUserSingEB.setFileName(fileInfo.getOriginalName());
			roboconUserSingEB.setChecksum(fileInfo.getChecksum());
			roboconUserSingEB.setContentType(fileInfo.getContentType());
			roboconUserSingEB.setExtension(FilenameUtils.getExtension(fileInfo.getOriginalName()));
			roboconUserSingEB.setFileType(FileTypeEnum.AUDIO);
			roboconUserSingEB.setFileSize(fileInfo.getFile().length());
			if(audioHeader != null){
				roboconUserSingEB.setBitRate(audioHeader.getBitRate());
				roboconUserSingEB.setFormat(audioHeader.getFormat());
				roboconUserSingEB.setSampleRate(audioHeader.getSampleRate());
				roboconUserSingEB.setChannels(audioHeader.getChannels());
				roboconUserSingEB.setDuration(audioHeader.getTrackLength());
			}
			return roboconUserSingEB;
		default :
			break;
		}
		
		return null;
	}
	
	private AudioHeader getAudioHeader(File file){
		AudioHeader audioHeader = null;
		try {
		audioFile = AudioFileIO.read(file);
		audioHeader = audioFile.getAudioHeader();
		logger.debug("File bitrate:{}, format:{}, sampleRate:{}, channels:{}, trackLength:{}", 
				audioHeader.getBitRate(),
				audioHeader.getFormat(), 
				audioHeader.getSampleRate(), 
				audioHeader.getChannels(),
				audioHeader.getTrackLength());
		} catch (CannotReadException e) {
			logger.error("Error occured {}", e);
		} catch (TagException e) {
			logger.error("Error occured {}", e);
		} catch (ReadOnlyFileException e) {
			logger.error("Error occured {}", e);
		} catch (InvalidAudioFrameException e) {
			logger.error("Error occured {}", e);
		} catch(Exception e){
			logger.error("Error occured {}", e);
		}
		return audioHeader;
	}
	
	public void download(Long dirId, AbstractRoboconFileEB<?> fileInfo, RoboconUserActionEnum roboconUserActionEnum, HttpServletRequest request, HttpServletResponse response){
		try{
			logger.debug("Downloading {} filename={} with id {}, content type '{}' checksum={}.", 
					fileInfo.getFileType(), 
					fileInfo.getFileName(), 
					fileInfo.pk(), 
					fileInfo.getContentType(), 
					fileInfo.getChecksum());
			
			manage(dirId, fileInfo, request, response, roboconUserActionEnum);
		} catch (IOException e) {
			String msg = "Exception is ocuured while downloading " + fileInfo.getFileName()
					+ " due to " + e.getMessage();
			logger.error(msg, e);
			throw new FileAccessException(msg);
		} catch (ServletException e) {
			String msg = "Exception is ocuured while downloading " + fileInfo.getFileName()
					+ " due to " + e.getMessage();
			logger.error(msg, e);
			throw new FileAccessException(msg);
		}
	}
	
	private void manage(Long dirId, AbstractRoboconFileEB<?> fileInfo, HttpServletRequest request, HttpServletResponse response, RoboconUserActionEnum roboconUserActionEnum) throws ServletException, IOException{
		File mediaFolder = getStoreFolder(fileInfo.getFileMimeType());
		File contentFolder = new File(mediaFolder, getSubDirectory(fileInfo.getFileMimeType(), dirId));
		if(!contentFolder.exists()){
			logger.error("Error: Directory {} not exist exception", dirId.toString());
			String msg = "trying to access directory " + dirId.toString() + " witch not exist exception.";
			throw new DirectoryNotExistException(msg);
		}
		if(!contentFolder.isDirectory()){
			logger.error("Error: {} is not a directory.", dirId.toString());
			String msg = dirId.toString() + " is not a directory.";
			throw new DirectoryNotExistException(msg);
		}
		
		String filename = fileInfo.getChecksum().concat(".").concat(fileInfo.getExtension());
		String originalFilename = FilenameUtils.getBaseName(fileInfo.getFileName()).concat(".").concat(fileInfo.getExtension());
		
		File file = new File(contentFolder, filename);
		if(!file.exists()){
			logger.error("Error: content file {} not found.", filename);
			String msg = "Content file " + filename + " not found Exception.";
			throw new RoboconSongContentNotFoundException(msg);
		}
		
		File tempFolder = getTempFolder();
		File tempFile = new File(tempFolder, fileInfo.getChecksum().concat("_").concat(originalFilename));
		if(tempFile.exists()){
			if(!tempFile.canRead()){
				logger.error("Content temp file {} can not read exception." + tempFile.getAbsolutePath());
				String msg = "Cannot read content temp file " + tempFile.getAbsolutePath() + " Exception.";
				throw new FileAccessException(msg);
			}
		}else{
			FileUtils.copyFile(file, tempFile);
		}
		
		appendFileToResponse(tempFile, request, response, fileInfo.getContentType(), roboconUserActionEnum);
	}
	
	protected RoboconFileInfo upload(Long dirId, MultipartFile multipartFile, RoboconMimeTypeEnum roboconMimeTypeEnum){
		
		String filename = multipartFile.getOriginalFilename();
		String contentType = multipartFile.getContentType();
		String subDirectory = getSubDirectory(roboconMimeTypeEnum, dirId);
		
		try{
			
			InputStream inStream = multipartFile.getInputStream();
			
			logger.info("Uploading filename=" + filename);
			
			// 1.Validate data
			if (contentType == null) {
				String msg = "ContentType must be set for uploaded file";
				logger.error(msg);
				throw new InvalidArgumentException(msg);
			}
			switch(roboconMimeTypeEnum){
				case FULL_TRACK:
				case SONG_KARAOKE:
				case USER_SING:
					if (CollectionUtils.isNotEmpty(supportedRoboconFileContentTypes)
							&& !supportedRoboconFileContentTypes.contains(contentType)) {
						String msg = "ContentType " + contentType
								+ " is not supported.Supported Contents are "
								+ supportedRoboconFileContentTypes;
						logger.error(msg);
						throw new FileUploadException(
								UploadError.FILE_UNSUPPORTED_TYPE, msg);
					}
					break;
				case SONG_LYRICS:
					if (CollectionUtils.isNotEmpty(supportedRoboconFileLyricsTypes)
							&& !supportedRoboconFileLyricsTypes.contains(contentType)) {
						String msg = "ContentType " + contentType
								+ " is not supported.Supported Contents are "
								+ supportedRoboconFileLyricsTypes;
						logger.error(msg);
						throw new FileUploadException(
								UploadError.FILE_UNSUPPORTED_TYPE, msg);
					}
					break;
				case VIDEO:
					break;
				default :
					break;
			}
			
			filename = FilenameUtils.getName(filename);
			
			// 2.Save File
			File mediaFolder = getStoreFolder(roboconMimeTypeEnum);
			String uuid = UUIDGenerator.generate();
			File mediaTempFile = new File(mediaFolder, TEMP_FOLDER
					+ File.pathSeparator + uuid);
			
			FileOutputStream output = new FileOutputStream(mediaTempFile);

			byte[] bytes = new byte[1024];
			int length = -1;
			while ((length = inStream.read(bytes)) != -1) {
				output.write(bytes, 0, length);
			}
			output.flush();
			output.close();

			FileInputStream fis = new FileInputStream(mediaTempFile);
			String checksum = DigestUtils.md5Hex(fis);
			fis.close();
			
			File contentFolder = new File(mediaFolder, subDirectory);
			try{
				boolean dirCreated = contentFolder.mkdir();
				if(!dirCreated){
					logger.error("Cannot create directory {}.", subDirectory);
				}
			}catch(SecurityException ex){
				logger.error("Failed to create directory for content {} due to {}.", subDirectory, ex.getMessage());
				String msg = "Exception is ocuured while upload " + filename
						+ " due to " + ex.getMessage();
				throw new FileUploadException(UploadError.IO_ERROR, msg);
			}
			
			File mediaFile = new File(contentFolder, checksum.concat(".").concat(FilenameUtils.getExtension(filename)));
			if (mediaFile.exists()) {
				mediaFile.delete();
				logger.info(
						"File {} is already exist, so we will delete old one and rename the temp one {}",
						checksum, uuid);
			}
			boolean renameTo = mediaTempFile.renameTo(mediaFile);
			if (renameTo) {
				logger.info("File {} is saved at the file system successfully ",
						checksum);
			} else {
				logger.error(
						"Failed to rename Temp File ({}) to File ({}), so we deleting it",
						mediaTempFile.getAbsolutePath(),
						mediaFile.getAbsolutePath());
				boolean delete = mediaTempFile.delete();
				if (!delete) {
					logger.warn("Failed to delete Temp File {} ", uuid);
				}
				throw new FileUploadException(UploadError.IO_ERROR,
						"Failed to rename Temp File " + uuid + " to File "
								+ checksum);
			}
			
			RoboconFileInfo fileInfo = new RoboconFileInfo();
			fileInfo.setFile(mediaFile);
			fileInfo.setChecksum(checksum);
			fileInfo.setOriginalName(filename);
			fileInfo.setContentType(contentType);
			fileInfo.setSizeInKb(mediaFile.length() / 1024);
			return fileInfo;
			
		}catch (IOException e) {
			String msg = "Exception is ocuured while upload "
					+ toString(multipartFile)
					+ " due to "
					+ (e.getMessage() != null ? e.getMessage() : e.getClass()
							.getSimpleName());
			logger.error(msg, e);
			throw new FileUploadException(UploadError.IO_ERROR, msg);
		}
	}
	
	private File getStoreFolder(RoboconMimeTypeEnum roboconMimeTypeEnum){
		String contentServerPath = configurationService
				.getStringParamValue(CONTENT_SERVER_PARAM);
		File mediaFolder = null;
		switch(roboconMimeTypeEnum){
		case TRUETONE:
			mediaFolder = new File(contentServerPath + CONTENT_FOLDER_PARAM);
			break;
		case STREAM:
			mediaFolder = new File(contentServerPath + CONTENT_FOLDER_PARAM);
			break;
		case FULL_TRACK:
			mediaFolder = new File(contentServerPath + CONTENT_FOLDER_PARAM);
			break;
		case SONG_KARAOKE:
			mediaFolder = new File(contentServerPath + KARAOKE_FOLDER_PARAM);
			break;
		case SONG_LYRICS:
			mediaFolder = new File(contentServerPath + LYRICS_FOLDER_PARAM);
			break;
		case USER_SING:
			mediaFolder = new File(contentServerPath + SING_FOLDER_PARAM);
		default:
			mediaFolder = new File(contentServerPath + DEFAULT_FOLDER_PARAM);
			break;
		}
		
		if (!mediaFolder.exists()) {
			String msg = "Store folder not exists, " + mediaFolder.getAbsolutePath();
			logger.error(msg);
			throw new ConfigurationException(msg);
		}
		if (!mediaFolder.isDirectory()) {
			String msg = "Store folder is not a directory, " + mediaFolder.getAbsolutePath();
			logger.error(msg);
			throw new ConfigurationException(msg);
		}
		return mediaFolder;
	}
	
	private File getTempFolder(){
		String contentServerPath = configurationService
				.getStringParamValue(CONTENT_SERVER_PARAM);
		File tempFolder = new File(contentServerPath + TEMP_FOLDER_PARAM);
		if (!tempFolder.exists()) {
			logger.error("Temp folder '{}' not exists, so we will create new one.", tempFolder.getAbsolutePath());
			try {
				tempFolder.mkdir();
			} catch (Exception e) {
				logger.error("Can not create directory {} due to {}", tempFolder.getAbsolutePath(), e);
				throw new ConfigurationException(e.getMessage());
			}
		}
		if (!tempFolder.isDirectory()) {
			String msg = "Temp folder is not a directory, " + tempFolder.getAbsolutePath();
			logger.error(msg);
			throw new ConfigurationException(msg);
		}
		return tempFolder;
	}
	
	private String getSubDirectory(RoboconMimeTypeEnum roboconMimeTypeEnum, Long dirId){
		String dir = "default";
		switch(roboconMimeTypeEnum){
			case VIDEO:
				dir = "v"+dirId;
				break;
			case TRUETONE:
				dir = "s"+dirId;
				break;
			case STREAM:
				dir = "s"+dirId;
				break;
			case FULL_TRACK:
				dir = "s"+dirId;
				break;
			case SONG_KARAOKE:
				dir = "s"+dirId;
				break;
			case VIDEO_KARAOKE:
				dir = "v"+dirId;
				break;
			case SONG_LYRICS:
				dir = "s"+dirId;
				break;
			case VIDEO_LYRICS:
				dir = "v"+dirId;
				break;
			case USER_SING:
				dir = "s"+dirId;
				break;
			default:
				dir = dirId.toString();
				break;
		}
		return dir;
	}
	
	private void appendFileToResponse(File file, HttpServletRequest request, HttpServletResponse response, String contentType, RoboconUserActionEnum roboconUserActionEnum) throws ServletException, IOException{
		ServletOutputStream stream = null;
		BufferedInputStream buf = null;
		try {
			FileInputStream input = new FileInputStream(file);
			response.setContentLength((int) file.length());
			response.setContentType(contentType);
			switch(roboconUserActionEnum){
			case PLAY:
				response.addHeader("Content-Disposition", "filename=" + file.getName());
				break;
			case PRINT:
				response.addHeader("Content-Disposition", "filename=" + file.getName());
				break;
			case DOWNLOAD:
				response.addHeader("Content-Disposition", "attachment; filename=" + file.getName());
				break;
			default:
				response.addHeader("Content-Disposition", "attachment; filename=" + file.getName());
				break;
			}
			stream = response.getOutputStream();
			buf = new BufferedInputStream(input);
			IOUtils.copy(input, stream);
			stream.flush();
			stream.close();
			buf.close();
		} catch (FileNotFoundException e) {
			String msg = "Exception is ocuured while trying to stream file " + file.getName()
					+ " due to " + e.getMessage();
			logger.error(msg, e);
			throw new RoboconSongContentNotFoundException(msg);
		} catch (IOException e) {
			String msg = "Exception is ocuured while trying to stream file " + file.getName()
			+ " due to " + e.getMessage();
			logger.error(msg, e);
			throw new FileAccessException(msg);
		} finally{
			if (stream != null)
		        stream.close();
		      if (buf != null){
		    	  buf.close();
		      }
		}
	}
	
	private String toString(MultipartFile file) {
		return "MultipartFile(paramName=" + file.getName() + ", contentType="
				+ file.getContentType() + ", originalFilename="
				+ file.getOriginalFilename() + ", size=" + file.getSize();
	}
	
	public List<String> getSupportedRoboconFileContentTypes() {
		return supportedRoboconFileContentTypes;
	}

	public void setSupportedRoboconFileContentTypes(List<String> supportedRoboconFileContentTypes) {
		this.supportedRoboconFileContentTypes = supportedRoboconFileContentTypes;
	}

	public List<String> getSupportedRoboconFileLyricsTypes() {
		return supportedRoboconFileLyricsTypes;
	}

	public void setSupportedRoboconFileLyricsTypes(List<String> supportedRoboconFileLyricsTypes) {
		this.supportedRoboconFileLyricsTypes = supportedRoboconFileLyricsTypes;
	}
	
}
