package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.KEYWORD;
import static com.masrawy.robocon.model.RoboconConstants.SPECIAL_CHARACTERS_PATTERN;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.dao.OperatorDAO;
import com.masrawy.robocon.dto.RoboconOperatorDTO;
import com.masrawy.robocon.model.RoboconOperatorEB;
import com.masrawy.robocon.query.RoboconOperatorCriteriaFilter;
import com.masrawy.robocon.services.exception.OperatorCreationException;
import com.masrawy.robocon.services.exception.OperatorUpdateException;
import com.masrawy.robocon.transformer.RoboconOperatorTransformer;
import com.masrawy.robocon.validation.RoboconBeanValidationManager;

@Service("operatorService")
@Transactional
public class RoboconOperatorServiceImpl extends AbstractCrudService<RoboconOperatorEB, RoboconOperatorDTO, RoboconOperatorDTO, Long, RoboconOperatorCriteriaFilter> implements RoboconOperatorService{

	private static final Logger logger = LoggerFactory.getLogger(RoboconOperatorServiceImpl.class);
	
	@Autowired
	private OperatorDAO operatorDAO;
	
	@Autowired
	private RoboconOperatorTransformer operatorTransformer;
	
	@Autowired
	private RoboconMessageService roboconMessageService;
	
	@Autowired
	private RoboconBeanValidationManager roboconBeanValidationManager;
	
	@Override
	public RoboconOperatorDTO createOperator(RoboconOperatorDTO operatorDTO) {
		logger.debug("Creating Operator with name {}", operatorDTO.getName());
		try{
			RoboconOperatorEB operatorEB = createEntity(operatorDTO);
			logger.debug("Successfully creating Operator {}", operatorDTO.getName());
			return operatorTransformer.transformEntityToDTO(operatorEB);
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Robocon Operator creation exception due to constraint {} violation.\n Message: {}.", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Robocon Operator creation exception due to " + message;
			throw new OperatorCreationException(msg);
		}catch(Exception e){
			String msg = "Error creating operator due to: " + e.getMessage();
			logger.error("Error creating operator due to: {}", e);
			throw new OperatorCreationException(msg);
		}
	}

	@Override
	public RoboconOperatorDTO updateOperator(RoboconOperatorDTO operatorDTO) {
		logger.debug("Updating Operator {} with name {}", operatorDTO.pk(), operatorDTO.getName());
		try{
			RoboconOperatorEB operatorEB = updateEntity(operatorDTO);
			logger.debug("Successfully updaing Operator {}", operatorDTO.getName());
			return operatorTransformer.transformEntityToDTO(operatorEB);
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Robocon Operator update exception due to constraint {} violation.\n Message: {}.", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Robocon Operator update exception due to " + message;
			throw new OperatorUpdateException(msg);
		}catch(Exception e){
			String msg = "Error updating Operator due to: " + e.getMessage();
			logger.error("Error updating Operator due to: {}", e);
			throw new OperatorUpdateException(msg);
		}
	}

	@Override
	public List<RoboconOperatorDTO> getOperators(Integer pageNum, Integer pageLimit, HttpServletResponse response) {
		logger.debug("Fetching Operators.");
		RoboconOperatorCriteriaFilter cf = new RoboconOperatorCriteriaFilter();
		String keyword = ControlFieldsHelper.getStrFieldValue(KEYWORD);
		try {
			if(keyword != null && !keyword.isEmpty()){
				keyword = URLDecoder.decode(keyword, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot decode keyword:{}, error {}", keyword, e);
		}
		if(keyword != null && !keyword.isEmpty()){
			cf.setOperatorName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
		}
		cf.orderById(true);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
		List<RoboconOperatorEB> operatorsList = operatorDAO.getEntityListByCriteria(cf);
		int count = operatorDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		List<RoboconOperatorDTO> operatorsDTOList = operatorTransformer.transformEntityToDTO(operatorsList);
		return operatorsDTOList;
	}

	@Override
	protected void doBeforeValidate(RoboconOperatorDTO dto, boolean isNew) {
		roboconBeanValidationManager.validate(dto);
		super.doBeforeValidate(dto, isNew);
	}
	
	@Override
	protected void doAfterPersist(RoboconOperatorEB entity, RoboconOperatorDTO dto, boolean isNew) {
		getDAO().flush();
		operatorDAO.refresh(entity);
		super.doAfterPersist(entity, dto, isNew);
	}
	
	@Override
	protected DTOTransformer<RoboconOperatorEB, RoboconOperatorDTO> getTransformer() {
		return operatorTransformer;
	}

	@Override
	protected DTOTransformer<RoboconOperatorEB, RoboconOperatorDTO> getSummaryTransformer() {
		return operatorTransformer;
	}

	@Override
	protected DAO<RoboconOperatorEB, Long, RoboconOperatorCriteriaFilter> getDAO() {
		return operatorDAO;
	}

}
