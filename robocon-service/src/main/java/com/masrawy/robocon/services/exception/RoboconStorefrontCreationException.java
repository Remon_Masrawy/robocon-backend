package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class RoboconStorefrontCreationException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;

	public static final String ERROR_MSG = "Cannot create storefront error.";
	
	public RoboconStorefrontCreationException(){
		super(ERROR_MSG);
	}
	
	public RoboconStorefrontCreationException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return RoboconErrorConstants.ROBOCON_CREATION_ERROR;
	}

}
