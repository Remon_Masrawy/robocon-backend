package com.masrawy.robocon.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.persistent.dao.DAO;
import com.cdm.core.security.dao.LanguageDAO;
import com.cdm.core.security.dto.LanguageDTO;
import com.cdm.core.security.model.LanguageEB;
import com.cdm.core.security.query.LanguageCriteriaFilter;
import com.cdm.core.security.transformer.LanguageTransformer;
import com.masrawy.robocon.services.exception.RoboconLanguageCreationException;
import com.masrawy.robocon.services.exception.RoboconLanguageUpdateException;

@Service("roboconLanguageService")
@Transactional
public class RoboconLanguageServiceImpl extends AbstractCrudService<LanguageEB, LanguageDTO, LanguageDTO, String, LanguageCriteriaFilter> implements RoboconLanguageService{

	private static final Logger logger = LoggerFactory.getLogger(RoboconLanguageServiceImpl.class);
	
	@Autowired
	private LanguageDAO languageDAO;
	
	@Autowired
	private LanguageTransformer languageTransformer;
	
	@Override
	public LanguageDTO createLanguage(LanguageDTO languageDTO) {
		logger.debug("Creating language with code ({}), locale ({}), and description ({}).", languageDTO.getCode(), languageDTO.getLocale(), languageDTO.getName());
		if(languageDTO.getCode() == null || languageDTO.getCode().isEmpty()){
			logger.error("Error: Language code must be set");
			String msg = "Cannot create language due to code not set";
			throw new RoboconLanguageCreationException(msg);
		}
		if(languageDTO.getLocale() == null || languageDTO.getLocale().isEmpty()){
			logger.error("Error: Language locale must be set");
			String msg = "Cannot create language due to locale not set";
			throw new RoboconLanguageCreationException(msg);
		}
		if(languageDTO.getName() == null || languageDTO.getName().isEmpty()){
			logger.error("Error: Language name must be set");
			String msg = "Cannot create language due to name not set";
			throw new RoboconLanguageCreationException(msg);
		}
		LanguageCriteriaFilter cf = new LanguageCriteriaFilter();
		cf.setCode(languageDTO.getCode());
		LanguageEB languageEB = languageDAO.getEntityByCriteriaIfExists(cf);
		if(languageEB != null){
			logger.error("Error: Language with code ({}) already exist.", languageDTO.getCode());
			String msg = "Language code already exist exception.";
			throw new RoboconLanguageCreationException(msg);
		}
		languageEB = createEntity(languageDTO);
		logger.debug("Language ({}) has been created successfully.", languageDTO.getName());
		return languageTransformer.transformEntityToDTO(languageEB);
	}

	@Override
	public LanguageDTO updateLanguage(LanguageDTO languageDTO) {
		logger.debug("Updating language with code ({})", languageDTO.getCode());
		if(languageDTO.getCode() == null || languageDTO.getCode().isEmpty()){
			logger.error("Error: Language code must be set");
			String msg = "Cannot update language due to code not set";
			throw new RoboconLanguageUpdateException(msg);
		}
		if(languageDTO.getLocale() == null || languageDTO.getLocale().isEmpty()){
			logger.error("Error: Language locale must be set");
			String msg = "Cannot update language due to locale not set";
			throw new RoboconLanguageUpdateException(msg);
		}
		if(languageDTO.getName() == null || languageDTO.getName().isEmpty()){
			logger.error("Error: Language name must be set");
			String msg = "Cannot update language due to name not set";
			throw new RoboconLanguageUpdateException(msg);
		}
		try{
			LanguageEB languageEB = updateEntity(languageDTO);
			logger.debug("Language ({}) has been updated successfully.", languageDTO.getCode());
			return languageTransformer.transformEntityToDTO(languageEB);
		}catch(RuntimeException ex){
			logger.error("Error: Cannot update language ({}) due to {}", languageDTO.getCode(), ex.getMessage());
			String msg = "Cannot update language "+languageDTO.getCode()+" due to "+ex.getMessage();
			throw new RoboconLanguageUpdateException(msg);
		}
	}

	@Override
	public List<LanguageDTO> listLanguages(Integer pageNum, Integer pageLimit) {
		logger.debug("Fetching Robocon languages by paging for page number ({}) and limit ({})", pageNum, pageLimit);
		LanguageCriteriaFilter cf = new LanguageCriteriaFilter();
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
		return getRecordsByCriteria(cf);
	}
	
	@Override
	protected DTOTransformer<LanguageEB, LanguageDTO> getTransformer() {
		return languageTransformer;
	}

	@Override
	protected DTOTransformer<LanguageEB, LanguageDTO> getSummaryTransformer() {
		return languageTransformer;
	}

	@Override
	protected DAO<LanguageEB, String, LanguageCriteriaFilter> getDAO() {
		return languageDAO;
	}

}
