package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconArtistEventDTO;
import com.masrawy.robocon.dto.RoboconArtistEventMetaDTO;
import com.masrawy.robocon.query.RoboconArtistEventCriteriaFilter;

public interface RoboconEventService extends CrudService<RoboconArtistEventDTO, RoboconArtistEventDTO, Long, RoboconArtistEventCriteriaFilter>{

	public RoboconArtistEventDTO createRoboconArtistEvent(RoboconArtistEventDTO roboconArtistEventDTO);
	public RoboconArtistEventDTO updateRoboconArtistEvent(RoboconArtistEventDTO roboconArtistEventDTO);
	public RoboconArtistEventDTO uploadPhoto(MultipartFile file, Long artistId);
	public RoboconArtistEventDTO uploadPhotos(List<MultipartFile> files, Long eventId);
	public RoboconArtistEventDTO getRoboconArtistEvent(Long eventId);
	public List<RoboconArtistEventDTO> fetchRoboconArtistEvents(Integer pageNum, Integer pageLimit, HttpServletResponse response);
	public void removeRoboconArtistEvent(Long eventId);
	public RoboconArtistEventDTO addRoboconArtistEventMeta(List<RoboconArtistEventMetaDTO> roboconArtistEventMetaDTO, Long roboconArtistEventId);
}
