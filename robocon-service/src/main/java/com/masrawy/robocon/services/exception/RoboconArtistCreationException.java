package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class RoboconArtistCreationException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	public static final String ERROR_MSG = "Cannot create robocon artist exception.";
	
	public RoboconArtistCreationException(){
		super(ERROR_MSG);
	}
	
	public RoboconArtistCreationException(String msg){
		super(msg);
	}
	
	@Override
	public int getErrorCode() {
		return RoboconErrorConstants.ROBOCON_CREATION_ERROR;
	}

}
