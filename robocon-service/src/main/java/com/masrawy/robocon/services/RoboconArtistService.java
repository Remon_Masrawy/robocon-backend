package com.masrawy.robocon.services;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconArtistDTO;
import com.masrawy.robocon.dto.RoboconArtistSocialDTO;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.dto.RoboconTagDTO;
import com.masrawy.robocon.query.RoboconArtistCriteriaFilter;

public interface RoboconArtistService extends CrudService<RoboconArtistDTO, RoboconArtistDTO, Long, RoboconArtistCriteriaFilter>{

	public List<RoboconArtistDTO> getRoboconArtists(Integer pageNum, Integer pageLimit, HttpServletResponse response);
	
	public RoboconArtistDTO fetchRoboconArtist(Long artistId);
	
	public Map<String, String> getSocials();
	
	public void removeArtist(Long artistId);
	
	public RoboconArtistDTO createRoboconArtist(RoboconArtistDTO roboconArtistDTO);
	
	public RoboconArtistDTO updateRoboconArtist(RoboconArtistDTO roboconArtistDTO);
	
	public RoboconArtistDTO addRoboconArtistMeta(List<RoboconMetaDTO> roboconMetaDTO, Long artistId);
	
	public RoboconArtistDTO addRoboconArtistTags(List<RoboconTagDTO> roboconTagsDTO, Long artistId);
	
	public RoboconArtistDTO addRoboconArtistSocials(List<RoboconArtistSocialDTO> roboconArtistSocialsDTO, Long artistId);
	
	public RoboconArtistDTO uploadPhoto(MultipartFile file, Long artistId);
	
	public RoboconArtistDTO uploadPhotos(List<MultipartFile> files, Long artistId);
}
