package com.masrawy.robocon.services;


import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.validation.FieldError;

@Service("roboconMessageService")
public class RoboconMessageServiceImpl implements RoboconMessageService{

	@Resource(name = "messageSource")
    private MessageSource messageSource;
	
	@Autowired
	private MessageSourceAccessor messageSourceAccessor;
	
	@Override
	public String getMessage(String code, String mainMessage) {
		String detail = StringUtils.substringAfter(mainMessage, "Detail");
		Object[] args = new Object[]{ detail };
		try{
			String message = messageSourceAccessor.getMessage(code, args);
			if(message == null || message.equalsIgnoreCase(code)){
				message = messageSourceAccessor.getMessage(code.toUpperCase(), args);
			}
			if(message == null || message.equalsIgnoreCase(code)){
				message = messageSourceAccessor.getMessage(code.toLowerCase(), args);
			}
			if(message == null || message.equalsIgnoreCase(code)){
				message = messageSourceAccessor.getMessage(code.trim(), args);
			}
			if(message == null || message.equalsIgnoreCase(code)){
				message = messageSourceAccessor.getMessage(code.trim().toUpperCase(), args);
			}
			if(message == null || message.equalsIgnoreCase(code)){
				message = messageSourceAccessor.getMessage(code.trim().toLowerCase(), args);
			}
			if(message == null || message.equalsIgnoreCase(code)){
				if(detail != null && !detail.isEmpty()){
					return detail;
				}
				return mainMessage;
			}
			return message;
		}catch(NoSuchMessageException e){
			return null;
		}catch(RuntimeException e){
			return null;
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public String getMessage(String code, Object... args) {
		try {
			String message = messageSourceAccessor.getMessage(code, args);
			if(message.equalsIgnoreCase(code)){
				return null;
			}
			return message;
		} catch (NoSuchMessageException e) {
			return null;
		} catch(RuntimeException e){
			return null;
		} catch(Exception e){
			return null;
		}
	}

	@Override
	public String getMessage(FieldError fieldError) {
		try {
			String message = messageSourceAccessor.getMessage(fieldError);
			return message;
		} catch (NoSuchMessageException e) {
			return null;
		} catch(RuntimeException e){
			return null;
		} catch(Exception e){
			return null;
		}
	}

}
