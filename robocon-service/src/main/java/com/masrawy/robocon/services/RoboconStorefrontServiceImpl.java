package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.KEYWORD;
import static com.masrawy.robocon.model.RoboconConstants.SPECIAL_CHARACTERS_PATTERN;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.dao.RoboconStorefrontDAO;
import com.masrawy.robocon.dto.RoboconStorefrontDTO;
import com.masrawy.robocon.model.RoboconStorefrontEB;
import com.masrawy.robocon.query.RoboconStorefrontCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconStorefrontCreationException;
import com.masrawy.robocon.services.exception.RoboconStorefrontNotFoundException;
import com.masrawy.robocon.services.exception.RoboconStorefrontUpdateException;
import com.masrawy.robocon.transformer.RoboconStorefrontTransformer;
import com.masrawy.robocon.validation.RoboconBeanValidationManager;

@Service("roboconStorefrontService")
@Transactional
public class RoboconStorefrontServiceImpl extends AbstractCrudService<RoboconStorefrontEB, RoboconStorefrontDTO, RoboconStorefrontDTO, Long, RoboconStorefrontCriteriaFilter> implements RoboconStorefrontService{

	private final static Logger logger = LoggerFactory.getLogger(RoboconStorefrontServiceImpl.class);
	
	@Autowired
	private RoboconStorefrontTransformer roboconStorefrontTransformer;
	
	@Autowired
	private RoboconStorefrontDAO roboconStorefrontDAO;
	
	@Autowired
	private RoboconMessageService roboconMessageService;
	
	@Autowired
	private RoboconBeanValidationManager roboconBeanValidationManager;
	
	@Override
	public RoboconStorefrontDTO createStorefront(RoboconStorefrontDTO roboconStorefrontDTO) {
		logger.debug("Creating Robocon Storefront");
		RoboconStorefrontEB roboconStorefrontEB = null;
		try{
			roboconStorefrontEB = createEntity(roboconStorefrontDTO);
			roboconStorefrontDTO = roboconStorefrontTransformer.transformEntityToDTO(roboconStorefrontEB);
			logger.debug("Robocon Storefront {} has been created successfully", roboconStorefrontDTO.pk());
			return roboconStorefrontDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Storefront creation exception due to constraint {} violation.\n Message: {}, \n Message: {}", ex.getConstraintName(), message, ex.getCause().getMessage());
			String msg = "Cannot create robocon storefront due to " + message;
			throw new RoboconStorefrontCreationException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Storefront creation error due to {}.", ex.getMessage());
			throw new RoboconStorefrontCreationException(ex.getMessage());
		}
	}

	@Override
	public RoboconStorefrontDTO updateStorefront(RoboconStorefrontDTO roboconStorefrontDTO) {
		logger.debug("Updating Robocon Storefront ({})", roboconStorefrontDTO.pk());
		RoboconStorefrontEB roboconStorefrontEB = null;
		try{
			roboconStorefrontEB = updateEntity(roboconStorefrontDTO);
			roboconStorefrontDTO = roboconStorefrontTransformer.transformEntityToDTO(roboconStorefrontEB);
			logger.debug("Robocon Storefront {} has been updated successfully", roboconStorefrontDTO.pk());
			return roboconStorefrontDTO;
		}catch(ConstraintViolationException ex){
			String message = roboconMessageService.getMessage(ex.getConstraintName(), ex.getCause().getMessage());
			logger.error("Error: Storefront update exception due to constraint {} violation.\n Message: {}", ex.getConstraintName(), ex.getCause().getMessage());
			String msg = "Cannot update robocon storefront due to " + message;
			throw new RoboconStorefrontUpdateException(msg);
		}catch(RuntimeException ex){
			logger.error("Error: Storefront update error due to {}.", ex.getMessage());
			throw new RoboconStorefrontUpdateException(ex.getMessage());
		}
	}

	@Override
	public RoboconStorefrontDTO getStorefront(Long storefrontId){
		RoboconStorefrontEB roboconStorefrontEB = getStorefrontIfExists(storefrontId);
		RoboconStorefrontDTO roboconStorefrontDTO = roboconStorefrontTransformer.transformEntityToDTO(roboconStorefrontEB);
		return roboconStorefrontDTO;
	}
	
	@Override
	public List<RoboconStorefrontDTO> getStorefronts(Integer pageNum, Integer pageLimit, HttpServletResponse response) {
		logger.debug("Fetching robocon storefronts");
		RoboconStorefrontCriteriaFilter cf = new RoboconStorefrontCriteriaFilter();
		String keyword = ControlFieldsHelper.getStrFieldValue(KEYWORD);
		try {
			if(keyword != null && !keyword.isEmpty()){
				keyword = URLDecoder.decode(keyword, "UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			logger.error("Cannot decode keyword:{}, error {}", keyword, e);
		}
		if(keyword != null && !keyword.isEmpty()){
			cf.setStorefrontName(keyword.replaceAll(SPECIAL_CHARACTERS_PATTERN, " "), true);
			cf.setStorefrontActivation(true);
		}
		cf.orderByStorefrontId(true);
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
		List<RoboconStorefrontEB> roboconStorefrontsEB = roboconStorefrontDAO.getEntityListByCriteria(cf);
		int count = roboconStorefrontDAO.getCountByCriteria(cf);
		response.setHeader("x-data-count", String.valueOf(count));
		return roboconStorefrontTransformer.transformEntityToDTO(roboconStorefrontsEB);
	}
	
	@Override
	public void removeStorefront(Long storefrontId){
		logger.debug("Try removing robocon storefront with id ({})", storefrontId);
		RoboconStorefrontCriteriaFilter cf = new RoboconStorefrontCriteriaFilter();
		cf.setStorefrontId(storefrontId);
		RoboconStorefrontEB roboconStorefrontEB = getStorefrontIfExists(storefrontId);
		roboconStorefrontDAO.deleteEntity(roboconStorefrontEB);
	}
	
	private RoboconStorefrontEB getStorefrontIfExists(Long storefrontId){
		RoboconStorefrontEB roboconStorefrontEB = roboconStorefrontDAO.getEntityByIDIfExists(storefrontId);
		if(roboconStorefrontEB == null){
			logger.error("Error: No Storefront found with id ({})", storefrontId);
			String msg = "No Storefront found with id (" + storefrontId + ")";
			throw new RoboconStorefrontNotFoundException(msg);
		}
		return roboconStorefrontEB;
	}
	
	@Override
	protected void doBeforeValidate(RoboconStorefrontDTO dto, boolean isNew) {
		roboconBeanValidationManager.validate(dto);
		super.doBeforeValidate(dto, isNew);
	}
	
	@Override
	protected void doAfterPersist(RoboconStorefrontEB entity, RoboconStorefrontDTO dto, boolean isNew) {
		roboconStorefrontDAO.flush();
		roboconStorefrontDAO.refresh(entity);
		super.doAfterPersist(entity, dto, isNew);
	}
	
	@Override
	protected DTOTransformer<RoboconStorefrontEB, RoboconStorefrontDTO> getTransformer() {
		return roboconStorefrontTransformer;
	}

	@Override
	protected DTOTransformer<RoboconStorefrontEB, RoboconStorefrontDTO> getSummaryTransformer() {
		return roboconStorefrontTransformer;
	}

	@Override
	protected DAO<RoboconStorefrontEB, Long, RoboconStorefrontCriteriaFilter> getDAO() {
		return roboconStorefrontDAO;
	}

}
