package com.masrawy.robocon.services;

import java.util.List;

import com.cdm.core.security.dto.LanguageDTO;
import com.cdm.core.security.query.LanguageCriteriaFilter;
import com.cdm.core.service.CrudService;

public interface RoboconLanguageService extends CrudService<LanguageDTO, LanguageDTO, String, LanguageCriteriaFilter>{

	public LanguageDTO createLanguage(LanguageDTO languageDTO);
	public LanguageDTO updateLanguage(LanguageDTO languageDTO);
	public List<LanguageDTO> listLanguages(Integer pageNum, Integer pageLimit);
}
