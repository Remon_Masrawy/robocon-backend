package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.USER_IP;
import static com.masrawy.robocon.model.RoboconConstants.X_FORWARDED_FOR;

import java.io.IOException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;

@Service("locationService")
public class LocationServiceImpl implements LocationService{

	private static final Logger logger = LoggerFactory.getLogger(LocationServiceImpl.class);
	
	private static final String LOCATION_DATA_FILE = "location/geo-lite-city.dat";
	
	private LookupService lookup;
	
	private Pattern pattern;
    private Matcher matcher;
    
    private static final String IPADDRESS_PATTERN = 
    		"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
    		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
    		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
    		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	
	@PostConstruct
	private void init(){
		pattern = Pattern.compile(IPADDRESS_PATTERN);
		try{
			URL url = getClass().getClassLoader().getResource(LOCATION_DATA_FILE);
			lookup = new LookupService(url.getPath(), LookupService.GEOIP_MEMORY_CACHE);
		}catch(IOException e){
			logger.error("Error: location database is not found [{}]", LOCATION_DATA_FILE);
		}
	}
	
	@Override
	public Location getLocation(final HttpServletRequest request){
		try{
			String userIP = ControlFieldsHelper.getStrFieldValue(USER_IP);
			if(userIP == null || userIP.isEmpty()){
				userIP = ControlFieldsHelper.getStrFieldValue(X_FORWARDED_FOR);
			}
			if(userIP == null || userIP.isEmpty()){
				userIP = request.getRemoteAddr();
			}
			if(userIP == null || userIP.isEmpty()){
				logger.error("Error: User IP not found in headers.");
				return null;
			}
			if(!valid(userIP)){
				logger.error("Error: User IP found not valid.");
				return null;
			}
			if(lookup != null){
				Location location = lookup.getLocation(userIP);
				return location;
			}
			logger.error("Error: Lookup Service didn't initialized.");
			return null;
		}catch(Throwable e){
			logger.error("Error: Cannot find location.");
			return null;
		}
	}
	
	@Override
	public Location getLocation(final String ip){
		try{
			if(ip == null || ip.isEmpty()){
				logger.error("Error: IP must not be empty.");
				return null;
			}
			if(!valid(ip)){
				logger.error("Error: IP is not valid.");
				return null;
			}
			if(lookup != null){
				Location location = lookup.getLocation(ip);
				return location;
			}
			logger.error("Error: Lookup Service didn't initialized.");
			return null;
		}catch(Throwable e){
			logger.error("Error: Cannot find location.");
			return null;
		}
	}
	
	@Override
	public boolean valid(final String ip){		  
	  matcher = pattern.matcher(ip);
	  return matcher.matches();	    	    
    }
}