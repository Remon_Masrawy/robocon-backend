package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class RoboconInvalidUserActionException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	private static final String ERROR_MSG = "User action not supported Exception.";
			
	public RoboconInvalidUserActionException(){
		super(ERROR_MSG);
	}
	
	public RoboconInvalidUserActionException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return RoboconErrorConstants.ROBOCON_INVALID_REQUEST;
	}

}
