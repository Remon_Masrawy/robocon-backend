package com.masrawy.robocon.services;

import static com.masrawy.robocon.model.RoboconConstants.OPERATOR_MCC;
import static com.masrawy.robocon.model.RoboconConstants.OPERATOR_MNC;
import static com.masrawy.robocon.model.RoboconConstants.STOREFRONT;
import static com.masrawy.robocon.model.RoboconConstants.USER_ACTION;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.cdm.core.controlfields.ControlFieldsHelper;
import com.cdm.core.crud.AbstractCrudService;
import com.cdm.core.dto.transformer.DTOTransformer;
import com.cdm.core.persistent.dao.DAO;
import com.masrawy.robocon.dao.RoboconSongTrendDAO;
import com.masrawy.robocon.dao.RoboconUserSongHistoryDAO;
import com.masrawy.robocon.dto.RoboconSongTrendDTO;
import com.masrawy.robocon.model.RoboconSongEB;
import com.masrawy.robocon.model.RoboconSongTrendEB;
import com.masrawy.robocon.model.RoboconUserActionEnum;
import com.masrawy.robocon.query.RoboconTrendCriteriaFilter;
import com.masrawy.robocon.query.RoboconUserSongHistoryCriteriaFilter;
import com.masrawy.robocon.services.exception.RoboconInvalidUserActionException;
import com.masrawy.robocon.transformer.RoboconSongTrendTransformer;
import com.maxmind.geoip.Location;

@Transactional
@Service("roboconTrendService")
public class RoboconTrendServiceImpl extends AbstractCrudService<RoboconSongTrendEB, RoboconSongTrendDTO, RoboconSongTrendDTO, Long, RoboconTrendCriteriaFilter> implements RoboconTrendService{

	private static final Logger logger = LoggerFactory.getLogger(RoboconTrendServiceImpl.class);
	
	@Autowired
	private RoboconSongTrendDAO roboconSongTrendDAO;
	
	@Autowired
	private RoboconUserSongHistoryDAO roboconUserSongHistoryDAO;
	
	@Autowired
	private LocationService locationService;
	
	@Autowired
	private RoboconSongTrendTransformer roboconSongTrendTransformer;
	
	@Override
	public List<RoboconSongTrendDTO> getSongTrends(HttpServletRequest request, Integer pageNum, Integer pageLimit){
		List<RoboconSongTrendEB> songTrends = new ArrayList<RoboconSongTrendEB>();
		RoboconUserSongHistoryCriteriaFilter cf = new RoboconUserSongHistoryCriteriaFilter();
		cf.setTrendFields();
		String mcc = ControlFieldsHelper.getStrFieldValue(OPERATOR_MCC);
		String mnc = ControlFieldsHelper.getStrFieldValue(OPERATOR_MNC);
		String storefront = ControlFieldsHelper.getStrFieldValue(STOREFRONT);
		String userAction = ControlFieldsHelper.getStrFieldValue(USER_ACTION);
		Location location = locationService.getLocation(request);
		
		// Action Filter
		if(!StringUtils.isEmpty(userAction)){
			try{
				RoboconUserActionEnum action = RoboconUserActionEnum.valueOf(userAction);
				cf.setUserAction(action);
			}catch(IllegalArgumentException ex){
				logger.error("Error: User action '{}' not supported exception.", userAction);
				String msg = "User action '"+userAction+"' not supported exception";
				throw new RoboconInvalidUserActionException(msg);
			}
		}
		
		// Operator Filter
		if(mcc != null && !mcc.isEmpty() && mnc != null && !mnc.isEmpty()){
			cf.setOperator(mcc, mnc);
		}else{
			cf.isEmptyOperator(true);
		}
		
		// Storefront Filter
		if(storefront != null && !storefront.isEmpty()){
			cf.setStorefrontName(storefront);
		}else{
			cf.isEmptyStorefront(true);
		}
		
		// Country Filter
		if(location != null && location.countryCode != null && !location.countryCode.isEmpty()){
			cf.setCountryCode(location.countryCode);
		}else{
			cf.setEmptyCountry(true);
		}
		
		// Pagination
		if(pageNum != null && pageNum != 0 && pageLimit != null && pageLimit != 0){
			cf.setFirstResultIndex((pageNum - 1) * pageLimit);
		}
		if(pageLimit != null && pageLimit != 0){
			cf.setMaxResult(pageLimit);
		}
		
		roboconUserSongHistoryDAO.enableMetaLangFilter();
		
		List<Map<String, Object>> trends = roboconUserSongHistoryDAO.getRecordsByCriteriaFilter(cf);
		long count = 0;
		for(Map<String, Object> trend : trends){
			RoboconSongTrendEB songTrend = new RoboconSongTrendEB();
			songTrend.setId(++count);
			songTrend.setRoboconSongEB((RoboconSongEB)trend.get("roboconSongEB"));
			songTrend.setCount(((Long) trend.get("count")).intValue());
			songTrends.add(songTrend);
		}
		return roboconSongTrendTransformer.transformEntityToDTO(songTrends);
	}

	@Override
	protected DTOTransformer<RoboconSongTrendEB, RoboconSongTrendDTO> getTransformer() {
		return roboconSongTrendTransformer;
	}

	@Override
	protected DTOTransformer<RoboconSongTrendEB, RoboconSongTrendDTO> getSummaryTransformer() {
		return roboconSongTrendTransformer;
	}

	@Override
	protected DAO<RoboconSongTrendEB, Long, RoboconTrendCriteriaFilter> getDAO() {
		return roboconSongTrendDAO;
	}
}
