package com.masrawy.robocon.services;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.file.service.FileService;
import com.cdm.core.persistent.model.Entity;
import com.masrawy.robocon.model.AbstractRoboconFileEB;
import com.masrawy.robocon.model.RoboconMimeTypeEnum;
import com.masrawy.robocon.model.RoboconUserActionEnum;

public interface RoboconFileService extends FileService{

	public static final String CONTENT_SERVER_PARAM = "CONTENT_SERVER";
	
	public static final String CONTENT_FOLDER_PARAM = "content";
	public static final String KARAOKE_FOLDER_PARAM = "karaoke";
	public static final String LYRICS_FOLDER_PARAM = "lyrics";
	public static final String SING_FOLDER_PARAM = "sing";
	public static final String DEFAULT_FOLDER_PARAM = "default";
	public static final String TEMP_FOLDER_PARAM = "temp";
	
	public Entity<?> uploadFile(MultipartFile multipartFile, Long dirId, RoboconMimeTypeEnum roboconMimeTypeEnum);
	
	public void download(Long dirId, AbstractRoboconFileEB<?> fileInfo, RoboconUserActionEnum roboconUserActionEnum, HttpServletRequest request, HttpServletResponse response);
}
