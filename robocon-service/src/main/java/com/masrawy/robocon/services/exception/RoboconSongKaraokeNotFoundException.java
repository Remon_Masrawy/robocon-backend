package com.masrawy.robocon.services.exception;

import com.cdm.core.exception.BaseRuntimeException;
import com.cdm.core.exception.ClientError;

public class RoboconSongKaraokeNotFoundException extends BaseRuntimeException implements ClientError{

	private static final long serialVersionUID = 1L;
	
	private static final String ERROR_MSG = "Robocon Song Karaoke Not Found Exception.";
			
	public RoboconSongKaraokeNotFoundException(){
		super(ERROR_MSG);
	}
	
	public RoboconSongKaraokeNotFoundException(String msg){
		super(msg);
	}

	@Override
	public int getErrorCode() {
		return RoboconErrorConstants.ROBOCON_INVALID_REQUEST;
	}

}
