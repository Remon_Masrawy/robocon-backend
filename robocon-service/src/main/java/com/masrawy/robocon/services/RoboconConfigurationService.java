package com.masrawy.robocon.services;

import java.util.List;

import com.cdm.core.configuration.dto.ConfigParameterDTO;
import com.masrawy.robocon.dto.RoboconConfigurationDTO;

public interface RoboconConfigurationService {

	public void createConfigParam(ConfigParameterDTO configParameterDTO);
	public void deleteConfigParam(String paramName);
	public void deleteAllConfigParams();
	public void updateConfigParam(ConfigParameterDTO configParameterDTO);
	public List<ConfigParameterDTO> getConfigParamsList();
	
	public void createRoboconConfiguration(RoboconConfigurationDTO roboconConfigurationDTO);
	public void deleteRoboconConfiguration(RoboconConfigurationDTO roboconConfigurationDTO);
	public void deleteAllRoboconConfigurations();
	public void updateRoboconConfiguration(RoboconConfigurationDTO roboconConfigurationDTO);
	public List<RoboconConfigurationDTO> getRoboconConfigurationsList();
}
