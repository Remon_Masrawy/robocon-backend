package com.masrawy.robocon.services;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.cdm.core.service.CrudService;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.dto.RoboconProviderDTO;
import com.masrawy.robocon.query.RoboconProviderCriteriaFilter;

public interface RoboconProviderService extends CrudService<RoboconProviderDTO, RoboconProviderDTO, Long, RoboconProviderCriteriaFilter>{

	public RoboconProviderDTO createProvider(RoboconProviderDTO roboconProviderDTO);
	public RoboconProviderDTO updateProvider(RoboconProviderDTO roboconProviderDTO);
	
	public RoboconProviderDTO fetchProvider(Long providerId);
	public List<RoboconProviderDTO> fetchProvidersByPaging(Integer pageNum, Integer pageLimit, HttpServletResponse response);
	
	public void removeProvider(Long providerId);
	
	public RoboconProviderDTO addMeta(RoboconMetaDTO roboconMetaDTO, Long providerId);
}
