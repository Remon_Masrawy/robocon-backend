package com.masrawy.robocon.validation;

import com.cdm.core.validation.exception.BeanValidationException;

public interface RoboconBeanValidationManager {

	public void validate(Object object) throws BeanValidationException;
	
	public void validate(Object object, Class<?>... groups) throws BeanValidationException;
}
