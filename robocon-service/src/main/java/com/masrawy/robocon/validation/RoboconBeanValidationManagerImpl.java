package com.masrawy.robocon.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;

import com.cdm.core.util.ClassUtils;
import com.cdm.core.validation.exception.BeanValidationException;
import com.masrawy.robocon.services.RoboconMessageService;

@Component("RoboconBeanValidationManager")
public class RoboconBeanValidationManagerImpl implements RoboconBeanValidationManager{

	private final static Logger logger = LoggerFactory.getLogger(RoboconBeanValidationManagerImpl.class);
	
	@Autowired
	private Validator validator;
	
	@Autowired
	private RoboconMessageService RoboconMessageService;
	
	@Override
	public void validate(Object object) throws BeanValidationException {
		Errors errors = new BeanPropertyBindingResult(object, ClassUtils.getSimpleClassName(object.getClass()));
		validator.validate(object, errors);
		if(errors.hasErrors()){
			String message = generateExceptionMessage(object, errors);
			logger.error("Errors: {}", message);
			throw new BeanValidationException(message);
		}
	}

	@Override
	public void validate(Object object, Class<?>... groups) throws BeanValidationException {
		Errors errors = new BeanPropertyBindingResult(object, ClassUtils.getSimpleClassName(object.getClass()));
		validator.validate(object, errors);
		if(errors.hasErrors()){
			String message = generateExceptionMessage(object, errors);
			logger.error("Errors: {}", message);
			throw new BeanValidationException(message);
		}
	}
	
	private String generateExceptionMessage(Object object, Errors errors){
		String dtoName = object.getClass().getSimpleName();
		StringBuilder msg = new StringBuilder("Validating " + dtoName + " caused " + errors.getErrorCount()
				+ " errors : ");
		for(FieldError fieldError : errors.getFieldErrors()){
			appendMsg(msg, fieldError);
		}
		return msg.toString();
	}
	
	private void appendMsg(StringBuilder msg, FieldError fieldError){
		String errorMessage = null;
		for(String code : fieldError.getCodes()){
			errorMessage = RoboconMessageService.getMessage(code, fieldError.getObjectName(), fieldError.getField(), fieldError.getRejectedValue(), fieldError.getArguments());
			if(errorMessage != null){
				break;
			}
		}
		if(errorMessage == null || errorMessage.isEmpty()){
			errorMessage = fieldError.getDefaultMessage();
		}
		msg.append(errorMessage);
		msg.append(", ");
	}
}
