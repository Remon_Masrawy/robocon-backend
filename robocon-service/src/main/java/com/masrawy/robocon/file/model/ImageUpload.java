package com.masrawy.robocon.file.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class ImageUpload implements Serializable{

	private static final long serialVersionUID = 1L;

	@NotNull
	private CommonsMultipartFile file;
	
	private Boolean isMainImage = false;

	public CommonsMultipartFile getFile() {
		return file;
	}

	public void setFile(CommonsMultipartFile file) {
		this.file = file;
	}

	public Boolean getIsMainImage() {
		return isMainImage;
	}

	public void setIsMainImage(Boolean isMainImage) {
		this.isMainImage = isMainImage;
	}
}
