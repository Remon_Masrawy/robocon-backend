package com.masrawy.robocon.file.model;

public class FileContentTypeUtil {

	public static final String CT_AUDIO_MP3 = "audio/mp3";
	public static final String CT_AUDIO_MPEG = "audio/mpeg";
	public static final String CT_AUDIO_AU = "audio/basic";
	public static final String CT_AUDIO_SND = "audio/basic";
	public static final String CT_AUDIO_MID = "audio/mid";
	public static final String CT_AUDIO_RMI = "audio/mid";
	public static final String CT_AUDIO_AIF = "audio/x-aiff";
	public static final String CT_AUDIO_AIFC = "audio/x-aiff";
	public static final String CT_AUDIO_AIFF = "audio/x-aiff";
	public static final String CT_AUDIO_M3U = "audio/x-mpegurl";
	public static final String CT_AUDIO_RA = "audio/x-pn-realaudio";
	public static final String CT_AUDIO_RAM = "audio/x-pn-realaudio";
	public static final String CT_AUDIO_WAV = "audio/x-wav";
	
	public static final String CT_LYRICS_TXT = "text/plain";
}
