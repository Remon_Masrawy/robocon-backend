package com.masrawy.robocon.file.model;

import com.cdm.core.datatypes.IntegerEnum;

public enum FileTypeEnum implements IntegerEnum{

	IMAGE(1), AUDIO(2), VIDEO(3), PDF(4), EXCEL(5), TEXT(6);

	private int value;

	private FileTypeEnum(int value) {
		this.value = value;
	}

	@Override
	public int getIntValue() {
		return value;
	}
}
