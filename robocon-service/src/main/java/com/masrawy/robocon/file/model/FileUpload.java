package com.masrawy.robocon.file.model;

import java.io.Serializable;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.masrawy.robocon.model.RoboconMimeTypeEnum;
import com.masrawy.robocon.services.exception.InvalidFileMimeTypeException;

/**
 * 
 * <p>
 * Upload Content (video, audio, image, ...) with File type
 * </p>
 * 
 * @author Remon Gaber
 * @since 21-02-2017
 * @version 1.0
 */

public class FileUpload implements Serializable{

	private static final long serialVersionUID = 1L;

	private CommonsMultipartFile file;
	
	private Integer fileType;
	
	
	public CommonsMultipartFile getFile() {
		return file;
	}
	public void setFile(CommonsMultipartFile file) {
		this.file = file;
	}
	public Integer getFileType() {
		return fileType;
	}
	public void setFileType(Integer fileType) {
		this.fileType = fileType;
	}
	public RoboconMimeTypeEnum getMimeTypeEnum(){
		try{
			for(RoboconMimeTypeEnum roboconMimeTypeEnum : RoboconMimeTypeEnum.values()){
				if(roboconMimeTypeEnum.getIntValue() == fileType){
					return roboconMimeTypeEnum;
				}
			}
			throw new InvalidFileMimeTypeException();
		}catch(Exception e){
			throw new InvalidFileMimeTypeException();
		}
	}
}
