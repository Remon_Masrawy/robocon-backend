package com.masrawy.robocon.file.model;

import java.io.File;

import com.cdm.core.file.model.FileInfo;

public class RoboconFileInfo extends FileInfo{

	private File file;

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
}
