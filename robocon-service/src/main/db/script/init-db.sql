--Configuration
INSERT INTO configuration(name,type,typeparam,value,description,editabled,viewed)
    VALUES ('MEDIA_FOLDER', 'String', null, '/home/robocon/images', 'MEDIA FOLDER', true, true);
INSERT INTO configuration(name,type,typeparam,value,description,editabled,viewed)
    VALUES ('CONTENT_SERVER', 'String', null, '/home/robocon/', 'CONTENT SERVER PATH', true, true);
INSERT INTO configuration(name,type,typeparam,value,description,editabled,viewed)
    VALUES ('SERVER_URL', 'String', null, 'http://196.202.91.160:7070/maz-web', 'Server Base Url', true, true);
INSERT INTO configuration(name,type,typeparam,value,description,editabled,viewed)
    VALUES ('WEB_SITE_URL', 'String', null, 'http://196.202.91.160:7070/maz-web', 'Web Site Url', true, true);
INSERT INTO configuration(name,type,typeparam,value,description,editabled,viewed)
    VALUES ('EXPIRY_DURATION_IN_HR', 'Integer', null, '48', 'Expiry duration in hours for Verfication Token', true, true);
INSERT INTO configuration(name,type,typeparam,value,description,editabled,viewed)
    VALUES ('WELCOME_MAIL_SUBJECT', 'String', null, 'WELCOME TO MAZIKA', 'Welcome Email Subject', true, true);
INSERT INTO configuration(name,type,typeparam,value,description,editabled,viewed)
    VALUES ('VERIFICATION_MAIL_SUBJECT', 'String', null, 'VERIFY YOUR MAZIKA ACCOUNT', 'Verification Email Subject', true, true);    
INSERT INTO configuration(name,type,typeparam,value,description,editabled,viewed)
    VALUES ('RETRIEVE_PASSWORD_MAIL_SUBJECT', 'String', null, 'YOUR MAZIKA ACCOUNT PASSWORD', 'Password Retrieve Email Subject', true, true);
INSERT INTO configuration(name,type,typeparam,value,description,editabled,viewed)
    VALUES ('SYS_ADMIN_ID', 'Long', null, '2', 'Administrator Id', true, true);--This is the id of the Administrator User
INSERT INTO configuration(name,type,typeparam,value,description,editabled,viewed)
	VALUES('VERIFY_EMAIL', 'Boolean', null, 'false','Either to send a verification email or not', true, true);
INSERT INTO configuration(name,type,typeparam,value,description,editabled,viewed)
	VALUES('RESET_PASSWORD_MAIL_SUBJECT', 'String', null, 'Password Reset Email Subject', 'Your Mazika password reset request', true, true);
INSERT INTO configuration(name,type,typeparam,value,description,editabled,viewed)
	VALUES('SEND_WELCOME_EMAIL', 'Boolean', null, 'false', 'Either to send a welcome email or not', true, true);

--Language
INSERT INTO languages(code, locale, name) VALUES ('en', 'en', 'English');
INSERT INTO languages(code, locale, name) VALUES ('ar', 'ar', 'Arabic');
INSERT INTO languages(code, locale, name) VALUES ('fr', 'fr', 'Frensh');

-- Role
INSERT INTO roles(code, name) VALUES ('ROLE_SYSTEM', 'System Role');
INSERT INTO roles(code, name) VALUES ('ROLE_SUPER', 'Super Admin Role');
INSERT INTO roles(code, name) VALUES ('ROLE_ADMIN', 'Admin Role');
INSERT INTO roles(code, name) VALUES ('ROLE_CONTENT_ADMIN', 'Content Admin Role');
INSERT INTO roles(code, name) VALUES ('ROLE_REPORTS_ADMIN', 'Report Admin Role');
INSERT INTO roles(code, name) VALUES ('ROLE_USER', 'User Role');

-- Config
--INSERT INTO maz_config(id, createdby, createdon, lastupdatedby, lastupdatedon, bannerurl, isanonymousstreamingenabled, isdownloadsenabled, isgoogleadsenabled, ispremiumserviceenabled, defaultbundle_id, bannerimage_checksum, bannerimagear_checksum, mobileCountryCode, theme, service_id) 
--VALUES(1, -1, NOW(), -1, NOW(), null, false, false, false, false, null, null, null, 'EG', 0, 1);

-- System User [Password=MWVZ3OWFPJMO48FH9HYW]
INSERT INTO users(id,username,password,first_name,last_name,geneder, birthdate, enabled,verified, type, lang_code)
    VALUES (nextval('User_ID_SEQ'), 'system', '*6CA2FB280D2511805252B4FAFF6E8A777BBAD6D5', 'system', null, 'M', '19800101',true,true,1,'en');
INSERT INTO users_roles(users_id, roles_code)VALUES (1, 'ROLE_SYSTEM');
INSERT INTO auth_tokens(token,user_id, provider, createdby, createdon, lastupdatedby, lastupdatedon) 
    VALUES ('7b9b7fd0-6991-4e23-8bb2-0fcd434f7d88',1,'Local',1, current_timestamp, 1,current_timestamp);

-- Super User [Password=thinkmore]
INSERT INTO users(id,username,password,first_name,last_name,geneder, birthdate, enabled,verified, type, lang_code)
    VALUES (nextval('User_ID_SEQ'), 'mazika', '*583F90D33C2E1AFC05A7DCDA46F88B4E7D4BFEB1', 'mazika', null, 'M', '19800101',true,true,1,'en');
INSERT INTO users_roles(users_id, roles_code)VALUES (2, 'ROLE_SUPER');
INSERT INTO users_roles(users_id, roles_code)VALUES (2, 'ROLE_ADMIN');
INSERT INTO users_roles(users_id, roles_code)VALUES (2, 'ROLE_CONTENT_ADMIN');
INSERT INTO users_roles(users_id, roles_code)VALUES (2, 'ROLE_REPORTS_ADMIN');
INSERT INTO users_roles(users_id, roles_code)VALUES (2, 'ROLE_USER');
--INSERT INTO auth_tokens(token,user_id, provider, createdby, createdon, lastupdatedby, lastupdatedon) 
--    VALUES ('7b9b7fd0-6991-4e23-8bb2-0fcd434f7d90',2,'Local',2, current_timestamp, 2,current_timestamp);


--Audit
INSERT INTO audit.rev(id, "timestamp", user_id)VALUES (nextval('REV_ID_SEQ'), current_timestamp, 0);
INSERT INTO audit.languages_aud (rev, revtype,code,locale,name) Select 1,0,code,locale,name from languages;
INSERT INTO audit.configuration_aud(rev, revtype,name,  description, editabled, type, typeparam,value, viewed)Select 1,0,name,  description, editabled, type, typeparam,value, viewed from configuration;
INSERT INTO audit.users_aud(rev, revtype,id,enabled,verified, password, type, username, lang_code)Select 1, 0, id,enabled,verified, password,type, username, lang_code from users;
INSERT INTO audit.users_roles_aud(rev,revtype, users_id, roles_code) Select 1,0,users_id, roles_code from users_roles;
