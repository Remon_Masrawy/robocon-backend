<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="false" %>
<html>
  <head>
    <meta charset="utf-8">
    <title>Robocon</title>

    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='<c:url value="/swagger-ui-1.1.7/bootstrap/css/bootstrap.min.css"/>' rel="stylesheet">
    <link href='<c:url value="/swagger-ui-1.1.7/bootstrap/css/bootstrap-responsive.min.css" />' rel="stylesheet">
    <link href='<c:url value="/swagger-ui-1.1.7/bootstrap/css/docs.css" />' rel="stylesheet">
   
   <style type="text/css">
   	body > .navbar .brand{
   		color: #999;
   	}
   </style>
  
  </head>

  <body data-spy="scroll" data-target=".bs-docs-sidebar">
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a href="" class="brand">Masrawy</a>
          
        </div>
      </div>
    </div>

 <div class="jumbotron masthead">
  <div class="container">
    <h1>Robocon</h1>
    <p>Robocon REST Server</p>
    <p><a href="apis/" class="btn btn-primary btn-large">APIs</a></p>
    <br>Version <spring:message code="info.release.version"/>
   	<br>SCM <spring:message code="info.scm.revision"/>
   	<br>Date <spring:message code="info.build.time"/>
   	<br>Started At <spring:message code="info.startup.date"/>
   	<!-- <br><a href="release-notes">Release Notes</a> -->
  </div>
</div>

<div class="container">
  <div class="marketing">
    <h1>Click to Start</h1>
    <p class="marketing-byline">Currently the control fields is fixed at swagger application.</p>
   </div>
</div>
 </body>
</html>