package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconGenreDTO;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.services.RoboconGenreService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("I")
@RequestMapping(value = "/api/genre")
@Api(value = "Genre", description = "Robocon Genre Controller")
public class GenreController {

	@Autowired
	private RoboconGenreService roboconGenreService;
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("I01")
	@ApiOperation(value = "Create Genre", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = {"", "/"}, method = RequestMethod.POST)
	public @ResponseBody RoboconGenreDTO createGenre(@RequestBody RoboconGenreDTO roboconGenreDTO){
		return roboconGenreService.createGenre(roboconGenreDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("I02")
	@ApiOperation(value = "Update Genre", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = {"", "/"}, method = RequestMethod.PUT)
	public @ResponseBody RoboconGenreDTO updateGenre(@RequestBody RoboconGenreDTO roboconGenreDTO){
		return roboconGenreService.updateGenre(roboconGenreDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("I03")
	@ApiOperation(value = "Add Genre Meta", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/meta/{genreId}", method = RequestMethod.PUT)
	public @ResponseBody RoboconGenreDTO addGenreMeta(@RequestBody RoboconMetaDTO roboconMetaDTO, @NotNull @PathVariable("genreId") Long genreId){
		return roboconGenreService.addGenreMeta(roboconMetaDTO, genreId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("I04")
	@ApiOperation(value = "Fetch Genre", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{genreId}", method = RequestMethod.GET)
	public @ResponseBody RoboconGenreDTO fetchGenre(@NotNull @PathVariable("genreId") Long genreId){
		return roboconGenreService.fetchGenre(genreId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("I05")
	@ApiOperation(value = "Fetch Genre By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconGenreDTO> fetchGenreByPaging(@Valid @NotNull @PathVariable("pageNum") Integer pageNum, @Valid @NotNull @PathVariable("pageLimit") Integer pageLimit, HttpServletResponse response){
		return roboconGenreService.fetchGenresByPaging(pageNum, pageLimit, response);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("I07")
	@ApiOperation(value = "Remove Genre", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/{genreId}", method = RequestMethod.DELETE)
	public @ResponseBody void removeGenre(@NotNull @PathVariable("genreId") Long genreId){
		roboconGenreService.removeGenre(genreId);
	}
}
