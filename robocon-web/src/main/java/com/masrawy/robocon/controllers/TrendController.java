package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconSongTrendDTO;
import com.masrawy.robocon.services.RoboconTrendService;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/api/trend")
public class TrendController {

	@Autowired
	private RoboconTrendService roboconTrendService;
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Fetch Trends", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconSongTrendDTO> songTrends(HttpServletRequest request, @PathVariable("page") Integer page, @PathVariable("limit") Integer limit){
		return roboconTrendService.getSongTrends(request, page, limit);
	}
}
