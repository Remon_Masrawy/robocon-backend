package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconSongDTO;
import com.masrawy.robocon.services.RoboconRecommendationService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("E")
@Api(value = "Recommender", description = "Robocon Recommender Controller")
@RequestMapping(value = "/api/recommender")
public class RecommenderController {

	@Autowired
	private RoboconRecommendationService roboconRecommendationService;
	
	@Secured({ ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Fetch Song", notes = API_NOTE_ROLE_USER)
	@RequestMapping(value = "/song", method = RequestMethod.GET)
	public @ResponseBody List<RoboconSongDTO> recommendation(){
		return roboconRecommendationService.recommend();
	}
}
