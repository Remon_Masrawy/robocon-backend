package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconStorefrontDTO;
import com.masrawy.robocon.services.RoboconStorefrontService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("S")
@Api(value = "Storefront", description = "Robocon Storefront Controller")
@RequestMapping(value = "/api/storefront")
public class StorefrontController {

	@Autowired
	private RoboconStorefrontService roboconStorefrontService;
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("S01")
	@ApiOperation(value = "Create Storefront", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboconStorefrontDTO create(@RequestBody RoboconStorefrontDTO roboconStorefrontDTO){
		return roboconStorefrontService.createStorefront(roboconStorefrontDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("S02")
	@ApiOperation(value = "Update Storefront", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboconStorefrontDTO update(@RequestBody RoboconStorefrontDTO roboconStorefrontDTO){
		return roboconStorefrontService.updateStorefront(roboconStorefrontDTO);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("S03")
	@ApiOperation(value = "Get Storefront By ID", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{storefrontId}", method = RequestMethod.GET)
	public @ResponseBody RoboconStorefrontDTO get(@PathVariable("storefrontId") Long storefrontId){
		return roboconStorefrontService.getStorefront(storefrontId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("S04")
	@ApiOperation(value = "List Storefront", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconStorefrontDTO> list(@NotNull @PathVariable("page") Integer page, @NotNull @PathVariable("limit") Integer limit, HttpServletResponse response){
		return roboconStorefrontService.getStorefronts(page, limit, response);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("S05")
	@ApiOperation(value = "Delete Storefront By ID", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/{storefrontId}", method = RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable("storefrontId") Long storefrontId){
		roboconStorefrontService.removeStorefront(storefrontId);
	}
}
