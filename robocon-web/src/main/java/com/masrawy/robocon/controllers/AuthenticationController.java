package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_ROLE_ANONYMOUS;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconUserDTO;
import com.masrawy.robocon.services.RoboconRegistrationService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("A")
@Api(value = "Authentication", description = "Authentication Operations")
@RequestMapping(value = "/api/auth")
public class AuthenticationController {

	@Autowired
	private RoboconRegistrationService roboconRegistrationService;
	
	@OperationOrder("A01")
	@ApiOperation(value = "Signup", notes = API_NOTE_ROLE_ANONYMOUS)
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public @ResponseBody RoboconUserDTO signup(@RequestBody RoboconUserDTO roboconUserDTO){
		return roboconRegistrationService.signup(roboconUserDTO);
	}
	
	@OperationOrder("A02")
	@ApiOperation(value = "Login", notes = API_NOTE_ROLE_ANONYMOUS)
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody RoboconUserDTO login(@RequestBody RoboconUserDTO roboconUserDTO){
		return roboconRegistrationService.login(roboconUserDTO.getEmail(), roboconUserDTO.getPassword());
	}
}
