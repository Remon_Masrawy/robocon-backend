package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.cdm.core.dto.model.DTO;
import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.file.model.FileUpload;
import com.masrawy.robocon.services.RoboconUploadService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Controller
@ControllerOrder("U")
@Api(value = "Upload", description = "Robocon Upload Controller")
@RequestMapping(value = "/api/upload")
public class UploadController {

	@Autowired
	private RoboconUploadService roboconUploadService;
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("U01")
	@ApiOperation(value = "Upload Single", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/single/{id}", method = RequestMethod.POST, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public @ResponseBody DTO<?> uploadContent(@ApiParam(name = "fileUpload", required = true) FileUpload fileUpload, @PathVariable("id") Long id){
		return roboconUploadService.uploadFile(fileUpload, id);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("U01")
	@ApiOperation(value = "Upload Single", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/{id}/{type}", method = RequestMethod.POST)
	public @ResponseBody DTO<?> uploadContent(@RequestParam("file") MultipartFile file, @PathVariable("type") Integer type, @PathVariable("id") Long id){
		return roboconUploadService.uploadFile(file, id, type);
	}
	
}