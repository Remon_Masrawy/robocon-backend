package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconFrontendDTO;
import com.masrawy.robocon.services.RoboconFrontendService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("C")
@Api(value = "Frontend", description = "Robocon Frontend Controller")
@RequestMapping(value = "/api/frontend")
public class FrontendController {

	@Autowired
	private RoboconFrontendService roboconFrontendService;
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("C01")
	@ApiOperation(value = "Create Frontend", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboconFrontendDTO createFrontend(@RequestBody RoboconFrontendDTO frontend){
		return roboconFrontendService.createFrontend(frontend);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("C02")
	@ApiOperation(value = "Update Frontend", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboconFrontendDTO updateFontend(@RequestBody RoboconFrontendDTO frontend){
		return roboconFrontendService.updateFrontend(frontend);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("C02")
	@ApiOperation(value = "Get Frontend By Id", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/{frontendId}", method = RequestMethod.GET)
	public @ResponseBody RoboconFrontendDTO frontend(@NotNull @PathVariable("frontendId") Long frontendId){
		return roboconFrontendService.getFrontend(frontendId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("C05")
	@ApiOperation(value = "List Frontends By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<RoboconFrontendDTO> listFrontend(HttpServletRequest request, HttpServletResponse response){
		return roboconFrontendService.getFrontends(request, response);
	}
	
	@Secured({ CONTENT_ADMIN , ROLE_USER})
	@OperationOrder("C05")
	@ApiOperation(value = "List Frontends By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconFrontendDTO> listFrontend(@NotNull @PathVariable("pageNum") Integer pageNum, @NotNull @PathVariable("pageLimit") Integer pageLimit, HttpServletResponse response){
		return roboconFrontendService.getFrontends(pageNum, pageLimit, response);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("C07")
	@ApiOperation(value = "Get Frontends Types", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/types", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> frontendTypes(){
		return roboconFrontendService.getFrontendTypes();
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("C02")
	@ApiOperation(value = "Remove Frontend By Id", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/{frontendId}", method = RequestMethod.DELETE)
	public @ResponseBody void removeFrontend(@NotNull @PathVariable("frontendId") Long frontendId){
		roboconFrontendService.removeFrontend(frontendId);
	}
}
