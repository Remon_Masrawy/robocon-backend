package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconOperatorDTO;
import com.masrawy.robocon.services.RoboconOperatorService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("O")
@RequestMapping(value = "/api/operator")
@Api(value = "Operator", description = "Robocon Operator Controller")
public class OperatorController {

	@Autowired
	private RoboconOperatorService operatorService;
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("O01")
	@ApiOperation(value = "Create", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboconOperatorDTO createOperator(@RequestBody RoboconOperatorDTO operatorDTO){
		return operatorService.createOperator(operatorDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("O03")
	@ApiOperation(value = "Update", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboconOperatorDTO updateOperator(@RequestBody RoboconOperatorDTO operatorDTO){
		return operatorService.updateOperator(operatorDTO);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("O04")
	@ApiOperation(value = "List", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconOperatorDTO> listOperators(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit, HttpServletResponse response){
		return operatorService.getOperators(page, limit, response);
	}
}
