package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.ROLE_SYSTEM;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_ROLE_SYSTEM;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cdm.core.configuration.dto.ConfigParameterDTO;
import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.services.RoboconConfigurationService;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("D")
@RequestMapping(value = "/api/config")
public class ConfigurationController {

	@Autowired
	private RoboconConfigurationService roboconConfigurationService;
	
	@Secured({ ROLE_SYSTEM })
	@OperationOrder("D01")
	@ApiOperation(value = "Create Config Parameter", notes = API_NOTE_ROLE_SYSTEM)
	@RequestMapping(value = "/params", method = RequestMethod.POST)
	public @ResponseBody void createConfigParam(@RequestBody ConfigParameterDTO configParameterDTO){
		roboconConfigurationService.createConfigParam(configParameterDTO);
	}
	
	@Secured({ ROLE_SYSTEM })
	@OperationOrder("D02")
	@ApiOperation(value = "Update Config Parameter", notes = API_NOTE_ROLE_SYSTEM)
	@RequestMapping(value = "/params", method = RequestMethod.PUT)
	public @ResponseBody void updateConfigParam(@RequestBody ConfigParameterDTO configParameterDTO){
		roboconConfigurationService.updateConfigParam(configParameterDTO);
	}
	
	@Secured({ ROLE_SYSTEM })
	@OperationOrder("D03")
	@ApiOperation(value = "List Config Parameters", notes = API_NOTE_ROLE_SYSTEM)
	@RequestMapping(value = "/params", method = RequestMethod.GET)
	public @ResponseBody List<ConfigParameterDTO> getConfigParamsList(){
		return roboconConfigurationService.getConfigParamsList();
	}
	
	@Secured({ ROLE_SYSTEM })
	@OperationOrder("D04")
	@ApiOperation(value = "Remove Config Parameter", notes = API_NOTE_ROLE_SYSTEM)
	@RequestMapping(value = "/params", method = RequestMethod.DELETE)
	public @ResponseBody void removeConfigParam(@RequestParam(required = true) String param){
		roboconConfigurationService.deleteConfigParam(param);
	}
	
	@Secured({ ROLE_SYSTEM })
	@OperationOrder("D05")
	@ApiOperation(value = "Remove All Config Parameters", notes = API_NOTE_ROLE_SYSTEM)
	@RequestMapping(value = "/params/all", method = RequestMethod.DELETE)
	public @ResponseBody void removeAllConfigParam(){
		roboconConfigurationService.deleteAllConfigParams();
	}
}
