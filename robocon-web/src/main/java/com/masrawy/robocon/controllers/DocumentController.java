package com.masrawy.robocon.controllers;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cdm.core.security.SecurityUtil;
import com.cdm.core.security.service.UserService;
import com.knappsack.swagger4springweb.controller.ApiDocumentationController;

@Controller
@RequestMapping(value = "/documentation")
public class DocumentController extends ApiDocumentationController{

	private static final Logger logger = LoggerFactory.getLogger(DocumentController.class);
	
	@Autowired
	private UserService userService;
	
	public DocumentController(){
		this.setApiVersion("v1");
		this.setBaseControllerPackage("com.masrawy.robocon.controllers");
		this.setBaseModelPackage("com.masrawy.robocon.dto");

		ArrayList<String> addModels = new ArrayList<String>();
		addModels.add("com.cdm.core.security.dto");
		addModels.add("com.cdm.core.security.model");
		addModels.add("com.masrawy.robocon.model");
		this.setAdditionalModelPackages(addModels);
	}
	
	@RequestMapping(value = "/apis", method = RequestMethod.GET)
	public ModelAndView documentation(){
		logger.debug("User '{}' requiring documentation page.", userService.getRecordById(SecurityUtil.getUserId()).getUsername());
		ModelAndView modelAndView = new ModelAndView("documentation");
		return modelAndView;
	}
	
	@RequestMapping(value = {"/"}, method = RequestMethod.GET)
	public ModelAndView home(){
		ModelAndView modelAndView = new ModelAndView("home");
		return modelAndView;
	}
}
