package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cdm.core.security.dto.LanguageDTO;
import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.services.RoboconLanguageService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("I")
@RequestMapping(value = "/api/language")
@Api(value = "Language", description = "Robocon Language Controller")
public class LanguageController {

	@Autowired
	private RoboconLanguageService roboconLanguageService;
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("I01")
	@ApiOperation(value = "Create Language", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody LanguageDTO createLanguage(@RequestBody LanguageDTO languageDTO){
		return roboconLanguageService.createLanguage(languageDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("I01")
	@ApiOperation(value = "Update Language", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody LanguageDTO updateLanguage(@RequestBody LanguageDTO languageDTO){
		return roboconLanguageService.updateLanguage(languageDTO);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("I05")
	@ApiOperation(value = "Fetch Language By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<LanguageDTO> list(@Valid @NotNull @PathVariable("pageNum") Integer pageNum, @Valid @NotNull @PathVariable("pageLimit") Integer pageLimit){
		return roboconLanguageService.listLanguages(pageNum, pageLimit);
	}
}
