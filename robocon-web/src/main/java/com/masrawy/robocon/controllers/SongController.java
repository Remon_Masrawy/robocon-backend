package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconSongDTO;
import com.masrawy.robocon.dto.RoboconSongKaraokeDTO;
import com.masrawy.robocon.dto.RoboconUserSingDTO;
import com.masrawy.robocon.services.RoboconSongService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("E")
@Api(value = "Song", description = "Robocon Song Controller")
@RequestMapping(value = "/api/song")
public class SongController {

	@Autowired
	private RoboconSongService roboconSongService;
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("E01")
	@ApiOperation(value = "Create", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboconSongDTO createSong(@RequestBody RoboconSongDTO roboconSongDTO){
		return roboconSongService.createRoboconSong(roboconSongDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("E01")
	@ApiOperation(value = "Update", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboconSongDTO updateSong(@RequestBody RoboconSongDTO roboconSongDTO){
		return roboconSongService.updateRoboconSong(roboconSongDTO);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Fetch Song", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{songId}", method = RequestMethod.GET)
	public @ResponseBody Object fetchSong(@PathVariable("songId") Long songId){
		return roboconSongService.getRoboconSong(songId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Fetch Songs By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody Object fetchSongs(@PathVariable("pageNum") Integer pageNum, @PathVariable("pageLimit") Integer pageLimit, HttpServletResponse response){
		return roboconSongService.fetchRoboconSongs(pageNum, pageLimit, response);
	}
	
	/*
	 * Karaoke
	 */
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Fetch Song karaoke By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/karaoke/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconSongKaraokeDTO> fetchSongKaraoke(@PathVariable("pageNum") Integer pageNum, @PathVariable("pageLimit") Integer pageLimit){
		return roboconSongService.fetchRoboconSongKaraoke(pageNum, pageLimit);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Filter Song karaoke By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/karaoke/{categoryId}/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconSongKaraokeDTO> filterSongKaraoke(@PathVariable("categoryId") Long categoryId, @PathVariable("pageNum") Integer pageNum, @PathVariable("pageLimit") Integer pageLimit){
		return roboconSongService.filterRoboconSongKaraoke(categoryId, pageNum, pageLimit);
	}
	
	/*
	 * Sing
	 */
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Fetch User Sing By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/sing/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconUserSingDTO> fetchUserSing(@PathVariable("pageNum") Integer pageNum, @PathVariable("pageLimit") Integer pageLimit){
		return roboconSongService.fetchRoboconUserSing(pageNum, pageLimit, true);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("E01")
	@ApiOperation(value = "Fetch Unverified User Sing By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/sing/unverified/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconUserSingDTO> fetchUnVerifiedUserSing(@PathVariable("pageNum") Integer pageNum, @PathVariable("pageLimit") Integer pageLimit){
		return roboconSongService.fetchRoboconUserSing(pageNum, pageLimit, false);
	}
	
	/*
	 * Play, Download And Print
	 * 
	 */
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Play", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/play/{contentId}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
	@ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
	public @ResponseBody void playSongContent(@PathVariable("contentId") Long contentId, HttpServletRequest request, HttpServletResponse response){
		roboconSongService.playSong(contentId, request, response);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Download", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/download/{contentId}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
	@ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
	public @ResponseBody void downloadSongContent(@PathVariable("contentId") Long contentId, HttpServletRequest request, HttpServletResponse response){
		roboconSongService.downloadSong(contentId, request, response);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Download Karaoke", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/karaoke/download/{karaokeId}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
	@ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
	public @ResponseBody void downloadSongKaraoke(@PathVariable("karaokeId") Long karaokeId, HttpServletRequest request, HttpServletResponse response){
		roboconSongService.downloadKaraoke(karaokeId, request, response);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Download Lyrics", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/lyrics/download/{lyricsId}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
	@ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
	public @ResponseBody void downloadSongLyrics(@PathVariable("lyricsId") Long lyricsId, HttpServletRequest request, HttpServletResponse response){
		roboconSongService.downloadLyrics(lyricsId, request, response);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Print Lyrics", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/lyrics/print/{lyricsId}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
	@ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
	public @ResponseBody void printSongLyrics(@PathVariable("lyricsId") Long lyricsId, HttpServletRequest request, HttpServletResponse response){
		roboconSongService.printLyrics(lyricsId, request, response);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Play User Sing", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/sing/play/{singId}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
	@ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
	public @ResponseBody void playUserSing(@PathVariable("singId") Long singId, HttpServletRequest request, HttpServletResponse response){
		roboconSongService.playUserSing(singId, request, response);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Download User Sing", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/sing/download/{singId}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
	@ResponseStatus(value = HttpStatus.MOVED_PERMANENTLY)
	public @ResponseBody void downloadUserSing(@PathVariable("singId") Long singId, HttpServletRequest request, HttpServletResponse response){
		roboconSongService.downloadUserSing(singId, request, response);
	}
	
	/*
	 * Remove
	 */
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("E01")
	@ApiOperation(value = "Remove Song", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/{songId}", method = RequestMethod.DELETE)
	public @ResponseBody void removeSong(@PathVariable("songId") Long songId){
		roboconSongService.removeRoboconSong(songId);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("E01")
	@ApiOperation(value = "Remove Song content", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/content/{contentId}", method = RequestMethod.DELETE)
	public @ResponseBody RoboconSongDTO removeSongContent(@PathVariable("contentId") Long contentId){
		return roboconSongService.removeContent(contentId);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("E01")
	@ApiOperation(value = "Remove Song karaoke", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/karaoke/{karaokeId}", method = RequestMethod.DELETE)
	public @ResponseBody RoboconSongDTO removeSongKaraoke(@PathVariable("karaokeId") Long karaokeId){
		return roboconSongService.removeKaraoke(karaokeId);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("E01")
	@ApiOperation(value = "Remove Song lyrics", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/lyrics/{lyricsId}", method = RequestMethod.DELETE)
	public @ResponseBody RoboconSongDTO removeSongLyrics(@PathVariable("lyricsId") Long lyricsId){
		return roboconSongService.removeLyrics(lyricsId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("E01")
	@ApiOperation(value = "Remove User Sing", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/sing/{singId}", method = RequestMethod.DELETE)
	public @ResponseBody void removeUserSing(@PathVariable("singId") Long singId){
		roboconSongService.removeUserSing(singId);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("E01")
	@ApiOperation(value = "Verify User Sing", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/sing/verify/{singId}", method = RequestMethod.PUT)
	public @ResponseBody void verifyUserSing(@PathVariable("singId") Long singId){
		roboconSongService.verifyUserSing(singId);
	}
}
