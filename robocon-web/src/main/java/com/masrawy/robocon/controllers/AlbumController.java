package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconAlbumDTO;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.dto.RoboconTagDTO;
import com.masrawy.robocon.services.RoboconAlbumService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("G")
@Api(value = "Album", description = "Robocon Album Controller")
@RequestMapping(value = "/api/album")
public class AlbumController {

	@Autowired
	private RoboconAlbumService roboconAlbumService;
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("G01")
	@ApiOperation(value = "Create Album", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.POST, 
					consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody RoboconAlbumDTO createAlbum(@RequestBody RoboconAlbumDTO roboconAlbumDTO){
		return roboconAlbumService.createAlbum(roboconAlbumDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("G03")
	@ApiOperation(value = "Update Album", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboconAlbumDTO updateAlbum(@RequestBody RoboconAlbumDTO roboconAlbumDTO){
		return roboconAlbumService.updateAlbum(roboconAlbumDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("G07")
	@ApiOperation(value = "Add Meta", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/meta/{albumId}", method = RequestMethod.PUT)
	public @ResponseBody RoboconAlbumDTO addMeta(@RequestBody RoboconMetaDTO roboconMetaDTO, @Valid @NotNull @PathVariable("albumId") Long albumId){
		return roboconAlbumService.addMeta(roboconMetaDTO, albumId);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("G09")
	@ApiOperation(value = "Add Tag", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/tag/{albumId}", method = RequestMethod.PUT)
	public @ResponseBody RoboconAlbumDTO addTags(@RequestBody RoboconTagDTO roboconTagDTO, @Valid @NotNull @PathVariable("albumId") Long albumId){
		return roboconAlbumService.addTag(roboconTagDTO, albumId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("G04")
	@ApiOperation(value = "Fetch Album By ID", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{albumId}", method = RequestMethod.GET)
	public @ResponseBody RoboconAlbumDTO fetchAlbum(@Valid @NotNull @PathVariable("albumId") Long albumId){
		return roboconAlbumService.fetchAlbum(albumId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("G05")
	@ApiOperation(value = "Fetch Albums By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconAlbumDTO> fetchAlbums(@Valid @NotNull @PathVariable("pageNum") Integer pageNum, @Valid @NotNull @PathVariable("pageLimit") Integer pageLimit, HttpServletResponse response){
		return roboconAlbumService.fetchAlbums(pageNum, pageLimit, response);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("G04")
	@ApiOperation(value = "Remove Album", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/{albumId}", method = RequestMethod.DELETE)
	public @ResponseBody void removeAlbum(@Valid @NotNull @PathVariable("albumId") Long albumId){
		roboconAlbumService.removeAlbum(albumId);
	}
}
