package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconArtistDTO;
import com.masrawy.robocon.dto.RoboconArtistSocialDTO;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.dto.RoboconTagDTO;
import com.masrawy.robocon.services.RoboconArtistService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("F")
@Api(value = "Artist", description = "Robocon Artist Controller")
@RequestMapping(value = "/api/artist")
public class ArtistController {

	@Autowired
	private RoboconArtistService roboconArtistService;
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("F00")
	@ApiOperation(value = "Fetch Artists By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconArtistDTO> fetchArtists(@Valid @NotNull @PathVariable("pageNum") Integer pageNum, @Valid @NotNull @PathVariable("pageLimit") Integer pageLimit, HttpServletResponse response){
		return roboconArtistService.getRoboconArtists(pageNum, pageLimit, response);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("F01")
	@ApiOperation(value = "Fetch Artist", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{artistId}", method = RequestMethod.GET)
	public @ResponseBody RoboconArtistDTO fetchArtist(@Valid @NotNull @PathVariable("artistId") Long artistId){
		return roboconArtistService.fetchRoboconArtist(artistId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("F01")
	@ApiOperation(value = "Fetch Socials", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/socials", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> fetchSocials(){
		return roboconArtistService.getSocials();
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("F03")
	@ApiOperation(value = "Create Artist", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboconArtistDTO createArtist(@RequestBody RoboconArtistDTO roboconArtistDTO){
		return roboconArtistService.createRoboconArtist(roboconArtistDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("F04")
	@ApiOperation(value = "Update Artist", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboconArtistDTO updateArtist(@RequestBody RoboconArtistDTO roboconArtistDTO){
		return roboconArtistService.updateRoboconArtist(roboconArtistDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("F05")
	@ApiOperation(value = "Add Artist Meta", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/meta/{artistId}", method = RequestMethod.PUT)
	public @ResponseBody RoboconArtistDTO addMeta(@RequestBody List<RoboconMetaDTO> roboconMetaDTO,@Valid @NotNull @PathVariable("artistId") Long artistId){
		return roboconArtistService.addRoboconArtistMeta(roboconMetaDTO, artistId);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("F09")
	@ApiOperation(value = "Add Artist Tags", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/tags/{artistId}", method = RequestMethod.PUT)
	public @ResponseBody RoboconArtistDTO addTags(@RequestBody List<RoboconTagDTO> roboconTagsDTO, @PathVariable("artistId") Long artistId){
		return roboconArtistService.addRoboconArtistTags(roboconTagsDTO, artistId);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("F11")
	@ApiOperation(value = "Add Artist Socials", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/social/{artistId}", method = RequestMethod.PUT)
	public @ResponseBody RoboconArtistDTO addSocials(@RequestBody List<RoboconArtistSocialDTO> roboconArtistSocialsDTO, @PathVariable("artistId") Long artistId){
		return roboconArtistService.addRoboconArtistSocials(roboconArtistSocialsDTO, artistId);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("F01")
	@ApiOperation(value = "Remove Artist", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/{artistId}", method = RequestMethod.DELETE)
	public @ResponseBody void removeArtist(@Valid @NotNull @PathVariable("artistId") Long artistId){
		roboconArtistService.removeArtist(artistId);
	}
}
