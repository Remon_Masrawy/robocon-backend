package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconArtistEventDTO;
import com.masrawy.robocon.dto.RoboconArtistEventMetaDTO;
import com.masrawy.robocon.services.RoboconEventService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("F")
@Api(value = "Event", description = "Robocon Event Controller")
@RequestMapping(value = "/api/event")
public class EventController {

	@Autowired
	private RoboconEventService roboconEventService;
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("F02")
	@ApiOperation(value = "Create Artist Event", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboconArtistEventDTO createEvent(@Valid @RequestBody RoboconArtistEventDTO roboconArtistEventDTO){
		return roboconEventService.createRoboconArtistEvent(roboconArtistEventDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("F03")
	@ApiOperation(value = "Update Artist Event", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboconArtistEventDTO updateEvent(@RequestBody RoboconArtistEventDTO roboconArtistEventDTO){
		return roboconEventService.updateRoboconArtistEvent(roboconArtistEventDTO);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("F01")
	@ApiOperation(value = "Fetch Events By id", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{eventId}", method = RequestMethod.GET)
	public @ResponseBody RoboconArtistEventDTO fetchArtistEvent(@Valid @NotNull @PathVariable("eventId") Long eventId){
		return roboconEventService.getRoboconArtistEvent(eventId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("F01")
	@ApiOperation(value = "Fetch Events By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconArtistEventDTO> fetchArtistEvents(@Valid @NotNull @PathVariable("pageNum") Integer pageNum, @Valid @NotNull @PathVariable("pageLimit") Integer pageLimit, HttpServletResponse response){
		return roboconEventService.fetchRoboconArtistEvents(pageNum, pageLimit, response);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("F04")
	@ApiOperation(value = "add Artist Event Meta", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/meta/{eventId}", method = RequestMethod.PUT)
	public @ResponseBody RoboconArtistEventDTO addEventMeta(@RequestBody List<RoboconArtistEventMetaDTO> roboconArtistEventMetaDTO, @PathVariable("eventId") Long eventId){
		return roboconEventService.addRoboconArtistEventMeta(roboconArtistEventMetaDTO, eventId);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("F05")
	@ApiOperation(value = "Remove Artist Event", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/{eventId}", method = RequestMethod.DELETE)
	public @ResponseBody void deleteEvent(@PathVariable("eventId") Long eventId){
		roboconEventService.removeRoboconArtistEvent(eventId);
	}
	
}
