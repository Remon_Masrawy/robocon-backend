package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.CountryDTO;
import com.masrawy.robocon.services.CountryService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("P")
@RequestMapping(value = "/api/country")
@Api(value = "Country", description = "Robocon Country Controller")
public class CountryController {

	@Autowired
	private CountryService countryService;
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("P01")
	@ApiOperation(value = "Create", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody CountryDTO createCountry(@RequestBody CountryDTO countryDTO){
		return countryService.createCountry(countryDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("P02")
	@ApiOperation(value = "Update", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody CountryDTO updateCountry(@RequestBody CountryDTO countryDTO){
		return countryService.updateCountry(countryDTO);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("P03")
	@ApiOperation(value = "List", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<CountryDTO> listCountries(@PathVariable("page") Integer page, @PathVariable("limit") Integer limit, HttpServletResponse response){
		return countryService.getCountries(page, limit, response);
	}
}
