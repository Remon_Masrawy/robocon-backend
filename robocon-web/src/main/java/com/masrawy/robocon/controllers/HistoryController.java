package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconUserSongHistoryDTO;
import com.masrawy.robocon.services.RoboconUserSongHistoryService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("I")
@RequestMapping(value = "/api/history")
@Api(value = "History", description = "Robocon History Controller")
public class HistoryController {

	@Autowired
	private RoboconUserSongHistoryService roboconUserSongHistoryService;
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("H00")
	@ApiOperation(value = "Fetch Song History By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/song/fetch/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconUserSongHistoryDTO> listSongHistory(@NotNull @PathVariable("pageNum") Integer pageNum, @NotNull @PathVariable("pageLimit") Integer pageLimit){
		return roboconUserSongHistoryService.listSongHistory(pageNum, pageLimit);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("H00")
	@ApiOperation(value = "Filter Song History By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/song/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconUserSongHistoryDTO> filterSongHistory(@NotNull @PathVariable("pageNum") Integer pageNum, @NotNull @PathVariable("pageLimit") Integer pageLimit, HttpServletRequest request){
		return roboconUserSongHistoryService.filterSongHistory(request, pageNum, pageLimit);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("H00")
	@ApiOperation(value = "Get Play Again Song List By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/song/again/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconUserSongHistoryDTO> playAgainSong(@NotNull @PathVariable("pageNum") Integer pageNum, @NotNull @PathVariable("pageLimit") Integer pageLimit, HttpServletRequest request){
		return roboconUserSongHistoryService.playAgain(request, pageNum, pageLimit);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("H00")
	@ApiOperation(value = "Delete song history", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/song/{historyId}", method = RequestMethod.DELETE)
	public @ResponseBody void deleteSongHistory(@NotNull @PathVariable("historyId") Long historyId){
		roboconUserSongHistoryService.deleteSongHistory(historyId);
	}
}