package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconMetaDTO;
import com.masrawy.robocon.dto.RoboconProviderDTO;
import com.masrawy.robocon.services.RoboconProviderService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("H")
@Api(value = "Provider", description = "Robocon Provider Controller")
@RequestMapping(value = "/api/provider")
public class ProviderController {

	@Autowired
	private RoboconProviderService roboconProviderService;
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("H00")
	@ApiOperation(value = "Create Provider", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboconProviderDTO createProvider(@RequestBody RoboconProviderDTO roboconProviderDTO){
		return roboconProviderService.createProvider(roboconProviderDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("H00")
	@ApiOperation(value = "Update Provider", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboconProviderDTO updateProvider(@RequestBody RoboconProviderDTO roboconProviderDTO){
		return roboconProviderService.updateProvider(roboconProviderDTO);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("H00")
	@ApiOperation(value = "Add Provider Meta", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/meta/{providerId}", method = RequestMethod.PUT)
	public @ResponseBody RoboconProviderDTO addProviderMeta(@RequestBody RoboconMetaDTO roboconMetaDTO, @NotNull @PathVariable("providerId") Long providerId){
		return roboconProviderService.addMeta(roboconMetaDTO, providerId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("H00")
	@ApiOperation(value = "Fetch Provider", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{providerId}", method = RequestMethod.GET)
	public @ResponseBody RoboconProviderDTO fetchProvider(@NotNull @PathVariable("providerId") Long providerId){
		return roboconProviderService.fetchProvider(providerId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("H00")
	@ApiOperation(value = "Fetch Provider By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconProviderDTO> fetchProvidersByPaging(@NotNull @PathVariable("pageNum") Integer pageNum, @NotNull @PathVariable("pageLimit") Integer pageLimit, HttpServletResponse response){
		return roboconProviderService.fetchProvidersByPaging(pageNum, pageLimit, response);
	}
	
	@Secured({ CONTENT_ADMIN })
	@OperationOrder("H00")
	@ApiOperation(value = "Remove Provider", notes = API_NOTE_CONTENT_ADMIN)
	@RequestMapping(value = "/{providerId}", method = RequestMethod.DELETE)
	public @ResponseBody void removeProvider(@NotNull @PathVariable("providerId") Long providerId){
		roboconProviderService.removeProvider(providerId);
	}
}
