package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.ROLE_SUPER;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.REPORTS_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_ROLE_SUPER;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_ROLE_ADMIN;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconUserDTO;
import com.masrawy.robocon.services.RoboconRegistrationService;
import com.masrawy.robocon.services.RoboconUserService;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("B")
@RequestMapping(value = "/api/user")
public class UserController {

	@Autowired
	private RoboconRegistrationService roboconRegistrationService;
	
	@Autowired
	private RoboconUserService roboconUserService;
	
	@Secured({ ROLE_SUPER , ROLE_ADMIN, CONTENT_ADMIN, REPORTS_ADMIN })
	@OperationOrder("B05")
	@ApiOperation(value = "Retrieve User Info", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody RoboconUserDTO getUser(){
		return roboconUserService.getUser();
	}
	
	@Secured({ ROLE_SUPER })
	@OperationOrder("B01")
	@ApiOperation(value = "Create User", notes = API_NOTE_ROLE_SUPER)
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboconUserDTO createUser(@RequestBody RoboconUserDTO roboconUserDTO){
		return roboconRegistrationService.signup(roboconUserDTO);
	}
	
	@Secured({ ROLE_SUPER })
	@OperationOrder("B01")
	@ApiOperation(value = "Update User", notes = API_NOTE_ROLE_SUPER)
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboconUserDTO updateUser(@RequestBody RoboconUserDTO roboconUserDTO){
		return roboconUserService.updateUser(roboconUserDTO);
	}
	
	@Secured({ ROLE_SUPER , ROLE_ADMIN, CONTENT_ADMIN, REPORTS_ADMIN })
	@OperationOrder("B01")
	@ApiOperation(value = "Update User Info", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(value = "/info", method = RequestMethod.PUT)
	public @ResponseBody RoboconUserDTO updateUserInfo(@RequestBody RoboconUserDTO roboconUserDTO){
		return roboconUserService.updateUserInfo(roboconUserDTO);
	}
	
	@Secured({ ROLE_SUPER , ROLE_ADMIN, CONTENT_ADMIN, REPORTS_ADMIN })
	@OperationOrder("B01")
	@ApiOperation(value = "Update User Photo", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(value = "/photo", method = RequestMethod.POST)
	public @ResponseBody RoboconUserDTO updateUserPhoto(@RequestParam("file") MultipartFile file){
		return roboconUserService.updateUserPhoto(file);
	}
	
	@Secured({ ROLE_SUPER, ROLE_ADMIN , CONTENT_ADMIN, REPORTS_ADMIN })
	@OperationOrder("B03")
	@ApiOperation(value = "Change Password", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(value = "/change/password", method = RequestMethod.PUT)
	public @ResponseBody RoboconUserDTO changePassword(@RequestParam(required = true) String oldPassword, @RequestParam(required = true) String newPassword){
		return roboconUserService.changePassword(oldPassword, newPassword);
	}
	
	@Secured({ ROLE_SUPER })
	@OperationOrder("B04")
	@ApiOperation(value = "Active User", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(value = "/active/{userId}", method = RequestMethod.PUT)
	public @ResponseBody RoboconUserDTO active(@PathVariable("userId") Long userId){
		return roboconUserService.activeUser(userId);
	}
	
	@Secured({ ROLE_SUPER })
	@OperationOrder("B05")
	@ApiOperation(value = "Inactive User", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(value = "/inactive/{userId}", method = RequestMethod.PUT)
	public @ResponseBody RoboconUserDTO inactive(@PathVariable("userId") Long userId){
		return roboconUserService.inactiveUser(userId);
	}
	
	@Secured({ ROLE_SUPER })
	@OperationOrder("B05")
	@ApiOperation(value = "Retrieve User", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public @ResponseBody RoboconUserDTO getUser(@PathVariable("userId") Long userId){
		return roboconUserService.getUser(userId);
	}
	
	@Secured({ ROLE_SUPER })
	@OperationOrder("S04")
	@ApiOperation(value = "List Users", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(value = "/{page}/{limit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconUserDTO> listUsers(@NotNull @PathVariable("page") Integer page, @NotNull @PathVariable("limit") Integer limit, HttpServletResponse response){
		return roboconUserService.getUsers(page, limit, response);
	}
	
	@Secured({ ROLE_SUPER })
	@OperationOrder("B05")
	@ApiOperation(value = "Delete User", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
	public @ResponseBody void deleteUser(@PathVariable("userId") Long userId){
		roboconUserService.deleteUser(userId);
	}
}
