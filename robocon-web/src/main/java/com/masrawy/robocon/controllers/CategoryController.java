package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_ROLE_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_ROLE_ADMIN_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_SUPER;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconCategoryDTO;
import com.masrawy.robocon.services.RoboconCategoryService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("S")
@Api(value = "Category", description = "Robocon Category Controller")
@RequestMapping(value = "/api/category")
public class CategoryController {

	@Autowired
	private RoboconCategoryService roboconCategoryService;
	
	@Secured({ ROLE_SUPER, ROLE_ADMIN })
	@OperationOrder("S01")
	@ApiOperation(value = "Create Category", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody RoboconCategoryDTO create(@RequestBody RoboconCategoryDTO roboconCategoryDTO){
		return roboconCategoryService.createCategory(roboconCategoryDTO);
	}
	
	@Secured({ ROLE_SUPER, ROLE_ADMIN })
	@OperationOrder("S02")
	@ApiOperation(value = "Update Category", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody RoboconCategoryDTO update(@RequestBody RoboconCategoryDTO roboconCategoryDTO){
		return roboconCategoryService.updateCategory(roboconCategoryDTO);
	}
	
	@Secured({ ROLE_SUPER, ROLE_ADMIN })
	@OperationOrder("S05")
	@ApiOperation(value = "Get Category", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(value = "/{categoryId}", method = RequestMethod.GET)
	public @ResponseBody RoboconCategoryDTO category(@NotNull @PathVariable("categoryId") Long categoryId){
		return roboconCategoryService.getCategory(categoryId);
	}
	
	@Secured({ ROLE_SUPER, ROLE_ADMIN, ROLE_USER })
	@OperationOrder("S06")
	@ApiOperation(value = "List Categories By Paging (0 is default, null not allowable.)", notes = API_NOTE_ROLE_ADMIN_ROLE_USER)
	@RequestMapping(value = "/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconCategoryDTO> list(@NotNull @PathVariable("pageNum") Integer pageNum, @NotNull @PathVariable("pageLimit") Integer pageLimit, HttpServletResponse response){
		return roboconCategoryService.listCategories(pageNum, pageLimit, response);
	}
	
	@Secured({ ROLE_SUPER, ROLE_ADMIN })
	@OperationOrder("S05")
	@ApiOperation(value = "Remove Category", notes = API_NOTE_ROLE_ADMIN)
	@RequestMapping(value = "/{categoryId}", method = RequestMethod.DELETE)
	public @ResponseBody void removeCategory(@NotNull @PathVariable("categoryId") Long categoryId){
		roboconCategoryService.removeCategory(categoryId);
	}
}
