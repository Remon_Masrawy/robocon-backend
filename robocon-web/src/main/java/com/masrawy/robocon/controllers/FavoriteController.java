package com.masrawy.robocon.controllers;

import static com.masrawy.robocon.dto.RoleConstants.ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.CONTENT_ADMIN;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_ROLE_USER;
import static com.masrawy.robocon.dto.RoleConstants.API_NOTE_CONTENT_ADMIN_ROLE_USER;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.knappsack.swagger4springweb.annotation.ControllerOrder;
import com.knappsack.swagger4springweb.annotation.OperationOrder;
import com.masrawy.robocon.dto.RoboconUserSongFavoriteDTO;
import com.masrawy.robocon.services.RoboconUserSongFavoriteService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
@ControllerOrder("F")
@Api(value = "Favorite", description = "Robocon Favorite Controller")
@RequestMapping(value = "/api/favorite")
public class FavoriteController {

	@Autowired
	private RoboconUserSongFavoriteService roboconUserSongFavoriteService;
	
	@Secured({ ROLE_USER })
	@OperationOrder("F01")
	@ApiOperation(value = "Create Song Favorite", notes = API_NOTE_ROLE_USER)
	@RequestMapping(value = "/song/{songId}", method = RequestMethod.POST)
	public @ResponseBody RoboconUserSongFavoriteDTO createSongFavorite(@PathVariable("songId") Long songId){
		return roboconUserSongFavoriteService.createSongFavorite(songId);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("F01")
	@ApiOperation(value = "Filter Favorites By Paging (0 is default, null not allowable.)", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/song/{pageNum}/{pageLimit}", method = RequestMethod.GET)
	public @ResponseBody List<RoboconUserSongFavoriteDTO> filterSongFavorites(@Valid @NotNull @PathVariable("pageNum") Integer pageNum, @Valid @NotNull @PathVariable("pageLimit") Integer pageLimit){
		return roboconUserSongFavoriteService.filterSongFavorites(pageNum, pageLimit);
	}
	
	@Secured({ CONTENT_ADMIN, ROLE_USER })
	@OperationOrder("F05")
	@ApiOperation(value = "Remove Song Favorite", notes = API_NOTE_CONTENT_ADMIN_ROLE_USER)
	@RequestMapping(value = "/song/{favoriteId}", method = RequestMethod.DELETE)
	public @ResponseBody void deleteSongFavorite(@PathVariable("favoriteId") Long favoriteId){
		roboconUserSongFavoriteService.deleteSongFavorite(favoriteId);
	}
}
