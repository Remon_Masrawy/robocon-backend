package com.masrawy.robocon.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.MultipartResolver;

import com.cdm.core.controlfields.web.ControlFieldsFilter;

public class RoboconControlFieldsFilter extends ControlFieldsFilter{

	static final String ORIGIN = "Origin";
	
	@Autowired
	private MultipartResolver multipartResolver;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		
		
		 if (request.getHeader(ORIGIN) != null) {
	            String origin = request.getHeader(ORIGIN);
	            response.addHeader("Access-Control-Allow-Origin", origin);
	            response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
	            response.setHeader("Access-Control-Max-Age", "3600");
	            response.setHeader("Allow", "GET, HEAD, POST, PUT, DELETE, TRACE, OPTIONS, PATCH");
	            response.addHeader("x-requested-with", "XMLHttpRequest");
	            response.addHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
		 }
		 if(request instanceof MultipartHttpServletRequest){
			 MultipartHttpServletRequest newRequest = (MultipartHttpServletRequest)request;
			 multipartResolver.cleanupMultipart(newRequest);
		 }
		super.doFilterInternal(request, response, filterChain);
	}
}
