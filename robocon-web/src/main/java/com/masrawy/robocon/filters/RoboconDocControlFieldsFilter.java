package com.masrawy.robocon.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cdm.core.controlfields.web.ControlFieldsFilter;

public class RoboconDocControlFieldsFilter extends ControlFieldsFilter{

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		response.addHeader("Cache-Control", "max-age=0, no-transform, private");
		super.doFilterInternal(request, response, filterChain);
	}
	
	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		if(request.getRequestURI().toString().equalsIgnoreCase("/robocon-web/documentation/")){
			return true;
		}
		return false;
	}
}
