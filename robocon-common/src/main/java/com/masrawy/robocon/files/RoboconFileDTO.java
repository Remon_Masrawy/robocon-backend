package com.masrawy.robocon.files;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class RoboconFileDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private MultipartFile file;
	
	private String fileName;
	
	private Boolean isMain;
	
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Boolean getIsMain() {
		return isMain;
	}
	public void setIsMain(Boolean isMain) {
		this.isMain = isMain;
	}
}
