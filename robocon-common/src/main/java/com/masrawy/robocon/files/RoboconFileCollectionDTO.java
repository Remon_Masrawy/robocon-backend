package com.masrawy.robocon.files;

import java.io.Serializable;
import java.util.List;

public class RoboconFileCollectionDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	List<RoboconFileDTO> files;

	public List<RoboconFileDTO> getFiles() {
		return files;
	}

	public void setFiles(List<RoboconFileDTO> files) {
		this.files = files;
	}
}
