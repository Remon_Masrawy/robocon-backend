package com.masrawy.robocon.dto;

import static com.cdm.core.validation.ValidationGroups.CREATE_VALIDATION_GROUP;
import static com.cdm.core.validation.ValidationGroups.UPDATE_VALIDATION_GROUP;

import java.util.List;

import com.cdm.core.datatypes.DateOnly;
import com.cdm.core.dto.model.AbstractDTO;
import com.cdm.core.validation.validator.NotEmptyString;

import net.sf.oval.constraint.MaxLength;
import net.sf.oval.constraint.MinLength;

public class RoboconUserDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	@MaxLength(value = 23, profiles = { CREATE_VALIDATION_GROUP })
	private String firstName;
	
	@MaxLength(value = 23, profiles = { CREATE_VALIDATION_GROUP })
	private String lastName;
	
	@NotEmptyString(profiles = { CREATE_VALIDATION_GROUP })
	@MinLength(value = 5, profiles = { UPDATE_VALIDATION_GROUP })
	@MaxLength(value = 23, profiles = { CREATE_VALIDATION_GROUP })
	private String username;

	@NotEmptyString(profiles = { CREATE_VALIDATION_GROUP })
	@MaxLength(value = 64, profiles = { CREATE_VALIDATION_GROUP })
	private String email;

	@NotEmptyString(profiles = { UPDATE_VALIDATION_GROUP })
	@MinLength(value = 5, profiles = { UPDATE_VALIDATION_GROUP })
	@MaxLength(value = 100, profiles = { UPDATE_VALIDATION_GROUP })
	private String password;
	
	@MinLength(value = 9, profiles = { UPDATE_VALIDATION_GROUP })
	@MaxLength(value = 17, profiles = { UPDATE_VALIDATION_GROUP })
	private String mobileNo;

	private DateOnly birthDate;
	
	private String authToken;
	
	private boolean verified;
	
	private List<String> roleCodes;
	
	private Boolean isActive;
	
	private String photo;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public DateOnly getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(DateOnly birthDate) {
		this.birthDate = birthDate;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public List<String> getRoleCodes() {
		return roleCodes;
	}

	public void setRoleCodes(List<String> roleCodes) {
		this.roleCodes = roleCodes;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	@Override
	public Long pk() {
		return getId();
	}

}
