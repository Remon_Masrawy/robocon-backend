package com.masrawy.robocon.dto;

import com.cdm.core.dto.model.AbstractDTO;

public class RoboconConfigurationDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String welcomeEmailSubject;

	private String resetPasswordEmailSubject;

	private String verifyEmailSubject;
	
	private Boolean allowNonverifiedEmails;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWelcomeEmailSubject() {
		return welcomeEmailSubject;
	}

	public void setWelcomeEmailSubject(String welcomeEmailSubject) {
		this.welcomeEmailSubject = welcomeEmailSubject;
	}

	public String getResetPasswordEmailSubject() {
		return resetPasswordEmailSubject;
	}

	public void setResetPasswordEmailSubject(String resetPasswordEmailSubject) {
		this.resetPasswordEmailSubject = resetPasswordEmailSubject;
	}

	public String getVerifyEmailSubject() {
		return verifyEmailSubject;
	}

	public void setVerifyEmailSubject(String verifyEmailSubject) {
		this.verifyEmailSubject = verifyEmailSubject;
	}

	public Boolean getAllowNonverifiedEmails() {
		return allowNonverifiedEmails;
	}

	public void setAllowNonverifiedEmails(Boolean allowNonverifiedEmails) {
		this.allowNonverifiedEmails = allowNonverifiedEmails;
	}

	@Override
	public Long pk() {
		return getId();
	}

}
