package com.masrawy.robocon.dto;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.constraints.NotBlankCollection;
import com.masrawy.robocon.constraints.NotEmptyCollection;
import com.masrawy.robocon.constraints.NotNull;

public class RoboconCategoryDTO extends AbstractDTO<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	@Valid
	@NotNull
	@NotEmptyCollection
	@NotBlankCollection
	private List<RoboconMetaDTO> meta;
	
	@NotNull
	private Map<Long, String> storefronts;
	private Map<Long, Map<String, String>> operators;
	private Map<Long, String> countries;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<RoboconMetaDTO> getMeta() {
		return meta;
	}

	public void setMeta(List<RoboconMetaDTO> meta) {
		this.meta = meta;
	}

	public Map<Long, String> getStorefronts() {
		return storefronts;
	}

	public void setStorefronts(Map<Long, String> storefronts) {
		this.storefronts = storefronts;
	}

	public Map<Long, Map<String, String>> getOperators() {
		return operators;
	}

	public void setOperators(Map<Long, Map<String, String>> operators) {
		this.operators = operators;
	}

	public Map<Long, String> getCountries() {
		return countries;
	}

	public void setCountries(Map<Long, String> countries) {
		this.countries = countries;
	}

	@Override
	public Long pk() {
		return getId();
	}

}
