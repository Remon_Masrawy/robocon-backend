package com.masrawy.robocon.dto;

import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.constraints.NotEmptyString;
import com.masrawy.robocon.constraints.NotNull;

public class RoboconTagDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotNull
	@NotEmptyString
	private String tagTitle;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTagTitle() {
		return tagTitle;
	}
	public void setTagTitle(String tagTitle) {
		this.tagTitle = tagTitle;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
