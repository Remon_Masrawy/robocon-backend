package com.masrawy.robocon.dto;

import com.cdm.core.dto.model.AbstractDTO;

public class RoboconMobilePhotoDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String checksum;
	private String contentType;
	private Boolean isMainPhoto;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Boolean getIsMainPhoto() {
		return isMainPhoto;
	}
	public void setIsMainPhoto(Boolean isMainPhoto) {
		this.isMainPhoto = isMainPhoto;
	}
	@Override
	public Long pk() {
		return getId();
	}
}
