package com.masrawy.robocon.dto;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.cdm.core.dto.model.AbstractDTO;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.masrawy.robocon.constraints.NotBlankCollection;
import com.masrawy.robocon.constraints.NotEmptyCollection;
import com.masrawy.robocon.constraints.NotEmptyString;
import com.masrawy.robocon.constraints.NotNull;

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
	@Type(value = RoboconSongFrontendDTO.class, name = "SONG"),
	@Type(value = RoboconAlbumFrontendDTO.class, name = "ALBUM"),
})
public abstract class RoboconFrontendDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@Valid
	@NotNull
	@NotEmptyCollection
	@NotBlankCollection
	private List<RoboconMetaDTO> meta;
	
	@NotNull
	@NotEmptyString
	private String type;
	
	private Map<Long, String> categories;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<RoboconMetaDTO> getMeta() {
		return meta;
	}

	public void setMeta(List<RoboconMetaDTO> meta) {
		this.meta = meta;
	}

	public Map<Long, String> getCategories() {
		return categories;
	}

	public void setCategories(Map<Long, String> categories) {
		this.categories = categories;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public Long pk() {
		return getId();
	}

}
