package com.masrawy.robocon.dto;

import javax.validation.constraints.NotNull;

import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.constraints.NotEmptyString;
import com.masrawy.robocon.model.RoboconSocialEnum;

public class RoboconArtistSocialDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotNull
	private RoboconSocialEnum social;
	
	@NotNull
	@NotEmptyString
	private String link;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public RoboconSocialEnum getSocial() {
		return social;
	}
	public void setSocial(RoboconSocialEnum social) {
		this.social = social;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
