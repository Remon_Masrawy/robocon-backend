package com.masrawy.robocon.dto;

import com.cdm.core.dto.model.AbstractDTO;

public abstract class AbstractUserFavoriteDTO extends AbstractDTO<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long userId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Override
	public Long pk() {
		return getId();
	}
}
