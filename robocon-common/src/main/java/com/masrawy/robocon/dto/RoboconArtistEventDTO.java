package com.masrawy.robocon.dto;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.cdm.core.datatypes.DateOnly;
import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.constraints.NotEmptyCollection;
import com.masrawy.robocon.constraints.NotNull;

public class RoboconArtistEventDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String bookingTicketUrl;
	@NotNull
	private DateOnly beginDate;
	@NotNull
	private DateOnly endDate;
	@NotNull
	private String beginTime;
	@NotNull
	private String endTime;
	private Double latitude;
	private Double longitude;
	private String telephone;
	private Boolean isActive;
	
	@NotNull
	@NotEmptyCollection
	private List<RoboconArtistEventMetaDTO> meta;
	@Valid
	private List<RoboconPhotoDTO> photos;
	@Valid
	private List<RoboconTagDTO> tags;
	
	@NotNull
	private Map<Long, String> artists;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBookingTicketUrl() {
		return bookingTicketUrl;
	}
	public void setBookingTicketUrl(String bookingTicketUrl) {
		this.bookingTicketUrl = bookingTicketUrl;
	}
	public DateOnly getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(DateOnly beginDate) {
		this.beginDate = beginDate;
	}
	public DateOnly getEndDate() {
		return endDate;
	}
	public void setEndDate(DateOnly endDate) {
		this.endDate = endDate;
	}
	public String getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public List<RoboconArtistEventMetaDTO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboconArtistEventMetaDTO> meta) {
		this.meta = meta;
	}
	public List<RoboconPhotoDTO> getPhotos() {
		return photos;
	}
	public void setPhotos(List<RoboconPhotoDTO> photos) {
		this.photos = photos;
	}
	public List<RoboconTagDTO> getTags() {
		return tags;
	}
	public void setTags(List<RoboconTagDTO> tags) {
		this.tags = tags;
	}
	public Map<Long, String> getArtists() {
		return artists;
	}
	public void setArtists(Map<Long, String> artists) {
		this.artists = artists;
	}
	@Override
	public Long pk() {
		return getId();
	}
}
