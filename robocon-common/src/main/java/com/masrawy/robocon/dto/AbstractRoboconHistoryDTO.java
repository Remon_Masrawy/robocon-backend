package com.masrawy.robocon.dto;

import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.model.RoboconUserActionEnum;

public abstract class AbstractRoboconHistoryDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	private RoboconUserActionEnum action;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public RoboconUserActionEnum getAction() {
		return action;
	}
	public void setAction(RoboconUserActionEnum action) {
		this.action = action;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
