package com.masrawy.robocon.dto;

public class RoboconUserSongFavoriteDTO extends AbstractUserFavoriteDTO{

	private static final long serialVersionUID = 1L;

	private RoboconSongDTO song;

	public RoboconSongDTO getSong() {
		return song;
	}

	public void setSong(RoboconSongDTO song) {
		this.song = song;
	}
	
}
