package com.masrawy.robocon.dto;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.masrawy.robocon.constraints.NotEmptyCollection;
import com.masrawy.robocon.constraints.NotNull;

public class RoboconArtistDTO extends AbstractRoboconPersonDTO{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@Valid
	@NotNull
	@NotEmptyCollection
	private List<RoboconMetaDTO> meta;
	@Valid
	private List<RoboconPhotoDTO> photos;
	@Valid
	private List<RoboconTagDTO> tags;
	@Valid
	private List<RoboconArtistSocialDTO> socials;

	private Map<Long, Map<String, String>> events;
	
	private Integer albums;
	private Integer songs;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<RoboconMetaDTO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboconMetaDTO> meta) {
		this.meta = meta;
	}
	public List<RoboconPhotoDTO> getPhotos() {
		return photos;
	}
	public void setPhotos(List<RoboconPhotoDTO> photos) {
		this.photos = photos;
	}
	public List<RoboconTagDTO> getTags() {
		return tags;
	}
	public void setTags(List<RoboconTagDTO> tags) {
		this.tags = tags;
	}
	public List<RoboconArtistSocialDTO> getSocials() {
		return socials;
	}
	public void setSocials(List<RoboconArtistSocialDTO> socials) {
		this.socials = socials;
	}
	public Map<Long, Map<String, String>> getEvents() {
		return events;
	}
	public void setEvents(Map<Long, Map<String, String>> events) {
		this.events = events;
	}
	public Integer getAlbums() {
		return albums;
	}
	public void setAlbums(Integer albums) {
		this.albums = albums;
	}
	public Integer getSongs() {
		return songs;
	}
	public void setSongs(Integer songs) {
		this.songs = songs;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
