package com.masrawy.robocon.dto;

import com.cdm.core.dto.model.AbstractDTO;

public class RoboconSongTrendDTO extends AbstractDTO<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private RoboconSongDTO song;
	private Integer count;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public RoboconSongDTO getSong() {
		return song;
	}
	public void setSong(RoboconSongDTO song) {
		this.song = song;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	@Override
	public Long pk() {
		return getId();
	}
	
}
