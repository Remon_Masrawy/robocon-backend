package com.masrawy.robocon.dto;

import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.constraints.NotEmptyString;
import com.masrawy.robocon.constraints.NotNull;
import com.masrawy.robocon.constraints.ValidID;

public class RoboconOperatorDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotNull
	@NotEmptyString
	private String mcc;
	
	@NotNull
	@NotEmptyString
	private String mnc;
	
	@NotNull
	@NotEmptyString
	private String name;
	
	private String description;
	
	private Boolean allowed;
	
	@NotNull
	@ValidID
	private CountryDTO country;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMcc() {
		return mcc;
	}

	public void setMcc(String mcc) {
		this.mcc = mcc;
	}

	public String getMnc() {
		return mnc;
	}

	public void setMnc(String mnc) {
		this.mnc = mnc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getAllowed() {
		return allowed;
	}
	public void setAllowed(Boolean allowed) {
		this.allowed = allowed;
	}
	public CountryDTO getCountry() {
		return country;
	}
	public void setCountry(CountryDTO country) {
		this.country = country;
	}

	@Override
	public Long pk() {
		return getId();
	}

}
