package com.masrawy.robocon.dto;

import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.constraints.NotEmptyString;
import com.masrawy.robocon.constraints.NotNull;

public class RoboconStorefrontDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotNull
	@NotEmptyString
	private String keyword;
	
	@NotNull
	@NotEmptyString
	private String name;
	
	private Boolean active;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
