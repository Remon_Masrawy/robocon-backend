package com.masrawy.robocon.dto;

import java.util.Date;

public class RoboconUserSongHistoryDTO extends AbstractRoboconHistoryDTO{

	private static final long serialVersionUID = 1L;

	private RoboconSongDTO song;
	private Long duration;
	private Date latestHit;
	private Integer hitCount;
	
	public RoboconSongDTO getSong() {
		return song;
	}
	public void setSong(RoboconSongDTO song) {
		this.song = song;
	}
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	public Date getLatestHit() {
		return latestHit;
	}
	public void setLatestHit(Date latestHit) {
		this.latestHit = latestHit;
	}
	public Integer getHitCount() {
		return hitCount;
	}
	public void setHitCount(Integer hitCount) {
		this.hitCount = hitCount;
	}
}
