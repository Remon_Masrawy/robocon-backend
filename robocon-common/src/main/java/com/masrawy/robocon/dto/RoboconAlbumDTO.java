package com.masrawy.robocon.dto;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.constraints.NotEmptyCollection;
import com.masrawy.robocon.constraints.NotNull;
import com.masrawy.robocon.constraints.ValidID;

public class RoboconAlbumDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@Valid
	@NotNull
	@NotEmptyCollection
	private List<RoboconMetaDTO> meta;
	private Map<Long, String> artists;
	private Map<Long, String> songs;
	@Valid
	private List<RoboconPhotoDTO> photos;
	@Valid
	private List<RoboconTagDTO> tags;
	@NotNull
	@ValidID
	private RoboconProviderDTO provider;
	private List<RoboconPlaylistDTO> playlists;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<RoboconMetaDTO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboconMetaDTO> meta) {
		this.meta = meta;
	}
	public Map<Long, String> getArtists() {
		return artists;
	}
	public void setArtists(Map<Long, String> artists) {
		this.artists = artists;
	}
	public Map<Long, String> getSongs() {
		return songs;
	}
	public void setSongs(Map<Long, String> songs) {
		this.songs = songs;
	}
	public List<RoboconPhotoDTO> getPhotos() {
		return photos;
	}
	public void setPhotos(List<RoboconPhotoDTO> photos) {
		this.photos = photos;
	}
	public List<RoboconTagDTO> getTags() {
		return tags;
	}
	public void setTags(List<RoboconTagDTO> tags) {
		this.tags = tags;
	}
	public RoboconProviderDTO getProvider() {
		return provider;
	}
	public void setProvider(RoboconProviderDTO provider) {
		this.provider = provider;
	}
	public List<RoboconPlaylistDTO> getPlaylists() {
		return playlists;
	}
	public void setPlaylists(List<RoboconPlaylistDTO> playlists) {
		this.playlists = playlists;
	}
	@Override
	public Long pk() {
		return getId();
	}

	
}
