package com.masrawy.robocon.dto;

import java.util.List;

public class RoboconAlbumFrontendDTO extends RoboconFrontendDTO{

	private static final long serialVersionUID = 1L;

	private List<RoboconAlbumDTO> albums;

	public List<RoboconAlbumDTO> getAlbums() {
		return albums;
	}

	public void setAlbums(List<RoboconAlbumDTO> albums) {
		this.albums = albums;
	}
}
