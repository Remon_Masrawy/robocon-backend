package com.masrawy.robocon.dto;

import javax.validation.constraints.NotNull;

import com.cdm.core.validation.validator.NotEmptyString;

public class RoboconArtistEventMetaDTO extends RoboconMetaDTO{

	private static final long serialVersionUID = 1L;
	
	@NotNull
	@NotEmptyString
	private String location;
	@NotNull
	@NotEmptyString
	private String city;
	@NotNull
	@NotEmptyString
	private String country;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
}
