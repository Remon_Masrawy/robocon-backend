package com.masrawy.robocon.dto;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.cdm.core.datatypes.DateOnly;
import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.constraints.NotNull;
import com.masrawy.robocon.model.RoboconSong;

public class RoboconSongDTO extends AbstractDTO<Long> implements RoboconSong{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String isrc;
	@NotNull
	private DateOnly createdDate;
	@NotNull
	private Long year;
	private String videoLink;
	@Valid
	private List<RoboconMetaDTO> meta;
	@NotNull
	private Map<Long, String> artists;
	private Map<Long, String> mainArtists;
	@NotNull
	private Map<Long, String> album;
	@NotNull
	private Map<Long, String> genres;
	@NotNull
	private Map<Long, String> provider;
	private List<RoboconSongContentDTO> content;
	private List<RoboconSongKaraokeDTO> karaoke;
	private List<RoboconSongLyricsDTO> lyrics;
	private List<RoboconPhotoDTO> photos;
	private List<RoboconTagDTO> tags;
//	private List<RoboconPlaylistDTO> playlists;
//	private List<RoboconUserSongHistoryDTO> history;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIsrc() {
		return isrc;
	}
	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}
	public DateOnly getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(DateOnly createdDate) {
		this.createdDate = createdDate;
	}
	public Long getYear() {
		return year;
	}
	public void setYear(Long year) {
		this.year = year;
	}
	public String getVideoLink() {
		return videoLink;
	}
	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}
	public Map<Long, String> getArtists() {
		return artists;
	}
	public void setArtists(Map<Long, String> artists) {
		this.artists = artists;
	}
	public Map<Long, String> getMainArtists() {
		return mainArtists;
	}
	public void setMainArtists(Map<Long, String> mainArtists) {
		this.mainArtists = mainArtists;
	}
	public Map<Long, String> getAlbum() {
		return album;
	}
	public void setAlbum(Map<Long, String> album) {
		this.album = album;
	}
	public List<RoboconMetaDTO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboconMetaDTO> meta) {
		this.meta = meta;
	}
	public List<RoboconSongContentDTO> getContent() {
		return content;
	}
	public void setContent(List<RoboconSongContentDTO> content) {
		this.content = content;
	}
	public List<RoboconSongKaraokeDTO> getKaraoke() {
		return karaoke;
	}
	public void setKaraoke(List<RoboconSongKaraokeDTO> karaoke) {
		this.karaoke = karaoke;
	}
	public List<RoboconSongLyricsDTO> getLyrics() {
		return lyrics;
	}
	public void setLyrics(List<RoboconSongLyricsDTO> lyrics) {
		this.lyrics = lyrics;
	}
	public List<RoboconPhotoDTO> getPhotos() {
		return photos;
	}
	public void setPhotos(List<RoboconPhotoDTO> photos) {
		this.photos = photos;
	}
	public List<RoboconTagDTO> getTags() {
		return tags;
	}
	public void setTags(List<RoboconTagDTO> tags) {
		this.tags = tags;
	}
	public Map<Long, String> getGenres() {
		return genres;
	}
	public void setGenres(Map<Long, String> genres) {
		this.genres = genres;
	}
	public Map<Long, String> getProvider() {
		return provider;
	}
	public void setProvider(Map<Long, String> provider) {
		this.provider = provider;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
