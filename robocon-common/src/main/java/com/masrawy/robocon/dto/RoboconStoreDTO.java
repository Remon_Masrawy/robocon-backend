package com.masrawy.robocon.dto;

import java.util.List;

import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.constraints.NotEmptyCollection;
import com.masrawy.robocon.constraints.NotNull;

public class RoboconStoreDTO extends AbstractDTO<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<RoboconSongDTO> songs;
	
	private List<RoboconAlbumDTO> albums;
	
	@NotNull
	@NotEmptyCollection
	private List<RoboconCategoryDTO> categories;
	
	public List<RoboconSongDTO> getSongs() {
		return songs;
	}
	public void setSongs(List<RoboconSongDTO> songs) {
		this.songs = songs;
	}
	public List<RoboconAlbumDTO> getAlbums() {
		return albums;
	}
	public void setAlbums(List<RoboconAlbumDTO> albums) {
		this.albums = albums;
	}
	public List<RoboconCategoryDTO> getCategories() {
		return categories;
	}
	public void setCategories(List<RoboconCategoryDTO> categories) {
		this.categories = categories;
	}

	@Override
	public Long pk() {
		return serialVersionUID;
	}

}
