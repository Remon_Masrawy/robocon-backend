package com.masrawy.robocon.dto;

import java.util.List;

import javax.validation.Valid;

import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.constraints.NotBlankCollection;
import com.masrawy.robocon.constraints.NotEmptyCollection;
import com.masrawy.robocon.constraints.NotNull;

public class RoboconGenreDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@Valid
	@NotNull
	@NotEmptyCollection
	@NotBlankCollection
	private List<RoboconMetaDTO> meta;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<RoboconMetaDTO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboconMetaDTO> meta) {
		this.meta = meta;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
