package com.masrawy.robocon.dto;

/**
 * 
 * @author Dalia M.Kamal
 * @since January 10, 2014
 * 
 */
public class RoleConstants {

	public static final String ROLE_SYSTEM 			= "ROLE_SYSTEM";
	public static final String ROLE_SUPER 			= "ROLE_SUPER";
	public static final String ROLE_ADMIN 			= "ROLE_CMS_ADMIN";
	public static final String CONTENT_ADMIN 		= "ROLE_CONTENT_ADMIN";
	public static final String REPORTS_ADMIN 		= "ROLE_REPORTS_ADMIN";
	public static final String ROLE_USER 			= "ROLE_USER";

	public static final String API_NOTE_ROLE_SYSTEM = "This can only be done by the System Admin user.";
	public static final String API_NOTE_ROLE_SUPER = "This can only be done by the Super Admin user.";
	public static final String API_NOTE_ROLE_ADMIN = "This can only be done by the Admin user.";
	public static final String API_NOTE_CONTENT_ADMIN = "This can only be done by the Content Admin user.";
	public static final String API_NOTE_ROLE_ADMIN_SERVICE_ADMIN = "This can only be done by the Admin user or service admin user.";
	public static final String API_NOTE_ROLE_USER = "This can only be done by the Robocon user.";
	public static final String API_NOTE_CONTENT_ADMIN_ROLE_USER = "This can only be done by the Content Admin user or Robocon user.";
	public static final String API_NOTE_ROLE_ADMIN_ROLE_USER = "This can only be done by the Admin user or Robocon user.";
	public static final String API_NOTE_EXTERNAL_ROLE_USER = "This can only be done by the Robocon external user.";
	public static final String API_NOTE_ROLE_AUTHENTICATED = "This can only be done by Authenticated and Verified user.";
	public static final String API_NOTE_ROLE_ANONYMOUS = "This can be done by anonymous users";
	public static final String API_NOTE_ROLE_OWNER = "This can only be done by Owner";

}
