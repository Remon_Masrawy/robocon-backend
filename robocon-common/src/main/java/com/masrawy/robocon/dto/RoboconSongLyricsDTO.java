package com.masrawy.robocon.dto;

public class RoboconSongLyricsDTO extends AbstractRoboconFileDTO<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long pk() {
		return getId();
	}

}
