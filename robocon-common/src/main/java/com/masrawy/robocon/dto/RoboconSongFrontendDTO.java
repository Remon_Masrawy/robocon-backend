package com.masrawy.robocon.dto;

import java.util.List;

public class RoboconSongFrontendDTO extends RoboconFrontendDTO{

	private static final long serialVersionUID = 1L;

	private List<RoboconSongDTO> songs;

	public List<RoboconSongDTO> getSongs() {
		return songs;
	}

	public void setSongs(List<RoboconSongDTO> songs) {
		this.songs = songs;
	}
}
