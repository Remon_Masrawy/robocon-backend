package com.masrawy.robocon.dto;

import com.cdm.core.dto.model.AbstractDTO;

public class RoboconPagingDTO extends AbstractDTO<String>{

	private static final long serialVersionUID = 1L;

	private Integer pageNum;
	private Integer pageLimit;
	
	public Integer getPageNum() {
		return pageNum;
	}
	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}
	public Integer getPageLimit() {
		return pageLimit;
	}
	public void setPageLimit(Integer pageLimit) {
		this.pageLimit = pageLimit;
	}
	
	@Override
	public String pk() {
		return null;
	}

}
