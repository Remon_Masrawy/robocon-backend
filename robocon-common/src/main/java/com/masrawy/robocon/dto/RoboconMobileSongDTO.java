package com.masrawy.robocon.dto;

import java.util.List;

import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.model.RoboconSong;

public class RoboconMobileSongDTO extends AbstractDTO<Long> implements RoboconSong{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;
	private String artist;
	private List<RoboconMobileSongContentDTO> content;
	private List<RoboconMobilePhotoDTO> photos;
	private List<RoboconMobileSongLyricsDTO> lyrics;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public List<RoboconMobileSongContentDTO> getContent() {
		return content;
	}
	public void setContent(List<RoboconMobileSongContentDTO> content) {
		this.content = content;
	}
	public List<RoboconMobilePhotoDTO> getPhotos() {
		return photos;
	}
	public void setPhotos(List<RoboconMobilePhotoDTO> photos) {
		this.photos = photos;
	}
	public List<RoboconMobileSongLyricsDTO> getLyrics() {
		return lyrics;
	}
	public void setLyrics(List<RoboconMobileSongLyricsDTO> lyrics) {
		this.lyrics = lyrics;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
