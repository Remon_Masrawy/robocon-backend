package com.masrawy.robocon.dto;

import java.util.List;

import com.cdm.core.dto.model.AbstractDTO;

public class RoboconMicroAlbumDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private List<RoboconMetaDTO> meta;
	private List<RoboconMicroSongDTO> songs;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<RoboconMetaDTO> getMeta() {
		return meta;
	}

	public void setMeta(List<RoboconMetaDTO> meta) {
		this.meta = meta;
	}

	public List<RoboconMicroSongDTO> getSongs() {
		return songs;
	}

	public void setSongs(List<RoboconMicroSongDTO> songs) {
		this.songs = songs;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
