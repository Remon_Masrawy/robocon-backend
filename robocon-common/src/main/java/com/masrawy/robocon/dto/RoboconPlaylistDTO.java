package com.masrawy.robocon.dto;

import java.util.List;

import com.cdm.core.dto.model.AbstractDTO;

public class RoboconPlaylistDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String keyword;
	
	private List<RoboconMetaDTO> meta;
	private List<RoboconSongDTO> songs;
	private List<RoboconAlbumDTO> albums;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public List<RoboconMetaDTO> getMeta() {
		return meta;
	}
	public void setMeta(List<RoboconMetaDTO> meta) {
		this.meta = meta;
	}
	public List<RoboconSongDTO> getSongs() {
		return songs;
	}
	public void setSongs(List<RoboconSongDTO> songs) {
		this.songs = songs;
	}
	public List<RoboconAlbumDTO> getAlbums() {
		return albums;
	}
	public void setAlbums(List<RoboconAlbumDTO> albums) {
		this.albums = albums;
	}
	@Override
	public Long pk() {
		return getId();
	}

}
