package com.masrawy.robocon.dto;

import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.constraints.NotEmptyString;
import com.masrawy.robocon.constraints.NotNull;

public class CountryDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotNull
	@NotEmptyString
	private String iso;
	
	@NotNull
	@NotEmptyString
	private String code;
	
	@NotNull
	@NotEmptyString
	private String name;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIso() {
		return iso;
	}

	public void setIso(String iso) {
		this.iso = iso;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public Long pk() {
		return getId();
	}

}
