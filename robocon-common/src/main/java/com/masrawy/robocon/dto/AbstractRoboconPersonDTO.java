package com.masrawy.robocon.dto;

import com.cdm.core.datatypes.DateOnly;
import com.cdm.core.dto.model.AbstractDTO;
import com.masrawy.robocon.constraints.NotNull;

public abstract class AbstractRoboconPersonDTO extends AbstractDTO<Long>{

	private static final long serialVersionUID = 1L;
	
	@NotNull
	private DateOnly birthDate;
	private DateOnly deathDate;
	private String geneder;
	private Double rate;
	
	public DateOnly getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(DateOnly birthDate) {
		this.birthDate = birthDate;
	}
	public DateOnly getDeathDate() {
		return deathDate;
	}
	public void setDeathDate(DateOnly deathDate) {
		this.deathDate = deathDate;
	}
	public String getGeneder() {
		return geneder;
	}
	public void setGeneder(String geneder) {
		this.geneder = geneder;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
}
