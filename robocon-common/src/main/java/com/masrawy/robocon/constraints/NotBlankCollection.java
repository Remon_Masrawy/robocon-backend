package com.masrawy.robocon.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Payload;

import com.masrawy.robocon.validator.NotBlankCollectionValidator;

import net.sf.oval.configuration.annotation.Constraint;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD })
@Documented
@Constraint(checkWith = NotBlankCollectionValidator.class)
public @interface NotBlankCollection {

	String[] profiles() default {};
	String message() default "com.masrawy.robocon.constraints.NotBlankCollection";
	
	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default {};
}
