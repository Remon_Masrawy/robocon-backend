package com.masrawy.robocon.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.masrawy.robocon.validator.NotEmptyStringValidator;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD })
@Documented
@Constraint(validatedBy = NotEmptyStringValidator.class)
public @interface NotEmptyString {

	String[] profiles() default {};
	String message() default "com.masrawy.robocon.constraints.NotEmptyString";
	
	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default {};
}
