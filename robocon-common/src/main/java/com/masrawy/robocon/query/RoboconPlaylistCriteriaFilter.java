package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.HQLCriteriaFilter;

public class RoboconPlaylistCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconPlaylistEB roboconPlaylistEB";
	
	public RoboconPlaylistCriteriaFilter(){
		super("RoboconPlaylistEB", "roboconPlaylistEB", false, FROM_CLAUSE);
	}
}
