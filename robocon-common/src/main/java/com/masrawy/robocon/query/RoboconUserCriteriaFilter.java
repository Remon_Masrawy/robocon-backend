package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconUserCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconUserEB roboconuser";
	
	private static final String USER_ID = "roboconuser.id";
	
	public RoboconUserCriteriaFilter(){
		super("RoboconUserEB", "roboconuser", true, FROM_CLAUSE);
	}
	
	public void setUserId(Long id){
		addCriteria(USER_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void exclude(Long id){
		addCriteria(USER_ID, FieldOperatorEnum.NOT_IN, id);
	}
	
	public void sortById(boolean asc){
		if(asc){
			addOrderBy(Order.asc(USER_ID));
		}else{
			addOrderBy(Order.desc(USER_ID));
		}
	}
}
