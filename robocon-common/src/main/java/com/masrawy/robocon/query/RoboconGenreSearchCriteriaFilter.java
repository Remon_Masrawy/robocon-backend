package com.masrawy.robocon.query;

public class RoboconGenreSearchCriteriaFilter extends RoboconGenreCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconGenreEB roboconGenreEB "
												+ "left join fetch roboconGenreEB.roboconGenreMetaEB roboconGenreMeta "
												+ "left join roboconGenreEB.roboconSongsEB song "
												+ "left join song.roboconAlbumEB album";

	public RoboconGenreSearchCriteriaFilter(){
		super(FROM_CLAUSE);
	}
}
