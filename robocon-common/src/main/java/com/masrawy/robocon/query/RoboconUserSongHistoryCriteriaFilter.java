package com.masrawy.robocon.query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cdm.core.persistent.query.CriteriaComponent;
import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.LogicalOperatorEnum;
import com.cdm.core.persistent.query.Order;
import com.masrawy.robocon.model.RoboconUserActionEnum;

public class RoboconUserSongHistoryCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconUserSongHistoryEB history " + 
												"inner join history.roboconSongEB song " +
												"left join song.roboconFrontendsEB frontend " +
												"left join frontend.roboconCategoriesEB category " +
												"left join category.roboconStorefrontsEB storefront " +
												"left join category.roboconOperatorsEB operator " +
												"left join category.countriesEB country";
	
	private static final String SONG = "song";
	private static final String SONG_COUNT = "COUNT(song)";
	private static final String HISTORY_ID = "history.id";
	private static final String USER_ID = "history.userId";
	private static final String USER_ACTION = "history.roboconUserAction";
	private static final String CREATED_DATE = "history.audit.createdOn";
	private static final String HIT_DATE = "history.latestHit";
	private static final String HIT_COUNT = "history.hitCount";
	private static final String SONG_ID = "song.id";
	private static final String OPERATOR = "operator";
	private static final String OPERATOR_MCC = "operator.mcc";
	private static final String OPERATOR_MNC = "operator.mnc";
	private static final String STOREFRONT = "storefront";
	private static final String STOREFRONT_NAME = "storefront.name";
	private static final String COUNTRY			= "country";
	private static final String COUNTRY_CODE = "country.iso";
	
	public RoboconUserSongHistoryCriteriaFilter(){
		super("RoboconUserSongHistoryEB", "history", true, FROM_CLAUSE);
	}
	
	public void setUserId(Long id){
		addCriteria(USER_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setSongId(Long songId){
		addCriteria(SONG_ID, FieldOperatorEnum.EQUAL, songId);
	}
	
	public void setBetweenDate(Date beginDate, Date endDate){
		addBetweenCriteria(CREATED_DATE, beginDate, endDate);
	}
	
	public void setOperatorMCC(String mcc){
		addCriteria(OPERATOR_MCC, FieldOperatorEnum.EQUAL, mcc);
	}
	
	public void setOperatorMNC(String mnc){
		addCriteria(OPERATOR_MNC, FieldOperatorEnum.EQUAL, mnc);
	}
	
	public void setOperator(String mcc, String mnc){
		CriteriaComponent criteria = new CriteriaComponent(LogicalOperatorEnum.AND);
		criteria.addCriteria(OPERATOR_MCC, FieldOperatorEnum.EQUAL, mcc);
		criteria.addCriteria(OPERATOR_MNC, FieldOperatorEnum.EQUAL, mnc);
		CriteriaComponent criteria2 = new CriteriaComponent(LogicalOperatorEnum.OR);
		criteria2.addCriteria(criteria);
		criteria2.addCriteria(OPERATOR, FieldOperatorEnum.IS_NULL, true);
		addCriteria(criteria2);
	}
	
	public void setStorefrontName(String storefrontName){
		CriteriaComponent criteria = new CriteriaComponent(LogicalOperatorEnum.OR);
		criteria.addCriteria(STOREFRONT_NAME, FieldOperatorEnum.EQUAL, storefrontName);
		criteria.addCriteria(STOREFRONT, FieldOperatorEnum.IS_NULL, true);
		addCriteria(criteria);
	}
	
	public void setCountryCode(String countryCode){
		CriteriaComponent criteria = new CriteriaComponent(LogicalOperatorEnum.OR);
		criteria.addCriteria(COUNTRY, FieldOperatorEnum.IS_NULL, true);
		criteria.addCriteria(COUNTRY_CODE, FieldOperatorEnum.EQUAL, countryCode);
		addCriteria(criteria);
	}
	
	public void setEmptyCountry(boolean empty){
		addCriteria(COUNTRY, FieldOperatorEnum.IS_NULL, empty);
	}
	
	public void setUserAction(RoboconUserActionEnum roboconUserAction){
		addCriteria(USER_ACTION, FieldOperatorEnum.EQUAL, roboconUserAction);
	}
	
	public void isEmptyOperator(boolean empty){
		addCriteria(OPERATOR, FieldOperatorEnum.IS_NULL, empty);
	}
	
	public void isEmptyStorefront(boolean empty){
		addCriteria(STOREFRONT, FieldOperatorEnum.IS_NULL, empty);
	}
	
	public void setTrendFields(){
		addSelectField(SONG, "roboconSongEB");
		addSelectField(SONG_COUNT, "count");
		List<String> groupBy = new ArrayList<String>();
		groupBy.add(SONG);
		addOrderBy(Order.desc(SONG_COUNT));
		setGroupByFields(groupBy);
	}
	
	public void orderByHitCount(Boolean asc){
		if(asc == null || asc){
			addOrderBy(Order.asc(HIT_COUNT));
		}else{
			addOrderBy(Order.desc(HIT_COUNT));
		}
	}
	
	public void orderByHitDate(Boolean asc){
		if(asc == null || asc){
			addOrderBy(Order.asc(HIT_DATE));
		}else{
			addOrderBy(Order.desc(HIT_DATE));
		}
	}
	
	public void orderByHistoryId(Boolean asc){
		if(asc == null || asc){
			addOrderBy(Order.asc(HISTORY_ID));
		}else{
			addOrderBy(Order.desc(HISTORY_ID));
		}
	}
}
