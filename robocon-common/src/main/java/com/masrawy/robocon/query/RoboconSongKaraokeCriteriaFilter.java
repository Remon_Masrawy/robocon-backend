package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconSongKaraokeCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconSongKaraokeEB roboconSongKaraokeEB inner join roboconSongKaraokeEB.roboconSongEB song left join song.roboconCategoriesEB category";
	
	private static final String KARAOKE_ID = "roboconSongKaraokeEB.id";
	private static final String SONG_ID = "song.id";
	private static final String CATEGORY_ID = "category.id";
	
	public RoboconSongKaraokeCriteriaFilter(){
		super("RoboconSongKaraokeEB", "roboconSongKaraokeEB", false, FROM_CLAUSE);
	}

	public void setSongId(Long songId){
		addCriteria(SONG_ID, FieldOperatorEnum.EQUAL, songId);
	}
	
	public void setKaraokeId(Long karaokeId){
		addCriteria(KARAOKE_ID, FieldOperatorEnum.EQUAL, karaokeId);
	}
	
	public void setCategoryId(Long categoryId){
		addCriteria(CATEGORY_ID, FieldOperatorEnum.EQUAL, categoryId);
	}
	
	public void orderByKaraokeId(Boolean asc){
		if(asc == null || asc == true){
			addOrderBy(Order.asc(KARAOKE_ID));
		}else{
			addOrderBy(Order.desc(KARAOKE_ID));
		}
	}
}
