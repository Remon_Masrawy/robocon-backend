package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconProviderCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconProviderEB roboconProviderEB inner join roboconProviderEB.roboconProviderMetaEB roboconProviderMeta";
	private static final String PROVIDER_ID = "roboconProviderEB.id";
	private static final String PROVIDER_NAME = "roboconProviderMeta.name";
	
	public RoboconProviderCriteriaFilter(){
		super("RoboconProviderEB", "roboconProviderEB", true, FROM_CLAUSE);
	}
	
	protected RoboconProviderCriteriaFilter(String fromClause){
		super("RoboconProviderEB", "roboconProviderEB", true, fromClause);
	}
	
	public void setProviderId(Long id){
		addCriteria(PROVIDER_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setProviderId(Long fromId, Long toId){
		addCriteria(PROVIDER_ID, FieldOperatorEnum.GREATER_OR_EQUAL, fromId);
		addCriteria(PROVIDER_ID, FieldOperatorEnum.LESS_OR_EQUAL, toId);
	}
	
	public void setProviderName(String name, Boolean like){
		if(like == null || like){
			String[] keywords = name.split(" ");
			for(String keyword : keywords){
				addCriteria(PROVIDER_NAME, FieldOperatorEnum.CONTAINS, keyword);
			}
		}else{
			addCriteria(PROVIDER_NAME, FieldOperatorEnum.LIKE, name);
		}
	}
	
	public void orderByProviderId(Boolean ASC){
		if(ASC == null || ASC){
			addOrderBy(Order.asc(PROVIDER_ID));
		}else{
			addOrderBy(Order.desc(PROVIDER_ID));
		}
	}
}
