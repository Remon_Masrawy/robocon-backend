package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconUserSingCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconUserSingEB userSing inner join userSing.roboconSongEB song";
	
	private static final String USER_SING_ID = "userSing.id";
	private static final String USER_ID = "userSing.userId";
	private static final String SONG_ID = "song.id";
	private static final String VERIFIED = "userSing.verified";
	
	public RoboconUserSingCriteriaFilter(){
		super("RoboconUserSingEB", "userSing", true, FROM_CLAUSE);
	}

	public void setUserSingId(Long id){
		addCriteria(USER_SING_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setSongId(Long id){
		addCriteria(SONG_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setUserId(Long userId){
		addCriteria(USER_ID, FieldOperatorEnum.EQUAL, userId);
	}
	
	public void isVerified(boolean verified){
		addCriteria(VERIFIED, FieldOperatorEnum.EQUAL, verified);
	}
	
	public void orderByUserSingId(Boolean asc){
		if(asc == null || asc){
			addOrderBy(Order.asc(USER_SING_ID));
		}else{
			addOrderBy(Order.desc(USER_SING_ID));
		}
		
	}
}
