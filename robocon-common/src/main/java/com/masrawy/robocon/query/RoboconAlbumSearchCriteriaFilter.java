package com.masrawy.robocon.query;

public class RoboconAlbumSearchCriteriaFilter extends RoboconAlbumCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconAlbumEB roboconAlbumEB "
												+ "inner join fetch roboconAlbumEB.roboconAlbumMetaEB roboconAlbumMeta "
												+ "left join roboconAlbumEB.roboconSongsEB song "
												+ "left join roboconAlbumEB.roboconProviderEB provider "
												+ "left join song.roboconArtistsEB artist";

	
	
	public  RoboconAlbumSearchCriteriaFilter(){
		super(FROM_CLAUSE);
	}
	
}
