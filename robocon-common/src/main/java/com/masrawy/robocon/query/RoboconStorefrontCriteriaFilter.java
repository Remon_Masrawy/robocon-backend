package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconStorefrontCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconStorefrontEB roboconStorefrontEB";
	
	private static final String STOREFRONT_ID = "roboconStorefrontEB.id";
	private static final String STOREFRONT_NAME = "roboconStorefrontEB.name";
	private static final String STOREFRONT_ACTIVATION = "roboconStorefrontEB.active";
	
	public RoboconStorefrontCriteriaFilter(){
		super("RoboconStorefrontEB", "roboconStorefrontEB", false, FROM_CLAUSE);
	}
	
	public void setStorefrontId(Long id){
		addCriteria(STOREFRONT_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setStorefrontActivation(boolean active){
		addCriteria(STOREFRONT_ACTIVATION, FieldOperatorEnum.EQUAL, active);
	}
	
	public void setStorefrontName(String name, boolean like){
		if(like){
			String[] keywords = name.split(" ");
			for(String keyword : keywords){
				addCriteria(STOREFRONT_NAME, FieldOperatorEnum.CONTAINS, keyword);
			}
		}else{
			addCriteria(STOREFRONT_NAME, FieldOperatorEnum.EQUAL, name);
		}
	}
	
	public void orderByStorefrontId(Boolean ASC){
		if(ASC == null || ASC){
			addOrderBy(Order.asc(STOREFRONT_ID));
		}else{
			addOrderBy(Order.desc(STOREFRONT_ID));
		}
	}
}
