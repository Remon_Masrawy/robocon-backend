package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconSongLyricsCriteriaFilter extends HQLCriteriaFilter{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String FROM_CLAUSE = "from RoboconSongLyricsEB roboconSongLyricsEB";
	
	private static final String LYRICS_ID = "roboconSongLyricsEB.id";
	
	public RoboconSongLyricsCriteriaFilter(){
		super("RoboconSongLyricsEB", "roboconSongLyricsEB", false, FROM_CLAUSE);
	}
	
	public void setSongLyricsId(Long id){
		addCriteria(LYRICS_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void orderByLyricsId(Boolean asc){
		if(asc == null || asc){
			addOrderBy(Order.asc(LYRICS_ID));
		}else{
			addOrderBy(Order.desc(LYRICS_ID));
		}
	}
}
