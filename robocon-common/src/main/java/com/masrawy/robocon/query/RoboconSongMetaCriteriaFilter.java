package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;

public class RoboconSongMetaCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;

	private static final String FROM_CLAUSE = "from RoboconSongMetaEB roboconSongMetaEB";
	
	private static final String SONG_META_ID = "roboconSongMetaEB.id";
	private static final String SONG_ID = "roboconSongMetaEB.roboconSongEB.id";
	
	public RoboconSongMetaCriteriaFilter(){
		super("RoboconSongMetaEB", "roboconSongMetaEB", false, FROM_CLAUSE);
	}
	
	public void setSongMetaId(Long id){
		addCriteria(SONG_META_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setSongId(Long id){
		addCriteria(SONG_ID, FieldOperatorEnum.EQUAL, id);
	}
}
