package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconArtistCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconArtistEB roboconArtistEB inner join roboconArtistEB.roboconArtistMetaEB roboconArtistMeta";
	
	private static final String ARTIST_ID = "roboconArtistEB.id";
	private static final String ARTIST_NAME = "roboconArtistMeta.name";
	
	public RoboconArtistCriteriaFilter(){
		super("RoboconArtistEB", "roboconArtistEB", true, FROM_CLAUSE);
	}
	
	public RoboconArtistCriteriaFilter(String fromClause){
		super("RoboconArtistEB", "roboconArtistEB", true, fromClause);
	}
	
	public void setArtistId(Long id){
		addCriteria(ARTIST_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setArtistId(Long fromId, Long toId){
		addCriteria(ARTIST_ID, FieldOperatorEnum.GREATER_OR_EQUAL, fromId);
		addCriteria(ARTIST_ID, FieldOperatorEnum.LESS_OR_EQUAL, toId);
	}
	
	public void setArtistName(String name, boolean like){
		if(like){
			String[] keywords = name.split(" ");
			for(String keyword : keywords){
				addCriteria(ARTIST_NAME, FieldOperatorEnum.CONTAINS, keyword);
			}
		}else{
			addCriteria(ARTIST_NAME, FieldOperatorEnum.EQUAL, name);
		}
	}
	
	public void orderByArtistId(Boolean ASC){
		if(ASC || ASC == null){
			addOrderBy(Order.asc(ARTIST_ID));
		}else{
			addOrderBy(Order.desc(ARTIST_ID));
		}
	}
}
