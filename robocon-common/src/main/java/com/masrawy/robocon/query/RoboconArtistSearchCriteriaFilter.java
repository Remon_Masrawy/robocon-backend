package com.masrawy.robocon.query;

public class RoboconArtistSearchCriteriaFilter extends RoboconArtistCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconArtistEB roboconArtistEB join fetch roboconArtistEB.roboconArtistMetaEB roboconArtistMeta";
	
	public RoboconArtistSearchCriteriaFilter(){
		super(FROM_CLAUSE);
	}

}
