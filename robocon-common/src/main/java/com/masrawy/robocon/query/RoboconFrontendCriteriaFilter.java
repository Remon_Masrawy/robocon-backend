package com.masrawy.robocon.query;

import org.apache.commons.lang.StringUtils;

import com.cdm.core.persistent.query.CriteriaComponent;
import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.LogicalOperatorEnum;
import com.cdm.core.persistent.query.Order;

public class RoboconFrontendCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconFrontendEB roboconFrontendEB "
												+ "left join roboconFrontendEB.roboconFrontendMetaEB meta "
												+ "left join roboconFrontendEB.roboconCategoriesEB category "
												+ "left join category.roboconStorefrontsEB storefront "
												+ "left join category.roboconOperatorsEB operator "
												+ "left join category.countriesEB country";
	
	private static final String FRONTEND_ID 	= "roboconFrontendEB.id";
	private static final String FRONTEND_TYPE	= "roboconFrontendEB.type";
	private static final String FRONTEND_NAME	= "meta.name";
	private static final String CATEGORY_ID 	= "category.id";
	private static final String OPERATOR_ID 	= "operator.id";
	private static final String OPERATOR_MCC 	= "operator.mcc";
	private static final String OPERATOR_MNC 	= "operator.mnc";
	private static final String OPERATOR 		= "operator";
	private static final String STOREFRONT_ID 	= "storefront.id";
	private static final String STOREFRONT_KEYWORD = "storefront.keyword";
	private static final String STOREFRONT 		= "storefront";
	private static final String COUNTRY			= "country";
	private static final String COUNTRY_CODE	= "country.iso";
	
	public RoboconFrontendCriteriaFilter(){
		super("RoboconFrontendEB", "roboconFrontendEB", true, FROM_CLAUSE);
	}
	
	public RoboconFrontendCriteriaFilter(String fromClause){
		super("RoboconFrontendEB", "roboconFrontendEB", true, fromClause);
	}
	
	public void setFrontendId(Long frontendID){
		addCriteria(FRONTEND_ID, FieldOperatorEnum.EQUAL, frontendID);
	}
	
	public void setFrontendId(Long fromID, Long toId){
		addCriteria(FRONTEND_ID, FieldOperatorEnum.GREATER_OR_EQUAL, fromID);
		addCriteria(FRONTEND_ID, FieldOperatorEnum.LESS_OR_EQUAL, toId);
	}
	
	public void setFrontendType(String type){
		addCriteria(FRONTEND_TYPE, FieldOperatorEnum.EQUAL, StringUtils.capitalize(type));
	}
	
	public void setCategoryId(Long categoryID){
		addCriteria(CATEGORY_ID, FieldOperatorEnum.EQUAL, categoryID);
	}
	
	public void setFrontendName(String name, boolean like){
		if(like){
			String[] keywords = name.split(" ");
			for(String keyword : keywords){
				addCriteria(FRONTEND_NAME, FieldOperatorEnum.CONTAINS, keyword);
			}
		}else{
			addCriteria(FRONTEND_NAME, FieldOperatorEnum.EQUAL, name);
		}
	}
	
	public void setOperatorId(Long operatorId){
		addCriteria(OPERATOR_ID, FieldOperatorEnum.EQUAL, operatorId);
	}
	
	public void setOperatorMCC(String MCC){
		addCriteria(OPERATOR_MCC, FieldOperatorEnum.EQUAL, MCC);
	}
	
	public void setOperatorMNC(String MNC){
		addCriteria(OPERATOR_MNC, FieldOperatorEnum.EQUAL, MNC);
	}
	
	public void setOperator(String mcc, String mnc){
		CriteriaComponent criteria = new CriteriaComponent(LogicalOperatorEnum.AND);
		criteria.addCriteria(OPERATOR_MCC, FieldOperatorEnum.EQUAL, mcc);
		criteria.addCriteria(OPERATOR_MNC, FieldOperatorEnum.EQUAL, mnc);
		CriteriaComponent criteria2 = new CriteriaComponent(LogicalOperatorEnum.OR);
		criteria2.addCriteria(criteria);
		criteria2.addCriteria(OPERATOR, FieldOperatorEnum.IS_NULL, true);
		addCriteria(criteria2);
	}
	
	public void setOperatorOnly(String mcc, String mnc){
		CriteriaComponent criteria = new CriteriaComponent(LogicalOperatorEnum.AND);
		criteria.addCriteria(OPERATOR_MCC, FieldOperatorEnum.EQUAL, mcc);
		criteria.addCriteria(OPERATOR_MNC, FieldOperatorEnum.EQUAL, mnc);
		addCriteria(criteria);
	}
	
	public void isEmptyOperator(Boolean empty){
		addCriteria(OPERATOR, FieldOperatorEnum.IS_NULL, empty == null ? true : empty);
	}
	
	public void setStorefrontId(Long storefrontId){
		addCriteria(STOREFRONT_ID, FieldOperatorEnum.EQUAL, storefrontId);
	}
	
	public void setStorefrontName(String storefrontName){
		CriteriaComponent criteria = new CriteriaComponent(LogicalOperatorEnum.OR);
		criteria.addCriteria(STOREFRONT_KEYWORD, FieldOperatorEnum.EQUAL, storefrontName);
		criteria.addCriteria(STOREFRONT, FieldOperatorEnum.IS_NULL, true);
		addCriteria(criteria);
	}
	
	public void setStorefrontNameOnly(String storefrontName){
		addCriteria(STOREFRONT_KEYWORD, FieldOperatorEnum.EQUAL, storefrontName);
	}
	
	public void isEmptyStorefront(Boolean empty){
		addCriteria(STOREFRONT, FieldOperatorEnum.IS_NULL, empty == null ? true : empty);
	}
	
	public void setCountryCode(String countryCode){
		CriteriaComponent criteria = new CriteriaComponent(LogicalOperatorEnum.OR);
		criteria.addCriteria(COUNTRY, FieldOperatorEnum.IS_NULL, true);
		criteria.addCriteria(COUNTRY_CODE, FieldOperatorEnum.EQUAL, countryCode);
		addCriteria(criteria);
	}
	
	public void setCountryCodeOnly(String countryCode){
		addCriteria(COUNTRY_CODE, FieldOperatorEnum.EQUAL, countryCode);
	}
	
	public void setEmptyCountry(boolean empty){
		addCriteria(COUNTRY, FieldOperatorEnum.IS_NULL, empty);
	}
	
	public void orderByFrontendId(Boolean ASC){
		if(ASC == null || ASC)
			addOrderBy(Order.asc(FRONTEND_ID));
		else
			addOrderBy(Order.desc(FRONTEND_ID));
	}
}
