package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;

public class RoboconSongContentCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconSongContentEB roboconSongContentEB";
	
	private static final String CONTENT_ID = "roboconSongContentEB.id";
	
	public RoboconSongContentCriteriaFilter(){
		super("RoboconSongContentEB", "roboconSongContentEB", false, FROM_CLAUSE);
	}
	
	public void setContentId(Long contentId){
		addCriteria(CONTENT_ID, FieldOperatorEnum.EQUAL, contentId);
	}
}
