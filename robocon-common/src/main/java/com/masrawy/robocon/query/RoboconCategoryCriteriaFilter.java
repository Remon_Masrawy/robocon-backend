package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconCategoryCriteriaFilter extends HQLCriteriaFilter{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconCategoryEB roboconCategoryEB left join roboconCategoryEB.roboconCategoryMetaEB meta";
	
	private static final String CATEGORY_ID = "roboconCategoryEB.id";
	private static final String CATEGORY_NAME = "meta.name";

	public RoboconCategoryCriteriaFilter(){
		super("RoboconCategoryEB", "roboconCategoryEB", false, FROM_CLAUSE);
	}
	
	public RoboconCategoryCriteriaFilter(String fromClause){
		super("RoboconCategoryEB", "roboconCategoryEB", false, fromClause);
	}
	
	public void setCategoryId(Long id){
		addCriteria(CATEGORY_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setCategoryName(String name, boolean like){
		if(like){
			String[] keywords = name.split(" ");
			for(String keyword : keywords){
				addCriteria(CATEGORY_NAME, FieldOperatorEnum.CONTAINS, keyword);
			}
		}else{
			addCriteria(CATEGORY_NAME, FieldOperatorEnum.EQUAL, name);
		}
	}
	
	public void orderByCategoryId(Boolean asc){
		if(asc == null || asc){
			addOrderBy(Order.asc(CATEGORY_ID));
		}else{
			addOrderBy(Order.desc(CATEGORY_ID));
		}
	}
}
