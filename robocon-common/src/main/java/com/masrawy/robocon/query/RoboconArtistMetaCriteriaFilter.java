package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;

public class RoboconArtistMetaCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconArtistMetaEB roboconArtistMetaEB";
	
	private static final String META_ID = "roboconArtistMetaEB.id";
	
	private static final String ARTIST_ID = "roboconArtistMetaEB.roboconArtistEB.id";
	
	public RoboconArtistMetaCriteriaFilter(){
		super("RoboconArtistMetaEB", "roboconArtistMetaEB", false, FROM_CLAUSE);
	}
	
	public void setMetaId(Long id){
		addCriteria(META_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setArtistId(Long id){
		addCriteria(ARTIST_ID, FieldOperatorEnum.EQUAL, id);
	}
}
