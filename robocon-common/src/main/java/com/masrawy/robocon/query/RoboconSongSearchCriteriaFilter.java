package com.masrawy.robocon.query;

public class RoboconSongSearchCriteriaFilter extends RoboconSongCriteriaFilter{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String FROM_CLAUSE = "from RoboconSongEB roboconSongEB "
												+ "LEFT JOIN FETCH roboconSongEB.roboconSongMetaEB meta "
												+ "LEFT JOIN roboconSongEB.roboconArtistsEB artist "
												+ "LEFT JOIN roboconSongEB.roboconAlbumEB album "
												+ "LEFT JOIN roboconSongEB.roboconProviderEB provider "
												+ "LEFT JOIN roboconSongEB.roboconGenresEB genre";

	public RoboconSongSearchCriteriaFilter(){
		super(FROM_CLAUSE);
	}
	
}
