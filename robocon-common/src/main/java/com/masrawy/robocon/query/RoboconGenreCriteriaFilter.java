package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconGenreCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconGenreEB roboconGenreEB "
												+ "left join roboconGenreEB.roboconGenreMetaEB roboconGenreMeta "
												+ "left join roboconGenreEB.roboconSongsEB song "
												+ "left join song.roboconAlbumEB album";
	private static final String GENRE_ID = "roboconGenreEB.id";
	private static final String GENRE_NAME = "roboconGenreMeta.name";
	private static final String SONG_ID = "song.id";
	private static final String ALBUM_ID = "album.id";
	
	public RoboconGenreCriteriaFilter(){
		super("RoboconGenreEB", "roboconGenreEB", true, FROM_CLAUSE);
	}
	
	protected RoboconGenreCriteriaFilter(String fromClause){
		super("RoboconGenreEB", "roboconGenreEB", true, fromClause);
	}
	
	public void setGenreId(Long genreId){
		addCriteria(GENRE_ID, FieldOperatorEnum.EQUAL, genreId);
	}
	
	public void setGenreId(Long fromId, Long toId){
		addCriteria(GENRE_ID, FieldOperatorEnum.GREATER_OR_EQUAL, fromId);
		addCriteria(GENRE_ID, FieldOperatorEnum.LESS_OR_EQUAL, toId);
	}
	
	public void setSongId(Long songId){
		addCriteria(SONG_ID, FieldOperatorEnum.EQUAL, songId);
	}
	
	public void setAlbumId(Long albumId){
		addCriteria(ALBUM_ID, FieldOperatorEnum.EQUAL, albumId);
	}
	
	public void setGenreName(String name, Boolean like){
		if(like == null || like){
			String[] keywords = name.split(" ");
			for(String keyword : keywords){
				addCriteria(GENRE_NAME, FieldOperatorEnum.CONTAINS, keyword);
			}
		}else{
			addCriteria(GENRE_NAME, FieldOperatorEnum.LIKE, name);
		}
	}
	
	public void orderByGenreId(Boolean ASC){
		if(ASC == null || ASC){
			addOrderBy(Order.asc(GENRE_ID));
		}else{
			addOrderBy(Order.desc(GENRE_ID));
		}
	}
}
