package com.masrawy.robocon.query;

import java.util.ArrayList;
import java.util.List;

public class RoboconUserSongTrendCriteriaFilter extends RoboconTrendCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = 	"FROM RoboconUserSongHistoryEB history " + 
												"inner join history.roboconSongEB song " +
												"left join song.roboconFrontendsEB frontend " +
												"left join frontend.roboconCategoriesEB category " +
												"left join category.roboconStorefrontsEB storefront " +
												"left join category.roboconOperatorsEB operator " +
												"left join category.countriesEB country ";
	
	private static final String SONG = "song";
	private static final String SONG_COUNT = "COUNT(song)";
	
	public RoboconUserSongTrendCriteriaFilter(){
		super("RoboconUserSongHistoryEB", "history", false, FROM_CLAUSE);
		addSelectField(SONG, "roboconSongEB");
		addSelectField(SONG_COUNT, "count");
		List<String> groupBy = new ArrayList<String>();
		groupBy.add(SONG);
		setGroupByFields(groupBy);
	}
	
	
}
