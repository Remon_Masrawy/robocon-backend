package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;

public class RoboconConfigurationParameterCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from ConfigParameterEB configParameterEB";
	
	private static final String PARAM_NAME = "configParameterEB.name";
	
	public RoboconConfigurationParameterCriteriaFilter(){
		super("ConfigParameterEB", "configParameterEB", false, FROM_CLAUSE);
	}
	
	public void setName(String name){
		addCriteria(PARAM_NAME, FieldOperatorEnum.EQUAL, name);
	}
}
