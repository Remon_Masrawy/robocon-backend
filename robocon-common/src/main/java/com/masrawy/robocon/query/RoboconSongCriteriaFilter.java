package com.masrawy.robocon.query;

import java.util.List;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconSongCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconSongEB roboconSongEB "
												+ "LEFT JOIN roboconSongEB.roboconSongMetaEB meta "
												+ "LEFT JOIN roboconSongEB.roboconArtistsEB artist "
												+ "LEFT JOIN roboconSongEB.roboconAlbumEB album "
												+ "LEFT JOIN roboconSongEB.roboconProviderEB provider "
												+ "LEFT JOIN roboconSongEB.roboconGenresEB genre";
	
	private static final String ROBOCON_SONG_ID 			= "roboconSongEB.id";
	private static final String ROBOCON_SONG_NAME 			= "meta.name";
	private static final String ROBOCON_SONG_ISRC 			= "roboconSongEB.isrc";
	private static final String ROBOCON_SONG_YEAR 			= "roboconSongEB.year";
	private static final String ROBOCON_SONG_ARTIST_ID 		= "artist.id";
	private static final String ROBOCON_SONG_ALBUM_ID 		= "album.id";
	private static final String ROBOCON_SONG_PROVIDER_ID 	= "provider.id";
	private static final String ROBOCON_SONG_GENRE_ID 		= "genre.id";
	
	
	public RoboconSongCriteriaFilter(){
		super("RoboconSongEB", "roboconSongEB", true, FROM_CLAUSE);
	}
	
	protected  RoboconSongCriteriaFilter(String fromClause){
		super("RoboconSongEB", "roboconSongEB", true, fromClause);
	}
	
	public void setSongId(Long id){
		addCriteria(ROBOCON_SONG_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setSongId(Long fromId, Long toId){
		addCriteria(ROBOCON_SONG_ID, FieldOperatorEnum.GREATER_OR_EQUAL, fromId);
		addCriteria(ROBOCON_SONG_ID, FieldOperatorEnum.LESS_OR_EQUAL, toId);
	}
	
	public void setSongId(List<Long> ids){
		addCriteria(ROBOCON_SONG_ID, FieldOperatorEnum.IN, ids);
	}
	
	public void setSongISRC(String isrc){
		addCriteria(ROBOCON_SONG_ISRC, FieldOperatorEnum.EQUAL, isrc);
	}
	
	public void setSongYear(Long year){
		addCriteria(ROBOCON_SONG_YEAR, FieldOperatorEnum.EQUAL, year);
	}
	
	public void setArtistId(Long id){
		addCriteria(ROBOCON_SONG_ARTIST_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setAlbumId(Long id){
		addCriteria(ROBOCON_SONG_ALBUM_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setProviderId(Long id){
		addCriteria(ROBOCON_SONG_PROVIDER_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setGenreId(Long id){
		addCriteria(ROBOCON_SONG_GENRE_ID, FieldOperatorEnum.EQUAL, id);
	}
	
	public void setSongName(String name, boolean like){
		if(like){
			String[] keywords = name.split(" ");
			for(String keyword : keywords){
				addCriteria(ROBOCON_SONG_NAME, FieldOperatorEnum.CONTAINS, keyword);
			}
		}else{
			addCriteria(ROBOCON_SONG_NAME, FieldOperatorEnum.EQUAL, name);
		}
	}
	
	public void orderBySongId(Boolean ASC){
		if(ASC == null || ASC){
			addOrderBy(Order.asc(ROBOCON_SONG_ID));
		}else{
			addOrderBy(Order.desc(ROBOCON_SONG_ID));
		}
		
	}
}
