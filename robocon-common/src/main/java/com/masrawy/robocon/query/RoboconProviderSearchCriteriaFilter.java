package com.masrawy.robocon.query;

public class RoboconProviderSearchCriteriaFilter extends RoboconProviderCriteriaFilter{

	private static final long serialVersionUID = 1L;

	private static final String FROM_CLAUSE = "from RoboconProviderEB roboconProviderEB join fetch roboconProviderEB.roboconProviderMetaEB roboconProviderMeta";
	
	public RoboconProviderSearchCriteriaFilter(){
		super(FROM_CLAUSE);
	}
}
