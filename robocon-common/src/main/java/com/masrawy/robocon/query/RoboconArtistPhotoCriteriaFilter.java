package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.HQLCriteriaFilter;

public class RoboconArtistPhotoCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconArtistPhotoEB roboconArtistPhotoEB";
	
	public RoboconArtistPhotoCriteriaFilter(){
		super("RoboconArtistPhotoEB", "roboconArtistPhotoEB", false, FROM_CLAUSE);
	}
}
