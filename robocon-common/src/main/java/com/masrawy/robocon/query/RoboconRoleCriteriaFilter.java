package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.HQLCriteriaFilter;

public class RoboconRoleCriteriaFilter extends HQLCriteriaFilter{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public final static String FROM_CLAUSE = "from RoleEB role";
	public RoboconRoleCriteriaFilter(){
		super("RoleEB", "role", true, FROM_CLAUSE);
	}
}
