package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconAlbumCriteriaFilter extends HQLCriteriaFilter{

private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconAlbumEB roboconAlbumEB "
												+ "inner join roboconAlbumEB.roboconAlbumMetaEB roboconAlbumMeta "
												+ "left join roboconAlbumEB.roboconSongsEB song "
												+ "left join roboconAlbumEB.roboconProviderEB provider "
												+ "left join song.roboconArtistsEB artist ";
	
	private static final String ALBUM_ID = "roboconAlbumEB.id";
	private static final String ARTIST_ID = "artist.id";
	private static final String PROVIDER_ID = "provider.id";
	private static final String ALBUM_NAME = "roboconAlbumMeta.name";
	
	public RoboconAlbumCriteriaFilter(){
		super("RoboconAlbumEB", "roboconAlbumEB", true, FROM_CLAUSE);
	}
	
	protected  RoboconAlbumCriteriaFilter(String fromClause){
		super("RoboconAlbumEB", "roboconAlbumEB", true, fromClause);
	}
	
	public void setAlbumId(Long albumId){
		addCriteria(ALBUM_ID, FieldOperatorEnum.EQUAL, albumId);
	}
	
	public void setAlbumId(Long fromId, Long toId){
		addCriteria(ALBUM_ID, FieldOperatorEnum.GREATER_OR_EQUAL, fromId);
		addCriteria(ALBUM_ID, FieldOperatorEnum.LESS_OR_EQUAL, toId);
	}
	
	public void setArtistId(Long artistId){
		addCriteria(ARTIST_ID, FieldOperatorEnum.EQUAL, artistId);
	}
	
	public void setProviderId(Long providerId){
		addCriteria(PROVIDER_ID, FieldOperatorEnum.EQUAL, providerId);
	}
	
	public void setAlbumName(String name, Boolean like){
		if(like){
			String[] keywords = name.split(" ");
			for(String keyword : keywords){
				addCriteria(ALBUM_NAME, FieldOperatorEnum.CONTAINS, keyword);
			}
		}else{
			addCriteria(ALBUM_NAME, FieldOperatorEnum.LIKE, name);
		}
		
	}
	
	public void orderByAlbumId(Boolean ASC){
		if(ASC == null || ASC){
			addOrderBy(Order.asc(ALBUM_ID));
		}else{
			addOrderBy(Order.desc(ALBUM_ID));
		}
	}
}
