package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.HQLCriteriaFilter;

public abstract class RoboconTrendCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;

	public RoboconTrendCriteriaFilter(String entityName, String entityAliase, boolean distinct, String fromClause) {
		super(entityName, entityAliase, distinct, fromClause);
	}

}
