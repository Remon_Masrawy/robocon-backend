package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.HQLCriteriaFilter;

public class RoboconAlbumMetaCriteriaFilter extends HQLCriteriaFilter{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String FROM_CLAUSE = "from RoboconAlbumMetaEB roboconAlbumMetaEB";
	
	public RoboconAlbumMetaCriteriaFilter(){
		super("RoboconAlbumMetaEB", "roboconAlbumMetaEB", false, FROM_CLAUSE);
	}
}
