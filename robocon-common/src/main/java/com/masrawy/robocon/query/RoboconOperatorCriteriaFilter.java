package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconOperatorCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconOperatorEB operatorEB";
	private static final String OPERATOR_ID = "operatorEB.id";
	private static final String OPERATOR_MCC = "operatorEB.mcc";
	private static final String OPERATOR_MNC = "operatorEB.mnc";
	private static final String OPERATOR_NAME = "operatorEB.name";
	
	public RoboconOperatorCriteriaFilter(){
		super("RoboconOperatorEB", "operatorEB", false, FROM_CLAUSE);
	}
	
	public void setOperatorMcc(String mcc){
		addCriteria(OPERATOR_MCC, FieldOperatorEnum.EQUAL, mcc);
	}
	
	public void setOperatorMnc(String mnc){
		addCriteria(OPERATOR_MNC, FieldOperatorEnum.EQUAL, mnc);
	}
	
	public void setOperatorName(String name, boolean like){
		if(like){
			String[] keywords = name.split(" ");
			for(String keyword : keywords){
				addCriteria(OPERATOR_NAME, FieldOperatorEnum.CONTAINS, keyword);
			}
		}else{
			addCriteria(OPERATOR_NAME, FieldOperatorEnum.EQUAL, name);
		}
	}
	
	public void orderById(Boolean asc){
		if(asc == null || asc)
			addOrderBy(Order.asc(OPERATOR_ID));
		else
			addOrderBy(Order.desc(OPERATOR_ID));
	}
}
