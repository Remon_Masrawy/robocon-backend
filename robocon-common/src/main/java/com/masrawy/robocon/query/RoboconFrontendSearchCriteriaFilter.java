package com.masrawy.robocon.query;

public class RoboconFrontendSearchCriteriaFilter extends RoboconFrontendCriteriaFilter{

	private static final long serialVersionUID = 1L;

	private static final String FROM_CLAUSE = "from RoboconFrontendEB roboconFrontendEB "
			+ "join fetch roboconFrontendEB.roboconFrontendMetaEB meta "
			+ "left join roboconFrontendEB.roboconCategoriesEB category "
			+ "left join category.roboconStorefrontsEB storefront "
			+ "left join category.roboconOperatorsEB operator "
			+ "left join category.countriesEB country";
	
	public RoboconFrontendSearchCriteriaFilter(){
		super(FROM_CLAUSE);
	}
}
