package com.masrawy.robocon.query;

public class RoboconArtistEventSearchCriteriaFilter extends RoboconArtistEventCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconArtistEventEB roboconArtistEventEB "
												+ "left join fetch roboconArtistEventEB.roboconArtistEventMetaEB meta "
												+ "left join roboconArtistEventEB.roboconArtistsEB artist";
	
	public RoboconArtistEventSearchCriteriaFilter(){
		super(FROM_CLAUSE);
	}

}
