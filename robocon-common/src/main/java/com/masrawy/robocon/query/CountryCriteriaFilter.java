package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class CountryCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from CountryEB countryEB";
	private static final String COUNTRY_CODE = "countryEB.code";
	private static final String COUNTRY_ID = "countryEB.id";
	private static final String COUNTRY_NAME = "countryEB.name";
	
	public CountryCriteriaFilter(){
		super("CountryEB", "countryEB", false, FROM_CLAUSE);
	}
	
	public void setCountryCode(String code){
		addCriteria(COUNTRY_CODE, FieldOperatorEnum.EQUAL, code);
	}
	
	public void setCountryName(String name, boolean like){
		if(like){
			String[] keywords = name.split(" ");
			for(String keyword : keywords){
				addCriteria(COUNTRY_NAME, FieldOperatorEnum.CONTAINS, keyword);
			}
		}else{
			addCriteria(COUNTRY_NAME, FieldOperatorEnum.EQUAL, name);
		}
	}
	
	public void orderByCountryId(Boolean ASC){
		if(ASC || ASC == null){
			addOrderBy(Order.asc(COUNTRY_ID));
		}else{
			addOrderBy(Order.desc(COUNTRY_ID));
		}
	}
}
