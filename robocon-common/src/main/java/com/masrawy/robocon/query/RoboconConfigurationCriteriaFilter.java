package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.HQLCriteriaFilter;

public class RoboconConfigurationCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconConfigurationEB roboconConfigurationEB";
	
	public RoboconConfigurationCriteriaFilter(){
		super("RoboconConfigurationEB", "roboconConfigurationEB", false, FROM_CLAUSE);
	}
}
