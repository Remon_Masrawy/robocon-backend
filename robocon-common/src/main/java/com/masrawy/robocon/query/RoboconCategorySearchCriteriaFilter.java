package com.masrawy.robocon.query;

public class RoboconCategorySearchCriteriaFilter extends RoboconCategoryCriteriaFilter{

	private static final long serialVersionUID = 1L;

	private static final String FROM_CLAUSE = "from RoboconCategoryEB roboconCategoryEB join fetch roboconCategoryEB.roboconCategoryMetaEB meta";
	
	public RoboconCategorySearchCriteriaFilter(){
		super(FROM_CLAUSE);
	}
}
