package com.masrawy.robocon.query;

import java.util.Date;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconUserSongFavoritesCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconUserSongFavoriteEB favorite " +
												"inner join favorite.roboconSongEB song " + 
												"left join song.roboconFrontendsEB frontend " +
												"left join frontend.roboconCategoriesEB category " +
												"left join category.roboconStorefrontsEB storefront " +
												"left join category.roboconOperatorsEB operator " +
												"left join category.countriesEB country";
	
	private static final String FAVORITE_ID = "favorite.id";
	private static final String SONG_ID = "song.id";
	private static final String USER_ID = "favorite.userId";
	private static final String DEVICE_ID = "favorite.deviceId";
	private static final String FAVORITE_CREATED_DATE = "favorite.audit.createdOn";
	
	private static final String OPERATOR 		= "operator";
	private static final String OPERATOR_MCC 	= "operator.mcc";
	private static final String OPERATOR_MNC 	= "operator.mnc";
	private static final String STOREFRONT 		= "storefront";
	private static final String STOREFRONT_NAME = "storefront.name";
	private static final String COUNTRY_ISO 	= "country.iso";
	
	public RoboconUserSongFavoritesCriteriaFilter(){
		super("RoboconUserSongFavoriteEB", "favorite", true, FROM_CLAUSE);
	}
	
	public void setFavoriteId(Long favoriteId){
		addCriteria(FAVORITE_ID, FieldOperatorEnum.EQUAL, favoriteId);
	}
	
	public void setSongId(Long songId){
		addCriteria(SONG_ID, FieldOperatorEnum.EQUAL, songId);
	}
	
	public void setUserId(Long userId){
		addCriteria(USER_ID, FieldOperatorEnum.EQUAL, userId);
	}
	
	public void setDeviceId(String deviceId){
		addCriteria(DEVICE_ID, FieldOperatorEnum.EQUAL, deviceId);
	}
	
	public void setBetweenCreatedDate(Date beginDate, Date endDate){
		addBetweenCriteria(FAVORITE_CREATED_DATE, endDate, endDate);
	}
	
	public void setOperatorMCC(String mcc){
		addCriteria(OPERATOR_MCC, FieldOperatorEnum.EQUAL, mcc);
	}
	
	public void setOperatorMNC(String mnc){
		addCriteria(OPERATOR_MNC, FieldOperatorEnum.EQUAL, mnc);
	}
	
	public void setCountryCode(String countryCode){
		addCriteria(COUNTRY_ISO, FieldOperatorEnum.EQUAL, countryCode);
	}
	
	public void setStorefrontName(String storefrontName){
		addCriteria(STOREFRONT_NAME, FieldOperatorEnum.EQUAL, storefrontName);
	}
	
	public void isEmptyOperator(boolean empty){
		addCriteria(OPERATOR, FieldOperatorEnum.IS_NULL, empty);
	}
	
	public void isEmptyStorefront(boolean empty){
		addCriteria(STOREFRONT, FieldOperatorEnum.IS_NULL, empty);
	}
	
	public void orderByFavoriteId(Boolean asc){
		if(asc == null || asc){
			addOrderBy(Order.asc(FAVORITE_ID));
		}else{
			addOrderBy(Order.desc(FAVORITE_ID));
		}
	}
	
	public void orderByFavoriteCreatedDate(Boolean asc){
		if(asc == null || asc){
			addOrderBy(Order.asc(FAVORITE_CREATED_DATE));
		}else{
			addOrderBy(Order.desc(FAVORITE_CREATED_DATE));
		}
	}
	
}
