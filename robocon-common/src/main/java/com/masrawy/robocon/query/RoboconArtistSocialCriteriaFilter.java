package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.HQLCriteriaFilter;

public class RoboconArtistSocialCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconArtistSocialEB roboconArtistSocialEB";
	
	public RoboconArtistSocialCriteriaFilter(){
		super("RoboconArtistSocialEB", "roboconArtistSocialEB", false, FROM_CLAUSE);
	}
}
