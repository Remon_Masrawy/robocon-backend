package com.masrawy.robocon.query;

import com.cdm.core.persistent.query.FieldOperatorEnum;
import com.cdm.core.persistent.query.HQLCriteriaFilter;
import com.cdm.core.persistent.query.Order;

public class RoboconArtistEventCriteriaFilter extends HQLCriteriaFilter{

	private static final long serialVersionUID = 1L;
	
	private static final String FROM_CLAUSE = "from RoboconArtistEventEB roboconArtistEventEB "
												+ "left join roboconArtistEventEB.roboconArtistEventMetaEB meta "
												+ "left join roboconArtistEventEB.roboconArtistsEB artist";
	private static final String EVENT_ID = "roboconArtistEventEB.id";
	private static final String EVENT_NAME = "meta.name";
	private static final String ARTIST_ID = "artist.id";
	
	public RoboconArtistEventCriteriaFilter(){
		super("RoboconArtistEventEB", "roboconArtistEventEB", false, FROM_CLAUSE);
	}
	
	public RoboconArtistEventCriteriaFilter(String fromClause){
		super("RoboconArtistEventEB", "roboconArtistEventEB", false, fromClause);
	}
	
	public void setEventId(Long eventId){
		addCriteria(EVENT_ID, FieldOperatorEnum.EQUAL, eventId);
	}
	
	public void setEventId(Long fromId, Long toId){
		addCriteria(EVENT_ID, FieldOperatorEnum.GREATER_OR_EQUAL, fromId);
		addCriteria(EVENT_ID, FieldOperatorEnum.LESS_OR_EQUAL, toId);
	}
	
	public void setArtistId(Long artistId){
		addCriteria(ARTIST_ID, FieldOperatorEnum.EQUAL, artistId);
	}
	
	public void setEventName(String name, boolean like){
		if(like){
			String[] keywords = name.split(" ");
			for(String keyword : keywords){
				addCriteria(EVENT_NAME, FieldOperatorEnum.CONTAINS, keyword);
			}
		}else{
			addCriteria(EVENT_NAME, FieldOperatorEnum.EQUAL, name);
		}
	}
	
	public void orderByEventId(Boolean ASC){
		if(ASC || ASC == null){
			addOrderBy(Order.asc(EVENT_ID));
		}else{
			addOrderBy(Order.desc(EVENT_ID));
		}
	}
}
