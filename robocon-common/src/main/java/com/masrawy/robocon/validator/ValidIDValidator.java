package com.masrawy.robocon.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cdm.core.dto.model.DTO;
import com.cdm.core.exception.common.InvalidArgumentException;
import com.masrawy.robocon.constraints.ValidID;

public class ValidIDValidator implements ConstraintValidator<ValidID, Object>{

	@Override
	public void initialize(ValidID constraintAnnotation) {
		
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (!(value instanceof DTO)) {
			throw new InvalidArgumentException("@ValidID should be attached to DTO Object. ");
		}

		DTO<?> dto = (DTO<?>) value;
		if (dto.pk() == null) {
			return false;
		}

		return true;
	}

}
