package com.masrawy.robocon.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.masrawy.robocon.constraints.NotNull;

public class NotNullValidator implements ConstraintValidator<NotNull, Object>{

	@Override
	public void initialize(NotNull constraintAnnotation) {
		
	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		if(value == null)
			return false;
		return true;
	}

}
