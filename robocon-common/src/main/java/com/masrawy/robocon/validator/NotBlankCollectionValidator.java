package com.masrawy.robocon.validator;

import java.util.Collection;

import com.masrawy.robocon.constraints.NotBlankCollection;

import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.context.OValContext;
import net.sf.oval.exception.OValException;

public class NotBlankCollectionValidator extends AbstractAnnotationCheck<NotBlankCollection>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("rawtypes")
	@Override
	public boolean isSatisfied(Object validatedObject, Object valueToValidate, OValContext context, Validator validator) throws OValException {
		if(valueToValidate == null)
			return true;
		if(!(valueToValidate instanceof Collection))
			return false;
		if(((Collection)valueToValidate).isEmpty())
			return false;
		return true;
	}

}
