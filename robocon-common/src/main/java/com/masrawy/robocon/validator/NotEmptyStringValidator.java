package com.masrawy.robocon.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;

import com.masrawy.robocon.constraints.NotEmptyString;

public class NotEmptyStringValidator implements ConstraintValidator<NotEmptyString, String>{

	@Override
	public void initialize(NotEmptyString constraintAnnotation) {
		
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if(StringUtils.isNotBlank(value)){
			return true;
		}
		return false;
	}

}
