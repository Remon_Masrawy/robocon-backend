package com.masrawy.robocon.validator;

import java.util.Collection;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.masrawy.robocon.constraints.NotEmptyCollection;

@SuppressWarnings("rawtypes")
public class NotEmptyCollectionValidator implements ConstraintValidator<NotEmptyCollection, Collection>{

	@Override
	public void initialize(NotEmptyCollection constraintAnnotation) {
		
	}

	@Override
	public boolean isValid(Collection value, ConstraintValidatorContext context) {
		if(value == null){
			return false;
		}
		if(!(value instanceof Collection)){
			return false;
		}
		if(value.isEmpty() || value.size() < 1){
			return false;
		}
		return true;
	}

}
