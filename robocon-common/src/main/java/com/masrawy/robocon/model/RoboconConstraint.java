package com.masrawy.robocon.model;

public final class RoboconConstraint {

	public static final String UNIQUE_PROVIDER_KEYWORD = "UNIQUE_PROVIDER_KEYWORD";
	
	public static final String UNIQUE_PROVIDER_NAME = "UNIQUE_PROVIDER_NAME";
	
	public static final String UNIQUE_PROVIDER_META_LANG = "UNIQUE_PROVIDER_META_LANG";
}
