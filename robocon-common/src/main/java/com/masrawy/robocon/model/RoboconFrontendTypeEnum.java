package com.masrawy.robocon.model;

import com.cdm.core.datatypes.StringEnum;

public enum RoboconFrontendTypeEnum implements StringEnum{
	ALBUM("Album"),	SONG("Song");

	String value;
	
	private RoboconFrontendTypeEnum(String value){
		this.value = value;
	}
	
	@Override
	public String getStrValue() {
		return value;
	}

}
