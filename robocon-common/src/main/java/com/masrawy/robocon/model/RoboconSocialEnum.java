package com.masrawy.robocon.model;

import com.cdm.core.datatypes.StringEnum;

public enum RoboconSocialEnum implements StringEnum{
	VK("VK"),
	VINE("Vine"),
	QUORA("Quora"),
	TUMBLR("Tumblr"),
	ASK_FM("Ask.fm"),
	REDDIT("Reddit"),
	MEETUP("Meetup"),
	FLICKR("Flickr"),
	TWITTER("Twitter"),
	YOUTUBE("Youtube"),
	SNAPCHAT("Snapchat"),
	FACEBOOK("Facebook"),
	BIZ_SUGAR("BizSugar"),
	LINKED_IN("LinkedIn"),
	PINTEREST("Pinterest"),
	INSTAGRAM("Instagram"),
	CLASS_MATES("ClassMates"),
	GOOGLE_PLUS("Google Plus+");

	private String value;
	
	private RoboconSocialEnum(String value){
		this.value = value;
	}
	
	@Override
	public String getStrValue() {
		return value;
	}

}
