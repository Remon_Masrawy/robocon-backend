package com.masrawy.robocon.model;

import com.cdm.core.datatypes.IntegerEnum;

public enum RoboconMimeTypeEnum implements IntegerEnum{
	VIDEO(1), 
	FULL_TRACK(2), 
	STREAM(3), 
	IMAGE(4), 
	TRUETONE(5), 
	GAME(6), 
	THEME(7), 
	APPLICATION(8), 
	POLYPHONIC(9), 
	MONOPHONIC_16(10), 
	MONOPHONIC_128(11), 
	SONG_KARAOKE(12), 
	VIDEO_KARAOKE(13), 
	SONG_LYRICS(14), 
	VIDEO_LYRICS(15),
	USER_SING(16), 
	ALBUM_IMAGE(17), 
	ARTIST_IMAGE(18), 
	SONG_IMAGE(19),
	EVENT_IMAGE(20);

	private int value;
	
	private RoboconMimeTypeEnum(int value){
		this.value = value;
	}
	
	@Override
	public int getIntValue() {
		return value;
	}

}
