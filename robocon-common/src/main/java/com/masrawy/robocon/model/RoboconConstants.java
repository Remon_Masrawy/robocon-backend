package com.masrawy.robocon.model;

public class RoboconConstants {

	public static final String SONG								= "SONG";
	
	public static final String ALBUM							= "ALBUM";
	
	public static final String TYPE								= "Type";
	
	public static final String USER_ID 							= "User-Id";
	
	public static final String USER_IP 							= "User-IP";
	
	public static final String SONG_ID 							= "Song-Id";
	
	public static final String ARTIST_ID 						= "Artist-Id";
	
	public static final String ALBUM_ID 						= "Album-Id";
	
	public static final String PROVIDER_ID						= "Provider-Id";
	
	public static final String GENRE_ID							= "Genre-Id";
	
	public static final String KEYWORD 							= "Keyword";
	
	public static final String OPERATOR_ID 						= "operator-id";
	
	public static final String OPERATOR_MCC 					= "operator-mcc";
	
	public static final String OPERATOR_MNC 					= "operator-mnc";
	
	public static final String STOREFRONT 						= "storefront";
	
	public static final String COUNTRY 							= "country";
	
	public static final String USER_ACTION 						= "User-Action";
	
	public static final String CODE_FROM 						= "Code-From";
	
	public static final String CODE_TO 							= "Code-To";
	
	public static final String X_FORWARDED_FOR 					= "X-FORWARDED-FOR";
	
	public static final String SPECIAL_CHARACTERS_PATTERN		= "[!”#$%٪&'()*+-./:;<=>?@\\^_{|}~`,.\\[\\]\\s]+";
	
	public static final String META_LANG_FILTER 				= "META_LANG_FILTER";
	
	public static final String SONG_META_LANG_FILTER 			= "SONG_META_LANG_FILTER";
	
	public static final String SONG_META_NAME_FILTER 			= "SONG_META_NAME_FILTER";
	
	public static final String ARTIST_META_LANG_FILTER 			= "ARTIST_META_LANG_FILTER";
	
	public static final String EVENT_META_LANG_FILTER 			= "EVENT_META_LANG_FILTER";
	
	public static final String ALBUM_META_LANG_FILTER 			= "ALBUM_META_LANG_FILTER";
	
	public static final String PROVIDER_META_LANG_FILTER 		= "PROVIDER_META_LANG_FILTER";
	
	public static final String GENRE_META_LANG_FILTER 			= "GENRE_META_LANG_FILTER";
	
	public static final String CATEGORY_META_LANG_FILTER 		= "CATEGORY_META_LANG_FILTER";
	
	public static final String FRONTEND_META_LANG_FILTER 		= "FRONTEND_META_LANG_FILTER";
	
	public static final String SONG_HISTORY_DURATION_FORMULA 	= "(select song_hit.duration from ROBOCON_USER_SONG_HIT song_hit inner join ROBOCON_HIT hit on song_hit.id = hit.id where song_hit.history_id = id order by hit.createdOn desc limit 1)";
	
	public static final String SONG_HISTORY_LATEST_HIT_FORMULA 	= "(select max(hit.createdOn) from ROBOCON_USER_SONG_HIT song_hit inner join ROBOCON_HIT hit on song_hit.id = hit.id where song_hit.history_id = id)";
	
	public static final String SONG_HISTORY_HIT_COUNT_FORMULA 	= "(select count(song_hit.id) from ROBOCON_USER_SONG_HIT song_hit where song_hit.history_id = id)";

	public static final String SONG_META_LANG_QUERY				= "lang = (COALESCE((SELECT meta.lang FROM ROBOCON_SONG_META meta WHERE meta.lang = :lang AND SONG_ID = meta.SONG_ID), :default_lang))";

	public static final String SONG_META_NAME_QUERY				= "name like :name";
}
