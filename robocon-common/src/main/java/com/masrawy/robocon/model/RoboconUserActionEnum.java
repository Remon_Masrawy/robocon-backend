package com.masrawy.robocon.model;

import com.cdm.core.datatypes.StringEnum;

public enum RoboconUserActionEnum implements StringEnum{
	PLAY("Play"), DOWNLOAD("Download"), PRINT("Print");

	private String value;
	
	private RoboconUserActionEnum(String value){
		this.value = value;
	}
	
	@Override
	public String getStrValue() {
		return value;
	}

}
